#!/usr/bin/env bash

# Docker Image 打上標簽
echo "Start Docker Tag beefun"
docker tag beefun:latest asia.gcr.io/okbank-180307/beefun:$(git describe)
echo "End Docker Tag beefun"

# Docker Image 上傳到Google Container Repo 專案
echo "Start Docker push to Kubernetes"
gcloud docker -- push asia.gcr.io/okbank-180307/beefun:$(git describe)
echo "End Docker push to Kubernetes"
#
kube_rc_name=`kubectl get rc |grep beefun |awk '{print $1}'`

# Rolling Update
echo "Start Kubernetes Rolling Update ${kube_rc_name}"
kubectl rolling-update  ${kube_rc_name} --image=asia.gcr.io/okbank-180307/beefun:$(git describe)
echo "End Kubernetes Rolling Update ${kube_rc_name}"

