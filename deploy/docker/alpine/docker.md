# 建立基本 alpine 的 Python 3.5
docker build -t okborn:alpine .

# 儲存到私有昌庫
docker tag okborn:alpine 192.168.1.220:5000/okborn:alpine
docker push 192.168.1.220:5000/okborn:alpine

