#!/bin/bash

# 檢查是否有創建過webbase:base
name=`docker images |grep beefun |grep ' base ' |wc -l`
if [ $name == 0 ];then
# 建立基礎Docker
echo "Start Build beefun:base"
docker build --rm -t beefun:base deploy/docker/alpine
echo "Finish Build beefun:base"
fi

# 建立Docker Image
echo "Start Build beefun"
docker build --rm -t beefun .
echo "Finish Build beefun"

# 檢查執行中的Docker
for docker_id in `docker ps -a |grep beefun |awk '{print $1}'`
do
    echo "docker delete $docker_id"
    docker rm -f $docker_id
done


# 執行Docker
echo "docker run beefun"
LocalIP=`ip addr |grep 192.168.1 |awk '{print $2}' |awk -F '/' '{print $1}'`
docker run -d -p 6543:6543 --name beefun \
-v `pwd`/logs/:/usr/src/app/logs \
-e TZ="Asia/Taipei" \
beefun
