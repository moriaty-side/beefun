匯入使用的插件
cd beefun/docker/base
pip freeze |grep -v oklib |grep -v git > requirements.txt

建立基本Docker
docker build --rm -t beefun:base .

建立Docker
cd beefun/
docker build --rm -t beefun .


啟動Container
docker run -d -P \
-v `pwd`/logs/:/usr/src/app/logs \
--name beefun \
--add-host='pg.okborn.com:127.0.0.1' \
--add-host='rd.okborn.com:127.0.0.1' \
beefun

查看Container log
docker logs -f contain_id