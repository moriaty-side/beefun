# coding=utf-8
from __future__ import unicode_literals

import os
import beefun
import yaml
import json


def load_app_cfg():
    """
    Load application configuration and return.
    """
    app_dir = os.path.abspath(os.path.dirname(beefun.__file__))
    app_dir, _ = os.path.split(app_dir)
    app_cfg_path = os.path.join(app_dir, 'app_cfg.yaml')
    app_cfg_env = 'APP_CFG_PATH'
    if app_cfg_env in os.environ:
        app_cfg_path = os.environ[app_cfg_env]
    with open(app_cfg_path, encoding='utf8') as f:
        app_cfg = yaml.full_load(f)
    return app_cfg


def load_country():
    """
    Load country map.
    """
    country_dir = os.path.abspath(os.path.dirname(beefun.__file__))
    country_dir, _ = os.path.split(country_dir)
    country_cfg_path = os.path.join(country_dir, 'iso-3166-ext.json')
    with open(country_cfg_path, 'r') as f:
        country_cfg = json.loads(f.read())
    return country_cfg


def load_optimization(**settings):
    from .dc_module.optimization.optimization_service import OptimizationService
    optimization_service = OptimizationService(settings.get('session'))
    optimization_list = optimization_service.get_list()
    for optimization in optimization_list:
        key = optimization.key
        value = optimization.value
        settings[key] = value
    return settings


