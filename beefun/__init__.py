# coding=utf-8
from __future__ import unicode_literals
import os
import time
import datetime
import redis
import requests

from pyramid.authentication import SessionAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.config import Configurator
# from pyramid_beaker import session_factory_from_settings
from pyramid_redis_sessions import session_factory_from_settings

from .lib.auth import get_group
from .base.meta_module import setup_database_setting
from .base.base_models import get_db_connection_dict
from .request import WebRequest
import transaction
from pytz import utc
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor

from .utils import load_app_cfg, load_country, load_optimization

from .dc_module.currency.currency_service import CurrencyService

from .dc_module.stock.stock_service import StockService

from .dc_module.currency_date_time_price.currency_date_time_price_service import CurrencyDateTimePriceService

from .dc_module.stock_date_time_price.stock_date_time_price_service import StockDateTimePriceService

from .dc_module.member.member_service import MemberService, MemberAttentionService

from .dc_module.web.web_service import WebService

import uuid

from .utils import load_app_cfg, load_country, load_optimization

from twstock import Stock

jobstores = {

}
executors = {
    'default': ThreadPoolExecutor(20),
    'processpool': ProcessPoolExecutor(5)
}
job_defaults = {
    'coalesce': False,
    'max_instances': 3
}

sched = BackgroundScheduler(jobstores=jobstores, executors=executors, job_defaults=job_defaults, timezone=utc)

def main(global_config, **settings):
    """ 
    This function returns a Pyramid WSGI application.
    """
    # load app cfg
    app_cfg = load_app_cfg()
    settings.update(app_cfg)

    # load country_map
    country_cfg = load_country()
    settings.update({'country_map': country_cfg})

    # load db connect info
    settings = setup_database_setting(**settings)

    
    # load optimization
    settings = load_optimization(**settings)
    
    authorization_policy = ACLAuthorizationPolicy()
    authentication_policy = SessionAuthenticationPolicy(callback=get_group)
    session_factory = session_factory_from_settings(settings)

    # redis db config
    redis_host = os.environ.get('REDIS_HOST') if os.environ.get('REDIS_HOST') else settings['redis.sessions.host']
    redis_port = os.environ.get('REDIS_PORT') if os.environ.get('REDIS_PORT') else settings['redis.sessions.port']
    redis_db = os.environ.get('REDIS_PORT') if os.environ.get('REDIS_DB') else settings['redis.sessions.db']
    connection_pool = redis.ConnectionPool(host=redis_host,
                                           port=redis_port,
                                           db=redis_db)
    settings['redis_client'] = redis.Redis(connection_pool=connection_pool)

    config = Configurator(settings=settings,
                          authentication_policy=authentication_policy,
                          authorization_policy=authorization_policy,
                          session_factory=session_factory,
                          request_factory=WebRequest)

    
    
    # setup view template engine
    config.include('pyramid_mako')

    # setup router
    config.include('.routes')

    config.include('.dc_module', route_prefix='/admin')

    # setup routter for static file
    config.add_static_view('beefun_backend', 'static', cache_max_age=3600)
    config.add_static_view('beefun_fronted', 'f_static', cache_max_age=3600)

    # setup i18n
    config.scan('.lib.i18n')

    ##
    sched.add_job(get_money, 'cron', hour='0,2,4,6,8,10', misfire_grace_time=3600, args=[settings])

    sched.add_job(get_money2, 'cron', hour='2', misfire_grace_time=3600, args=[settings])

    sched.add_job(get_stock, 'cron', hour='3', misfire_grace_time=3600, args=[settings])

    sched.add_job(get_a_stock, 'cron', hour='13', misfire_grace_time=3600, args=[settings])

    # sched.add_job(push_test, 'interval', minutes=1, misfire_grace_time=3600, args=[settings])
    #
    # sched.add_job(get_money2, 'interval', minutes=1, misfire_grace_time=3600, args=[settings])
    #
    # sched.add_job(get_stock, 'interval', minutes=1, misfire_grace_time=3600, args=[settings])
    #
    # sched.add_job(web_check, 'interval', minutes=3, misfire_grace_time=3600, args=[settings])

    sched.start()

    return config.make_wsgi_app()


def get_money_v2(settings):

    currency_service = CurrencyService(settings['session'])

    currency_date_time_price_service = CurrencyDateTimePriceService(settings['session'])

    member_attention_service = MemberAttentionService(settings['session'])

    r = requests.get('https://tw.rter.info/capi.php')
    currency = r.json()

    import datetime
    import pytz

    ##### 推波測試

    token = "VodbvDDlQe6flSxiMRIKEDoNnQLXubpg1PH8fSy59mv"

    headers = {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/x-www-form-urlencoded"
    }

    currency_list = currency_service.get_list()
    member_attention_list = member_attention_service.get_list()
    with transaction.manager:
        for c in currency_list:

            code_c = str(c.currency_code)+'TWD'

            timeString = currency.get(code_c).get('UTC')

            struct_time = datetime.datetime.strptime(timeString, "%Y-%m-%d %H:%M:%S")

            d = struct_time

            tw = pytz.timezone('Asia/Taipei')

            old_obj = currency_date_time_price_service.get_by_n_sequence_max(currency_id=c.currency_id)
            if old_obj:
                n_sequence = old_obj.n_sequence + 1

                amplitude = abs(currency.get(code_c).get('Exrate') - float(old_obj.datetime_price))

                if str(currency.get(code_c).get('Exrate')) == float(old_obj.datetime_price):
                    status = 'flat'
                elif str(currency.get(code_c).get('Exrate')) < float(old_obj.datetime_price):
                    status = 'decline'
                else:
                    status = 'rise'
            else:
                n_sequence = 1
                amplitude = 0
                status = 'error'


            data_set = {
                'currency_id':c.currency_id,
                'datetime_price': str(currency.get('USDTWD').get('Exrate')),
                'datetime_time': tw.fromutc(d),
                'n_sequence': n_sequence,
                'amplitude': amplitude,
                'status': status,
            }
            currency_date_time_price_service.create(**data_set)

            # notice_buy_in_price = Column('f_notice_buy_in_price', String, nullable=False, doc=u'最新買價')
            #
            # notice_sell_out_price

            for m in member_attention_list:
                if c.currency_id == m.currency_id:
                    if currency.get(code_c).get('Exrate') <= float(m.notice_buy_in_price):
                        msg = str(tw.fromutc(d)).split('.')[0]+' 目前'+c.currency_name+'對台幣即期匯率以低於您設定的買入價 '+m.notice_buy_in_price+' 為 '+str(currency.get(code_c).get('Exrate'))

                        payload = {'message': msg}
                        r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)

                    if currency.get(code_c).get('Exrate') >= float(m.notice_sell_out_price):
                        msg = str(tw.fromutc(d)).split('.')[
                                  0] + ' 目前' + c.currency_name + '對台幣即期匯率以高於您設定的賣出價 ' + m.notice_buy_in_price + ' 為 ' + str(
                            currency.get(code_c).get('Exrate'))

                        payload = {'message': msg}
                        r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)

    return {}

def get_money(settings):

    r = requests.get('https://tw.rter.info/capi.php')
    currency = r.json()
    # r.json().get('USDTWD').get('Exrate')

    import datetime
    import pytz

    ##### 推波測試

    token = "OvpjIkEtbEFV9t2UMaDitwRwh5sVaSTrro5gRpFF4AR"

    headers = {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/x-www-form-urlencoded"
    }

    timeString = currency.get('USDTWD').get('UTC')

    struct_time = datetime.datetime.strptime(timeString, "%Y-%m-%d %H:%M:%S")

    d = struct_time

    tw = pytz.timezone('Asia/Taipei')

    msg = str(tw.fromutc(d)).split('.')[0]+' 目前美金對台幣即期匯率為 '+str(currency.get('USDTWD').get('Exrate'))

    payload = {'message': msg}
    r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)

    #####

    return {}

def get_money2(settings):

    r = requests.get('https://openapi.taifex.com.tw/v1/DailyForeignExchangeRates')
    currency = r.json()

    r_gold = requests.get('https://www.tpex.org.tw/openapi/v1/tpex_gold_latest')
    currency_gold = r_gold.json()

    T_gold_price_m = r_gold.json()[0].get('QuotedSellingS.Price')

    g_gold_price_m  = str(float(r_gold.json()[0].get('QuotedSellingS.Price'))/3.75)

    import datetime
    import pytz

    ##### 推波測試

    token = "OvpjIkEtbEFV9t2UMaDitwRwh5sVaSTrro5gRpFF4AR"

    headers = {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/x-www-form-urlencoded"
    }

    msg = '台灣銀行 '+r.json()[-1].get('Date')+' 每日外幣參考匯率（美金/台幣） '+str(r.json()[-1].get('USD/NTD'))

    msg_g = '台灣銀行 民國'+r_gold.json()[0].get('Date')+' 黃金價格（1台錢）'+T_gold_price_m+' / 黃金價格（1公克）'+g_gold_price_m

    payload = {'message': msg}
    r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)

    token_g = "wmdpZC5gxatvwzj0VjpYFenkAjXWA0WrKgIkdxdgfXn"

    headers = {
        "Authorization": "Bearer " + token_g,
        "Content-Type": "application/x-www-form-urlencoded"
    }

    payload_g = {'message': msg_g}
    r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload_g)

    #####

    return {}

def get_stock(settings):

    import datetime
    import pytz

    ##### 推波測試

    token = "RhtzyxkGbW61DaUQavnyQyxNOYWfWwzHYaj5cwP7rgL"

    headers = {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/x-www-form-urlencoded"
    }

    ### 關注清單

    attention_list = [{
        'name': '2330(台積電)',
        'code': '2330'
    },
        {
            'name': '0050(台灣50)',
            'code': '0050'
        },
        {
            'name': '2731(雄獅旅遊)',
            'code': '2731'
        }
    ]

    for a in attention_list:
        ### 取得股價
        stock = Stock(a.get('code'))

        now_price = str(stock.open[-1])

        ###

        d = datetime.datetime.utcnow()

        tw = pytz.timezone('Asia/Taipei')

        tw_now_time = str(tw.fromutc(d)).split('.')[0]

        msg = '當下時間' +tw_now_time+' 台股 '+a.get('name')+ '今日開盤股價' + now_price

        payload = {'message': msg}
        r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)

    return {}

def get_a_stock(settings):

    import datetime
    import pytz

    # fred_api_key = '1183a2601149018153f4ecae8aed8e3b'

    import yfinance

    ### 關注清單

    attention_list = [
                        {
                        'name':'VOO',
                        'code':'VOO'
                        },
                        {
                            'name': '3M',
                            'code': 'MMM'
                        },
                        {
                            'name': 'DIS',
                            'code': 'DIS'
                        }
                    ]

    ##### 推波測試

    token = "RhtzyxkGbW61DaUQavnyQyxNOYWfWwzHYaj5cwP7rgL"

    headers = {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/x-www-form-urlencoded"
    }

    ### 取得股價
    for a in attention_list:
        stock = yfinance.Ticker(a.get('code'))
        if stock.info.get('ask') != 0:
            if isinstance(stock.info.get('ask'),float) or isinstance(stock.info.get('ask'),int):
                now_price = str(stock.info.get('ask'))
                s_now_price = str(stock.info.get('bid'))
            else:
                now_price = str(stock.info.get('ask').get('raw'))
                s_now_price = str(stock.info.get('bid').get('raw'))

            ###
            d = datetime.datetime.utcnow()

            tw = pytz.timezone('Asia/Taipei')

            tw_now_time = str(tw.fromutc(d)).split('.')[0]

            msg = '當下時間 '+tw_now_time+' 美股 '+a.get('name')+' 目前買入股價' + now_price +'美金' +' / '+' 目前賣出股價' + s_now_price

            payload = {'message': msg}
            r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)

    return {}

def push_test(settings):



    ##### 推波測試

    token = "RhtzyxkGbW61DaUQavnyQyxNOYWfWwzHYaj5cwP7rgL"

    headers = {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/x-www-form-urlencoded"
    }

    msg='test'

    payload = {'message': msg}
    r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)

    token = "OvpjIkEtbEFV9t2UMaDitwRwh5sVaSTrro5gRpFF4AR"

    headers = {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/x-www-form-urlencoded"
    }

    msg='test'

    payload = {'message': msg}
    r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)


    return {}

def web_check(settings):
    import datetime
    import pytz

    ##### 推波測試

    web_service = WebService(settings['session'])

    web_list = web_service.get_list()

    token = "VyyGlKGUkmuhKSAGeVH8vExKHUGo1ucIWPHyYgLYQ0e"

    headers = {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/x-www-form-urlencoded"
    }

    d = datetime.datetime.utcnow()

    tw = pytz.timezone('Asia/Taipei')

    tw_now_time = str(tw.fromutc(d)).split('.')[0]

    for web in web_list:
        r = requests.get(web.web_url)

        if r.status_code !=200:
            msg = '站點 '+web.web_name +' 於 '+ tw_now_time +' 失去聯絡'

            payload = {'message': msg}
            r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)
        # else:
        #     msg = '站點 '+web.web_name +' 於 '+ tw_now_time +' 聯絡正常'
        #
        #     payload = {'message': msg}
        #     r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)


    return {}