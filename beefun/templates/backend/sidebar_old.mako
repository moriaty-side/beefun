<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark" data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

        
            
            ## 首頁
            <li class="m-menu__item" aria-haspopup="true">
                <a data-link="index" href="${ request.route_url('backend.page.home')}" class="m-menu__link">
                    <i class="m-menu__link-icon fa fa-desktop"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                ${ _(u'首頁')}
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            ## 帳號
            <li class="m-menu__item" aria-haspopup="true" >
                <a data-link="account_list" href="${ request.route_url('backend.page.account_list') }" class="m-menu__link ">
                    <i class="m-menu__link-icon  flaticon-users"></i>
                    <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                      <span class="m-menu__link-text">
                          ${ _(u'帳號管理') }
                      </span>
                    </span>
                    </span>
                </a>
            </li>
            

            

            
            ## 網站管理
            <li class="m-menu__item" aria-haspopup="true">
                <a data-link="web_set" href="${ request.route_url('page.optimization.webset',language=request.registry.settings.get('available_langs', [['zh_Hant', '繁體中文']])[0][0]) }" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-map"></i>
                    <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                    <span class="m-menu__link-text">
                      ${ _(u'網站管理')}
                    </span>
                    </span>
                    </span>
                </a>
            </li>

           ## 消息管理
            <li class="m-menu__item  m-menu__item--submenu" data-parent="case" aria-haspopup="true" data-menu-submenu-toggle="hover">
                <a  href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-notes"></i>
                    <span class="m-menu__link-text">
                        ${ _(u'報修管理')}
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item" aria-haspopup="true">
                            <a data-link="client_list" href="${ request.route_url('page.client.backend.list') }" class="m-menu__link">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    ${ _(u'廠商列表')}
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item" aria-haspopup="true">
                            <a data-link="case_list" href="${ request.route_url('page.case.backend.list') }" class="m-menu__link">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    ${ _(u'案件列表')}
                                </span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>
            

            
            ## 首頁管理
            <li class="m-menu__item" aria-haspopup="true">
                <a  href="${ request.route_url('page.banner.list') }" class="m-menu__link">
                    <i class="m-menu__link-icon fa fa-desktop"></i>
                    <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                    <span class="m-menu__link-text">
                        ${ _(u'Banner')}
                    </span>
                    </span>
                    </span>
                </a>
            </li>
            

            
            <li class="m-menu__item  m-menu__item--submenu" data-parent="home_set" aria-haspopup="true" data-menu-submenu-toggle="hover">
                <a  href="${ request.route_url('page.video',language=request.registry.settings.get('available_langs', [['zh_Hant', '繁體中文']])[0][0]) }" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon fa fa-desktop"></i>
                    <span class="m-menu__link-text">
                        ${ _(u'影片')}
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
            </li>

            <li class="m-menu__item  m-menu__item--submenu" data-parent="home_set" aria-haspopup="true" data-menu-submenu-toggle="hover">
                <a  href="${ request.route_url('page.web.list',language=request.registry.settings.get('available_langs', [['zh_Hant', '繁體中文']])[0][0]) }" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon fa fa-desktop"></i>
                    <span class="m-menu__link-text">
                        ${ _(u'站點偵測')}
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
            </li>
            

            

            
            ## 消息管理
            <li class="m-menu__item  m-menu__item--submenu" data-parent="news" aria-haspopup="true" data-menu-submenu-toggle="hover">
                <a  href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-notes"></i>
                    <span class="m-menu__link-text">
                        ${ _(u'最新消息管理')}
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item" aria-haspopup="true">
                            <a data-link="news_category" href="${ request.route_url('page.category.news',type='news') }" class="m-menu__link">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    ${ _(u'服務管理')}
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item" aria-haspopup="true">
                            <a data-link="news_category" href="${ request.route_url('page.category.news',type='news') }" class="m-menu__link">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    ${ _(u'前台標籤管理')}
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item" aria-haspopup="true">
                            <a data-link="news_category" href="${ request.route_url('page.category.news',type='news') }" class="m-menu__link">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    ${ _(u'後台標籤管理')}
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item" aria-haspopup="true">
                            <a data-link="news_list" href="${ request.route_url('page.news.list') }" class="m-menu__link">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    ${ _(u'廠商列表')}
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            

            
            ## 產品管理
            <li class="m-menu__item  m-menu__item--submenu" data-parent="product" aria-haspopup="true" data-menu-submenu-toggle="hover">
                <a  href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-network"></i>
                    <span class="m-menu__link-text">
                        ${ _(u'產品管理')}
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item" aria-haspopup="true">
                            <a data-link="product_category" href="${ request.route_url('page.category.product',type='product') }" class="m-menu__link">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    ${ _(u'分類')}
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item" aria-haspopup="true">
                            <a data-link="product_list" href="${ request.route_url('page.product.list') }" class="m-menu__link">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    ${ _(u'產品列表')}
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            

            

        
    </ul>
</div>