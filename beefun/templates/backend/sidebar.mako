<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark" data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

        
            
            ## 首頁
            <li class="m-menu__item" aria-haspopup="true">
                <a data-link="index" href="${ request.route_url('backend.page.home')}" class="m-menu__link">
                    <i class="m-menu__link-icon fa fa-desktop"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                ${ _(u'首頁')}
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            ## 帳號
            <li class="m-menu__item" aria-haspopup="true" >
                <a data-link="account_list" href="${ request.route_url('backend.page.account_list') }" class="m-menu__link ">
                    <i class="m-menu__link-icon  flaticon-users"></i>
                    <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                      <span class="m-menu__link-text">
                          ${ _(u'帳號管理') }
                      </span>
                    </span>
                    </span>
                </a>
            </li>


           ## 消息管理
            <li class="m-menu__item  m-menu__item--submenu" data-parent="case" aria-haspopup="true" data-menu-submenu-toggle="hover">
                <a  href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-notes"></i>
                    <span class="m-menu__link-text">
                        ${ _(u'身份管理')}
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item" aria-haspopup="true">
                            <a data-link="client_list" href="${ request.route_url('page.client.backend.list') }" class="m-menu__link">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    ${ _(u'客戶列表')}
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            





    </ul>
</div>