<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
        ${ _(u'beefun')}
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- <link rel="import" href="m_ver_menu.html"> -->
    <!--begin::Base Styles -->
    <link href="${ request.static_path('beefun:static/assets/vendors/base/vendors.bundle.css')}" rel="stylesheet" type="text/css" />
    <link href="${ request.static_path('beefun:static/assets/demo/default/base/style.bundle.css')}" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <!--begin::Base Scripts -->
    <script src="${ request.static_path('beefun:static/assets/vendors/base/vendors.bundle.js')}" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/demo/default/base/scripts.bundle.js')}" type="text/javascript"></script>
    <!--end::Base Scripts -->
    <!--end::Base Styles -->

    <!-- BEGIN OTHER STYLES -->
    <%block name="css"/>
    <!-- END OTHER STYLES -->

    <link href="${ request.static_path('beefun:static/assets/main.css')}" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="${ request.static_path('beefun:static/assets/demo/default/media/img/logo/favicon.ico')}" />

    <script src="${ request.static_path('beefun:static/assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js')}" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/demo/default/custom/components/forms/widgets/bootstrap-datetimepicker.js')}" type="text/javascript"></script>

<!-- END THEME STYLES -->

</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Header -->
    <header class="m-grid__item m-header"  data-minimize-offset="200" data-minimize-mobile-offset="200">
        <!-- load m-header.html -->
        <div class="m-container m-container--fluid m-container--full-height">
            <div class="m-stack m-stack--ver m-stack--desktop">
                <!-- BEGIN: Brand -->

                <!-- END: Brand -->

                    
                    <!-- END: Topbar -->
                </div>
            </div>
        </div>
    </header>
    <!-- END: Header -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->

        <!-- END: Left Aside -->

        <%block name="content"/>

    </div>
    <!-- end:: Body -->
    <!-- begin::Footer -->
    <footer class="m-grid__item m-footer">
        <!-- load m-footer.html -->
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                            <span class="m-footer__copyright">
                                2018 &copy; Metronic theme by
                            </span>
                </div>
            </div>
        </div>

        <!-- begin::Scroll Top -->
        <div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
            <i class="la la-arrow-up"></i>
        </div>
        <!-- end::Scroll Top -->
    </footer>
    <!-- end::Footer -->
</div>
<!-- end:: Page -->
<script>
    var datamenu    = $('.m-content').attr('data-menu');
    var dataparent  = $('.m-content').attr('data-parent');
    if (dataparent !="")
    {
        $('li.m-menu__item--submenu[data-parent="'+dataparent+'"]' ).addClass(' m-menu__item--expanded m-menu__item--open');
        $('li.m-menu__item--submenu a[data-link="'+datamenu+'"]').parent().parent().parent().css("display", "block");
        $('li.m-menu__item  a[data-link="' + datamenu + '"]').parent("li").addClass('  m-menu__item--active');
    }
    else
    {
        $('li.m-menu__item  a[data-link="' + datamenu + '"]').parent("li").addClass('  m-menu__item--active');
    }



</script>
    <%block name="script"/>
</body>
<!-- end::Body -->
</html>
