from __future__ import unicode_literals

import base64
import hashlib
import hmac
import os
import random
import string

def generate_random_sha():
    return ''.join(
        random.choice(
            string.ascii_uppercase + string.digits
        ) for _ in range(10)
    )


def generate_random_code():
    """Generate random code."""
    random_code = os.urandom(60)
    return hmac.new(random_code).hexdigest()


def salt_password(password, salt=None, hash_name='sha1'):
    """Generate hashed password with salt hash(password + salt).

    if salt is not given,
        generate_random_code will be used for generating the salt value
    """
    if isinstance(password, str):
        password = password.encode('utf8')
    if isinstance(salt, str):
        salt = salt.encode('utf8')
    if not salt:
        salt = generate_random_code().encode('utf8')

    hash_method = getattr(hashlib, hash_name)

    # generate hashed password
    hashed_pwd = hash_method()
    hashed_pwd.update(password)
    hashed_pwd.update(salt)
    hex_hashed_pwd = hashed_pwd.hexdigest()
    return hash_name, salt, hex_hashed_pwd


def secret(content, salt=None):
    if not salt:
        salt = generate_random_code()
    return base64.b64encode(hmac.new(bytearray(content.upper(), "UTF8"), bytearray(salt, "UTF8"),
                                     digestmod=hashlib.sha256).digest()).decode()



