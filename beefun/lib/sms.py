# coding=utf8
import logging
import requests

logger = logging.getLogger(__name__)

class SMS(object):
    def __init__(self, request=None):
        self.request = request
        self.url = request.setting['sms_url']
        self.username = request.setting['sms_username']
        self.password = request.setting['sms_password']

        # self.response_url = request.route_url('api.sms.response')

    def send_one(self, mobile, user_name, message, sms_type='', rel_id=''):
        # TODO: 簡訊點數<200時須發通知
        """

        :param mobile: 收件者手機號碼
        :param user_name: 收件者名
        :param message: 簡訊內容
        :param sms_type: 簡訊類型 (若有要統計簡訊發送內容, 透過callback回傳參數回來做統計)
        :param rel_id: 相關ID
        """
        request_url = self.url + '/SmSendGet.asp'

        params = {
            'username': self.username,
            'password': self.password,
            'dstaddr': mobile,
            # 'encoding': 'Unicode',
            'DestName': user_name.encode('big5'),
            'dlvtime': '',
            'vldtime': '',
            'smbody': message.encode('big5'),
            # 'response': '{0}?sms_type={1}&rel_id={2}'.format(self.response_url, sms_type, rel_id),
            'ClientID': ''
        }
        r = requests.get(request_url, params=params)
        logger.info('status_code: {0}, '.format(r.status_code))
        logger.info('text: {0}, '.format(r.text))
        print(r.status_code)
        print(r.text)
        """
        回傳內容:
        [1]
        msgid=0869713200
        statuscode=1
        AccountPoint=549
        """

    def send_multi(self):
        pass

    def query(self):
        pass

    def cancel(self):
        pass

# sms = SMS()
# sms.send_one(u'0983742247', u'Millet', u'恭喜您獲得 100元折價卷，消費後請出示此封簡訊折價卷有效日期為XX月XX日')