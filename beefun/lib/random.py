import os
import hmac
import random
import string

def generate_random_code(num):
    """Generate random code."""
    random_code = os.urandom(num)
    return hmac.new(random_code).hexdigest()

def generate_random_sha(num):
    return ''.join(
        random.choice(
            string.ascii_uppercase + string.digits
        ) for _ in range(num)
    )