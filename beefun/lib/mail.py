# -*- coding: utf-8 -*-
import logging
import requests
from .my_exception import MyException
from .issue import issue
logger = logging.getLogger(__name__)

def send_mail_by_smtp(request, subject, to_list, content):
    logger.info('Send mail to: %r', to_list)

    import email
    import smtplib
    from email.mime.text import MIMEText

    mail = request.registry.settings['mail_smtp']
    mail_host = mail.get('host', '')
    mail_user = mail.get('user', '')
    mail_pass = mail.get('passwd', '')
    mail_sender = mail.get('sender', 'service')
    sender_addr = mail.get('sender_email', '')

    msg = MIMEText(content, _subtype='html', _charset='utf8')
    msg['Subject'] = subject
    msg['From'] = email.utils.formataddr((mail_sender, sender_addr))
    msg['To'] = ";".join(to_list)
    try:
        s = smtplib.SMTP(mail_host)
        s.ehlo()
        s.starttls()
        s.ehlo()
        s.login(mail_user, mail_pass)
        s.sendmail(sender_addr, to_list, msg.as_string())
        s.close()
        return True
    except Exception as err:
        logger.error('error: {0}'.format(err))
        return False


def send_mail_by_req(request, subject, to_list, content):
    logger.info('Send mail to: %r', to_list)

    mail = request.registry.settings['mail_req']
    mail_url = mail.get('url', '')
    mail_key = mail.get('key', '')
    mail_from = mail.get('from', '')
    mail_to = ",".join(to_list)

    try:
        data = {
            'from': mail_from,
            'to': mail_to,
            'subject': subject,
            'text': u'請用html支援的Email系統打開此信',
            'html': content
        }
        r = requests.post(mail_url, auth=('api', mail_key), data=data)

        logger.info('Mail status: {0}'.format(r.status_code))
        logger.info('Mail body: {0}'.format(r.text))

        if r.status_code == 200:
            return True
        return False
    except Exception as err:
        logger.error('error: {0}'.format(err))
        return False


def send_mail_by_mailgun_text(request, subject, content, email):
    try:
        url = request.registry.settings['mail_gun_url']
        api_key = request.registry.settings['mail_gun_api_key']
        data = {"from": request.registry.settings['mail_gun_from'],
               "to": email,
               "subject": subject,
               "text": content}
        mail_gun = requests.post(url=url,
                                 auth=("api", api_key),
                                 data=data)

        if mail_gun.status_code != 200:
            return MyException(code=1008)

        return mail_gun

    except MyException as e:
        raise e

    except Exception as err:
        raise MyException(code=1008)

def send_mail_by_mailgun_html(request, subject, content, email):
    try:
        url = request.registry.settings['mail_gun_url']
        api_key = request.registry.settings['mail_gun_api_key']
        data = {"from": request.registry.settings['mail_gun_from'],
               "to": email,
               "subject": subject,
               "html": content}
        mail_gun = requests.post(url=url,
                                 auth=("api", api_key),
                                 data=data)

        if mail_gun.status_code != 200:
            return MyException(code=1008)

        return mail_gun

    except MyException as e:
        raise e

    except Exception as err:
        raise MyException(code=1008)