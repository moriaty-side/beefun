# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# 測試
# suntech_paid_creditcard_etopm_url = 'https://test2.esafe.com.tw/Service/Etopm.aspx'
# suntech_paid_creditcard_refund_url = 'https://test.esafe.com.tw/Service/Hx_CardRefund.ashx'
# suntech_paid_creditcard_buysafe = 'S1709289035'
# suntech_paid_creditcard_pass = '524a19b152'

# 正式
suntech_paid_creditcard_etopm_url = 'https://www.esafe.com.tw/Service/Etopm.aspx'
suntech_paid_creditcard_refund_url = 'https://www.esafe.com.tw/Service/Hx_CardRefund.ashx'
suntech_paid_creditcard_buysafe = 'S1712130119'
suntech_paid_creditcard_pass = '05A3W2yJTk5'


def ChkValue(list=None):
    if list is None:
        list = []
    # 交易代碼產生
    import hashlib
    ChkValue = hashlib.sha1()
    ChkValue.update(str(suntech_paid_creditcard_buysafe).encode('utf-8'))
    ChkValue.update(str(suntech_paid_creditcard_pass).encode('utf-8'))
    for value in list:
        ChkValue.update(str(value).encode('utf-8'))

    return ChkValue.hexdigest().upper()

def Returns_ChkValue(list=None):
    if list is None:
        list = []
    # 交易代碼產生
    import hashlib
    ChkValue = hashlib.sha256()
    ChkValue.update(str(suntech_paid_creditcard_buysafe).encode('utf-8'))
    ChkValue.update(str(suntech_paid_creditcard_pass).encode('utf-8'))
    for value in list:
        ChkValue.update(str(value).encode('utf-8'))

    return ChkValue.hexdigest()




