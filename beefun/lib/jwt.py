# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import time

from jwcrypto.common import json_decode, json_encode
from jwcrypto.jwe import JWE
from jwcrypto.jwk import JWK
from jwcrypto.jws import JWS
from jwcrypto.jwt import JWT

"""
JWT utility.

pip install jwcrypto, cryptography
works in Python 2.7 and 3.3+

使用 jwcrypto
    http://jwcrypto.readthedocs.org/en/stable/
需要 cryptography
    https://cryptography.io/en/latest/
"""


def jwk_oct_identifier_key(identifier):
    """
    產生長度 512 位元的亂碼，可以當成加密鑰匙或簽名。.
    identifier ==>標示码
    Key or signarture generator, in symmetric key form.
    """
    return JWK(generate='oct', size=512, kid=identifier)


def jwk_oct_key(size=256):
    """
    產生長度 128,256,512 位元的亂碼，可以當成加密鑰匙或簽名。.

    Key or signarture generator, in symmetric key form.
    """
    return JWK(generate='oct', size=size)


def jwe_genertor(byte_string, encrypt_key):
    """
    將 JWS 字串變成 bytes 之後放進來，變成 JWE。.

    Make JWE token, with symmetry key.
    Parameter reference:
        http://connect2id.com/products/nimbus-jose-jwt
    """
    parameters = {'alg': 'dir', 'enc': 'A128CBC-HS256'}
    e = JWE(byte_string, json_encode(parameters))
    e.add_recipient(encrypt_key)
    return e.serialize(compact=True)


def jws_generator(claims, signature):
    """
    生成 JWS，不能只生成 JWT。.

    Make JWS token.
    """
    header = dict(alg='HS256')
    t = JWT(header, claims)
    t.make_signed_token(signature)
    signed_token = t.serialize(compact=True)
    return signed_token


def construct_claims(issuer='My IP', who_can_use_this_token='Some IP'):
    """
    生成 token 內要包含的資料，及修改保留資料。.

    Pack message you want.
    """
    # Token expires at 600 seconds after current time.
    token_expire_time = time.time() + 600
    # Token would be activated 5 seconds after current time.
    # This could delay DoS attack.
    token_activate_time = time.time() + 5
    purpose_of_this_token = str()
    # This should be changed accordingly
    issue_time = time.time()
    token_id = int()
    claims = dict(
        exp=token_expire_time,
        iat=issue_time,
        nbf=token_activate_time,
        iss=issuer,
        aud=who_can_use_this_token,
        sub=purpose_of_this_token,
        jti=token_id,
    )
    return claims


def jwt_decode(token_string, signature, encrypt_key, from_whom, to_whom):
    """
    JWT(JWS or JWS/JWE) decodor.

    可帶入要檢查的保留字內容。
    """
    token = JWT(jwt=token_string).token
    if isinstance(token, JWE):
        token.decrypt(encrypt_key)
        raw_jws = token.payload
        jws = raw_jws.decode('utf-8')
        token = JWT(jwt=jws).token
    token.verify(signature)
    payload = json_decode(token.payload)
    claim = check_claims(payload, issuer=from_whom, audience=to_whom)
    if claim:
        return payload
    elif isinstance(claim, str):
        raise Exception(claim)


def jwe_decode(encrypted_token_string, key, expire_pass=False):
    """
    JWE decoder.

    可帶入要檢查的保留字內容。
    """
    e = JWE()
    e.deserialize(encrypted_token_string)
    e.decrypt(key)
    raw_payload = e.payload
    payload = json_decode(raw_payload)
    try:
        claim = check_claims(payload)
        if claim:
            return payload
        elif isinstance(claim, str):
            raise Exception(claim)
    except Exception as err:
        if "exp" in err.args[0] and expire_pass:
                return payload
        else:
            raise err





def jws_decode(signed_token, signature):
    """
    JWS decoder.

    可帶入要檢查的保留字內容。
    """
    s = JWS()
    s.deserialize(signed_token, key=signature)
    raw_payload = s.payload
    payload = json_decode(raw_payload)
    claim = check_claims(payload)
    if claim is True:
        return payload
    elif isinstance(claim, str):
        raise Exception(claim)


def check_claims(claims, audience=str(), issuer=str(),
                 issue_time=int(), subject=str(),
                 token_id=int(), leeway=int()):
    """
    檢查保留字。.

    JWS claim check.
    """
    if claims.get('aud') and claims.get('aud') != audience:
        raise Exception('audience not match')
    if claims.get('nbf') and claims.get('nbf') > time.time():
        raise Exception('token not activate')
    if claims.get('exp') and claims.get('exp') < time.time() + leeway:
        raise Exception('token expire')
    if claims.get('iss') and claims.get('iss') != issuer:
        raise Exception('issuer not match')
    if claims.get('sub') and claims.get('sub') != subject:
        raise Exception('subject not match')
    if claims.get('iat') and claims.get('iat') != int(issue_time):
        raise Exception('issue time not match')
    if claims.get('jti') and claims.get('jti') != int(token_id):
        raise Exception('token ID not match')
    return True
