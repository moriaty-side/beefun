# coding=utf8
from __future__ import unicode_literals
import redis
import json


class RedisService(object):
    def __init__(self, host, port):
        try:
            self.redis_db = redis.StrictRedis(host=host, port=port, db=0)
        except Exception as e:
            self.redis_db = None
            print("Redis Connect Error {0}".format(e))

    def get(self, redis_key):
        # 連線redis
        redis_result_byte = self.redis_db.get(redis_key)

        return redis_result_byte.decode("utf-8") if redis_result_byte else None

    def set(self, key, value, ex=86400):
        # 連線redis
        self.redis_db.set(key, value, ex)  # expire sec = 60 X 60 X 24 = 86400 sec(1 day)
