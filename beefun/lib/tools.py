# coding=utf8
from __future__ import unicode_literals

import string
import random
import hashlib


def saltpassword(password, salt='okborn'):
    return hashlib.md5((password + salt).encode('utf-8')).hexdigest()


def generate_token(num=6):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(num))
