from __future__ import unicode_literals
from collections import defaultdict

class RecursiveDefaultDict(defaultdict):
    def __init__(self, *args, **kwargs):
        super(RecursiveDefaultDict, self).__init__(_dict_factory,
                                                   *args, **kwargs)

        del_list = list()
        for each in self:
            self[each] = _check_and_build(self[each])
            if not self[each]:
                del_list.append(each)

        for each in del_list:
            del self[each]

    def __str__(self):
        return ''


def _dict_factory(*args, **kwargs):
    return RecursiveDefaultDict(*args, **kwargs)

def _check_and_build(obj):
    if isinstance(obj, dict):
        return RecursiveDefaultDict(obj)
    elif isinstance(obj, list):
        _rlist = list()

        for each in obj:
            r_obj = _check_and_build(each)
            if r_obj:
                _rlist.append(r_obj)

        return _rlist
    else:
        return obj
