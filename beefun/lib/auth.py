# -*- coding: utf-8 -*-
from __future__ import unicode_literals

def get_group(member_id, request):
    """
    須先區分登入者為管理員(@account)或會員(@member)
    userid: account_id@admin / member_id@member
    """
    if member_id:
        try:
            _id, user_type = member_id.split('@')
            if user_type in ['account']:
                result = {'account', '%s:%s' % ('account', _id)}

                

            elif user_type in ['member']:
                result = {'member', '%s:%s' % ('member', _id)}

            return result
        except:
            return set()
    else:
        return set()
