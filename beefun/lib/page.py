# coding=utf8
from __future__ import unicode_literals

import logging
import time
from pyramid.httpexceptions import HTTPFound, HTTPNotFound
from pyramid.url import urlencode
from .my_exception import MyException

def return_page_format(some_view):
    def wrapper(context, request):
        system_name = request.registry.settings.get('system_name', '')
        logger = logging.getLogger(system_name)
        start = time.time()
        # 合法的網址
        if request.route_name:
            route_type = None
            if "backend.page" in request.route_name:
                route_type = "backend_page"
            elif "frontend.page" in request.route_name:
                route_type = "frontend_page"
            elif "api" in request.route_name:
                route_type = "api"
            else:
                route_type = "backend_page"

        try:
            r = some_view(context, request)
            stop = time.time()
            run_time = stop - start
            logger_json = {"route_name ": system_name.lower() + "." + request.route_name,
                           "method": request.method,
                           "path_url": request.path_url,
                           "ip": request.real_ip,
                           "run_time": run_time}
            # check user type
            if request.member_id:
                logger_json['user'] = str(request.member_id)

            logger.info(logger_json)
            request.db_session.close()
            return r
        except MyException as err:
            stop = time.time()
            run_time = stop - start
            logger_json = {"route_name ": system_name.lower() + "." + request.route_name,
                           "method": request.method,
                           "path_url": request.path_url,
                           "ip": request.real_ip,
                           "run_time": run_time,
                           "err_message": err.message,
                           "err_code": err.code,
                           "err_source": err.source}
            # check user type
            if request.member_id:
                logger_json['user'] = str(request.member_id)

            logger.warning(logger_json)
            request.db_session.rollback()

            if route_type == "backend_page":
                if request.member_id:
                    return HTTPFound(location=request.route_url('page.error', _query={'message': err.message,'from': 'backend_page'}))
                return HTTPFound(location=request.route_url('backend.page.login'))

            if route_type == "frontend_page":
                # return HTTPFound(location=request.route_url('page.error', _query={'message': err.message,'from': 'frontend_page'}))
                return HTTPFound(location=request.route_url('frontend.page.index'))

            return HTTPFound(location=request.route_url('page.error', _query={'message':  err.message}))
        except Exception as err:
            stop = time.time()
            run_time = stop - start
            msg = err.args[0] if err.args else ""
            logger_json = {"route_name ": system_name.lower() + "." + request.route_name,
                           "method": request.method,
                           "path_url": request.path_url,
                           "ip": request.real_ip,
                           "run_time": run_time,
                           "err": msg}
            # check user type
            if request.member_id:
                logger_json['user'] = str(request.member_id)

            logger.warning(logger_json)
            request.db_session.rollback()

            if route_type == "backend_page":
                if request.member_id:
                    return HTTPFound(location=request.route_url('page.error', query={'message': msg, 'from': 'backend_page'}))
                return HTTPFound(location=request.route_url('backend.page.login'))

            if route_type == "frontend_page":
                # return HTTPFound(location=request.route_url('page.error', query={'message': msg, 'from': 'frontend_page'}))
                return HTTPFound(location=request.route_url('frontend.page.index'))

            return HTTPFound(location=request.route_url('page.error')+'?'+urlencode({'message': msg}))

    return wrapper


