# coding=utf8
from __future__ import unicode_literals
import os
import sys
import transaction
import uuid

from pyramid.paster import (
    get_appsettings,
    setup_logging,
)

from pyramid.scripts.common import parse_vars
from sqlalchemy_utils import create_database, database_exists

from ..base.meta_module import Base
from ..base.base_models import (
    get_engine,
    get_session_factory,
    get_tm_session,
)

from ..dc_module.banner import banner_domain

from ..dc_module.news import news_domain

from ..dc_module.video import video_domain

from ..dc_module.category import category_domain

from ..dc_module.account import account_domain

from ..dc_module.optimization import optimization_domain

from ..dc_module.tag import tag_domain

from ..dc_module.product import product_domain

from ..dc_module.image import image_domain

from ..dc_module.currency import currency_domain

from ..dc_module.currency_date_time_price import currency_date_time_price_domain

from ..dc_module.stock import stock_domain

from ..dc_module.stock_date_time_price import stock_date_time_price_domain

from ..dc_module.member import member_domain

from ..dc_module.web import web_domain

from ..dc_module.john_work import john_work_domain



def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri> [var=value]\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def main(argv=sys.argv):
    if len(argv) < 2:
        usage(argv)
    config_uri = argv[1]
    options = parse_vars(argv[2:])
    setup_logging(config_uri)
    settings = get_appsettings(config_uri, options=options)

    # 檢查DB是否存在
    if not database_exists(settings.get('sqlalchemy.url')):
        # 創建DB
        create_database(settings.get('sqlalchemy.url'))

    # 創建資料表
    engine = get_engine(settings)
    Base.metadata.create_all(engine)

    session_factory = get_session_factory(engine)

    # 加入欄位的doc欄位到DB的註解欄位
    table_doc_comment(Base, engine)
    dbsession = get_tm_session(session_factory, transaction.manager)
    
    with transaction.manager:
        from ..dc_module.account.account_service import AccountService
        from ..dc_module.currency.currency_service import CurrencyService
        from ..dc_module.stock.stock_service import StockService
        account_service = AccountService(dbsession)
        currency_service = CurrencyService(dbsession)
        stock_service = StockService(dbsession)
        account_dic_list = [{'name': 'admin', 'account': 'admin@gmail.com', 'password': 'admin123'}]
        currency_dic_list = [{'currency_name': '美金', 'currency_code': 'USD'},
                             {'currency_name': '日幣', 'currency_code': 'JPY'},
                             {'currency_name': '台幣', 'currency_code': 'TWD'}]
        stock_dic_list = [{'stock_name': '台積電', 'stock_code': '2330', 'type': 'TW', 'sec_type': 'individual'},
                          {'stock_name': 'VOO', 'stock_code': 'voo', 'type': 'USA', 'sec_type': 'etf'},
                          {'stock_name': '台灣50', 'stock_code': '0050', 'type': 'TW', 'sec_type': 'etf'}]
        for account_dic in account_dic_list:
            account_obj = account_service.get_by(**{'name': account_dic.get('name')})
            if not account_obj:
                account_service.create(**account_dic)
            else:
                account_service.update(account_obj, **account_dic)

        for currency_dic in currency_dic_list:
            currency_obj = currency_service.get_by(**{'currency_code': currency_dic.get('currency_code')})
            if not currency_obj:
                currency_service.create(**currency_dic)
            else:
                currency_service.update(currency_obj, **currency_dic)

        for stock_dic in stock_dic_list:
            stock_obj = stock_service.get_by(**{'stock_code': stock_dic.get('stock_code')})
            if not stock_obj:
                stock_service.create(**stock_dic)
            else:
                stock_service.update(stock_obj, **stock_dic)

    # with transaction.manager:
        from ..dc_module.image.image_service import ImageService
        # image_service = ImageService(settings['repository_path'], dbsession)
        # admin_account_obj = account_service.get_by(**{'name': 'admin'})
        # image_dic_list = {'image_id': uuid.UUID('aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa'),
        #                   'other_file': [],
        #                   'image_base64': [
        #                       '{"data":"iVBORw0KGgoAAAANSUhEUgAAAIwAAABkCAYAAABU19jRAAAFJUlEQVR4Xu2aMU/kMBCFQwcldFBCCR2UUPKrKaGEDkoooWNL6O40kQb5ck48zxsnWvGtdNKJvDgzz58nEyd7m83mT8cPB4IO7AFM0ClkvQMAAwiSAwAj2YUYYGBAcgBgJLsQAwwMSA4AjGQXYoCBAckBgJHsQgwwMCA5ADCSXYgBBgYkBwBGsgsxwMCA5ADASHYhBhgYkBwAGMkuxAADA5IDACPZhRhgYEByAGAkuxADDAxIDgCMZBdigIEByQGAkexCDDAwIDkAMJJdiAEGBiQHAEayCzHAwIDkAMBIdiEGGBiQHAAYyS7EAAMDkgMAI9mFGGBgQHIAYCS7EAMMDEgOAIxkF2KAgQHJgcWA+fr66l5fX7vNZtN9f393+/v73cHBQXd6etodHR2Fgn58fOzPv729Lerf3987+2f6mmsVL5ARtI5vjZyGaS4CjMFiZhooud/5+Xl3cnJSnKO7u7teUwLm+fm5+/j42OpaxWAygpbxrZXTKsDc39//wHJ8fNxXFIPIVoxDdH193VecsZ9Vp7e3tyIwNubLy0uvOzw8/AHRzvdrXV5ehqtaFJyW8a2VUy735hUmTXY4UWnlMZAuLi7+idGOO1hpxZiqMA5nbjy/ZRhIV1dXURZGdUvFt2ROJVOaA+OlNDeBFpwDZX3Gzc3NT7w2GQ8PD9n4x4BJ4cxp0jFLFa1k3FLxLZlTKWc73hwYX9VjfUpqfDrJNRNSgtMS9tXq8Xx+fnZPT0+9V9aAn52d/VflHNwU+qXiq8kpMvG1mubA+ARN9Q3eLA5XvU2K/+z/PrFjFaYEp43lvUY6+WlDOYxh6tgS8dXmVAtE6bzmwNgE2c+egnJN7ViFGQYe0UXg9BKf9jHp2GNVJFd90hhbxVebU2nia483B6YUmK/4YQ9TA8xYpUrH8lvQ8Hq5SuJ/K8Vm40eAqYmv5pyS59scXxWYUv+grOBoQzsGzLDKWC/jvUtkn6gETE189iTnMUw16WM5bQPG2LmrAZN2/3Os4NKEpb2QT8KwF0r3UuyWZbvE0Ufw0vVLx3PxGSRjsSqLaU5wVgEmLf8Gi62kqU27SMmvWcHpY7ybmm4y2t+im3wlIGri+/UVZviKwFavlfsSLBFgTDPH/X64qxrd4CsBUxvfHDntZIVJ+xVLoPTUUdP0zvFEMax+uSqUm4AIMDXx1ZwzJyDDsRa5JaVmRm9BNcBsu2eR24yLgh0Bpia+mnN2Hpj0HU70FlQDjLIrmgPB4zSo7Xbp768irxEiwNTEV3POTgOTPvJFmtuxZCMTss17l/SWaY2u9VW5VwJLx7dNTi3AaX5L8kfVaGnfZkLs3No3u7k32VOvBWoqYG18tTntJDA+EbblXnoaGnt9EH1KMl3NtyNjn2CkVa20HxOpgHPEt+Q3PjngmleY4b7GFPVTvUJ0Qmz8qa/TcpXOY8xBkW7mTe3JtIyvJqcW1cXGbA6M7yNEEpgLGF/JkW96I0BMAeV5KcAo8aW+/ZpveiOwoNkNB5pXmN2wgSijDgBM1Cl0vQMAAwiSAwAj2YUYYGBAcgBgJLsQAwwMSA4AjGQXYoCBAckBgJHsQgwwMCA5ADCSXYgBBgYkBwBGsgsxwMCA5ADASHYhBhgYkBwAGMkuxAADA5IDACPZhRhgYEByAGAkuxADDAxIDgCMZBdigIEByQGAkexCDDAwIDkAMJJdiAEGBiQHAEayCzHAwIDkAMBIdiEGGBiQHAAYyS7EAAMDkgMAI9mFGGBgQHLgL8JPbm+p0XbRAAAAAElFTkSuQmCC","filename":"AAAAAA&text=210x100.png"}'],
        #                   'image_file': [],
        #                   'image_url': []}
        #
        # set_json = {
        #     'data': 'iVBORw0KGgoAAAANSUhEUgAAAIwAAABkCAYAAABU19jRAAAFJUlEQVR4Xu2aMU/kMBCFQwcldFBCCR2UUPKrKaGEDkoooWNL6O40kQb5ck48zxsnWvGtdNKJvDgzz58nEyd7m83mT8cPB4IO7AFM0ClkvQMAAwiSAwAj2YUYYGBAcgBgJLsQAwwMSA4AjGQXYoCBAckBgJHsQgwwMCA5ADCSXYgBBgYkBwBGsgsxwMCA5ADASHYhBhgYkBwAGMkuxAADA5IDACPZhRhgYEByAGAkuxADDAxIDgCMZBdigIEByQGAkexCDDAwIDkAMJJdiAEGBiQHAEayCzHAwIDkAMBIdiEGGBiQHAAYyS7EAAMDkgMAI9mFGGBgQHIAYCS7EAMMDEgOAIxkF2KAgQHJgcWA+fr66l5fX7vNZtN9f393+/v73cHBQXd6etodHR2Fgn58fOzPv729Lerf3987+2f6mmsVL5ARtI5vjZyGaS4CjMFiZhooud/5+Xl3cnJSnKO7u7teUwLm+fm5+/j42OpaxWAygpbxrZXTKsDc39//wHJ8fNxXFIPIVoxDdH193VecsZ9Vp7e3tyIwNubLy0uvOzw8/AHRzvdrXV5ehqtaFJyW8a2VUy735hUmTXY4UWnlMZAuLi7+idGOO1hpxZiqMA5nbjy/ZRhIV1dXURZGdUvFt2ROJVOaA+OlNDeBFpwDZX3Gzc3NT7w2GQ8PD9n4x4BJ4cxp0jFLFa1k3FLxLZlTKWc73hwYX9VjfUpqfDrJNRNSgtMS9tXq8Xx+fnZPT0+9V9aAn52d/VflHNwU+qXiq8kpMvG1mubA+ARN9Q3eLA5XvU2K/+z/PrFjFaYEp43lvUY6+WlDOYxh6tgS8dXmVAtE6bzmwNgE2c+egnJN7ViFGQYe0UXg9BKf9jHp2GNVJFd90hhbxVebU2nia483B6YUmK/4YQ9TA8xYpUrH8lvQ8Hq5SuJ/K8Vm40eAqYmv5pyS59scXxWYUv+grOBoQzsGzLDKWC/jvUtkn6gETE189iTnMUw16WM5bQPG2LmrAZN2/3Os4NKEpb2QT8KwF0r3UuyWZbvE0Ufw0vVLx3PxGSRjsSqLaU5wVgEmLf8Gi62kqU27SMmvWcHpY7ybmm4y2t+im3wlIGri+/UVZviKwFavlfsSLBFgTDPH/X64qxrd4CsBUxvfHDntZIVJ+xVLoPTUUdP0zvFEMax+uSqUm4AIMDXx1ZwzJyDDsRa5JaVmRm9BNcBsu2eR24yLgh0Bpia+mnN2Hpj0HU70FlQDjLIrmgPB4zSo7Xbp768irxEiwNTEV3POTgOTPvJFmtuxZCMTss17l/SWaY2u9VW5VwJLx7dNTi3AaX5L8kfVaGnfZkLs3No3u7k32VOvBWoqYG18tTntJDA+EbblXnoaGnt9EH1KMl3NtyNjn2CkVa20HxOpgHPEt+Q3PjngmleY4b7GFPVTvUJ0Qmz8qa/TcpXOY8xBkW7mTe3JtIyvJqcW1cXGbA6M7yNEEpgLGF/JkW96I0BMAeV5KcAo8aW+/ZpveiOwoNkNB5pXmN2wgSijDgBM1Cl0vQMAAwiSAwAj2YUYYGBAcgBgJLsQAwwMSA4AjGQXYoCBAckBgJHsQgwwMCA5ADCSXYgBBgYkBwBGsgsxwMCA5ADASHYhBhgYkBwAGMkuxAADA5IDACPZhRhgYEByAGAkuxADDAxIDgCMZBdigIEByQGAkexCDDAwIDkAMJJdiAEGBiQHAEayCzHAwIDkAMBIdiEGGBiQHAAYyS7EAAMDkgMAI9mFGGBgQHLgL8JPbm+p0XbRAAAAAElFTkSuQmCC',
        #     'filename': 'AAAAAA&text=210x100.png'}
        # image_service.upload_base64(image_base64_json=set_json,
        #                             member=admin_account_obj.account_id,
        #                             **image_dic_list)
    
def table_doc_comment(table_module, engine):
    for table in table_module.metadata.tables:
        print("COMMENT ON TABLE \"{0}\"  IS ' 資料表';".format(table))
        for column in table_module.metadata.tables[table]._columns._data:
            doc = table_module.metadata.tables[table]._columns._data[column].doc
            command = "COMMENT ON COLUMN \"{0}\".{1} IS \'{2}\';".format(table, column, doc)
            print(command)
