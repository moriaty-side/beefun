from __future__ import unicode_literals

from . import web_views as views
from ...base.context import AdminContext
from ...lib.api import return_format
from ...lib.page import return_page_format


def includeme(config):
    # Page
    config.add_route('page.web.list', '/page/web/list', factory=AdminContext)
    config.add_view(
        views.WebPageView, attr='list',
        route_name='page.web.list',
        renderer='templates/web_list.mako',
        decorator=return_page_format,

        permission='login',

    )
    config.add_route('page.web.create', '/page/web/create', factory=AdminContext)
    config.add_view(
        views.WebPageView, attr='create',
        route_name='page.web.create',
        renderer='templates/web_create.mako',
        decorator=return_page_format,

        permission='login',

    )
    config.add_route('page.web.update', '/page/web/update/{web_id}', factory=AdminContext)
    config.add_view(
        views.WebPageView, attr='update',
        route_name='page.web.update',
        renderer='templates/web_edit.mako',
        decorator=return_page_format,

        permission='login',

    )

    # API
    # news create
    config.add_route('api.web', '/api/web', factory=AdminContext)
    config.add_view(
        views.WebJsonView, attr='create',
        route_name='api.web',
        request_method='POST',
        decorator=return_format,
        renderer='json',

        permission='login',

    )
    config.add_view(
        views.WebJsonView, attr='search',
        route_name='api.web',
        request_method='GET',
        decorator=return_format,
        renderer='json',

        permission='login',

    )

    config.add_route('api.web.detail', '/api/web/{web_id}', factory=AdminContext)
    config.add_view(
        views.WebJsonView, attr='get',
        route_name='api.web.detail',
        request_method='GET',
        decorator=return_format,
        renderer='json',

        permission='login',

    )

    config.add_view(
        views.WebJsonView, attr='edit',
        route_name='api.web.detail',
        request_method='POST',
        decorator=return_format,
        renderer='json',

        permission='login',

    )

    config.add_view(
        views.WebJsonView, attr='delete',
        route_name='api.web.detail',
        request_method='DELETE',
        decorator=return_format,
        renderer='json',

        permission='login',

    )
