# coding=utf8
from __future__ import unicode_literals
import transaction, json
from . import web_form as form
from ...base.base_view import BaseView
from .web_service import WebService

class WebPageView(BaseView):
    def __init__(self, request):
        super(WebPageView, self).__init__(request)
        self.web_service = WebService(self.session, logger=self.logger)

    def list(self):
        _ = self.localizer
        return {}

    def create(self):
        _ = self.localizer


        return {}

    def update(self):
        _ = self.localizer
        web_obj = self.web_service.get_by_id(web_id=self.request.matchdict['web_id'],
                                               check=True)

        return {'web_obj': web_obj.__json__(show_image=True, detail=True)}


class WebJsonView(BaseView):
    def __init__(self, request):
        super(WebJsonView, self).__init__(request)
        self.web_service = WebService(self.session, logger=self.logger)

    def create(self):
        """
        創建標籤
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.WebCreateForm().to_python(self.request_params)

        with transaction.manager:
            web_obj = self.web_service.create(**request_data)

        return {'response': {'web': web_obj.__json__()},
                'message': _(u'News創建成功')}

    def search(self):
        """
        搜尋標籤
        """

        # 檢查輸入參數
        request_data = form.WebSearchForm().to_python(self.request_params)

        # 分頁機制
        current_page = request_data.get('page', 1)
        limit = request_data.get('limit', 10)
        request_data['offset'] = (current_page - 1) * limit
        request_data['limit'] = limit
        request_data['order_by'] = [('created', -1)]

        # 搜尋帳號
        web_list, total_count = self.web_service.get_list(show_count=True, **request_data)

        return {'response': {'web_list': [a.__json__() for a in web_list],
                             'total_count': total_count,
                             'total_page': (int(total_count / limit) + 1 if total_count % limit else 0)
                             },
                'message': u'News搜尋成功'}

    def get(self):
        """
        讀取標籤
        """

        # 用 category_id 取得分類
        web_obj = self.web_service.get_by_id(web_id=self.request.matchdict['web_id'],
                                               check=True)

        return {'response': {'web': web_obj.__json__()},
                'message': 'web讀取成功'}

    def edit(self):
        """
        編輯標籤
        """

        # 檢查輸入參數
        request_data = form.WebEditForm().to_python(self.request_params)

        # 用 category_id 取得分類
        web_obj = self.web_service.get_by_id(web_id=self.request.matchdict['web_id'],
                                               check=True)

        # 更新帳號
        with transaction.manager:
            web_obj = self.web_service.update(old_obj=web_obj,
                                                user_id=self.account_id,
                                                **request_data)

        return {'response': {'web': web_obj.__json__()},
                'message': u'Web修改成功'}

    def delete(self):
        """
        刪除標籤
        """

        # 用 tag_id 取得分類
        web_obj = self.web_service.get_by_id(web_id=self.request.matchdict['web_id'],
                                               check=True)
        # 更新 status 達到軟刪除
        with transaction.manager:
            self.web_service.delete(old_obj=web_obj)

        return {'message': u'Web刪除成功'}

