# coding=utf8
import uuid
from sqlalchemy import (Column, Integer, String, DateTime, Text, ForeignKey)
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from ...base.meta_module import (Base, TimestampTable, GUID, uuid4, now_func)

DeclarativeBase = declarative_base()


class Web(Base, TimestampTable):
    """
    文章資料表單
    """
    __tablename__ = 't_web'
    web_id = Column('f_web_id', GUID, primary_key=True, default=uuid4, doc=u"文章ID")

    web_name = Column('f_web_name', String, nullable=True, doc=u'站點名稱')

    web_url = Column('f_web_url', String, nullable=True, doc=u'標題')

    web_ip = Column('f_web_ip', String, nullable=True, doc=u'標題')

    _status = Column('f_status', Integer, nullable=False, default=10,
                     info={"show": 10, "hiden": 20, "delete": 40},
                     doc=u"狀態:10.上架,20.下架,40:封存")

    def __repr__(self):
        return '<WebObject (web_id={0})>'.format(self.web_id)

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    def __json__(self):
        d = {'web_id': str(self.web_id),

             'web_name': str(self.web_name),

             'web_url': str(self.web_url),

             'web_ip': str(self.web_ip),

             }

        return d

