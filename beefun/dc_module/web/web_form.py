# coding=utf8
from __future__ import unicode_literals

import formencode, uuid
from formencode import validators


class WebCreateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    web_name = validators.String(not_empty=True)
    web_url = validators.String(not_empty=True)
    web_ip = validators.String(not_empty=True)


class WebSearchForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True

    page = validators.Int(not_empty=True, min=1)  # 第幾頁
    limit = validators.Int(not_empty=True, min=1)  # 一頁幾筆


class WebEditForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    web_name = validators.String(not_empty=True)
    web_url = validators.String(not_empty=True)
    web_ip = validators.String(not_empty=True)

