# coding=utf8
from __future__ import unicode_literals

from sqlalchemy import (
    Column,
    Integer,
    String,
    ARRAY,
    ForeignKey,
    DateTime
)
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
from ...base.meta_module import (Base, TimestampTable, GUID, uuid4)


class Stock(Base, TimestampTable):
    """
    股票
    """
    __tablename__ = 't_stock'
    stock_id = Column('f_stock_id', GUID, primary_key=True, default=uuid4, doc=u"股票編號")

    stock_name = Column('f_stock_name', String(100), nullable=False, doc=u"股票名稱")

    stock_code = Column('f_stock_code', String(256), doc=u"股票代碼")

    remark = Column('f_remark', String(512), doc=u'備註')

    _type = Column('f_type', Integer, nullable=False, default=10,
                     info={"USA": 10, "TW": 11},
                     doc=u"狀態:10.美國,11.台灣")

    _sec_type = Column('f_sec_type', Integer, nullable=False, default=10,
                     info={"etf": 10, "individual": 11},
                     doc=u"狀態:10.指數型基金,11.個股")

    _status = Column('f_status', Integer, nullable=False, default=10,
                     info={"show": 10, "hiden": 11, "delete": 12},
                     doc=u"狀態:10.顯示,11.隱藏,12:封存")

    def __repr__(self):
        return '<StockObject (stock_id={0})>'.format(self.stock_id)

    @hybrid_property
    def sec_type(self):
        if hasattr(self.__class__, '_sec_type'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._sec_type.info
            for k in info_dic.keys():
                if info_dic[k] == self._sec_type:
                    return k

    @sec_type.setter
    def sec_type(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._sec_type.info.get(value)
            if not v:
                raise Exception('sec_type column input value {} is error'.format(value))
            self._sec_type = v
        else:
            raise Exception('sec_type column input value_type {} is error')

    @sec_type.expression
    def sec_type(cls):
        return cls._sec_type

    @hybrid_property
    def type(self):
        if hasattr(self.__class__, '_type'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._type.info
            for k in info_dic.keys():
                if info_dic[k] == self._type:
                    return k

    @type.setter
    def type(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._type.info.get(value)
            if not v:
                raise Exception('type column input value {} is error'.format(value))
            self._type = v
        else:
            raise Exception('type column input value_type {} is error')

    @type.expression
    def type(cls):
        return cls._type

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @classmethod
    def __getattributes__(cls):
        return [i[1:] if i[:1] == '_' else i for i in cls.__dict__.keys() if
                i[:1] != '_' or i == '_update_user_id' or i == '_create_user_id']

    @classmethod
    def __likeattribute__(cls, key_word):
        map_args = [i for i in cls.__dict__.keys() if key_word in i and i[:1] != '_']
        return map_args[0] if map_args else None

    def __json__(self):
        d = {
            'stock_id': str(self.stock_id),
            'stock_name': self.stock_name,
            'stock_code': self.stock_code,
            'status': self.status,
        }

        return d

