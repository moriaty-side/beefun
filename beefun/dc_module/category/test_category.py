from ...base.base_test import BaseTest, dummy_request
from .category_domain import Category
from .category_views import CategoryJsonView

class CategoryAPITest(BaseTest):

    def setUp(self):
        super(CategoryAPITest, self).setUp()
        self.init_database()

    def create(self, name='user', type=10):
        category_obj = Category(name=name, _type=type)
        self.session.add(category_obj)
        category_obj = self.session.query(Category).filter(Category.name == name).first()
        return category_obj

    def test_create(self):
        """
        測試 創建帳號
        """
        name = 'admin'
        request = dummy_request(self.session)
        request.params = {'name': name}
        category_json_view = CategoryJsonView(request)
        category_resp = category_json_view.create()
        self.assertEqual(category_resp.get('message'), '分類創建成功')

    def test_search(self):
        """
        測試 創建帳號
        """
        request = dummy_request(self.session)
        category_json_view = CategoryJsonView(request)
        category_resp_list = category_json_view.search()
        self.assertEqual(category_resp_list.get('message'), '分類搜尋成功')
        self.assertEqual(category_resp_list.get('response', {}).get('total_count'), 0)
        self.assertEqual(category_resp_list.get('response', {}).get('total_page'), 0)
        self.assertEqual(len(category_resp_list.get('response', {}).get('account_list', [])), 0)

    def test_get(self):
        """
        測試 讀取帳號
        """
        category = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'category_id': str(category.category_id)}
        category_json_view = CategoryJsonView(request)
        category_resp = category_json_view.get()
        self.assertEqual(category_resp.get('message'), '分類讀取成功')

    def test_update(self):
        """
        測試 更新帳號
        """
        category = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'category_id': str(category.category_id)}
        name = '123'
        request.params = {'name': name}
        category_json_view = CategoryJsonView(request)
        category_resp = category_json_view.edit()
        self.assertEqual(category_resp.get('message'), '分類修改成功')
        self.assertEqual(category_resp.get('response', {}).get('category', {}).get('name'), name)

    def test_delete(self):
        """
        測試 更新帳號
        """
        category = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'category_id': str(category.category_id)}
        category_json_view = CategoryJsonView(request)
        category_resp = category_json_view.delete()
        self.assertEqual(category_resp.get('message'), '刪除分類成功')


class CategoryAPILevelTest(BaseTest):

    def setUp(self):
        super(CategoryAPILevelTest, self).setUp()
        self.init_database()

    def create(self, name='user', type=10):
        category_obj = Category(name=name, _type=type)
        self.session.add(category_obj)
        category_obj = self.session.query(Category).filter(Category.name == name).first()
        return category_obj

    def test_get_level1_info(self):
        """
        測試 更新帳號
        """
        category = self.create()
        request = dummy_request(self.session)
        category_json_view = CategoryJsonView(request)
        category_resp = category_json_view.get_level1_info()
        self.assertEqual(category_resp.get('message'), '分類層級查詢成功')

    def test_set_level1_info(self):
        """
        測試 更新帳號
        """
        category = self.create()
        request = dummy_request(self.session)
        category_json_view = CategoryJsonView(request)
        name = '123'
        request.params = {'name': name}
        category_resp = category_json_view.set_level1_info()
        self.assertEqual(category_resp.get('message'), '分類層級更新成功')
