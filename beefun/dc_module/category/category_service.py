# coding=utf-8
from __future__ import unicode_literals

import uuid
from .category_domain import Category
from sqlalchemy import func
from ...base.base_service import EMPTY_DIC, NOT_SET, BaseService
from ...lib.my_exception import MyException
from sqlalchemy import or_

class CategoryService(BaseService):

    TABLE = Category

    def __init__(self, session, logger=None):
        super(CategoryService, self).__init__(session,logger)
        self.table_args = self.TABLE.__getattributes__()

    def cud_single(self, choose, level=1, category_obj=None, category_id=None, **data):
        """
        :功能:創建 更新 刪除
        :param choose: 選擇 true false
        :param level:  level 層級
        :param category_obj:分類
        :param category_id: 分類id
        :param data:
        :return:
        """

        if not category_obj:
            if category_id is not None:
                category_obj = self.get_by_id(category_id=category_id)

        # 有 category_obj 和 choose = True 表示更新
        if category_obj and choose:
            return self.update(old_obj=category_obj, level=level, **data)
        # 有 category_obj 和 choose = false 表示刪除
        elif category_obj and not choose:
            #self.delete(category_obj)
            self.delete_by_id(category_obj.category_id)
        # 沒有 category_obj 和 choose = True 表示創建
        elif not category_obj and choose:
            return self.create(level=level, **data)

    def create(self, **data):
        # 不可用參數
        un_available_args = []
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        create_data = {key: data[key] for key in available_args if key in data}

        # 資料創建
        try:
            if create_data.get('category_id'):
                create_data['category_id'] = uuid.UUID(create_data.get('category_id'))
            else:
                create_data['category_id'] = uuid.uuid4()

            if create_data.get('parent_id') and not isinstance(create_data.get('parent_id'), uuid.UUID):
                create_data['parent_id'] = uuid.UUID(create_data['parent_id'])

            if create_data.get('parent_ids'):
                new_parent_ids = []
                for parent_id in create_data['parent_ids']:
                    if not isinstance(parent_id, uuid.UUID):
                        parent_id = uuid.UUID(parent_id)
                    new_parent_ids.append(parent_id)
                create_data['parent_ids'] = new_parent_ids

            return self._create(**create_data)
        except Exception as e:
            raise MyException(code=6001, message=e)

    def update(self, old_obj, **data):
        # 不可用參數
        un_available_args = ['category_id']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        # 特殊資料格式
        if update_data.get('parent_id') and not isinstance(update_data.get('parent_id'), uuid.UUID):
            update_data['parent_id'] = uuid.UUID(update_data.get('parent_id'))

        if update_data.get('parent_ids'):
            new_parent_ids = []
            for parent_id in update_data.get('parent_ids'):
                if not isinstance(parent_id, uuid.UUID):
                    parent_id = uuid.UUID(parent_id)
                new_parent_ids.append(parent_id)
            update_data['parent_ids'] = new_parent_ids

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=6002, message=e)

    def update_by_id(self, category_id, **data):
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['category_id']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 抓取對應物件
        old_obj = self.get_by_id(category_id)
        # 選填關聯參數
        old_obj, data = self._relationship_obj_ud(old_obj, data)
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=6002, message=e)

    def get_by(self, **data):
        try:
            return self._get_by(**data)
        except Exception as e:
            raise MyException(code=6004, message=e)

    def get_by_id(self, category_id, check=False):
        id = category_id if isinstance(category_id, uuid.UUID) else uuid.UUID(category_id)
        obj = self._get_by(category_id=id)
        if check and not obj:
            raise MyException(code=6005)
        return obj
    
    def get_by_position_max(self, type):
    
        """
        抓取物件依據 article_id
        :param article_id:
        :return: article_obj
        """
        type_value = self.TABLE._type.info.get(type, None)
        if type_value is None:
            raise '{0} obj attribute is error '.format(self.TABLE.__name__)
        query = self.session.query(func.max(self.TABLE.position))
        
        max_date = query.filter(self.TABLE.type==type_value).scalar()
        
        return max_date
    
    def get_by_position_minus_one(self, number_set):
    

        """
        抓取物件依據 article_id
        :param article_id:
        :return: article_obj
        """
        
        self.session.query(self.TABLE).filter(self.TABLE.position > number_set)\
                    .update({self.TABLE.position: self.TABLE.position - 1})
        
        return None

    def delete(self, old_obj):
        try:
            return self._delete(old_obj)
        except Exception as e:
            raise MyException(code=6006, message=e)

    def delete_by_id(self, category_id):
        obj = self.get_by_id(category_id=category_id, check=True)
        return self.delete(obj)

    def get_list(self, status=None, index_status=None, type=None, order_by=EMPTY_DIC, group_by=EMPTY_DIC, name=None, language=None,
                 offset=0, limit=None, show_count=False, category_ids=NOT_SET, parent_id=None, types=None,
                 get_children_by_category_id=None, **search_condition):

        # 可不用參數
        un_available_args = ['']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        filter_data = {key: search_condition[key]
                       for key in available_args if key in search_condition}

        # 查詢條件製成
        query = self.session.query(self.TABLE)

        try:
            if name is not None:
                query = query.filter(self.TABLE.name.ilike('%{}%'.format(name)))

            if language is not None:
                query = query.filter(self.TABLE.language==language)
            # status
            if status is not None:
                status_value = self.TABLE._status.info.get(status, None)
                if status_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._status == status_value)


            # index_status
            if index_status is not None:
                index_status_value = self.TABLE._index_status.info.get(index_status, None)
                if index_status_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._index_status == index_status_value)

            # type
            if type is not None:
                type_value = self.TABLE._type.info.get(type, None)
                if type_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._type == type_value)

            if types is not None:
                type_list = []
                for type in types:
                    value = self.TABLE._type.info.get(type, None)
                    if value is None:
                        raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                    type_list.append(value)
                query = query.filter(self.TABLE._type.in_(type_list))

            # 當get_children不為空的時候表示,抓取子子孫孫層級
            if get_children_by_category_id is not None:
                query = query.filter(self.TABLE.parent_ids.any(self._type_to_uuid(get_children_by_category_id)))

            if parent_id is not None:
                query = query.filter(self.TABLE.parent_id == self._type_to_uuid(parent_id))

            # 特殊條件
            if category_ids is not NOT_SET:
                u_category_ids = [self._type_to_uuid(category_id) for category_id in category_ids]
                query = query.filter(self.TABLE.parent_ids.in_(u_category_ids))
            return self._get_list(query, order_by, group_by,
                                  offset, limit, show_count, **filter_data)

        except Exception as e:
            raise MyException(code=6003, message=e)

    def get_relation(self, category_id=None, category_obj=None, return_self_category_obj=True):

        if category_obj is None:
            category_obj = self.get_by_id(category_id)

        u_category_ids = [self._type_to_uuid(category_id) for category_id in category_obj.parent_ids]
        u_category_ids.append(category_obj.category_id)

        query = self.session.query(self.TABLE)
        query = query.filter(self.TABLE.category_id.in_(u_category_ids))

        if return_self_category_obj:
            return category_obj, self._get_list(query, order_by=[('level', -1)])

        return self._get_list(query, order_by=[('level', -1)])

    def get_category_child_relation(self, category_ids, level=3, get_all_relation=False):
        """
        取得傳數 Category ids 的 子層級關係,可以用level 取得欲取得的層級,get_all_relation 取得完整關聯關係
        :param category_ids:
        :param level:
        :param get_relation:
        :return:
        """
        u_category_ids = [self._type_to_uuid(category_id) for category_id in category_ids]

        # ==== 取得所有 傳入分類的指定層級 的子分類 id start ===
        l3_query = self.session.query(self.TABLE.category_id.label('category_id'),self.TABLE.parent_ids.label('parent_ids'))
        # l3_query = self.session.query(self.TABLE)
        # 抓出該類別的全部子類類別
        cond1 = or_(self.TABLE.parent_ids.any(self._type_to_uuid(u_category_id)) for u_category_id in u_category_ids)
        # 抓出於該類別相同編號的資料
        cond2 = or_(self.TABLE.category_id == self._type_to_uuid(u_category_id) for u_category_id in u_category_ids)
        l3_query = l3_query.filter(cond1 | cond2).filter(self.TABLE.level == level)
        # ==== 取得所有 傳入分類的指定層級 的子分類 id end ===
        #l3_query = l3_query.subquery()

        dbcategory_list =l3_query.all()
        c_ids = []
        for dbcategory in dbcategory_list:
            c_ids.append(dbcategory.category_id)
            c_ids.extend(dbcategory.parent_ids)
        #parent_ids
        # #
        # #query = self.session.query(self.TABLE).join(l3_query,l3_query.c.category_id == self.TABLE.category_id | l3_query.c.parent_ids.any(self.TABLE.category_id))
        #
        query = self.session.query(self.TABLE)
        query = query.filter(self.TABLE.category_id.in_(c_ids))

        if get_all_relation:
            return dbcategory_list, self._get_list(query, order_by=[('level', -1)])

        return dbcategory_list

    def soft_delete_by_id(self, parents_gid):
        query = self.session.query(self.TABLE)
        id = parents_gid if isinstance(parents_gid, uuid.UUID) else uuid.UUID(parents_gid)
        query.filter(or_(self.TABLE.parent_ids.any(id), self.TABLE.category_id == id))\
            .update({'status': 400}, synchronize_session='fetch')
        self.session.flush()
        return {}

    def _create_for_init(self, **data):
        # 不可用參數
        un_available_args = []
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        create_data = {key: data[key] for key in available_args if key in data}

        create_data['category_id'] = self._type_to_uuid(create_data['category_id'])

        if create_data['parent_id'] and not isinstance(create_data['parent_id'], uuid.UUID):
            create_data['parent_id'] = uuid.UUID(create_data['parent_id'])

        if create_data['parent_ids']:
            new_parent_ids = []
            for parent_id in create_data['parent_ids']:
                if not isinstance(parent_id, uuid.UUID):
                    parent_id = uuid.UUID(parent_id)
                new_parent_ids.append(parent_id)
            create_data['parent_ids'] = new_parent_ids

        # 資料創建
        return self._create(**create_data)