# coding=utf8
from __future__ import unicode_literals

import json
import transaction

from . import category_form as form
from ...base.base_view import BaseView
from ...lib.my_exception import MyException
from .category_service import CategoryService

class CategoryPageView(BaseView):
    def __init__(self, request):
        super(CategoryPageView, self).__init__(request)
        self.category_service = CategoryService(self.session)

    def single_manage(self):
        _ = self.localizer

        return {"category_type": self.request.matchdict['type']}

    def news_category(self):
        _ = self.localizer

        return {"category_type": 'news'}


    def product_category(self):
        _ = self.localizer

        return {"category_type": 'product'}

    def works_category(self):
        _ = self.localizer

        return {"category_type": 'work'}

    def multi_manage(self):
        """頁面首頁"""
        # 多國語系
        _ = self.localizer

        category_type = self.request_params.get('type', 'article')
        if not category_type:
            raise Exception(_(u'Type參數錯誤'))

        request_param = {'type': category_type}
        # name
        if self.request_params.get('name'):
            request_param['name'] = self.request_params.get('name')

        # status
        if self.request_params.get('status'):
            request_param['status'] = self.request_params.get('status')

        category_obj_list = self.category_service.get_list(**request_param)

        # 分類階層
        category_level_dic_obj = {}
        category_level_2_obj_list = []
        # 抓出第一層
        for category in category_obj_list:
            if category.level == 1:
                category_level_dic_obj[str(category.category_id)] = category.__json__()
            else:
                category_level_2_obj_list.append(category)
        # 組裝第二層
        for category in category_level_2_obj_list:
            if category_level_dic_obj.get(str(category.parent_id), {}).get('children'):
                category_level_dic_obj[str(category.parent_id)]['children'].append(category.__json__())
            else:
                if category_level_dic_obj.get(str(category.parent_id)):
                    category_level_dic_obj[str(category.parent_id)]['children'] = []
                    category_level_dic_obj[str(category.parent_id)]['children'].append(category.__json__())

        category_level_list = []
        for key, value in category_level_dic_obj.items():
            category_level_list.append(value)

        return { 'category_type': category_type,
                 'category_list': [c.__json__() for c in category_obj_list],
                 'category_level_list': category_level_list,
                 'params': self.request_params}

class CategoryJsonView(BaseView):
    def __init__(self, request):
        super(CategoryJsonView, self).__init__(request)
        self.category_service = CategoryService(self.session)

    def create(self):
        """
        創建分類
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.CategoryCreateForm().to_python(self.request_params)
        
        if self.category_service.get_by_position_max(type=request_data.get('type')) == None:
        
            request_data['position'] = 1
        else:
            
            request_data['position'] = self.category_service.get_by_position_max(type=request_data.get('type')) + 1
            
        with transaction.manager:
            category_obj = self.category_service.create(**request_data)

        return {'response': {'category': category_obj.__json__()},
                'message': _(u'分類創建成功')}

    def search(self):
        """
        搜尋分類
        """

        # 檢查輸入參數
        request_data = form.CategorySearchForm().to_python(self.request_params)

        # 分頁機制
        current_page = request_data.get('page', 1)
        limit = request_data.get('limit', 10)
        request_data['offset'] = (current_page - 1) * limit
        request_data['limit'] = limit
        request_data['order_by'] = [('created', -1)]

        # 搜尋帳號
        category_list, total_count = self.category_service.get_list(show_count=True, **request_data)

        return {'response': {'category_list': [a.__json__(show_image=True) for a in category_list],
                             'total_count': total_count,
                             'total_page': (int(total_count / limit) + 1 if total_count % limit else 0)
                             },
                'message': u'分類搜尋成功'}

    def get(self):
        """
        讀取分類
        """

        # 用 category_id 取得分類
        category_obj = self.category_service.get_by_id(category_id=self.request.matchdict['category_id'],
                                                       check=True)

        return {'response': {'category': category_obj.__json__()},
                'message': '分類讀取成功'}

    def edit(self):
        """
        編輯分類
        """

        # 檢查輸入參數
        request_data = form.CategoryEditForm().to_python(self.request_params)

        # 用 category_id 取得分類
        category_obj = self.category_service.get_by_id(category_id=self.request.matchdict['category_id'],
                                                       check=True)



        # 更新帳號
        with transaction.manager:

            category_obj = self.category_service.update(old_obj=category_obj,
                                                        user_id=self.account_id,
                                                        **request_data)

        return {'response': {'category': category_obj.__json__()},
                'message': u'分類修改成功'}

    def delete(self):
        """
        刪除分類
        """

        # 用 category_id 取得分類
        category_obj = self.category_service.get_by_id(category_id=self.request.matchdict['category_id'],
                                                       check=True)
        # 更新 status 達到軟刪除
        with transaction.manager:
            self.category_service.update(old_obj=category_obj, status='delete')

        return {'message': u'刪除分類成功'}

    def get_level1_info(self):
        """
        用於抓取各個網站各種分類的後台層級一的對應權限分類
        """
        _ = self.localizer

        form_data = form.GetCategoryFormValid().to_python(self.request_params)

        search_args = {}

        if form_data.get('level'):
            search_args['level'] = form_data.get('level')
        if form_data.get('category_id'):
            search_args['get_children_by_category_id'] = form_data.get('category_id')
        if form_data.get('type'):
            search_args['type'] = form_data.get('type')

        db_category_list = self.category_service.get_list(**search_args)

        response_category_list = []

        for db_category in db_category_list:
            response_category_list.append(db_category.__json__())

        return {
            'response': {'category_list': response_category_list},
            'message': _(u'分類層級查詢成功')
        }

    def set_level1_info(self):
        """
        用於新增、修改、刪除各個網站各種分類的後台層級一的對應權限分類
        """
        _ = self.localizer

        # get request to json_data
        request_json_list = json.loads(self.request_params.get('json_data', '{}').replace('\r\n', '\\r\\n'))

        response_category_list = []

        # 經過表格過濾轉換器獲得對應的資料
        for request_json in request_json_list:
            # from_data = form.SetCategoryJsonFormValid().to_python(request_json)

            # 依據category_id 來創建或更新cateogry
            if not request_json.get('category_id'):
                with transaction.manager:
                    category_obj = self.category_service.create(**request_json)
            else:

                category_obj = self.category_service.get_by_id(category_id=request_json.get('category_id'))
                if not category_obj:
                    with transaction.manager:
                        category_obj = self.category_service.create(**request_json)
                else:
                    with transaction.manager:
                        category_obj = self.category_service.update(category_obj, **request_json)

            response_category_list.append(category_obj.__json__())

        return {'response': {'category_list': response_category_list},
                'message': _(u'分類層級更新成功')}

    def multi_set(self):
        """
        一次處理多筆
        :return:
        """
        _ = self.localizer

        # 檢查輸入參數
        request_json_list = json.loads(self.request_params.get('json_data', '{}').replace('\r\n', '\\r\\n'))

        response = []
        with transaction.manager:
            for category_data in request_json_list:
                category_obj = self.category_service.update_by_id(**category_data)
                response.append(category_obj.__json__())

        return {'response': {'category_list': response},
                'message': _(u'banner 創建更新成功')}

