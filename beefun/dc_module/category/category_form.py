# coding=utf8
from __future__ import unicode_literals

import formencode, uuid
from formencode import validators, ForEach


class CategoryCreateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True
    
    name = validators.String(not_empty=True)
    parent_id = validators.Int(not_empty=True)  # 父類別編號
    status = validators.String(not_empty=True)
    index_status = validators.String(not_empty=True)
    intro = validators.String(not_empty=False)
    description = validators.String(not_empty=False)
    type = validators.String(not_empty=True)
    link = validators.String(not_empty=True)
    position = validators.Int(not_empty=True)
    image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    level = validators.Int(not_empty=True)

class CategorySearchForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True
    
    name = validators.String(not_empty=True, strip=True)
    status = validators.String(not_empty=True, strip=True)
    type = validators.String(not_empty=True, strip=True)

    page = validators.Int(not_empty=True, min=1)  # 第幾頁
    limit = validators.Int(not_empty=True, min=1)  # 一頁幾筆


class CategoryEditForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True
    
    category_id = validators.Int(not_empty=True)  # 分類id
    parent_id = validators.Int(not_empty=True)  # 父類別編號
    parent_ids = ForEach(validators.Int(not_empty=True))   # 家族編號
    status = validators.String(not_empty=True)
    index_status = validators.String(not_empty=True)
    intro = validators.String(not_empty=False)
    image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    description = validators.String(not_empty=False)
    type = validators.String(not_empty=True)
    img1_name = validators.String(not_empty=True)
    img2_name = validators.String(not_empty=True)
    link = validators.String(not_empty=True)
    position = validators.Int(not_empty=True)
    level = validators.Int(not_empty=True)
    name = validators.String(not_empty=True)


class GetCategoryFormValid(formencode.Schema):
    """分類層級取得格式"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True
    
    type = validators.String(not_empty=True)  # 分類類型
    level = validators.Int(not_empty=True)  # 分類類型
    category_id = validators.String(not_empty=True)  # 分類id