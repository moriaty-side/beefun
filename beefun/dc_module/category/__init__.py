from __future__ import unicode_literals

from . import category_views as views
from ...base.context import AdminContext
from ...lib.api import return_format
from ...lib.page import return_page_format


def includeme(config):
    #Page
    config.add_route('page.category.single', '/page/category/single_manage/{type}', factory=AdminContext)
    config.add_view(
        views.CategoryPageView, attr='single_manage',
        route_name='page.category.single',
        renderer='templates/single_category.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )

    config.add_route('page.category.news', '/page/category/news', factory=AdminContext)
    config.add_view(
        views.CategoryPageView, attr='news_category',
        route_name='page.category.news',
        renderer='templates/news_category.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )

    config.add_route('page.category.product', '/page/category/product', factory=AdminContext)
    config.add_view(
        views.CategoryPageView, attr='product_category',
        route_name='page.category.product',
        renderer='templates/product_category.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )

    config.add_route('page.category.works', '/page/category/works', factory=AdminContext)
    config.add_view(
        views.CategoryPageView, attr='works_category',
        route_name='page.category.works',
        renderer='templates/work_category.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )

    # API
    # category create

    # 多筆儲存
    config.add_route('api.category.multi.set', '/api/category/multi_set', factory=AdminContext)
    config.add_view(
        views.CategoryJsonView, attr='multi_set',
        route_name='api.category.multi.set',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )


    config.add_route('api.category', '/api/category', factory=AdminContext)
    config.add_view(
        views.CategoryJsonView, attr='create',
        route_name='api.category',
        renderer='json',
        request_method='POST',
        decorator=return_format,
        
        permission='login',
        
    )
    config.add_view(
        views.CategoryJsonView, attr='search',
        route_name='api.category',
        request_method='GET',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_route('api.category.detail', '/api/category/{category_id}', factory=AdminContext)
    config.add_view(
        views.CategoryJsonView, attr='get',
        route_name='api.category.detail',
        request_method='GET',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_view(
        views.CategoryJsonView, attr='edit',
        route_name='api.category.detail',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_view(
        views.CategoryJsonView, attr='delete',
        route_name='api.category.detail',
        request_method='DELETE',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_route('api.category.level_info.get', '/level1_info/get', factory=AdminContext)
    config.add_view(
        views.CategoryJsonView, attr='get_level1_info',
        route_name='api.category.level_info.get',
        renderer='json',
        request_method='GET',
        decorator=return_format,
        
        permission='login',
        
    )

    config.add_route('api.category.level1_info.set', '/level1_info/set', factory=AdminContext)
    config.add_view(
        views.CategoryJsonView, attr='set_level1_info',
        route_name='api.category.level1_info.set',
        renderer='json',
        request_method='POST',
        decorator=return_format,
        
        permission='login',
        
    )
