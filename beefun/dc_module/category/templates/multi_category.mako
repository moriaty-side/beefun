<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

</head>
<body>

    <!--portlet-body-->
    <div class="portlet-body">
    <form class="form-horizontal">
      <!--按鈕列-->
      <div class="portlet">
        <div class="portlet-title">
          <div class="actions btn-set">
            <button type="button" class="btn yellow btn-lg category_search"><i class="fa  fa-search"></i>${ _(u'查詢')}</button>
            <button type="button" class="btn green-seagreen save_btn" ><i class="fa fa-check"></i>${ _(u'確認存檔')}</button>
          </div>
        </div>
      </div>
      <!--按鈕列 end-->
      <div class="row">
          <div class="col-xs-12 col-md-6 col-lg-4">
              <div class="form-group">
                  <label class="control-label col-xs-4">${ _(u'分類名稱')}${ _(u'')}：</label>
                  <div class="col-xs-7">
                      <input type="text" class="form-control" placeholder="" name="search_name" value="${params.get('name', '')}">
                  </div>
              </div>
          </div>
          <div class="col-xs-12 col-md-6 col-lg-4">
              <div class="form-group">
                  <label class="control-label col-xs-4">${ _(u'分類狀態')}${ _(u'')}：</label>
                  <div class="col-xs-7">
                      <select class="form-control select2-multiple category_status_select" data-placeholder="Select..." name="status">
                          <option value=""></option>
                          % for v, s in {'show': _(u'顯示'), 'hiden': _(u'不顯示')}.items():
                              % if params.get('status') == v:
                                  <option value="${v}" selected>${s}</option>
                              % else:
                                  <option value="${v}">${s}</option>
                              % endif
                          % endfor
                        </select>
                  </div>
              </div>
          </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <table class="table combination autotable_tab_5_1">
            <thead class="bg-green-seagreen bg-font-green-seagreen">
              <th class="w60">${ _(u'排序')}</th>
              <th>${ _(u'分類名稱(繁體中文)')}</th>
              <th><!--操作--></th>
            </thead>
            <!--動態內容-->
            <tfoot>
            <tr>
              <td colspan="6"><div> <a href="#" onclick="return addParent(this)" class="addtr btn yellow-casablanca" data-preantTable="autotable_tab_5_1"> <i class="fa fa-plus"></i> 新增大分類</a> </div></td>
            </tr>
            <tr> </tr>
            </tfoot>
            % for category in sorted(category_level_list, key=lambda k: k['position']):
                <tbody class="${ category['category_id'] }">
                  <tr class="parent_tr">
                      <td>
                        <input type="text" class="txt form-control" name="position" value="${ category['position'] }"/>
                      </td>
                      <td>
                        <div>
                          <input type="text" cat_id="${ category['category_id'] }" name="name" placeholder="${ _(u'大分類名稱(繁體中文)')}" class="txt form-control category_name" value="${ category.get('name') }"/>
                        </div>
                      </td>
                      <td>
                       % if category['status'] == 'show':
                           <a class="btn grey-salt category_lock" cat_id="${ category['category_id'] }"><i class="fa fa-download"></i>${ _(u'封存')}</a>
                       % else:
                           <a class="btn red category_open" cat_id="${ category['category_id'] }"><i class="fa fa-download"></i>${ _(u'解封存')}</a>
                       % endif

                      </td>
                  </tr>
                  % for category_child in sorted(category.get('children', []), key=lambda k: k['position']):
                      <tr class="${category_child['category_id']} child_tr" child_id="${category_child['category_id']}">
                        <td class="w60"><input type="text" class="txt form-control" name="position" value="${ category_child.get('position',1) }"/></td>

                        <td>
                          <div class="branch">
                            <input type="text" name="name" cat_id="${ category_child['category_id'] }" placeholder="${ _(u'子分類名稱(繁體中文)')}" class="txt form-control category_name" value="${ category_child.get('name','') }"/>
                          </div>
                        </td>
                        <td>
                          % if category_child['status'] == 'show':
                              <a class="btn grey-salt category_lock" cat_id="${ category_child['category_id'] }"><i class="fa fa-download"></i>${ _(u'封存')}</a>
                          % else:
                              <a class="btn red category_open" cat_id="${ category_child['category_id'] }"><i class="fa fa-download"></i>${ _(u'解封存')}</a>
                          % endif

                        </td>
                      </tr>
                  % endfor
                  <tr>
                      <td colspan="1"></td>
                      <td colspan="4">
                          <div class="coda"><a href="#"  class="addtr addSub btn yellow-casablanca"  data-id=">uuid"><i class="fa fa-plus"></i>${ _(u'新增小分類')}</a></div>
                      </td>
                  </tr>
                </tbody>
            % endfor
          </table>
        </div>
      </div>
    </form>
    </div>
    <!--portlet-body end-->

</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.91/jsrender.min.js" type="text/javascript"></script>
##<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/js/fileinput.min.js" type="text/javascript"></script>


<script src="${ request.static_path('beefun:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
<script src="${ request.static_path('beefun:static/assets/make_table/make_table.js')}" type="text/javascript"></script>
<script src="${ request.static_path('beefun:static/assets/other/readImage.js') }" type="text/javascript"></script>

<!--新增大分類功能-->
<script type="text/xrender" id="tb_parent_row">
    <tbody class=":uuid">
      <tr class="parent_tr">
          <td>
            <input type="text" class="txt form-control" name="position" value="0"/>
          </td>
          <td>
            <div>
              <input type="text" name="name" cat_id=":uuid" placeholder="${ _(u'大分類名稱(繁體中文)')}" class="txt form-control category_name"/>
            </div>
          </td>
      </tr>
      <tr>
          <td colspan="1"></td>
          <td colspan="4">
              <div class="coda"><a href="#"  class="addtr addSub btn yellow-casablanca"  data-id=">uuid"><i class="fa fa-plus"></i> ${ _(u'新增小分類')}</a></div>
          </td>
      </tr>
    </tbody>



</script>
<!--新增子分類-->
<script type="text/x-jsrender" id="tb_sub_row">
  <tr class=":uuid child_tr" child_id=":uuid">
    <td class="w60"><input type="text" class="txt form-control" name="position" value="1"/></td>

    <td>
      <div class="branch">
        <input type="text" name="name" cat_id=":uuid" placeholder="${ _(u'子分類名稱(繁體中文)')}" class="txt form-control category_name"/>
      </div>
    </td>
    <td>
          <a href="#"  class="btn grey-salt remove_Sub"><i class="fa fa-download"></i> ${ _(u'封存')}</a>
    </td>
  </tr>
</script>

<script type="text/JavaScript">
    var current_uuid = -1;
    var addParent_warning = 0;
    function addParent(obj){
      if(addParent_warning) {
        if(!confirm('New Dimensions will reset all setting of this product, are you sure?'))
        {
          return false;
        }
      }
      var tb_parent_row = $('#tb_parent_row').render({'uuid':guid()});
      $(obj).attr("data-preantTable")
      //alert($(obj).attr("data-preantTable"));
      var preant=$(obj).attr("data-preantTable");
      ##  alert(preant);
      $('.'+preant).append(tb_parent_row);
      current_uuid = current_uuid-1;

      addParent_warning=0;
      ##  Metronic.init();
      return false;

    }
    $(function() {
      $(document).on('click','.addSub',function(){
          var elem = $(this).parents('tr');
          var parent_id = $(this).attr('data-id');
          var tb_sub_row = $('#tb_sub_row').render({'uuid':guid(),'parent_id':parent_id});
          elem.before(tb_sub_row);
          current_uuid = current_uuid-1;
          ##  Metronic.init();
          return false;

        });

      $(document).on('click','.remove_Sub',function(){
          var elem = $(this).parents('tr');
          var parents=elem.prop('class').split(' ')[0];
          alert('.'+parents);
          $('.'+parents).remove();
          current_uuid = current_uuid+1;
          return false;

        });
      $(document).on('click','.remove_parent',function(){
          var parents = $(this).parents('tbody')[0];
          parents.remove();
          current_uuid = current_uuid+1;
          return false;

        });
    })
    </script>

<script>
<%!
     import simplejson as json
     def to_json( d ):
        return json.dumps( d )
    %>
    var category_type = "${category_type}";

    var csrfToken = "${request.session.get_csrf_token()}";

    $(document).ready(function (e) {

        $(document).on('click','.save_btn',function(){
            var event_btn = $(this);

            var category_data_list = [];
            //第一層
            $('.parent_tr').each(function(i, v){
                var category_data = {
                      'name': $(v).find('input[name="name"]').val(),
                      'show': '',
                      'type': category_type,
                      'level': 1,
                      "position": $(v).find('input[name="position"]').val(),
                }

                // 檢查是否有category_id
                var class_name = $(v).parent().attr('class');
                category_data['category_id'] = class_name;

                category_data_list.push(category_data);
                ##  console.log(category_data);
            });

            //第二層
            $('.child_tr').each(function(i, v){
                var category_data = {
                      'name': $(v).find('input[name="name"]').val(),
                      'parent_id': $(v).parent().attr('class'),
                      'show': '',
                      'type': category_type,
                      'level': 2,
                      "position": $(v).find('input[name="position"]').val(),
                }
                // 檢查是否有category_id
                var class_name = $(v).attr('child_id');
                category_data['category_id'] = class_name;

                // 多國語系
                category_data_list.push(category_data);
                ##  console.log(category_data);

            });
            category_save(event_btn, category_data_list);
            return false;

        });

        $('.category_lock').on('click', function(){
            var event_btn = $(this);
            var category_id = $(this).attr('cat_id');
            var category_data_list = [{
                'category_id': category_id,
                'status': 'hiden'
            }];
             category_save(event_btn, category_data_list)
        });

        $('.category_open').on('click', function(){
            var event_btn = $(this);
            var category_id = $(this).attr('cat_id');
            var category_data_list = [{
                'category_id': category_id,
                'status': 'show'
            }];
             category_save(event_btn, category_data_list)
        });

        $('.category_search').on('click', function(){
            var url = "${ request.route_url('page.category.multi')}?type="+category_type
            var search_name = $('input[name="search_name"]').val();
            if (search_name.length>0){
                url+='&name='+search_name;
            }
            var search_status = $('.category_status_select').val();
            if (search_status.length>0){
                url+='&status='+search_status;
            }
            location.href = url;
        });


    });

    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
    }

    function category_save(event_btn, category_data_list){
        var url = "${ request.route_url('api.category.level1_info.set')}";
        var category_data_form = new FormData();
        category_data_form.append('json_data', JSON.stringify(category_data_list))

        ajax(url, 'POST', category_data_form, event_btn, function(data){
            if (event_btn) event_btn.attr("disabled", false);
            console.log(data);
            if (data['status']) {
                location.href = "${ request.route_url('page.category.multi')}?type="+category_type;
            } else {
                alert('${_(u"創建失敗")}')
            }
        });

    }

    function ajax(url, method, form_data, btn, callback){
        $.ajax({
            url: url,
            type: method,
            data: form_data,
            ##  dataType: 'JSON',
            contentType: false,
            processData: false,
            headers: {'X-CSRF-Token': csrfToken},
            beforeSend: function () {
                if (btn) btn.attr("disabled", true);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
                if (btn) btn.attr("disabled", false)
             },
            success: function (response) {
                callback(response);
            },
            complete: function () {
                if (btn) btn.attr("disabled", false);
            }
        });
    }

</script>
</html>