<%inherit file="beefun:templates/backend/master.mako"/>

<%block name="css">
    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/allen_assets/cropper/cropper.min.css') }">
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/allen_assets/cropper/cropper_plugin.css') }">

    <!-- 圖片預覽 -->

</%block>

<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span class="m-font-898b96">${ _(u'產品管理')}</span> <i class="fa fa-caret-right"></i>
                        <span>${ _(u'分類')}</span>
                    </h3>
                </div>
                <div>
                    <button type="button" class="btn btn-success m-btn m-btn--outline-2x btn-sm save_btn">
                    <span>
                                        <i class="fa fa-check"></i>
                                        <span>
                                            ${ _(u'序列存檔')}
                                        </span>
                    </span>
                    </button>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="product_category" data-parent="product">
            <!--begin::Portlet-->
            <div class="languages hide"></div>
            <div class="m-portlet">
                <div class="m-portlet__body">
                    <ul class="nav nav-tabs" role="tablist">
                        %for index, language_list in enumerate(request.registry.settings.get('available_langs',[])):
                            %if index == 0:
                                <li class="nav-item">
                                    <a class="nav-link active" id="${ language_list[0] }" data-toggle="tab" href="#m_tabs_1_${index}">${ language_list[1] }</a>
                                </li>
                            %else:
                                <li class="nav-item">
                                    <a class="nav-link" id="${ language_list[0] }" data-toggle="tab" href="#m_tabs_1_${index}">${ language_list[1] }</a>
                                </li>
                            %endif
                        %endfor
                    </ul>
                    <div class="tab-content">
                        %for index, language_list in enumerate(request.registry.settings.get('available_langs',[])):
                            %if index == 0:
                                <div class="tab-pane active" id="m_tabs_1_${index}" role="tabpanel">
                                    <!--load work_list-1.html-->
                                </div>
                            %else:
                                <div class="tab-pane" id="m_tabs_1_${index}" role="tabpanel">
                                    <!--load work_list-3.html-->
                                </div>
                            %endif
                        %endfor
                    </div>
                </div>
                <!--Begin::Section-->
                <!--begin::Form-->
                <button hidden type="button"
                        class="btn btn-warning m-btn m-btn--outline-2x btn-sm mr-5px btn-search">
									<span>
										<i class="fa fa-search"></i>
										<span>
											${ _(u'搜尋')}
										</span>
									</span>
                </button>
                <form class="m-form m-form--fit m-form--label-align-right" id="search_form">
                    <!--Begin::Section-->
                    <div class="m-portlet__body">
                        <div class="row">

                            <div class="col-sm-6 offset-sm-3">


                                <div class=" m-input-group ">
                                    <label class="col-form-label">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'分類名稱')}：
                                    </label>
                                    <select class="form-control se1 selectOne" id="category_type">
                                        <option value="product_light">${ _(u'燈具')}</option>
                                        <option value="product_green">${ _(u'綠建材')}</option>
                                    </select>
                                </div>


                                <div class=" m-form__group">
                                    <a href="#" class="m-portlet__nav-link m-btn--pill">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" class="form-control m-input m-input--solid m-input--pill search" name="name" placeholder="Search...">
                                            <span class="m-input-icon__icon m-input-icon__icon--right">
                                                            <span>
                                                                <i class="la la-search m--font-brand"></i>
                                                            </span>
                                                        </span>
                                        </div>
                                    </a>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'分類名稱')}：
                                    </label>
                                    <div class="col-8">
                                        <input type="text" class="form-control m-input" placeholder="" id="category_name">
                                    </div>

                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 ">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'圖片')}
                                    </label>
                                    <div class="col-8 set_obj_index">
                                        <div class="set_cropper_upload_btn obj_here" data-fk="" data-cut="false"></div>

                                        <div class="help-block">${ _(u'尺寸')}：350*250</div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4">
                                        ${ _(u'簡述')}：
                                    </label>
                                    <div class="col-8">
                                        <input type="text" class="form-control m-input" placeholder="" id="intro">
                                        <div class="help-block">${ _(u'會在首頁產品資訊顯示')}</div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4">
                                        ${ _(u'分類說明')}：
                                    </label>
                                    <div class="col-8">
                                        <textarea class="form-control" rows="5" style="resize:none;" placeholder="" id="description"></textarea>
                                        <div class="help-block">${ _(u'會在產品列表頁顯示')}</div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4">
                                        ${ _(u'首頁顯示')}：
                                    </label>
                                    <div class="col-8">
                                        <label class="m-checkbox">
                                            <input type="checkbox" id="index_status">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group m-form__group">
                                    <button type="button" class="btn btn-brand m-btn m-btn--outline-2x btn-sm H-center product_category_btn">
                                    <span>
                                                        <i class="fa fa-plus"></i>
                                                        <span>
                                                            ${ _(u'新增')}
                                                        </span>
                                    </span>
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group m-form__group cont_a">
                                    <table class="table m-table m-table--head-bg-success">
                                        <thead>
                                        <tr>
                                            <th> ${ _(u'首頁顯示與排序')}</th>
                                            <th> ${ _(u'分類顯示')}</th>
                                            <th> ${ _(u'分類')} </th>

                                            <th> ${ _(u'簡述')}</th>
                                            <th class="w25p"> ${ _(u'說明')}</th>
                                            <th> ${ _('圖片')}</th>
                                            <th>
                                                <!--操作-->&nbsp;</th>
                                        </tr>
                                        </thead>
                                        <tbody id="product_category_tbody">

                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group m-form__group cont_b hide">
                                    <table class="table m-table m-table--head-bg-success">
                                        <thead>
                                        <tr>
                                            <th> ${ _(u'首頁顯示與排序')}</th>
                                            <th> ${ _(u'分類顯示')}</th>
                                            <th> ${ _(u'分類')} </th>
                                            <th> ${ _(u'圖')} </th>
                                            <th> ${ _(u'簡述')} </th>
                                            <th class="w25p"> ${ _(u'說明')}</th>
                                            <th> ${ _('圖片')}</th>
                                            <th>
                                                <!--操作-->&nbsp;</th>
                                        </tr>
                                        </thead>
                                        <tbody id="product_category_tbody">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>
</%block>



<%block name="script">


    <!-- 圖片預覽 -->
    <script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/readimage.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/readimage.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/allen_assets/cropper/cropper2.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/allen_assets/cropper/cropper_plugin2.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/allen_assets/cropper/cropper.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/allen_assets/cropper/cropper_plugin3.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/make_table/make_table.js')}" type="text/javascript"></script>
    <!-- 圖片預覽 -->
    <script>

        var csrfToken = "${request.session.get_csrf_token()}";

        var type_set="product_light";
        var this_obj;
        var language='${ request.registry.settings.get('available_langs', [['zh_Hant', u'繁體中文']])[0][0] }';

        $(document).ready(function (e) {
            // 裁切圖片功能
            var upload_callback = function(return_data, element) {
                var new_image_link = '${ request.static_path('beefun:static/uploads')}'+return_data["url"];
                ##  $('.cropper_upload_btn').find('img').attr('src', new_image_link);
                ##  $('.cropper_upload_btn').find('img').attr('image_id', return_data['image_id']);
                ##  $('.cropper_upload_btn').parent().parent().removeClass('has-error')
                ##
                ##  $('.cropper_upload_btn').find('span.help-block').remove();
                this_obj.closest('.cropper_upload_btn').find('img').attr('src', new_image_link);
                this_obj.closest('.cropper_upload_btn').find('img').attr('image_id', return_data['image_id']);
                this_obj.closest('.cropper_upload_btn').parent().parent().removeClass('has-error')

                this_obj.closest('.cropper_upload_btn').find('span.help-block').remove();
                var src = "${ request.static_path('beefun:static/uploads')}"+return_data["url"];
                this_obj.closest('.obj_index').find('.image_url').attr('src',src);

                ## $('#bounce-window').find('.cropper-oragin.active-cropper img').attr('src', new_image_link).closest('.active-cropper').removeClass('active-cropper');
               $(element).modal('hide');


                this_obj.closest('.obj_index').find('.image_id').val( return_data["image_id"]);

                var url = "${ request.route_url('api.category.detail',category_id='') }"+this_obj.closest(".obj_index").find('.category_id').val();
                var form_data = new FormData();

                form_data.append('image_id', return_data["image_id"]);


                ajax(url, "POST", form_data, null, function (response) {
                    if (response['status']) {

                        alert("${_('儲存成功')}");
                        $('#product_category_tbody').empty();
                        Table.start();
                    } else {
                        alert(response['message']);
                    }
                });


                ## $('#bounce-window #bounce-content').show();
            }
            $('.cropper_upload_btn').cropper_plugin2(options = {
                url: "${request.route_url('api.image')}",
                image_path: 'https://www.placehold.it/330x235/EFEFEF/AAAAAA&text=350:250',
                image_ratio: '210/100',
                csrfToken: csrfToken
            }, callback = upload_callback);

            // 裁切圖片功能
            var set_upload_callback = function(return_data, element) {
                var new_image_link = '${ request.static_path('beefun:static/uploads')}'+return_data["url"];
                ##  $('.cropper_upload_btn').find('img').attr('src', new_image_link);
                ##  $('.cropper_upload_btn').find('img').attr('image_id', return_data['image_id']);
                ##  $('.cropper_upload_btn').parent().parent().removeClass('has-error')
                ##
                ##  $('.cropper_upload_btn').find('span.help-block').remove();
                this_obj.closest('.set_cropper_upload_btn').find('img').attr('src', new_image_link);
                this_obj.closest('.set_cropper_upload_btn').find('img').attr('image_id', return_data['image_id']);
                this_obj.closest('.set_cropper_upload_btn').parent().parent().removeClass('has-error')

                this_obj.closest('.set_cropper_upload_btn').find('span.help-block').remove();
                var src = "${ request.static_path('beefun:static/uploads')}"+return_data["url"];
                this_obj.closest('.obj_index').find('.image_url').attr('src',src);

                ## $('#bounce-window').find('.cropper-oragin.active-cropper img').attr('src', new_image_link).closest('.active-cropper').removeClass('active-cropper');
               $(element).modal('hide');
                this_obj.closest('.obj_index').find('.image_id').val( return_data["image_id"]);
                ## $('#bounce-window #bounce-content').show();
            }
            $('.set_cropper_upload_btn').cropper_plugin(options = {
                url: "${request.route_url('api.image')}",
                image_path: 'https://www.placehold.it/330x235/EFEFEF/AAAAAA&text=350:250',
                image_ratio: '350/250',
                csrfToken: csrfToken
            }, callback = set_upload_callback);

            Table.search_parameter='&language='+$('.nav-link.active').attr('id')+'&type=product_light';


            Table.start();
        });

        $(document).on('click', '.upload', function (e) {

            this_obj = $(this);
        });




        $(document).on('click', '.nav-link', function (e) {

            type_set = $('#category_type').val();
            $('#product_category_tbody').empty();
            Table.search_parameter='&language='+$(this).attr('id')+'&type='+type_set;
            Table.start();
        });




        //新增分類
        $(document).on('click', '.product_category_btn', function() {
            if($('#category_name').val()==""){
                alert('${_(u"請輸入名稱")}')
                return false
            }
            if($('#category_name').val().length>100){
                alert('${_(u"字數大於100")}')
                return false
            }
            if($('#intro').val().length>30){
                alert('${_(u"簡述字數大於30")}')
                return false
            }
            if($('#description').val().length>30){
                alert('${_(u"分類說明字數大於30")}')
                return false
            }
            if( $('#category_name').val().match('[\(\)\<\>\/\?\!\~]')!=null){
                alert('${_(u"請勿輸入特殊字元")}')
                return false
            }
             if( $('#intro').val().match('[\(\)\<\>\/\?\!\~]')!=null){
                alert('${_(u"請勿輸入特殊字元")}')
                return false
            }
             if( $('#description').val().match('[\(\)\<\>\/\?\!\~]')!=null){
                alert('${_(u"請勿輸入特殊字元")}')
                return false
            }
            if ($('.set_obj_index').find('.my_image_data').attr('image_id') == undefined){
                alert('${_(u"請上傳圖片")}');
                return false
            }

            var event_btn = $(this);
            var url = "${ request.route_url('api.category')}";
            var category_data_form = new FormData();
            category_data_form.append('type', $('#category_type').val());
            category_data_form.append('name', $('#category_name').val());
            category_data_form.append('intro', $('#intro').val());
            category_data_form.append('description', $('#description').val());
            category_data_form.append('image_id', $('.set_obj_index').find('.my_image_data').attr('image_id'));

            category_data_form.append('language',$('.nav-link.active').attr('id'));



            if($('#index_status')[0].checked==true){
                category_data_form.append('index_status','show');
            }else{
                category_data_form.append('index_status','hiden');
            }

            ajax(url, 'POST', category_data_form, event_btn, function(data){
                if (event_btn) event_btn.attr("disabled", false);

                if (data['status']) {
                    $('#category_name').val("");
                    $('#intro').val("");
                    $('#description').val("");
                    $('#product_category_tbody').empty();
                    $(".set_image_id").closest('.set_obj_index').find('.fileinput').removeClass('fileinput-exists').addClass('fileinput-new');

                    $('.set_obj_index').find('.my_image_data').attr('src','https://www.placehold.it/330x235/EFEFEF/AAAAAA&text=350:250');
                    $('.set_obj_index').find('.my_image_data').attr('image_id','');
                    $('#index_status')[0].checked=false;
                    Table.start();


                    // 裁切圖片功能


                } else {
                    alert('${_(u"創建失敗")}')
                }
            });
        });

        var Table = new MakeTable({
            'dom_search_form': $('form#search_form'),
            'dom_search_btn': $('i.la.la-search'),
            'dom_table_tbody': $('.produc_category_list'),
            'init_search_parameter': '&type=product_light&language='+language,
            'ajax_search_url':  "${ request.route_url('api.category') }",
            'ajax_csrf_token':csrfToken,
            'table_model': function (obj, page) {
                //開始產生頁面資料
                var $list = obj['category_list'];
                var html_str = '';
                //var data_table_row = [];

                $.each($list, function (i, item) {
                    item.index = (i + 1 + ((page - 1) * 10));
                    var status=""
                    var index_status=""
                    switch (item.status) {
                        case "show":
                            status = "${ _(u'開通')}";
                            break;
                        case "hiden":
                            status = "${ _(u'關閉')}";
                            break;
                        case "delete":
                            status = "${ _(u'刪除')}";
                            break;
                    }
                    switch (item.index_status) {
                        case "show":
                            index_status = "${ _(u'開通')}";
                            break;
                        case "hiden":
                            index_status = "${ _(u'關閉')}";
                            break;
                        case "delete":
                            index_status = "${ _(u'刪除')}";
                            break;
                    }

                    var tb_row = " <tr class='obj_index'>" ;
                    tb_row += "<td>";
                    tb_row += "<div class='table-cell'>";
                    tb_row += "<label class='m-checkbox'>";
                    if(index_status=="${ _(u'開通')}") {
                        tb_row+="<input class='index_check' type='checkbox' checked>"
                    }else{
                        tb_row+="<input class='index_check' type='checkbox'>"
                    }

                    tb_row += "<span></span>";
                    tb_row +="<input type='text' value='"+item.category_id+"'  class='form-control category_id'>";
                    tb_row += "</label>";

                    tb_row += "</div>";

                    tb_row += "</div>";

                    tb_row += "<div class='table-cell'>";
                    tb_row += " <input type='text' class='form-control input-mini number_set' value='"+item.position+"'>";

                    tb_row += "</div>";

                    tb_row += "</td>";
                    tb_row += "<td>";
                    tb_row += "<label class='m-checkbox'>";
                    if(status=="${ _(u'開通')}") {
                        tb_row+="<input class='check' type='checkbox' checked>"
                    }else{
                        tb_row+="<input class='check' type='checkbox'>"
                    }
                    tb_row += "<span></span>";
                    tb_row += "</label>";
                    tb_row += "</td>";
                    tb_row += "<td>";
                    tb_row += "<ul class='ulli-no'>";
                    if(item.type=="product_light") {
                        tb_row+="<li>${ _(u'燈具類')}</li>"
                    }else{
                        tb_row+="<li>${ _(u'綠建築')}</li>"
                    }
                    tb_row += "<li>";
                    tb_row += "<div class='title'>"+item.name+"</div>";
                    tb_row += "<div class='into-name hide'>";
                    tb_row += "<input class='edit-title form-control m-input edit_input name' type='text' value='"+item.name+"'>";
                    tb_row += "</div>";
                    tb_row += "</li>";
                    tb_row += "</ul>";
                    tb_row += "</td>";

                    tb_row += "<td>";
                    tb_row += "<div class='brief'>"+item.intro+"</div>";
                    tb_row += '<div class="brief-name hide">';
                    tb_row += '<input class="edit-title form-control m-input intro" type="text" value="'+item.intro+'">';
                    tb_row += '</div>';
                    tb_row += "</td>";
                    tb_row += "<td>";
                    tb_row += '<div class="Description">'+item.description.replace("\n", "<br>")+'</div>';
                    tb_row += '<div class="Description-name hide">'+'<textarea class="form-control description_edit" rows="5" style="resize:none;">'+item.description+'</textarea>'+'</div>';

                    tb_row += "</td>";

                    tb_row += "<td>";
                    if(item.image['url']!=undefined){
                        tb_row +='<img class="image_url" src="${ request.static_path('beefun:static/uploads')}'+item.image["url"]+'" style="max-height: 100px;">'
                    }else{
                        tb_row +='<img class="image_url" src="http://www.placehold.it/231x175/EFEFEF/AAAAAA&amp;text=350x250" style="max-height: 100px;">'
                    }

                    tb_row += "</td>";
                    tb_row += "<td align=‘right’>";

                    tb_row +='<div class="col-8" style="width: 200px">';
                    ##  tb_row +='<div class="fileinput fileinput-new" data-provides="fileinput">';
                    ##  tb_row +='<div class="fileinput-new thumbnail text-l hide" style="width: 384px; height: 178px;">';
                    ##  tb_row +='<img src="http://www.placehold.it/384x178/EFEFEF/AAAAAA&amp;text=1920x890" alt="">';
                    ##  tb_row +='</div>';
                    ##  tb_row +='<input type="text" class="form-control m-input set_image_id hide" placeholder="">';
                    ##
                    ##  tb_row +='<div class="fileinput-preview fileinput-exists thumbnail hide" style="max-width: 384px; max-height: 178px;"><img src="" alt="" style="width: 100%;">';
                    ##
                    ##  tb_row +='</div><div><span class="btn default btn-file">';
                    ##  tb_row +='<span class="fileinput-new"> ${ _(u"選擇圖片")} </span>';
                    ##  tb_row +='<span class="fileinput-exists"> ${ _(u"選擇圖片")}</span>';
                    ##  tb_row +='<input type="file" class="image_file" onchange="readImage2(this,350,250);"></span>';
                    ##
                    ##
                    ##
                    ##  tb_row +='<a href="#" class="btn red fileinput-exists btn_del_pic hide" data-dismiss="fileinput"> ${ _(u"移除")} </a></div></div>';
                    ##
                       tb_row +='<div class="cropper_upload_btn obj_here" data-fk="" data-cut="false"></div>';

                    tb_row +='</div>';






                    tb_row += '<button type="button" class="btn btn-accent m-btn m-btn--icon btn-sm m-btn--icon-only sav_cont hide"><i class="fa fa-check"></i></button>';
                    tb_row += '<button type="button" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only edit_cont"><i class="fa fa-edit"></i></button>';

                    tb_row += "</td>";




                    tb_row += "</tr>";
                    html_str += tb_row;
                });
                $('#product_category_tbody').empty()
                $('#product_category_tbody').append(html_str);//Table頁面資料繪製
                // 裁切圖片功能
                var upload_callback = function(return_data, element) {
                    var new_image_link = '${ request.static_path('beefun:static/uploads')}'+return_data["url"];
                    ##  $('.cropper_upload_btn').find('img').attr('src', new_image_link);
                ##  $('.cropper_upload_btn').find('img').attr('image_id', return_data['image_id']);
                ##  $('.cropper_upload_btn').parent().parent().removeClass('has-error')
                ##
                ##  $('.cropper_upload_btn').find('span.help-block').remove();
               this_obj.closest('.cropper_upload_btn').find('img').attr('src', new_image_link);
                    this_obj.closest('.cropper_upload_btn').find('img').attr('image_id', return_data['image_id']);
                    this_obj.closest('.cropper_upload_btn').parent().parent().removeClass('has-error')

                    this_obj.closest('.cropper_upload_btn').find('span.help-block').remove();
                    var src = "${ request.static_path('beefun:static/uploads')}"+return_data["url"];
                    this_obj.closest('.obj_index').find('.image_url').attr('src',src);

                    ## $('#bounce-window').find('.cropper-oragin.active-cropper img').attr('src', new_image_link).closest('.active-cropper').removeClass('active-cropper');
               $(element).modal('hide');


                    this_obj.closest('.obj_index').find('.image_id').val( return_data["image_id"]);

                    var url = "${ request.route_url('api.category.detail',category_id='') }"+this_obj.closest(".obj_index").find('.category_id').val();
                    var form_data = new FormData();

                    form_data.append('image_id', return_data["image_id"]);


                    ajax(url, "POST", form_data, null, function (response) {
                        if (response['status']) {

                            alert("${_('儲存成功')}");
                            $('#product_category_tbody').empty();
                            Table.start();
                        } else {
                            alert(response['message']);
                        }
                    });


                    ## $('#bounce-window #bounce-content').show();
            }
                $('.cropper_upload_btn').cropper_plugin2(options = {
                    url: "${request.route_url('api.image')}",
                    image_path: 'https://www.placehold.it/330x235/EFEFEF/AAAAAA&text=350:250',
                    image_ratio: '350/250',
                    csrfToken: csrfToken
                }, callback = upload_callback);
            }
        });



        $(document).on('change', '.check', function (e) {
            var url = "${ request.route_url('api.category.detail',category_id='') }"+$(this).closest(".obj_index").find('.category_id').val();
            var form_data = new FormData();
            if(this.checked){
                form_data.append('status', 'show');
            }else{
                form_data.append('status','hiden');
            }
            ajax(url, "POST", form_data, null, function (response) {
                if (response['status']) {

                    alert("${_('儲存成功')}");
                    $('#product_category_tbody').empty();
                    Table.start();
                } else {
                    alert(response['message']);
                }
            });
        });

        $(document).on('change', '.index_check', function (e) {
            var url = "${ request.route_url('api.category.detail',category_id='') }"+$(this).closest(".obj_index").find('.category_id').val();
            var form_data = new FormData();
            if(this.checked){
                form_data.append('index_status', 'show');
            }else{
                form_data.append('index_status','hiden');
            }
            ajax(url, "POST", form_data, null, function (response) {
                if (response['status']) {

                    alert("${_('儲存成功')}");
                    $('#product_category_tbody').empty();
                    Table.start();
                } else {
                    alert(response['message']);
                }
            });
        });


        $(document).on('change', '#category_type', function (e) {

            type_set = $('#category_type').val();
            $('#product_category_tbody').empty();
            Table.search_parameter='&language='+$('.nav-link.active').attr('id')+'&type='+type_set;
            Table.start();
        });

        $(document).on('click', '.sav_cont', function (e) {
            var url = "${ request.route_url('api.category.detail',category_id='') }"+$(this).closest(".obj_index").find('.category_id').val();
            var form_data = new FormData();
            if($(this).closest(".obj_index").find('.name').val()==""){
                alert("名稱不可為空");
                $('#product_category_tbody').empty();
                Table.start();
                return false
            }
            if($(this).closest(".obj_index").find('.intro').val().length>30){
                alert('${_(u"字數大於30")}')
                   Table.start();
                return false
            }
            if($(this).closest(".obj_index").find('.description_edit').val().length>30){
                alert('${_(u"字數大於30")}')
                   Table.start();
                return false
            }
              if( $(this).closest(".obj_index").find('.name').val().match('[\(\)\<\>\/\?\!\~]')!=null){
                alert('${_(u"請勿輸入特殊字元")}')
                  Table.start();
                return false
            }
             if( $(this).closest(".obj_index").find('.intro').val().match('[\(\)\<\>\/\?\!\~]')!=null){
                alert('${_(u"請勿輸入特殊字元")}')
                  Table.start();
                return false
            }
             if( $(this).closest(".obj_index").find('.description_edit').val().match('[\(\)\<\>\/\?\!\~]')!=null){
                alert('${_(u"請勿輸入特殊字元")}')
                  Table.start();
                return false
            }


            form_data.append('name', $(this).closest(".obj_index").find('.name').val());
            form_data.append('intro', $(this).closest(".obj_index").find('.intro').val());
            form_data.append('description', $(this).closest(".obj_index").find('.description_edit').val());

            ajax(url, "POST", form_data, null, function (response) {
                if (response['status']) {

                    alert("${_('儲存成功')}");
                    $('#product_category_tbody').empty();
                    Table.start();
                } else {
                    alert(response['message']);
                }
            });
        });

        ajax_reload = Table;


        //修改名稱事件
        $(document).on('click', '.btn.edit_cont', function() {
            $(this).parents('tr').find('.title').addClass('hide').siblings('.hide').removeClass('hide');
            $(this).parents('tr').find('.Description').addClass('hide').siblings('.hide').removeClass('hide');
            $(this).parents('tr').find('.brief').addClass('hide').siblings('.hide').removeClass('hide');
            $(this).addClass('hide').siblings('.hide').removeClass('hide');
        });

        $(document).on('click', '.btn.sav_cont', function() {
            $(this).addClass('hide').siblings('.hide').removeClass('hide');
            $(this).parents('tr').find('.into-name').addClass('hide').siblings('.hide').removeClass('hide');
            $(this).parents('tr').find('.brief-name').addClass('hide').siblings('.hide').removeClass('hide');
            $(this).parents('tr').find('.Description-name').addClass('hide').siblings('.hide').removeClass('hide');

            var name = $(this).parents('tr').find('.into-name input').val();
            $(this).parents('tr').find('.title').text(name);
            var name = $(this).parents('tr').find('.brief-name input').val();
            $(this).parents('tr').find('.brief').text(name);
            var dd = $(this).parents('tr').find('.Description-name textarea').val();
            $(this).parents('tr').find('.Description').val(name);
        });


        $(document).on('click','.save_btn',function(){
            var event_btn = $(this);

            var category_data_list = [];
            var number_set_check = [];
            //第一層
            $('.obj_index').each(function(i, v){
                if(parseInt($(v).find('.number_set').val())==0){
                    alert('請勿輸入0');
                    return false;
                }
                var category_data = {
                    'category_id': $(v).find('.category_id').val(),
                    'position': $(v).find('.number_set').val()
                }
                category_data_list.push(category_data);
                number_set_check.push($(v).find('.number_set').val());
            });

            var number_set_check_onleny=number_set_check.filter(function (el, i, arr) {
                return arr.indexOf(el) === i;
            });

            if(category_data_list.length==number_set_check_onleny.length){
                category_save(event_btn, category_data_list);
            }else{
                alert("${ _(u'序列有重複，請修正！！')}");
            }

            return false;

        });

        function category_save(event_btn, category_data_list){
            var url = "${ request.route_url('api.category.multi.set')}";
            var category_data_form = new FormData();
            category_data_form.append('json_data', JSON.stringify(category_data_list))

            ajax(url, 'POST', category_data_form, event_btn, function(data){
                if (event_btn) event_btn.attr("disabled", false);

                if (data['status']) {
                    $('#product_category_tbody').empty();
                    Table.start();
                } else {
                    alert('${_(u"創建失敗")}')
                }
            });
        }


        $('.search').on('keydown', function (e) {
            if (e.which == 13) {
                $('#product_category_tbody').empty();
                ajax_reload.dom_search_btn.click();
                e.preventDefault();

            }
        });


        function ajax(url, method, form_data, btn, callback){
            $.ajax({
                url: url,
                type: method,
                data: form_data,
                ##  dataType: 'JSON',
                contentType: false,
                processData: false,
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    if (btn) btn.attr("disabled", true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                    if (btn) btn.attr("disabled", false)
                },
                success: function (response) {
                    callback(response);
                },
                complete: function () {
                    if (btn) btn.attr("disabled", false);
                }
            });
        }

        function readImage(file,img_w,img_h) {
            var src  =$(file).parents('.fileinput').find('.fileinput-preview.thumbnail img').attr('src'),
                    input = file,
                    exsits=$(file).parents('.fileinput').hasClass('fileinput-exists');
            btn_check = $(file).closest('.set_obj_index');


            var file_id= file.id,
                    imageFile = file.files[0];

            var reader = new FileReader();
            var image  = new Image();
            reader.readAsDataURL(imageFile);
            var w = 0,h=0;
            reader.onload = function(_file) {
                image.src    = _file.target.result;
                image.onload = function() {
                    w = this.width,
                            h = this.height;
                    // t = file.type,
                    // n = file.name,
                    // s = ~~(file.size/1024) +'KB';

                    if(w !=img_w || h !=img_h){

                        if(!exsits){

                            $(file).parents('.fileinput').addClass('fileinput-new').removeClass('fileinput-exists');
                        }

                        $(input).parents('.fileinput').find('.fileinput-preview.thumbnail img').attr('src',src);
                        alert("${ _(u'請選擇尺寸為')} "+img_w+' X '+img_h+"${ _(u' 的圖片')}");
                        $('#product_category_tbody').empty();
                        Table.start();

                    }else {

                        upload_image(function (status, image_data) {
                            if (status) {
                                btn_check.find('.set_image_id').val( image_data["image_id"]);

                            }
                        });
                    }
                }
            };
            image.onerror= function() {
                alert('Invalid file type: '+ file.type);
            };





        }
        function readImage2(file,img_w,img_h) {
            var src  =$(file).parents('.fileinput').find('.fileinput-preview.thumbnail img').attr('src'),
                    input = file,
                    exsits=$(file).parents('.fileinput').hasClass('fileinput-exists');
            btn_check = $(file).closest('.obj_index');


            var file_id= file.id,
                    imageFile = file.files[0];

            var reader = new FileReader();
            var image  = new Image();
            reader.readAsDataURL(imageFile);
            var w = 0,h=0;
            reader.onload = function(_file) {
                image.src    = _file.target.result;
                image.onload = function() {
                    w = this.width,
                            h = this.height;
                    // t = file.type,
                    // n = file.name,
                    // s = ~~(file.size/1024) +'KB';

                    if(w !=img_w || h !=img_h){

                        if(!exsits){

                            $(file).parents('.fileinput').addClass('fileinput-new').removeClass('fileinput-exists');
                        }

                        $(input).parents('.fileinput').find('.fileinput-preview.thumbnail img').attr('src',src);
                        alert("${ _(u'請選擇尺寸為')} "+img_w+' X '+img_h+"${ _(u' 的圖片')}");
                        $('#product_category_tbody').empty();
                        Table.start();

                    }else {

                        upload_image(function (status, image_data) {
                            if (status) {
                                var src = "${ request.static_path('beefun:static/uploads')}"+image_data["url"];
                                btn_check.find('.image_url').attr('src',src);
                                btn_check.find('.image_id').val( image_data["image_id"]);

                                var url = "${ request.route_url('api.category.detail',category_id='') }"+btn_check.closest(".obj_index").find('.category_id').val();
                                var form_data = new FormData();

                                form_data.append('image_id', image_data["image_id"]);


                                ajax(url, "POST", form_data, null, function (response) {
                                    if (response['status']) {

                                        alert("${_('儲存成功')}");
                                        $('#product_category_tbody').empty();
                                        Table.start();
                                    } else {
                                        alert(response['message']);
                                    }
                                });

                            }
                        });
                    }
                }
            };
            image.onerror= function() {
                alert('Invalid file type: '+ file.type);
            };





        }

        function upload_image(callback){
            // 上傳圖片
            if (btn_check.find('.image_file')[0].files[0] == undefined){
                var image_id = btn_check.find('.logo_image').attr('image_id');
                if(image_id){
                    callback(true, {'image_id': image_id});
                }else{
                    alert("${ _(u'圖片上傳錯誤')}");
                    callback(false, null);
                }
            }else{
                var image_form_data = new FormData();
                image_form_data.append('image_file', btn_check.find('.image_file')[0].files[0]);
                var upload_image_url = "${ request.route_url('api.image') }";
                ajax(upload_image_url, "POST", image_form_data, $('.create_btn'), function(response){
                    if (response['status']) {

                        callback(true, response['response']['image_list'][0])
                    } else {
                        alert(response['message']);
                    }
                });
            }
        }




        $('.se1').select2();
    </script>

</%block>


