<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

</head>
<body>

<!--portlet-body-->
    <div class="portlet-body">
        <form class="form-horizontal">
            <!--按鈕列-->
            <!-- <div class="portlet">
              <div class="portlet-title">
                <div class="actions btn-set">
                  <button type="button" class="btn green-seagreen" onclick="javascript:window.open('firm.html','_self')"><i class="fa fa-check"></i> 確認存檔 </button>
                </div>
              </div>
            </div> -->
            <!--按鈕列 end-->

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        <label class="control-label col-xs-4">${ _(u'分類名稱')}：</label>
                        <div class="col-xs-7">
                            <input type="text" class="form-control input-inline" placeholder="${ _(u'請填入新增或是查詢的名稱')}"
                                   name="name">
                            <button type="button" class="btn purple btn-sm btn-circle btn-create"><i
                                    class="fa fa-plus-circle"></i> ${ _(u'新增')}</button>
                            <button type="button" class="btn yellow btn-sm btn-circle btn-search"><i
                                    class="fa fa-search"></i> ${ _(u'查詢')}</button>
                        </div>
                    </div>
                    <table class="table table-striped table-hover" id="category_list_tab">
                        <thead>
                        <tr class="green">
                            <th> #</th>
                            <th> ${ _(u'分類名稱')}</th>
                            <th><!--操作-->&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody class="category_list">

                        </tbody>
                    </table>
                    <!--頁碼-->
                    <div id="page-selection"></div>
                    <!--頁碼  end-->
                </div>
            </div>

        </form>

    </div>
    <!--portlet-body end-->


</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js"></script>

<script src="${ request.static_path('beefun:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
<script src="${ request.static_path('beefun:static/assets/make_table/make_table.js')}" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/custom_validation.js')}"
            type="text/javascript"></script>

<script>

    var csrfToken = "${request.session.get_csrf_token()}";

    var Table = new MakeTable({
        'dom_search_btn': $('.btn-search'),
        'dom_table_tbody': $('.category_list'),
        'ajax_search_url':  "${ request.route_url('api.category') }",
        'ajax_csrf_token':csrfToken,
        'table_model': function (obj, page) {
            //開始產生頁面資料
            var $list = obj['category_list'];
            var html_str = '';
            //var data_table_row = [];
              console.log(obj)
            $.each($list, function (i, item) {
                item.index = (i + 1 + ((page - 1) * 10));
                var tb_row = "<tr>" +
                            "<td>" + item.index + "</td>" +
                            "<td>" + "<div class='brand-name'>" + item['name'] + "</div>" +
                            "<div class='into-name hidden'>" +
                            "<input class='edit-title' type='text' value='" + item['name'] + "' data-id='" + item['category_id'] + "'>" +
                            "</td>" +
                            "<td class='set-btn' align='right'>" +
                            "<button type='button' class='btn green-seagreen hidden sav'>" +
                            "<i class='fa fa-check'></i> 存檔" +
                            "</button>" +
                            "<button type='button' class='btn red class_edit'>" +
                            "<i class='fa fa-pencil'></i> 修改" +
                            "</button>" +
                            "</td>" +
                            "</tr>";
                html_str += tb_row;
            });
            $('.category_list').append(html_str);//Table頁面資料繪製
        }
    });
    Table.start();

    jQuery(document).ready(function () {
        jQuery.validator.addMethod("alnum", function (value, element) {
            return this.optional(element) || /^[\u4e00-\u9fa5_a-zA-Z0-9]+$/.test(value);
        }, "只能包括英文字母和数字");

        $('form').validate({
            ignore: ":hidden",
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            rules: {
                name: {
                    alnum: true,
                },
            },
            messages: {
                name: {
                    alnum: "${ _(u'分類不允許特殊字元')}(keyword is not allow specific word).",
                },
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.appendTo(element.closest("div"));
            },
        });

        //修改類別名稱事件
        $('.category_list').on('click', '.btn.red.class_edit', function () {
            $(this).parents('tr').find('.brand-name').addClass('hidden').siblings('.hidden').removeClass('hidden');
            $(this).addClass('hidden').siblings('.hidden').removeClass('hidden');
        });

        //儲存類別名稱事件
        $('.category_list').on('click', '.btn.green-seagreen.sav', function () {
            var event_btn = $(this);
            var input_element = $(this).parents('tr').find('.into-name input');
            var name = input_element.val();
            var category_id = input_element.data('id');
            var form_data = {'name': name}
            var url = "${ request.route_url('api.category.detail', category_id='') }"+category_id;
            ajax(url, 'POST', form_data, event_btn, function(data){
                ##  var msg = data['message'];
                ##  var response = data['response'];
                if (data['status']) {
                    event_btn.parents('tr').find('.brand-name').text(name);
                    event_btn.addClass('hidden').siblings('.hidden').removeClass('hidden');
                    event_btn.parents('tr').find('.into-name').addClass('hidden').siblings('.hidden').removeClass('hidden');
                } else {
                    alert('更新失敗');
                    event_btn.addClass('hidden').siblings('.hidden').removeClass('hidden');
                    event_btn.parents('tr').find('.into-name').addClass('hidden').siblings('.hidden').removeClass('hidden');
                }
            });
        });

        $('form').on('click', '.btn-create', function () {
            var $valid = $("form").valid();
            if (!$valid) {
                $validator.focusInvalid();
                return false;
            }
            var event_btn = $(this);
            var input_element = $(this).parents('tr').find('.into-name input');
            var create_parameter = { "type": "${category_type}"};
            var form_data = $('form').serializeArray();
            jQuery.map(form_data, function (n, i) {
                if (n.value) create_parameter[n.name] = n.value;
            });
            var url = "${ request.route_url('api.category') }";
            ajax(url, 'POST', create_parameter, event_btn, function(data){
                var msg = data['message'];
                var response = data['response'];
                if (data['status']) {
                    $('form').find('input[name="name"]').val('');
                    $('.fa-search').click();
                } else {
                    alert('創建失敗');
                }
            });
        });

    });

    function ajax(url, method, form_data, btn, callback){
        $.ajax({
            url: url,
            type: method,
            data: form_data,
            dataType: 'JSON',
            ##  contentType: false,
            ##  processData: false,
            headers: {'X-CSRF-Token': csrfToken},
            beforeSend: function () {
                if (btn) btn.attr("disabled", true);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
                if (btn) btn.attr("disabled", false)
             },
            success: function (response) {
                callback(response);
            },
            complete: function () {
                if (btn) btn.attr("disabled", false);
            }
        });
    }

</script>
</html>