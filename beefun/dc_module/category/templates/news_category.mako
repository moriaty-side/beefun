<%inherit file="beefun:templates/backend/master.mako"/>

<%block name="css">
    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css"/>
    <!-- 圖片預覽 -->

</%block>

<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span class="m-font-898b96">${ _('服務管理管理')}</span> <i class="fa fa-caret-right"></i>
                        <span>${ _(u'服務')}</span>
                    </h3>
                </div>
                <div>
                    <button type="button" class="btn btn-success m-btn m-btn--outline-2x btn-sm save_btn">
                    <span>
                                        <i class="fa fa-check"></i>
                                        <span>
                                            ${ _(u'序列存檔')}
                                        </span>
                    </span>
                    </button>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="news_category" data-parent="news">
            <!--begin::Portlet-->
            <div class="languages hide"></div>
            <div class="m-portlet">
                
                <!--Begin::Section-->
                <!--begin::Form-->
                <button hidden type="button"
                        class="btn btn-warning m-btn m-btn--outline-2x btn-sm mr-5px btn-search">
									<span>
										<i class="fa fa-search"></i>
										<span>
											搜尋
										</span>
									</span>
                </button>
                <form class="m-form m-form--fit m-form--label-align-right" id="search_form">
                    <!--Begin::Section-->
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-6 offset-3 mb-15px">



                                <a href="#" class="m-portlet__nav-link m-btn--pill">
                                    <div class="m-input-icon m-input-icon--right">

                                        <input type="text" class="form-control m-input m-input--solid m-input--pill search" name="name" placeholder="Search...">

                                        <span class="m-input-icon__icon m-input-icon__icon--right">
                                                        <span>
                                                            <i class="la la-search m--font-brand"></i>
                                                        </span>
                                                    </span>
                                    </div>
                                </a>
                                <div class="form-group m-form__group mt-15px">
                                    <label class="col-form-label col-4">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'服務名稱')}：
                                    </label>
                                    <div class="col-8">
                                        <input type="text" class="form-control m-input" placeholder="" id="category_name">
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <button type="button" class="btn btn-brand m-btn m-btn--outline-2x btn-sm H-center news_category_btn">
                                    <span>
                                                        <i class="fa fa-plus"></i>
                                                        <span>
                                                            ${ _(u'新增')}
                                                        </span>
                                    </span>
                                    </button>
                                </div>
                                <table class="table m-table m-table--head-bg-success">
                                    <thead>
                                    <tr>

                                        <th> ${ _(u'分類顯示')}</th>
                                        <th> ${ _(u'名稱')} </th>
                                        <th>
                                            <!--操作-->&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody id="news_category_tbody">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>
</%block>



<%block name="script">
    <!-- 圖片預覽 -->
    <script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/readimage.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/make_table/make_table.js')}" type="text/javascript"></script>
    <!-- 圖片預覽 -->
    <script>
        $('.se1').select2();
        var csrfToken = "${request.session.get_csrf_token()}";
        var language ='${ request.registry.settings.get('available_langs', [['zh_Hant', u'繁體中文']])[0][0] }';

        
        $(document).ready(function (e) {
            
            Table.start();
        });
        //新增分類
        $(document).on('click', '.news_category_btn', function() {
            if($('#category_name').val()==""){
                alert('${_(u"請輸入名稱")}')
                return false
            }
             if($('#category_name').val().length>100){
                alert('${_(u"字數大於100")}')
                return false
            }
                   if( $('#category_name').val().match('[\(\)\<\>\/\?\!\+\~]')!=null){
                alert('${_(u"請勿輸入特殊字元")}')
                return false
            }
            var event_btn = $(this);
            var url = "${ request.route_url('api.category')}";
            var category_data_form = new FormData();
            category_data_form.append('type', 'news');
            category_data_form.append('name', $('#category_name').val());
            
            if($('#category_name').val()!=""){
                ajax(url, 'POST', category_data_form, event_btn, function(data){
                if (event_btn) event_btn.attr("disabled", false);

                if (data['status']) {
                    $('#news_category_tbody').empty();
                     $('#category_name').val("");
                    Table.start();
                } else {
                    alert('${_(u"創建失敗")}')
                }
            });
            }else{
                alert('${_(u"請輸入名稱")}')
            }


        });

        var Table = new MakeTable({
            'dom_search_form': $('form#search_form'),
            'dom_search_btn': $('i.la.la-search'),
            'dom_table_tbody': $('.news_category_list'),
            
            'init_search_parameter': '&type=news',
            
            'ajax_search_url':  "${ request.route_url('api.category') }",
            'ajax_csrf_token':csrfToken,
            'table_model': function (obj, page) {
                //開始產生頁面資料
                var $list = obj['category_list'];
                var html_str = '';
                //var data_table_row = [];

                $.each($list, function (i, item) {
                    item.index = (i + 1 + ((page - 1) * 10));
                    var status=""
                    switch (item.status) {
                        case "show":
                            status = "${ _(u'開通')}";
                            break;
                        case "hiden":
                            status = "${ _(u'關閉')}";
                            break;
                        case "delete":
                            status = "${ _(u'刪除')}";
                            break;
                    }

                    var tb_row = " <tr class='obj_index'>" ;
                    tb_row += "<td>"+"<input type='text' class='form-control input-mini number_set' value='"+item.position+"'>"+"</td>";
                    tb_row+="<td>"+"<label class='m-checkbox'>";
                    if(status=="${ _(u'開通')}") {
                        tb_row+="<input class='check' type='checkbox' checked>"
                    }else{
                        tb_row+="<input class='check' type='checkbox'>"
                    }
                    tb_row +="<input type='text' value='"+item.category_id+"'  class='form-control category_id'>";
                    tb_row+="<span></span>"+"</label></td>";
                    tb_row+="<td>";
                    tb_row+="<div class='title'>"+item.name+"</div>";
                    tb_row+='<div class="into-name hide">';
                    tb_row+='<input class="edit-title form-control m-input edit_input" type="text" value="'+item.name+'">';
                    tb_row+='</div>';
                    tb_row+="</td>";
                    tb_row+='<td align="right">';
                    tb_row+='<button type="button" class="btn btn-accent m-btn m-btn--icon btn-sm m-btn--icon-only sav_cont hide"><i class="fa fa-check"></i></button>';
                    tb_row+='<button type="button" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only edit_cont"><i class="fa fa-edit"></i></button>';

                    tb_row+='</td>';
                    tb_row += "</tr>";
                    html_str += tb_row;
                });
                $('#news_category_tbody').empty()
                $('#news_category_tbody').append(html_str);//Table頁面資料繪製
            }
        });

        $(document).on('change', '.check', function (e) {
            var url = "${ request.route_url('api.category.detail',category_id='') }"+$(this).closest(".obj_index").find('.category_id').val();
            var form_data = new FormData();
            if(this.checked){
                form_data.append('status', 'show');
            }else{
                form_data.append('status','hiden');
            }
            ajax(url, "POST", form_data, null, function (response) {
                if (response['status']) {

                    alert("${_('儲存成功')}");
                    $('#news_category_tbody').empty();
                    Table.start();
                } else {
                    alert(response['message']);
                }
            });
        });

        $(document).on('click', '.sav_cont', function (e) {
            if($(this).closest(".obj_index").find('.edit_input').val()==""){
                alert('${_(u"請輸入名稱")}')
                  Table.start();
                return false
            }
             if($(this).closest(".obj_index").find('.edit_input').val().length>100){
                alert('${_(u"字數大於100")}')
                  Table.start();
                return false
            }
             if( $(this).closest(".obj_index").find('.edit_input').val().match('[\(\)\<\>\/\?\!\~]')!=null){
                alert('${_(u"請勿輸入特殊字元")}')
                  Table.start();
                return false
            }
            var url = "${ request.route_url('api.category.detail',category_id='') }"+$(this).closest(".obj_index").find('.category_id').val();
            var form_data = new FormData();
            form_data.append('name', $(this).closest(".obj_index").find('.edit_input').val());
            ajax(url, "POST", form_data, null, function (response) {
                if (response['status']) {
                    alert("${_('儲存成功')}");
                    $('#news_category_tbody').empty();
                    Table.start();
                } else {
                    alert(response['message']);
                }
            });
        });
        ajax_reload = Table;

        //修改名稱事件
        $(document).on('click', '.btn.edit_cont', function() {
            $(this).parents('tr').find('.title').addClass('hide').siblings('.hide').removeClass('hide');
            $(this).addClass('hide').siblings('.hide').removeClass('hide');
        });

        $(document).on('click', '.btn.sav_cont', function() {
            $(this).addClass('hide').siblings('.hide').removeClass('hide');
            $(this).parents('tr').find('.into-name').addClass('hide').siblings('.hide').removeClass('hide');

            var name = $(this).parents('tr').find('.into-name input').val();
            $(this).parents('tr').find('.title').text(name);
        });

        $(document).on('click','.save_btn',function(){
            var event_btn = $(this);

            var category_data_list = [];
            var number_set_check = [];
            //第一層
            $('.obj_index').each(function(i, v){
                if(parseInt($(v).find('.number_set').val())==0){
                    alert('請勿輸入0');
                    return false;
                }
                var category_data = {
                    'category_id': $(v).find('.category_id').val(),
                    'position': $(v).find('.number_set').val()
                }
                category_data_list.push(category_data);
                number_set_check.push($(v).find('.number_set').val());
            });
            var number_set_check_onleny=number_set_check.filter(function (el, i, arr) {
                return arr.indexOf(el) === i;
            });
            if(category_data_list.length==number_set_check_onleny.length){
                category_save(event_btn, category_data_list);
            }else{
                alert("${ _(u'序列有重複，請修正！！')}");
            }
            return false;

        });

        function category_save(event_btn, category_data_list){
            var url = "${ request.route_url('api.category.multi.set')}";
            var category_data_form = new FormData();
            category_data_form.append('json_data', JSON.stringify(category_data_list))
            ajax(url, 'POST', category_data_form, event_btn, function(data){
                if (event_btn) event_btn.attr("disabled", false);

                if (data['status']) {
                    $('#news_category_tbody').empty();
                    Table.start();
                } else {
                    alert('${_(u"創建失敗")}')
                }
            });
        }

        $('.search').on('keydown', function (e) {
            if (e.which == 13) {
                $('#news_category_tbody').empty();
                ajax_reload.dom_search_btn.click();
                e.preventDefault();

            }
        });

        function ajax(url, method, form_data, btn, callback){
            $.ajax({
                url: url,
                type: method,
                data: form_data,
                ##  dataType: 'JSON',
                contentType: false,
                processData: false,
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    if (btn) btn.attr("disabled", true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                    if (btn) btn.attr("disabled", false)
                },
                success: function (response) {
                    callback(response);
                },
                complete: function () {
                    if (btn) btn.attr("disabled", false);
                }
            });
        }
    </script>
</%block>


