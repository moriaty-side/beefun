# coding=utf-8
from __future__ import unicode_literals

import uuid
from .optimization_domain import Optimization
from ...base.base_service import EMPTY_DIC, BaseService
from ...lib.my_exception import MyException

class OptimizationService(BaseService):

    TABLE = Optimization

    def __init__(self, session, logger=None):
        super(OptimizationService, self).__init__(session, logger)
        self.table_args = self.TABLE.__getattributes__()

    def create(self, **data):
        # 不可用參數
        un_available_args = []
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        create_data = {key: data[key] for key in available_args if key in data}

        # 資料創建
        try:
            return self._create(**create_data)
        except Exception as e:
            raise MyException(code=3001, message=e)

    def update(self, old_obj, **data):
        # 不可用參數
        un_available_args = ['optimization_id']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=952, message=e)

    def update_by_id(self, optimization_id, **data):
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['optimization_id']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 抓取對應物件
        old_obj = self.get_by_id(optimization_id)
        # 選填關聯參數
        old_obj, data = self._relationship_obj_ud(old_obj, data)
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=952, message=e)

    def get_by(self, **data):
        try:
            return self._get_by(**data)
        except Exception as e:
            raise MyException(code=954, message=e)

    def get_by_id(self, optimization_id, check=False):
        id = optimization_id if isinstance(optimization_id, uuid.UUID) else uuid.UUID(optimization_id)
        obj = self._get_by(optimization_id=id)
        if check and not obj:
            raise MyException(code=954)
        return obj

    def delete(self, old_obj):
        try:
            return self._delete(old_obj)
        except Exception as e:
            raise MyException(code=956, message=e)

    def delete_by_id(self, optimization_id):
        obj = self.get_by_id(optimization_id=optimization_id, check=True)
        return self.delete(obj)

    
    def get_list(self, order_by=EMPTY_DIC, group_by=EMPTY_DIC, key=None,
    
                 offset=0, limit=None, show_count=False, **search_condition):

        # 可不用參數
        un_available_args = ['']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        filter_data = {key: search_condition[key]
                       for key in available_args if key in search_condition}

        # 查詢條件製成
        query = self.session.query(self.TABLE)

        try:
            
            # if key is not None:
            #     query = query.filter(self.TABLE.key==key)

            return self._get_list(query, order_by, group_by,
                                  offset, limit, show_count, **filter_data)

        except Exception as e:
            raise MyException(code=953, message=e)



