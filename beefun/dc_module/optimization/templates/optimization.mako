<%inherit file="beefun:templates/backend/master.mako"/>

<%block name="css">

</%block>

<%block name="content">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    <span>${ _(u'首頁')}</span>
                </h3>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content" data-menu="index" data-parent="">
        <div class="row ">
        </div>
    </div>
</div>
</%block>

<%block name="script">
</%block>
