<%inherit file="beefun:templates/backend/master.mako"/>

<%block name="css">
    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css" />
    <!-- 圖片預覽 -->

</%block>

<%block name="content">

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span>${ _(u'網站管理')}</span>
                    </h3>
                </div>
                <div>
                    <button type="button" id="webpage_save" class="btn btn-success m-btn m-btn--outline-2x btn-sm">

                    <span>
                        <i class="fa fa-check"></i>
                        <span>
                            ${ _(u'存檔')}
                        </span>
                    </span>
                    </button>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="web_set" data-parent="">
            <!--begin::Portlet-->
            <div class="m-portlet">
                
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                            <h3 class="m-portlet__head-text">
                                ${ _(u'Logo')}
                            </h3>
                        </div>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right" id="image_create_form">
                    <!--Begin::Section-->
                    <div class="m-portlet__body">
                        <div class="row">

                            <div class="col-md-12">

                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                        *
                                    </span>

                                        ${ _(u'檔案')}：
                                    </label>
                                    <div class="col-8">
                                        %if webset.get('value',{}).get('logo'):
                                            <div class="fileinput fileinput-exists" data-provides="fileinput">
                                        %else:
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                        %endif
                                        <div class="fileinput-new thumbnail" style="width: 185px; height: 90px;">
                                            <img src="http://www.placehold.it/185x90/EFEFEF/AAAAAA&amp;text=185x90" alt=""/>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 185px; max-height: 90px;">
                                            %if webset.get('value',{}).get('logo'):
                                                <img class="logo_image" src="${ request.static_path('beefun:static/uploads')+webset.get('value',{}).get('logo', {}).get('url')}" alt="" image_id="${webset.get('value',{}).get('logo', {}).get('image_id')}"/>
                                            %else:
                                                <img class="logo_image" src="" alt="">
                                            %endif

                                        </div>
                                        <div>
                                          <span class="btn default btn-file">
                                            <span class="fileinput-new"> ${ _(u'選擇圖片')} </span>
                                            <span class="fileinput-exists"> ${ _(u'更換')}</span>
                                            <input type="file" name="logo" onchange="readImage(this,185,90);">

                                          </span>
                                            <a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"> ${ _(u'移除')} </a>
                                        </div>
                                    </div>
                                        <div class="help-block">${ _(u'尺寸')}：185x90</div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End::Section-->
                </form>
                <!--end::Form-->
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                            <h3 class="m-portlet__head-text">
                                ${ _(u'客服管理')}
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right" id="webset_create_form">
                    <!--Begin::Section-->
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group m-form__group element-group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                        *
                                    </span>

                                        ${ _(u'E-Mail')}：
                                    </label>
                                    <div class="col-8">
                                        <input type="email" name="support_email" class="form-control m-input input-group" placeholder="" value="${ webset.get('value',{}).get('support_email', '') }">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>

        <!--end::Portlet-->
    </div>
</%block>



<%block name="script">
    <!-- JQ Validate -->
    <script src="${ request.static_path('beefun:static/assets/global/plugins/jquery-validation/js/jquery.validate.js')}" type="text/javascript"></script>

    <!-- 圖片預覽 -->
    <script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/readimage.js') }" type="text/javascript"></script>
    <!-- 圖片預覽 -->
    <script>
        $('.se1').select2();
        var csrfToken = "${request.session.get_csrf_token()}";
        

        $('#webset_create_form').validate({
            ignore: ":hidden",
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                support_email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                support_email: {
                    required:  "${_(u'Email為必填值')}",
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.element-group').addClass('has-error').removeClass('has-success'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.element-group').addClass('has-success').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                ##                 $(window).scrollTop(element.offset().top - 80);
                error.insertAfter(element.closest('.input-group'));
            }
        });

        // 儲存圖片
        $(document).on('click', '#webpage_save', function() {
            var save_btn = $(this);
            upload_image(function(status, image_data) {
                if (status) {
                    var key = "${ key }";
                    var url = "${ request.route_url('api.optimization.info') }";
                    var form_data = new FormData();
                    form_data.append('key', key);
                    form_data.append('logo', image_data["image_id"]);
                    language = $('.nav-link.active').attr('id');
                    
                    form_data.append('support_email', $('input[name="support_email"]').val());
                    if($('#webset_create_form').validate().form()){
                        ajax(url, "POST", form_data, save_btn, function(response) {
                            if (response['status']) {
                                alert("${_('儲存成功')}");
                            } else {
                                alert(response['message']);
                            }
                        });
                    }
                }
            });
        });

        function upload_image(callback) {
            // 上傳圖片
            if ( $('input[name="logo"]')[0].files == null || $('input[name="logo"]')[0].files.length == 0) {
                var image_id = $('.logo_image').attr('image_id');
                if (image_id != undefined) {
                    callback(true, { 'image_id': image_id });
                } else {
                    alert("${_('請選擇圖片')}");
                    callback(false, null);
                }
            } else {
                var image_form_data = new FormData();
                image_form_data.append('image_file', $('input[name="logo"]')[0].files[0]);
                var upload_image_url = "${ request.route_url('api.image') }";
                ajax(upload_image_url, "POST", image_form_data, $('.create_btn'), function(response) {
                    if (response['status']) {

                        callback(true, response['response']['image_list'][0])
                    } else {
                        alert(response['message']);
                    }
                });
            }
        }

        function ajax(url, method, form_data, btn, callback) {
            $.ajax({
                url: url,
                type: method,
                data: form_data,
                contentType: false,
                processData: false,

                headers: { 'X-CSRF-Token': csrfToken },
                beforeSend: function() {
                    $(btn).attr('disabled', true);
                },
                error: function(xhr) {
                    $(btn).attr('disabled', true);
                    alert('Ajax request 發生錯誤');
                },
                success: function(response) {
                    callback(response);
                },
                complete: function() {

                    $(btn).attr('disabled', false);
                }
            });
        }
    </script>

</%block>


