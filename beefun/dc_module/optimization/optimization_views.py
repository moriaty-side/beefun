# coding=utf8
from __future__ import unicode_literals

import json
import transaction
from pyramid.csrf import check_csrf_token
from . import optimization_form as form
from ...base.base_view import BaseView
from ...lib.my_exception import MyException
from .optimization_service import OptimizationService

from ..image.image_service import ImageService


class OptimizationPageView(BaseView):
    def __init__(self, request):
        super(OptimizationPageView, self).__init__(request)
        self.optimization_service = OptimizationService(self.session)

    def single_manage(self):
        _ = self.localizer
        return {}

    def webset(self):
        """
        網站管理
        :return:
        """
        _ = self.localizer
        
        key = "webset"

        webset_obj = self.optimization_service.get_by(key=key)
        if webset_obj:
            webset_json = webset_obj.__json__()
        else:
            webset_json = {}

        return {"webset": webset_json,
                'key': key}
        


class OptimizationJsonView(BaseView):
    def __init__(self, request):
        super(OptimizationJsonView, self).__init__(request)
        self.optimization_service = OptimizationService(self.session)
        
        repository_path = self.request.registry.settings.get('repository_path')
        self.image_service = ImageService(repository_path, self.session)
        check_csrf_token(request)
        

    def info(self):
        """
        創建或更新
        """
        _ = self.localizer

        request_data = form.OptimizationInfoForm().to_python(self.request_params)

        
        optimization_obj = self.optimization_service.get_by(key=request_data['key'])

        # webset
        
        if request_data.get('key') == 'webset':
            image_id = request_data.get('logo')
            image_obj = self.image_service.get_by_id(image_id=image_id)
            if image_obj:
                request_data['logo'] = image_obj.__json__()
            else:
                request_data['logo'] = {}
        

        with transaction.manager:
            if not optimization_obj:
                optimization_obj = self.optimization_service.create(key=request_data['key'],
                                                                    value=request_data)
            else:
                optimization_obj = self.optimization_service.update(old_obj=optimization_obj,
                                                                    value=request_data)
        

        # settings
        self.settings[request_data['key']] = request_data

        return {'response': {'optimization': optimization_obj.__json__()},
                'message': _(u'Optimization 編輯成功')}
