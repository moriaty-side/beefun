from __future__ import unicode_literals

from . import optimization_views as views
from ...base.context import AdminContext
from ...lib.api import return_format
from ...lib.page import return_page_format


def includeme(config):
    #Page
    config.add_route('page.optimization', '/page/optimization', factory=AdminContext)
    config.add_view(
        views.OptimizationPageView, attr='single_manage',
        route_name='page.optimization',
        renderer='templates/optimization.mako',
        decorator=return_page_format,
        permission='login'
    )
    
    config.add_route('page.optimization.webset', '/page/optimization/webset', factory=AdminContext)
    
    config.add_view(
        views.OptimizationPageView, attr='webset',
        route_name='page.optimization.webset',
        renderer='templates/webset.mako',
        decorator=return_page_format,
        permission='login',
    )

    # API
    # optimization create
    config.add_route('api.optimization.info', '/api/optimization', factory=AdminContext)
    config.add_view(
        views.OptimizationJsonView, attr='info',
        route_name='api.optimization.info',
        renderer='json',
        request_method='POST',
        decorator=return_format,
        permission='login'
    )
