# coding=utf8
from __future__ import unicode_literals

import formencode
from formencode import validators


class OptimizationInfoForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True

    
    
    key = validators.String(not_empty=False)

    # meta
    meta_description = validators.String(not_empty=False)
    meta_keywords = validators.String(not_empty=False)

    # ga
    google_type = validators.String(not_empty=False)
    google_gtm_id = validators.String(not_empty=False)
    google_gtm_code = validators.String(not_empty=False)
    google_ga_code = validators.String(not_empty=False)

    # logo
    logo = validators.String(not_empty=False)
    support_email = validators.String(not_empty=False)

