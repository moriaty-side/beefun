# coding=utf8
from __future__ import unicode_literals

from sqlalchemy import (
    Column,
    String,
    JSON
)
from ...base.meta_module import (Base, TimestampTable, GUID, uuid4)
from sqlalchemy_utils import UUIDType

class Optimization(Base, TimestampTable):
    """
    網站設定
    """

    __tablename__ = 't_website_optimization'
    optimization_id = Column('f_website_optimization_id', GUID, default=uuid4, primary_key=True,
                         doc=u"網站設定參數")

    

    key = Column(String(32), nullable=False, doc=u'設定key (GA、OG、Meta)')

    value = Column(JSON, nullable=False, doc=u'設定值')

    def __repr__(self):
        return '<WebsiteOptimization object (optimization_id={0})>'.format(self.optimization_id)

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    @classmethod
    def __likeattribute__(cls, key_word):
        map_args = [i for i in cls.__dict__.keys() if key_word in i and i[:1] != '_']
        return map_args[0] if map_args else None

    def __json__(self):
        js = {
            'optimization_id': str(self.optimization_id),
            
            'key': self.key,
            'value': self.value,
        }
        return js
