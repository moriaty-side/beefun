import transaction
from ...base.base_test import BaseTest, dummy_request
from .optimization_views import OptimizationJsonView as View

class TagAPITest(BaseTest):

    def setUp(self):
        super(TagAPITest, self).setUp()
        self.init_database()

    def test_info(self):
        """
        測試創建
        """
        key = 'test'
        value = "admin@double-cash.com"
        request = dummy_request(self.session)
        request.params = {'key': key, 'support_email': value}
        view = View(request)
        response = view.info()
        self.assertEqual(response.get('message'), 'Optimization 編輯成功')

