# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import io, re, os, magic, logging, hashlib, transaction, requests, base64, uuid
from PIL import Image as PIL_Image
from PIL import ImageDraw, ImageFont, ImageEnhance
from ...base.base_service import BaseService
from ...lib.my_exception import MyException
from .image_domain import Images


NOT_SET = object()
EMPTY_DIC = {}
EMPTY_LIST = []

logger = logging.getLogger(__name__)

class ImageService(BaseService):

    TABLE = Images

    def __init__(self, repository_path, session, logger=None):
        super(ImageService, self).__init__(session, logger=logger)
        self.repository_path = repository_path
        self.font_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'font/Helvetica.dfont')


    def upload_file(self, image, member, **json_data):
        """
        上傳圖檔
        :param image:
        :param member:
        :param json_data:
        :return:
        """
        image.file.seek(0)
        json_data['check_magic'] = magic.from_buffer(image.file.read(), mime=True)
        json_data['image_format'] = json_data['check_magic'].split('/')[-1]

        if self.validate(json_data['check_magic']):
            # 將檔案讀入記憶體並用Pillow Image 開啟
            json_data['image_file_name'] = image.filename
            json_data['image'] = PIL_Image.open(image.file)

            # 存儲到DB
            return self.save_image(member, **json_data)

        else:
            raise MyException(code=2404)

    def upload_url(self, img_url, member, **json_data):
        """
        上傳 URL
        :param img_url:
        :param member:
        :param json_data:
        :return:
        """
        # 下載圖片，依據URL下載
        result = requests.get(url=img_url)
        rep_str = result.content

        # pythom magic 檢查檔案格式
        json_data['check_magic'] = magic.from_buffer(rep_str, mime=True)
        json_data['image_format'] = json_data['check_magic'].split('/')[-1]

        # 如檢查檔案正確，才儲存
        if self.validate(json_data['check_magic']):
            # 將檔案讀入記憶體並用Pillow Image 開啟
            json_data['image_file_name'] = img_url.split('/')[-1]
            json_data['image'] = PIL_Image.open(io.BytesIO(rep_str))
            # 存儲到DB
            return self.save_image(member, **json_data)

        else:
            raise MyException(code=2404)

    def upload_base64(self, image_base64_json, member, **json_data):
        """
        上傳 Base64
        :param image_base64_json:
        :param member:
        :param json_data:
        :return:
        """
        if len(image_base64_json.get('data').split('base64,'))>1:
            rep_str = base64.b64decode(image_base64_json.get('data').split('base64,')[1])
        else:
            rep_str = base64.b64decode(image_base64_json.get('data').split('base64,')[0])
        # pythom magic 檢查檔案格式
        json_data['check_magic'] = magic.from_buffer(rep_str, mime=True)
        json_data['image_format'] = json_data['check_magic'].split('/')[-1]

        if self.validate(json_data['check_magic']):
            # 將檔案讀入記憶體並用Pillow Image 開啟
            json_data['image_file_name'] = image_base64_json.get('filename', 'undefine.'+json_data['image_format'])
            json_data['image'] = PIL_Image.open(io.BytesIO(rep_str))

            # 存儲到DB
            return self.save_image(member, **json_data)

        else:
            raise MyException(code=2404)

    def upload_other(self, member, input_file):
        try:
            # 取得檔案
            data = input_file.file.read()
            # 計算hash值
            sha1string = hashlib.sha1(data).hexdigest()
            #  絕對路徑 和 相對路徑
            origin_name, file_extension = os.path.splitext(input_file.filename)
            # 更改檔名
            file_type = input_file.type if hasattr(input_file, 'type') else file_extension[1:]
            file_path_name, file_path = self.imagepath(file_type, sha1string + file_extension)
            temp_file_path = file_path_name + '~'
            # 儲存檔案
            with open(temp_file_path, 'wb') as output_file:
                output_file.write(data)
            # 改檔名
            os.rename(temp_file_path, file_path_name)

            # google upload file
            # upload_to_bucket(file_path_name=file_path_name,
            #                  check_magic=input_file.type)

            # 儲存資料
            result = {}
            result['filename'] = "{}{}".format(sha1string, file_extension)
            result['name'] = input_file.filename
            result['type'] = file_type
            result['size_KB'] = int(os.path.getsize(file_path_name) / 1024)  # KB
            result['filepath'] = file_path
            result['height'] = 0
            result['width'] = 0

            return self.create(create_by=member, **result)


        except MyException as e:
            raise e
        except Exception as e:
            raise MyException(code=2408, message=e)

    def save_image(self, member, **result):
        """
        上傳檔案
        :param result:
        :return:
        """
        try:
            image_file_name = result.get('image_file_name')
            load_image = result.get('image')
            image_format = result.get('image_format')

            # 調整大小(width x height)
            if result.get('width') and result.get('height'):
                load_image = load_image.resize((int(result['width']), int(result['height'])))

            else:
                # 如果沒有指定檔案長寬，且超過1920，限制等比例縮小到1920
                wid_x1, hei_y1 = load_image.size
                if (wid_x1 > 1920):
                    wid_x2 = 1920
                    hei_y2 = (hei_y1 * wid_x2) / wid_x1
                    load_image = load_image.resize((int(wid_x2), int(hei_y2)), PIL_Image.ANTIALIAS)

            # 計算圖檔的hash值 (sha1)
            with io.BytesIO() as tt:
                load_image.save(tt, image_format)
                image_data = tt.getvalue()
                sha1string = hashlib.sha1(image_data).hexdigest()

            origin_name, file_extension = os.path.splitext(image_file_name)

            if not file_extension:
                if image_format == "jpeg":
                    file_extension = ".jpg"
                elif image_format in ["jpg", "gif", "png"]:
                    file_extension = "."+image_format
            result['filename'] = "{}{}".format(sha1string, file_extension)
            result['name'] = '{}{}'.format(origin_name, file_extension)
            result['type'] = image_format

            # 檢查是否傳過該圖片，若有直接回傳
            image_obj = self.get_by_file_name(filename=result['filename'])
            if image_obj:
                return image_obj

            # 加浮水印
            if result.get('watermark_text'):
                image_data, result = self.watermark_image_with_text(origin_filename=image_data, **result)
            else:
                image_data = PIL_Image.open(io.BytesIO(image_data)).convert('RGBA')

            # 取得檔案 絕對路徑 和 相對路徑
            file_path_name, file_path = self.imagepath(result.get('check_magic'), result['filename'])

            # 存儲檔案
            #if result['type'] == 'jpeg' or result['type'] == 'jpg':
            image_data = image_data.convert("RGB")

            image_data.save(file_path_name)

            result['width'], result['height'] = self.get_file_width_height(file_path_name)
            result['size_KB'] = int(os.path.getsize(file_path_name) / 1024)  # KB
            result['filepath'] = file_path

            # 上傳到Google
            # from ..other_service.google_bucket import upload_to_bucket
            # upload_to_bucket(file_path_name=file_path_name,
            #                  check_magic=result.get('check_magic'))

            return self.create(create_by=member, **result)

        except MyException as e:
            raise e

        except Exception as e:
            raise MyException(code=2411, message=e)

    def get_by_file_name(self, filename, check=False):
        """
        抓取資訊依據 file_name
        :param file_name: 檔案名稱
        :param check_mechanism: 嚴格檢查機制
        :return: Image Object
        """
        image = self._get_by(filename=filename)

        if check and not image:
            raise MyException(code=2401)

        return image

    def imagepath(self, file_type, name):
        """
        組合路徑和檔案，形成完整路徑檔 repository_path/image/jpeg
        :param name:
        :return:
        """
        # 路徑加上app_site_id image/jpeg repository_path/image/jpeg
        repository_path = self.repository_path
        type_list = file_type.split('/')
        file_path = "/" + "/".join(type_list) +"/"
        image_dir = repository_path + file_path
        if not os.path.exists(image_dir):
            os.makedirs(image_dir)

        return os.path.join(image_dir, name), file_path

    def validate(self, file_type):
        """
        檢查有效的檔案
        :param file:
        :return:
        """
        IMAGE_TYPES = re.compile('image/(gif|p?jpeg|(x-)?png|vnd.microsoft.icon|x-icon)')

        if IMAGE_TYPES.match(file_type):
            return True

        return False

    def get_file_width_height(self, filename):
        """
        檢查檔案大小
        :param filename:
        :return:
        """
        image = PIL_Image.open(filename)
        width, height = image.size
        return width, height

    def reduce_opacity(self, im, opacity):
        """
        浮水印透明度 0(不透明) 1(透明)
        :param opacity:
        :return:
        """

        alpha = im.split()[3]
        alpha = ImageEnhance.Brightness(alpha).enhance(opacity)
        im.putalpha(alpha)
        return im

    def watermark_image_with_text(self, origin_filename, **kwages):
        """
        加浮水印
        :param text:
        :param color:
        :param fontfamily:
        :return:
        """

        watermark_text = kwages['watermark_text'] = kwages.get('watermark_text', 'doublecash.com')
        watermark_color = kwages['watermark_color'] = kwages.get('watermark_color', (255, 238, 169))
        watermark_opacity = kwages['watermark_opacity'] = kwages.get('watermark_opacity', 1)

        image = PIL_Image.open(io.BytesIO(origin_filename)).convert('RGBA')

        imageWatermark = PIL_Image.new('RGBA', image.size, (255, 255, 255, 0))


        draw = ImageDraw.Draw(imageWatermark)

        width, height = image.size
        margin = 10

        # font = ImageFont.truetype('/System/Library/Fonts/Helvetica.dfont', int(height / 20))
        path_font_file = self.font_file
        font = ImageFont.truetype(path_font_file, int(height / 20))
        textWidth, textHeight = draw.textsize(watermark_text, font)
        x = width - textWidth - margin
        y = height - textHeight - margin

        draw.text((x, y), watermark_text, watermark_color, font)

        # 深度
        imageWatermark = self.reduce_opacity(imageWatermark, watermark_opacity)

        return PIL_Image.alpha_composite(image, imageWatermark), kwages

    def get_by(self, **data):
        return self._get_by(**data)

    def get_by_id(self, image_id, check=False):
        """
        抓取物件依據 image_id
        :param image_id:
        :return: image_obj
        """
        id = image_id if isinstance(image_id, uuid.UUID) else uuid.UUID(image_id)
        obj = self._get_by(image_id=id)
        if check and not obj:
            raise MyException(code=2401)
        return obj

    def create(self, filename, name, **data):
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = []
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 資料過濾
        create_data = {key: data[key] for key in available_args if key in data}

        try:
            create_data['filename'] = filename
            create_data['name'] = name

            if create_data.get('watermark_color'):
                create_data['watermark_color'] = str(create_data.get('watermark_color'))

            with transaction.manager:
                self._create(**create_data)

            image_obj = self.get_by(**{'filename': create_data.get('filename')})
            return image_obj

        except MyException as e:
            raise e
        except Exception as e:
            raise MyException(code=2411, message=e)

    def update_by_id(self, image_id, **data):
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['filename']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            # 檢查 Image
            image_obj = self.get_by_id(image_id=image_id, check=True)

            # 更新 Image
            image_obj = self._update(image_obj, **update_data)

            return image_obj

        except MyException as e:
            raise e
        except Exception as e:
            raise MyException(code=2413, message=e)

    def delete(self, image_obj):
        try:
            image_folder = self.repository_path
            file_path = image_folder + image_obj.filepath + image_obj.filename
            os.remove(file_path)
        except OSError:
            pass

        with transaction.manager:
            self._delete(image_obj)

        return True

    def get_list(self, type=None, status=None, order_by=EMPTY_DIC, group_by=EMPTY_DIC,
                 offset=0, limit=None, page=None, show_count=False, filename=None, filenames=None,
                 image_ids=NOT_SET, name=None, description=None, **search_condition):

        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 可不用參數
        un_available_args = []
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 查詢條件製成
        query = self.session.query(self.TABLE)

        # name
        if name is not None:
            query = query.filter(self.TABLE.name.ilike('%{}%'.format(name)))

        # description
        if description is not None:
            query = query.filter(self.TABLE.description.ilike('%{}%'.format(description)))

        if filename is not None:
            query = query.filter(self.TABLE.filename == filename)

        # 特殊條件
        if filenames is not None:
            query = query.filter(self.TABLE.filename.in_(filenames))

        # 特殊條件
        if image_ids is not NOT_SET:
            u_image_ids = [self._type_to_uuid(image_id) for image_id in image_ids]
            query = query.filter(self.TABLE.image_id.in_(u_image_ids))

        # type
        if type is not None:
            query = query.filter(self.TABLE.type == type)

        # status
        if status is not None:
            status_value = self.TABLE._status.info.get(status, None)
            if status_value is None:
                raise MyException(code=901, message='{0}-{1}'.format(self.TABLE.__name__, 'Status'))
            query = query.filter(self.TABLE._status == status_value)

        # 資料防護
        for key in search_condition:
            if key in un_available_args:
                raise 'input query args is Disable in {0} '.format(self.TABLE.__name__)
            elif key not in available_args:
                raise 'input query args is error in {0} '.format(self.TABLE.__name__)

        return self._get_list(query, order_by, group_by,
                              offset, limit, show_count, **search_condition)