import transaction, os
from ...base.base_test import BaseTest, dummy_request
from .image_domain import Images
from .image_views import ImagesJsonView

class ImageAPITest(BaseTest):

    def setUp(self):
        super(ImageAPITest, self).setUp()
        self.init_database()

    def create(self, name='test', filename='test.png'):
        with transaction.manager:
            obj = Images(name=name, filename=filename)
            self.session.add(obj)

        obj = self.session.query(Images).filter(Images.name==name).first()
        return obj

    def test_file_upload(self):
        """
        測試圖片上傳
        """
        class MockCGIFieldStorage(object):
            pass
        upload = MockCGIFieldStorage()
        upload.file = open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'image.jpeg'), 'rb')
        upload.filename = 'image.jpeg'
        request = dummy_request(self.session)
        request.params = {'image_file': [upload],
                          'name': '123',
                          'description': 'description',
                          # 'width': 100,
                          # 'height': 100,
                          'watermark_opacity': 0.5,
                          'watermark_text': 'Double Cash',
                          'watermark_color': "255, 255, 255"}
        view = ImagesJsonView(request)
        response = view.upload()
        self.assertEqual(response.get('message'), '圖片上傳成功')
        return response.get('response', {}).get('image_list', [])[0]

    def test_url_upload(self):
        """
        測試URL上傳
        """
        request = dummy_request(self.session)
        request.params = {'image_url': ['https://wallpaperbrowse.com/media/images/68640504-full-hd-wallpapers.jpg']}
        view = ImagesJsonView(request)
        response = view.upload()
        self.assertEqual(response.get('message'), '圖片上傳成功')

    def test_base64_upload(self):
        """
        測試URL上傳
        """
        import json
        request = dummy_request(self.session)
        base64_data = [json.dumps({"data": "image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTEhIVFRUVFxcYFxcWFxgYGBgXFxcWGBYVFxcYHSggGBolHRcXITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGy4lICUtLS8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAKgBLAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAQMEBQYABwj/xABEEAABAwIEAwYDBQUGBAcAAAABAAIRAyEEEjFBBVFhBhMicYGRMqGxQlLB0fAHFCNy4RUzYoKS8RZTwtIXNENUY6Ky/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAECAwQF/8QALhEAAgIBAwMCBAYDAQAAAAAAAAECEQMEEiETMVFBYRQiMpEFUnGBofBCsdEj/9oADAMBAAIRAxEAPwCtDUYanAxGGLqsxADUYanAxGGIsQ2Gow1OBiMMSsBsNRhqcDEYYiwGw1EGp0MRBiVgNhqINTgYjDEWMaDUQanQ1EGIsKGg1EGp0NShqVjobDUQanA1EGosKGg1EGpwNSVHtaJcQB1SckuWNJvhA5UWVVdXtJhW/bJ8mP8AxCfw/G6DhmzOaOb2OaPciFHXx/mX3Nehk/K/sTsqUNQ4bFU6nwVGO/lcD9PNSA1WpJ9jNpruNZUuVOhqj4jG0qYl9RrfMifQalDklywUW+EOZUuVQMPx7CvIDazJJgB0tk8vEBdWduaSmmrTG4yi6aG8qXKnLJQE7JoaypcqdyriE7AayrsqF+LpgSXtjNl1+1pl8+iZdxForjD5XZyM02gAhxnWT8MabhKwJGVdlUVmKqGjUqZGy01MonXuyR4jFvhKquH9qBWa1zGt/vQx8PzQ0h0OaYGYkjQTYEosC6xFQMaXGYFzFzHQbqHw/iTKlNryQ3MJAMAgHQESbwqTtvxCk0sYXtabnMSCBANss3dpHhKzHZfj7KdANd3riHOktq02DpZxnSLpOaDazUNYjDE1w/Ed4CcpEOc2+hyuLZHspoYnuFQ2GIwxOBicDEWFDQYjDE6Gog1KwobDEQYnQ1EGpWOhsNRBicDUYYix0NBiIMToYiDEWFDQaiDU6GIg1FjoaDUQanQ1KRGtktwUNhqSo4NBJMAAk+Q1Kr8ZxtjXmmyC4GCSQGtP/URyCqa2JFWq+kKhf4CCYHiNz3YLRZgGunmuLPr4Y+I8s7cOinPmXCLPAcS78Zmwxkkay633rQ21xdR+L4KkQPFcOmSSScsEscSbat5aj1kU20W0gSWZG3AJFovI5eQVdj8Q6owtEskxJEalpkyIGgO594Xh5M+TLK5Nnq48MYfSiHiuEnPIcYIIF7NOwDYgyd9PVLW4VUYCAc0x8YkC4ggCBY7CLBWmDY2A0um+YybON5M6xYR6BWIo/wAMhxkbDfLOnPSyjqNGjaXcyrOGtLBUawFxOUkCXNIhpEjQAnSNIBhT+F8PxLnEtqvptZAy2LXQ61jdo1EWJnawVzwTCA1KjhdjoMbBwiDA1MZr+Q2vdCm1ohoEEzb6/RdMdyTlf/Tky5Y/TRS4nAOfGd7piOTSeYEwPWVHodnKZjP497wB/paAPUhXJe0vyEy4gmNssgJC9gd3bSM8Zsu8TE+UrmlKbdt2Usm1VFV+xnsb2cw1NhcxkOMgCSSSdgJsVBpcDpuzTmaNqYNhcm5O9xpAjndairUl8ZSY+1Agcx7W9ehVN2urdxhqlWIJGVkX8ZnLMjTVEZTlLan3NYyio3IravZ6IcAAMtnZzmNpmMsaDmFT8LxzmFzaFV4vBymWEfayiNRzn5KrwNetig1mJe4tBLg0eFrhAEuIEWED2srag9jWWDWgRAboASQCSb3NvRexg0Uo8zl9jz82tUuIx+5YUeNY2nH8UOZeTUAc6+mU7+vNVXHBXxDw6pVdZsAg5BlJBcABAvA2vClAuJEX5wI/2R/2aDym3WV6UY8Ujz5St2yubw9xcXd64l5LnHYuJJJ+Z6XU8iuHCoa7szRDTm8cQRrroSnm8PDIk21j8zy0sm++Zchw12v/AEVUSQcTg3EZe9rQbFoLnAyIMztcj1TeF4XkcQGktyjWQCZOxtp9VLxFU3yZjblv6KZw+lVcCSInSdh5f1TUUFlDjMLEjK0/4R8V/QlV+CwIyAg68qeaOhO5V7x8EeEEAwZ1zQYBgaQb7KFwZrjSb4q4iwyVCGx0Hy9FLSugssOyHGQ6nlORrKepJJJzF0NaI6Fa+lBEggjmF4x2daH4hjCYDrfaMyT4fCCbz08wvZ8FTYwCkHSWtFiZMaTckxt6Kdw9o6Gow1GGow1LeG0bDUYanA1EGpbx7QA1EGpwNRBqNw9o2Gow1GGog1G8e0ANRBqMNRBqW8No2Gog1OALIca49XNZ9KhlDKcBz9yTrBmwGlr6rPJmjBWzXFhlkdRNBj8aGB0RLW5jOgHuJKzPGOI03wSTL6WZ7QRYAgMaPJ5Lp18KqqmEc9xfU8OaZdnDfDcl5cdQfwSCn/Dg035S3vHOfAIa0w0E8wCBA1vqvKy6ieR9+PCPVxaWGP8AXyMGvmaWtgGTc3dGxzRtJ8yjo4ZjjAElt3bgAiYcetvnzVM/jbe7eGgmoSAHfCI3MC4i/uhZjfC+aj/FfIMzQXbTGoB+iuGknL2FPWY4+5fDEycxqAjNdoOXKBaGg2G0nknKrKrmBzw4k6QA0Nba7o5mNisp31YBviI5GADbfr5p6k97gc73ZtQTJnnqb+av4CflEfHw8Mv2teS1suIbeYFiTqMv1POVrcPXJi8iwPz62/rNlicNxSoxgDsuW05rX199Ezh8dis+Jcx2TvQ3KXRAyNcLMdJEgmSBvMrKehyS4K+Mxm8r8Wp4cvcXgSwQ0kw4knxED7uUz/MAslT/AGlHvnHuB3b4aDmyvDWkwdxvMKu7TcUqYqjRw4pAOZBeQJzEi7sx0BIHL2VD/YdSwMNsdT+S6cGiWz/0/Q482oTl8pseLduBTxpcG52MFRkA+UDl8Q+ar+x3aCpV4l3ld3961zdbNEy1jegJsqhvBc8AktJ+IQZ1+Ik6AosFwo06oMZ7+CTAN4+Kbmx0Wy0cNjVctUZvUS3ex6zjeLYZmaazLXcGkONtoH0WB4xxGpi3EuHhFqdOTDRzIB+Ixc/gE3WZUcRTOWX65RAA5Am/O5J+q7ih7lgFK7nCPKNHfVPBo4YXa7k5NRKa2lfWbkLGiBBMmJggTAJ2T/B2VfF4SBYDML2cTp7eyGnVL4kAQSRGxtJ8zYK4w2BfEiplnWRIH4LsUTnbGP3ggEHMCLmGknzk2UjBYpsmHOdz0F+pifmo9SnVPhDXEfacGyf5j8+ei0HDsC1xytpiRY2mJEguPrKtEsqm56hJnI0bkz8zcp7C4Ckxsgh3Xz5TZXWK4W9v2BUHp6Ag/wBVEyN0NKHDWWj/AHj0Q0wHcO1sQDHoEVWuxvxva2ZiSB9VCqmJ/hj2GvnF/l6KPW+GCLHUHK2PL0nmnyIz3G8bQLiW02ukmT3jiCQSJgGDr6SoGH4kxrGNNFriGi8vv/8AWPZLxAUpcRlAE3Od5mLCdOcfiFRiuRYZfZp/BYt8lUPcAwxq1w0VHB7pILQTJvYkObGmvNeycG4e2lTaAxrXQA4gQSQAJNzsOZ8yvF+BvY0uLi0ANiHNqODjMn4CPsg6mF6/2Yqh9IVIYM0iGU+7AyucIjMSdNZ22usckqNoRsugEYCbY8GemqcBWPVNOmEAjAQAog5LqD6YYCIBAHIg5HUDphgIgEAcl7wc0uoPphgIgsvj+3mBpOe01S5zNmNc4OMTDXAZTGmsSrfhnFDXZnbTexp07wZSQdw3WNdYlLJl2K5BGG50iHj+LOL6lFk03Me1ubXM1zM3h/xbcwDPlUDhji0MgscRdwAgmWgGDOyv8RhQQcxGhLnmB76afgoFHi1GmYl1Z02yaX0lzoE7WnQLzHLLqJ/Kj0oPHhgQMLhqbKPePqOy+NhLrkua8tMNPxXZYdeSpe0WID6bcrnOYKbbkZTUdl+ItFtT5arV4zhYrsfULmw1r8rWXAt8LXaXtJueqyPHXNp0DBE5A1scyI+S9TBoum90+WcObVua2x7GfNFtIBzhbQRqSJkmdRP6KtOFUKdUHLJgXtedR6D6qi4lWdUyfeDQCIjxTe3XX1U3C4gUsPlayKjnguLp+ACQRzkmPfou1HEWtamzMR92Ad9SRFtTZ1hyTGIpvLmhhAaABcS69y3KbC5N1Dw2Lawl7nySSXHcSCctupmyv8NQzNBOYA30udxFxCpKxNkelhXZvEbiwPSx+cDTkjOBDRmMZT9dNfkrUnDNHjNUZQJPhj3VZxLi7KlqbCabTLswAJAF7iefJU6Qu5Jw3DrkxOkjSBFvW6rcZimaAEBpkECb6XPKbeq7EcZJs1+UnWDr0Jifr5KrxeLzuBBMi+YwYAiAYmLDlqdFLn4HRe8GwzcTiGtcAKYBeYOsQA0kGw0nyhXPF34WmMlFlMOP2msFtrEfaWX4ZWcwFzTL3jLsAG7kgdf6px9WWHxE5xAMF3xTII8hsqT4Ex11cCS27tJOgO7Rz29uVlU16bqg+IXMTOvT6eymDAZWOl0CD8WrjJEhp+EREAFJwXB+KX2cJi+s7+cFSMl8OwTQQ0G8Aza8bxvp81ainlgOYBJ+I39G8iobsKwg3Oa2gmOUmycq4Z0jNVdABOWRM7XIsOqtElwKomLfiSuwfGBTJa0CXeJodIMACfDHl7qgxFZrBYvZfxQ2SRobu+qgY3EVO8bT7zM0OyuDfAJLQ8eOOQiZ5o3UFGqxPFKjzGfc/CDB6GDMeZVW2q//AJgbLb+EQL2IBsJ6kqLR4k5wZdrWvJa0AtJsCZJOyRtCno/NW0zAy5k6nKCYGye6xUWZc4ASXGJ5GYGpcS0c9VU46o52GNVj2jMSNfulw10HRWFQdcrR1uQNAQZ9xyWU4mQamVjQ0OgtLiSRMyIgkGYMbJSdIEVtenUy/DAdJk5b5b6yoOQq6xZrMpwXM1AJbr5kjX221VXUEbrJloiUqY5ies36BXnCePYqi2GPIZOYiG+KTJ8RE3VPhKZJMAHWxifO/mrDieOqODWkDKNm02U50+4Li3NKUbXI02uwg4viTVdVDyHue15jSWj7s6XVjxfjeMqPDhXNi4tyAsyh0eHS+m5KouG1IeJBNxIBielusLVU6zX2FMg7tMNPpPxDqE44osHOSLZvbuoGmMOXHM2C52UZcrcwIAMkuD72sQmHdusZJLaFEAgQCHmImTIcJ/ogo4camB0j8006i28mY5N/KyFpsa9B9aZzu3XEBfLQjl3bv++U2e3uNOYTTBd8J7uMn8oJMj+bNohqNt8Lvx+qaNJh0HnDRz0knVHQh4F1ZAM7U41z2ziHgnwzDALkXIDQFYUOzOIrFzauOGWplL8z3EPLAGtc7LINhHUDyVLU8DhlMRDgTFjM2vaFuuB8cx4q5SwucG58jg8FwdliJ23HqpnBRVxRUJbn8zB7PdlMFhy1z6jqrwSYDYaCDbUeRiVd4rjNVxLKTcsT4iCSY2F7frRSeD9raDndxi6T8PUBM5yRq7XXSd1oMZ2TbU8dGu4EiR4iWkHldcTjjcryJ378/wCjptpfK0YHGNe8gvzVDyNh0kRHy0UQuIbnLGjpqRqCQP1votHxjs5iad4c8DUsJJA8lmDp3ZcYnQjef0F1wcKqFfsYyUu8rK/+1q9fPh2vLKUum2ombHUGdipH7m2dZnqSbbgaDdM0+DVaQfWbDqQLpZYEQAbNOpvYLm1qczlAMbiDB2jfQ+y3il6mDI9bDFrnFpEkiHSCNZvFxa36hJiOGQCS5ryYzaQeo+7z6bpMRiZLg1osRbSToJO+iStxBsEd2GkXItA9ZvI90MYNWnTiQ4tIywGkai5kQpnC+KuAc18wwkSNSZJuQPoNwq+rVpxYSTGUtHM3kzby5qZwzh7iHOfo46bg6fqOQUgPYzGmpOW9gcozHaRqMsqqxNA5mzmaTNtJsSXBup0AVni8LkEtMQAJvpoJmyqcRXJLTBdB1tbaJ13BQ6Dkj44MGZrRoPCIIIgi14mcxNk7QFJx1DRGgHtJHkLpjGVGklwsSLayZy/SD7pzDvYCPDIiTcyBGh8lPqBPpNzVA1niEAHJ4RkiwLueuykM4ZkDWl58U6OJJaDvFrdOaiUanjDmWtJy/dGxB3T7eIFwDjT+EmIAFielufsrVeoiZVwGYgZwAfIkegsCnG4Gm0audqXeIaeQNpUatjMpB7sEW2g++6L96Y4fAQTa4HUzZVwHI+/ibQzw5pkDKSNyBJnXyUg4xozZBLogATBPIk2Hkq6nw8gEtjNbaBFiQd9FJbhGwQLkCRm08j00TEVfG31auUnK3LPhz5nu0kZQItHPdVtfD4gktDDB8RgWBiLuiJ2U3jDHU4lrXOcbOGoiNWmRF1EGIqZrNpja4EefMLOTV8jRFc2tRe2WtsbXBBJEanTXopdShW8TnOEnZtwDGxnpCccTIc4Uo2DM5IPUQJS1MYTaGjzmPaUriOmdgKQaS9xOkRJLj1b4ba81H4njs4DQ7wkg3zeG2kncTe2oT9TH1LQ1o6xPsq1r5+MFwGgkAfKDHqhyVcBTGcU8WFjBN+d7T/W6j1Hgk7oqtAkkkRPKw+qWjgHOEtgjnIH4qR9hnuXbG3r+SfIeRDnmOQKd7wJS/op3MZGpUg0gg3HVWArSIOYjlmP+yYkJf8yN7CkSKL43cBewLh8xqnWVm38JJPNxP1ChsaBvP65BcXHl8kdRipEs1R9xvnCcYQfDN9rAD0soIqH9BKap5fJNZGg2j7qZDmwWuBImSLXGs2jqF9KntDhA0O/eaIa4SD3jQCIm0nkQvl6oM259ENOmGnUlDlY1wfUuHr4XEsc9jqVVrgWOcINh9knb4tOqdwHDqVFgZRGRgAAaPhEcgvl+ni6gaWhzg06tBIaf5hMEqZwztFi8Kc1HEVWC0tzSy0atdI29rKHGL7opTZ7/AMc4niaRijRpVHWMOrmn4TaY7s79VE47wv8AeKFRuSmKpb/Ce5vwmAbkXg35x1XmtD9qFQ1A+vh2OcWd2alNxBAzZgRTcSDcXEiROi2XCOOsxDM1IlxdmLQ6WgAEjLG5nlyXl6lZMMlJdv73O/AoZE16mBx/Z7iDC9z7hhyueA14BgG8OJFiDcbqE+qKlDK8kkAguLS24JgieX5r1rh2OqQczW+N8uI0dOVokG4OUNHosX2mxFB7wwU2gtzB2UZBJeSAW8wDB6yuzS6uWeWyjDPpliW6zzqs1waw3Lj6mZsPPRWWFwFSpRdmBz5hlLwRb7TY13V8zEBgAa0NHIQOk21MJP3vMbxHv/svSWNeTh3FNS4Y7RzBA3BFxvMGysqNUsAEGBseQ5KwY5p5BC4g7/kq2JdhbhWVqJA7ym5xIg+L8NFC4hgaQGenLW2zCZsdVKLGnYA8x+oUTFnLuMqUkNMbdwyfgAJ1vEgHT1VVjMPliNRPUQTv5firKjj4IE7euqaxYYfFFydDz3P65LNxRVgcLoOqHuxBdBc0jmNR1SV8G64zFsfZ2t+iiw2I7mpTey5bAy82wQflKuuJcXoVh8Lmv+yS3fkeiaSoTKGhi3hpBE6g5uZMiN9xZDwrGFrvFebTsOSkPaCcps4zB/Pr+SrsUHsHr6JDLjvWTOQF50N5+XopVSq8ZZZY26zt/sqnB4+YJsQI6JzGcWA+ElzhP8o/qnuSQh/iXDu9AADgQeZB6xz9FS4rAZCPGb6wQSOUhdV4hXdq8ieVtNNFGBI1KxlJMY85kfDUJ82j80TZA1lMZ026rzj3/osttlWSi8boHPCZDwlBT2gEYO6aLh94eyVzo5eoXa8k0hDJd5LhUGyYy8iia31VUgHg49EpKCV2ZKgDzFddN5wkFUJ0A4ajhpdB37pvKRz026oOadAPHE9SEw7FkFTeD8Dq4pwbSkkmPhJA3u4WC3uE/ZF/D/iYqKkWDWSwHrJl3pCtQC0ebsxY9VIp5zo2Oun1W3f+yauCctekRtZwMfNXnBf2WUqdRrq9c1mNgmmAWtLuRvdvTdGz2FaMT2U7N4rF1sjQwUxOao8EtEfZBaJzdLL1Wj2LxGVgfjv7poZTDKDWhrR9n4pK1FEta0NaA1oEACwA5AJwVVUoJqmuBxm4u0zIYvg+IpFoBdUzAAnMADEwTGl4Nl4rjRXZVe2of4ge7PMyXSZJvzX0y9wIgrEdp+xLq9XvKbqYmA6QZ/mJBgx0APmuTDpejkbj2f8AB05dT1YJS7r+TxyniK+xHzUmnia/NvrJ+pWuxnY6vTjOWiZiAYsTImDsPmo9fszVYWyLu+GZg9NNVs9Rig6lKjNYMslaiZ5lTEH7Q9vxTze//wCZHz+q2uE7E1TOZ4tILWj2hxP4KbR7P0mfCJqaEPAMCYJHI8isMn4jhj9LbNYaLLLvSMfgsLiHkDNIO5AaNL3PkrzC9mKxqhriCw0nvDx8IqAtDWmRezp02V6zhTy25EyC3SIjWRePyWlwGDIYA4yW2Pp9QuHL+I5J8R4OuOjxw5lyee8d7JUqGAh9QfvMksMWfDgSxvKBad5KxdHAVdXVQyNLlx9gvYuJdlqGNrA1KtWmcuWabgJgmGtD2kNvJtr81WYj9j9PbGVcoJmQ0u6XELq0uZdO5Nvyc2oxvfSR5fjKzmNDQGONyXi8ny2TGAqPr1mMP23BttRO4Xq//h9RpVKAz1KoNQZi4gWa1ziPCPhMCxnQpl/YFtDiVCtRH8BznOLf+WWtmJ3BMxyhOWshyo329fIlpm2m/wCoqePdgsTRY6ox3eNYMwgeK02y6m3rbdZHFYnvKcOtAzW1JA0X0DiKwnKZgix/X6uvMv2g9kgwPxVDKGxNSmLQZ+Jg0i4kdCVODWqUtswyaaluiebMxCdBlC1ucACxJjaT5+yYfhagncCfkbrscTjHyeo90meFBzOGyU4l3KEtoEzvEhLf1dRDjXIRjOgT2sCcANj9EsdVC/exySfvQ8ktrGTYO8n1SCVFGIC7948/dOmFkEv6o2VjumsyCFpQWSzVPMoHPnVC3DPInK72KQ0iNZHmEUAefqia9BTpE2aCfIT9FYYDgtaqYZScfMR9UUBEbU6A+qQglbTgXYGo4ziPA3k0gk6HyW3wvZfBsjLRba/qND9fdUsbJc0d+zHDmlgmhwguc52l7n+i1/eqtY8AQLAJMQ1tRjmPaHNcCHNNwQditaM7Bx3arC0pzVgSNWs8Z9cunqsvxL9prdKDI/xVNf8ASLD1JTOL/Z5hyZo1atEfdnO30zX9yUyf2c0PtYmp6hgClplpopa/bbFufDa1SXfZaT6BrR+C9D7Cux3dOdjHHxQabX3qNF5L+U28Oo6aKq4FwrAYFxe2sHVCMsvc0uaNw0NEifwV9/b1CJ7wezp9oTSfqJvwXwqohVWbPaWgNXOjnkd+ARN7S4bTvgPMOH1CdCs0LyHCHAEdVX4vAZgA10NDmkCB4SPtNPSdElDH03/A9rvJwJ9gnhVWOXT48qqaNceeeN3Fldgsc5tSpSc27LgiYcD4pvpPLbmplejIlzTJEGDp4hpztJRVGtdcgSNDuPIoKofBDXjS0jS2sheLn/DMkXePlHp4tdCX1cMrsDmps8T+fKQ4mPbaOisKONAGZxvpAGkc/msxjsJiYDcr3ZRqIObznUocBhar3HMyqN8rvhdOtzFvfRcvw2X8r+x1vLif+S+5fYXiGaoKk+GkNvtF0ECOQgHnp63WA7Rte4McQMzQQepE66beayWF4NiAQJpsZ4p1OsXj+uysWdn6dsz3GDNjlk3uY3Ersw6PPfhe5y5dRg/V+xpH4lpggyelwkpYibFw/X6KrX4NmQMaSwD7u9ovOqhVuFVLmnWLSeYnfe8fJLJoNQnxTJhqsDXPBf4k2JESOehG6pji++kRaCCCNiI0Oo187qo/s/iDnGazA3kbgjyF1O/4fkDNiKsgWiABIgwOX5LP4DPPmqNPisMOLsxFfsRTptcKdb+LILM4Ia5s3bMWMCVROwdRpe1zD4T4iLt1gEHkvTcXwWmxmd+IrZWzJkepmLLL4rjWAoOqFmatUHwZrtkHnvoL8iu/DDUw+tqjkyz00l8t2ZCpTEXhM1QD9kHlpKd4rxZtWIosp+ImRJJnYmVX965dW84yQcOw6tA+SR/D6e3109FGLzuVwJS3AFU4ZT5x6Jmpwto+18inMzkmbqUbmBFdgRzSHh/VS7HddnA3RuYFRkKvOAdnH1/FmAaCAefWFDwjC5sCJBJE72mPktz2WpllBs6meXPot4Rt8kzlSFwfY/DNMuzP8zbTeNVPwvZvCt/9MOjTMZA91JFVOCqujYjLcx/DYWlT+Cm1vkAFG4hx7D4dzWOnMQSA0fiYCcFVM4zDU6oy1GNeOouPI6j0Q14Be5WO7V1nE5RTaDMTLjG03AJ9EbeM4pwtUA8msn5qi4z2drMObDEuZ9wnxN/ln4h8/NZbEYmqDD8zTycCD7FYuTXc1ST7HoZ4hX3xBHmR/wBKadjqh+LEvPRv9SvPRiTzKNmNePtFLex7D0RuOJEGT1cZnzUeqxxnK4QdBoAFi6fF37mU8zjTp8V/KyOoG012HZkBc+oCOU2HXTVDV4jT5SeiyNbicxFvM284TDuIzu79ck+p4Ft8mprcZ2Bj5wmanECT/eEC1v8AdZY4l3X2SOqVCp3srajVji4YZDo5XM++y9G7M8bGJoCpuCWO6loFx5yPmvDTScdSvVP2e4apSwsVJGao57QbENIaNNpIJ9VUJNsiaSRtRVSiooIqpRVWtEWTu9RCqoAqpRVRQWTxVSiqoIqIu8RQWTRVSmqqrG49tJhe+zW6+SxPFu3byXNotGW4DjMnkY2USko9xpNno9bGNaCXEADn6fmsfxvt61ojD+J06uHhjmI3Xn+M4rWqnxvJMRc7KCWmdVzyzN9i1EtuIdoq9UFrqsNP2W2F/ruqlwGxXFvMoCAsu40dHquKQhCSP0bKqHQuey4vQGpt/VC143ToYZPRAHuOw+SXMBufwXPcOaYAEnlHkUQcf0VzPNOT5e6ANXwfCim0S0A+V5EjNOum3mrNjwNFBFRGKi7Vwc75J4qohVUAVEYqJ2FE8VkQqqAKiIVUWKiwFVc8h1nAEdQD9VBFVEKiADr8Nw7/AIqNM9coB9xdQKvZfDH4czfIz/8AoFTxVSiok4xfoVbRRv7HjaqI6sv7hyT/AIKZH99f+S31WgFVEKqnpxHvkY+v2Lqg+F1Nw5yQfaPxXf8ACGI/+M/5j+S2QqoxVS6UR9RmOZ2Or86Y/wAx/BqnUOxh+1WA6NbPzJH0WkFVEKiOlEW9kThnZzD0iHRncNC+CByhuk9Veiqq/vUQqq1FLsS3ZYCqiFVV4qpRVToRYd6lFRQBVRCqgCeKqYx3EWUWF9R2Vo+uwA3KZ75YbtrxHvKgphwLaeoAMh/MnQ2O3VROW1WVFWxntD2hqYh1pbTGjTvzJ/JUjpSA8kBzFcLbk7ZskOTzSPfyKTu+aLuW7iEuAED0gPVcWckhPSwTGc52uibNVJlM7eqUiRYKqQB94PLmhlu5SMYd/wAFzmeVkAdUAKXIClygJEANuY2UWUbXSwf0EBJ5JgawVEQqLly7TAIVEYqJFyADD0QeuXIAIVEQekXIAMVEQeuXIHQXeIhUXLkCCFREKi5cgKCFRKKq5cmAQqIhUXLkCoLvEoqLlyB0KKqLvFy5MQFfFBrS46Bef4qp3lR75+JxIJ5SY+S5cuTUt8I1ghsjokaEi5cxYRB2hLlHPouXJADlRk81y5MBrO0mEZAXLk2qCgHNQCnvCVchMBcPSLnBuhc4AdJIH4rfu/ZPV1/fKXL+7dsY+8uXLSKsVi/+E1T/AN3S/wBDuv8Ai6IXfspeD/5ul/od/wBy5crpCP/Z"})]
        request.params = {'image_base64': base64_data}
        view = ImagesJsonView(request)
        response = view.upload()
        self.assertEqual(response.get('message'), '圖片上傳成功')

    def test_other_upload(self):
        """
        測試其他上傳
        """
        class MockCGIFieldStorage(object):
            pass
        upload = MockCGIFieldStorage()
        upload.file = open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'other.pdf'), 'rb')
        upload.filename = 'other.pdf'
        request = dummy_request(self.session)
        request.params = {'other_file': [upload]}
        view = ImagesJsonView(request)
        response = view.upload()
        self.assertEqual(response.get('message'), '圖片上傳成功')

    def test_search(self):
        """
        測試 創建圖片
        """
        request = dummy_request(self.session)
        view = ImagesJsonView(request)
        response = view.search()
        self.assertEqual(response.get('message'), '圖片搜尋成功')
        self.assertEqual(response.get('response', {}).get('total_count'), 0)
        self.assertEqual(response.get('response', {}).get('total_page'), 0)
        self.assertEqual(len(response.get('response', {}).get('image_list', [])), 0)

    def test_get(self):
        """
        測試 讀取圖片
        """
        image = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'image_id': str(image.image_id)}
        view = ImagesJsonView(request)
        response = view.get()
        self.assertEqual(response.get('message'), '圖片讀取成功')

    def test_update(self):
        """
        測試 更新帳號
        """
        name = '123'
        description = '456'
        image = self.create(name=name)
        request = dummy_request(self.session)
        request.matchdict = {'image_id': str(image.image_id)}
        request.params = {'name': name, 'description': description}
        view = ImagesJsonView(request)
        response = view.edit()
        self.assertEqual(response.get('message'), '圖片修改成功')
        self.assertEqual(response.get('response', {}).get('image', {}).get('description'), description)

    def test_delete(self):
        """
        測試 更新帳號
        """
        image_json = self.test_file_upload()
        request = dummy_request(self.session)
        request.matchdict = {'image_id': image_json.get('image_id')}
        view = ImagesJsonView(request)
        category_resp = view.delete()
        self.assertEqual(category_resp.get('message'), '圖片刪除成功')

