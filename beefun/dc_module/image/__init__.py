from __future__ import unicode_literals

from . import image_views as views
from ...lib.api import return_format
from ...base.context import AdminContext
from ...lib.page import return_page_format

def includeme(config):
    # Page
    config.add_route('page.image', '/page/image', factory=AdminContext)
    config.add_view(
        views.ImagesPageView, attr='manager',
        route_name='page.image',
        request_method='GET',
        renderer='templates/image.mako',
        decorator=return_page_format,
        permission='login'
    )

    # API
    config.add_route('api.image', '/api/image', factory=AdminContext)
    config.add_view(
        views.ImagesJsonView, attr='search',
        route_name='api.image',
        renderer='json',
        request_method='GET',
        decorator=return_format,
        permission='login'
    )

    config.add_view(
        views.ImagesJsonView, attr='upload',
        route_name='api.image',
        renderer='json',
        request_method='POST',
        decorator=return_format,
        permission='login'
    )

    config.add_route('api.image.info', '/api/image/{image_id}', factory=AdminContext)
    config.add_view(
        views.ImagesJsonView, attr='get',
        route_name='api.image.info',
        renderer='json',
        request_method='GET',
        decorator=return_format,
        permission='login'
    )
    config.add_view(
        views.ImagesJsonView, attr='edit',
        route_name='api.image.info',
        renderer='json',
        request_method='POST',
        decorator=return_format,
        permission='login'
    )
    config.add_view(
        views.ImagesJsonView, attr='delete',
        route_name='api.image.info',
        renderer='json',
        request_method='DELETE',
        decorator=return_format,
        permission='login'
    )