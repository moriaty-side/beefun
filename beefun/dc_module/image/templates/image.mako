<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <!-- Latest compiled and minified CSS & JS -->
    <link rel="shortcut icon" href="${request.static_url('beefun:static/favicon.ico')}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- 圖片裁切 -->
##     <link href="${ request.static_path('beefun:static/assets/cropper/cropper.min.css')}" rel="stylesheet" type="text/css"/>
##     <link href="${ request.static_path('beefun:static/assets/cropperMultiple/cropperMultiple.css')}" rel="stylesheet" type="text/css"/>
##     <link href="${ request.static_path('beefun:static/assets/cropper/cropper_plugin.css')}" rel="stylesheet" type="text/css"/>
    <!-- 圖片裁切 -->

    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css"/>
    <!-- 圖片預覽 -->
</head>
<body>

    <!--portlet-body-->
    <div class="message"></div>
    <div class="portlet-body">
        <form class="form-horizontal" id="search_form">
            <!--按鈕列-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="actions btn-set">
                        <button type="button" class="btn yellow btn-search"><i class="fa fa-search"></i>查詢</button>
                        <button type="button" class="btn blue btn-add" onclick="show_upload_image_file()"><i class="fa fa-search"></i>圖片上傳</button>
                        <button type="button" class="btn blue btn-add" onclick="show_upload_image_cropper()"><i class="fa fa-search"></i>圖片預覽上傳</button>
                    </div>
                </div>
            </div>
            <!--按鈕列 end-->
            <!--資料01-->
            <div class="row ">
                <div class="col-xs-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label class="control-label col-xs-4">名稱:</label>
                        <div class="col-xs-7">
                            <input type="text" class="form-control" placeholder="" name="name">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label class="control-label col-xs-4">描述:</label>
                        <div class="col-xs-7">
                            <input type="text" class="form-control" placeholder="" name="description">
                        </div>
                    </div>
                </div>
            </div>
            <!--資料01 end-->
        </form>
        <!--資料02 -->
        <table class="table table-striped table-hover">
            <thead>
                <tr class="green">
                    <th> #</th>
                    <th> 圖片</th>
                    <th> 名稱</th>
                    <th> 描述</th>
                    <th> <!--操作-->&nbsp;</th>
                </tr>
            </thead>
            <tbody class="image_list">
            </tbody>
        </table>
        <!--資料02  end-->
        <!--頁碼-->
        <div id="page-selection"></div>
        <!--頁碼  end-->
    </div>
    <!--portlet-body end-->

    <div id="upload_image_file" class="modal fade" role="dialog" aria-labelledby="myModalLabel10">
        <!--modal-dialog-->
        <div class="modal-dialog modal-lg">
            <!--modal-content-->
            <div class="modal-content">
                <div id="bounce-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"></button>
                        <h4 class="modal-title">上傳圖片</h4>
                    </div>
                    <div class="modal-body">
                        <form action="#" class="form-horizontal" role="form" id="upload_image_file_form">
                            <!--資料01-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label col-xs-4"><span class="required">*</span>圖片：</label>
                                        <div class="col-xs-7">
                                            <input type="file" class="form-control" placeholder="" name="image">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4"><span class="required">*</span>描述：</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" placeholder="" name="description">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer no-line">
                                <button type="submit" class="btn btn-success uppercase pull-right" >上傳圖片</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--modal-content end-->
        </div>
        <!--modal-dialog end-->
    </div>

    <div id="update_image" class="modal fade" role="dialog" aria-labelledby="myModalLabel10">
        <!--modal-dialog-->
        <div class="modal-dialog modal-lg">
            <!--modal-content-->
            <div class="modal-content">
                <div id="bounce-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"></button>
                        <h4 class="modal-title">上傳圖片</h4>
                    </div>
                    <div class="modal-body">
                        <form action="#" class="form-horizontal" role="form" id="update_image_form">
                            <!--資料01-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label col-xs-4"><span class="required">*</span>名稱：</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" placeholder="" name="image_id" style="display: none;">
                                            <input type="text" class="form-control" placeholder="" name="name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4"><span class="required">*</span>描述：</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" placeholder="" name="description">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer no-line">
                                <button type="submit" class="btn btn-success uppercase pull-right update_btn" >確認修改</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--modal-content end-->
        </div>
        <!--modal-dialog end-->
    </div>

    <!-- 圖片裁切 -->
    <div id="upload_preview_image_file" class="modal fade" role="dialog" aria-labelledby="myModalLabel10">
        <!--modal-dialog-->
        <div class="modal-dialog modal-lg">
            <!--modal-content-->
            <div class="modal-content">
                <div id="bounce-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"></button>
                        <h4 class="modal-title">上傳圖片</h4>
                    </div>
                    <div class="modal-body">
                        <form action="#" class="form-horizontal" role="form" id="upload_preview_image_file_form">
                            <!--資料01-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group ">
                                        <label class="control-label col-xs-4"><span class="required">*</span>圖片：</label>
                                        <div class="col-xs-7">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 344px; height: 178px;">
                                                    <img src="http://www.placehold.it/344x178/EFEFEF/AAAAAA&amp;text=378x80" alt=""/>
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 344px; max-height: 178px;">
                                                    <img src="" alt="">
                                                </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                    <span class="fileinput-new"> 選擇圖片 </span>
                                                    <span class="fileinput-exists"> 更換</span>
                                                        <input type="file" name="image" id="image_web" onchange="readImage(this,378,80);">
                                                    </span>
                                                    <a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"> 移除 </a>
                                                </div>
                                            </div>
                                            <div class="help-block">尺寸：378x80</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4"><span class="required">*</span>描述：</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" placeholder="" name="description">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer no-line">
                                <button type="submit" class="btn btn-success uppercase pull-right" >上傳圖片</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--modal-content end-->
        </div>
        <!--modal-dialog end-->
    </div>
    <!-- 圖片裁切 -->

</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js"></script>

<script src="${ request.static_path('beefun:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
<script src="${ request.static_path('beefun:static/assets/make_table/make_table.js')}" type="text/javascript"></script>

<!-- 圖片裁切 -->
## <script src="${ request.static_path('beefun:static/assets/cropper/cropper.min.js') }" type="text/javascript"></script>
## <script src="${ request.static_path('beefun:static/assets/cropperMultiple/cropperMultiple.js') }" type="text/javascript"></script>
## <script src="${ request.static_path('beefun:static/assets/cropper/cropper_plugin.js') }" type="text/javascript"></script>
<!-- 圖片裁切 -->

<!-- 圖片預覽 -->
<script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
<script src="${ request.static_path('beefun:static/assets/other/readimage.js') }" type="text/javascript"></script>
<!-- 圖片預覽 -->

<script>

    var csrfToken = "${request.session.get_csrf_token()}";

    <!-- 圖片裁切 -->
##     <%
##         import json
##         json_str_images = json.dumps([])
##         print(json_str_images)
##     %>
##     $('.cropper-oragin').eq(0).cropperList({
##         imgList: ${json_str_images| n },
##         ratio: '1/1',
##         itemWidth: '50%',
##         uploadUrl: "${ request.route_url('api.image')}",
##         deleteUrl: "${ request.route_url('api.image.info', image_id='')}"
##     });

    ##  var callback = function(return_data,element){
	 ##   $('#bounce-window').find('.cropper-oragin.active-cropper img').attr('src', return_data).closest('.active-cropper').removeClass('active-cropper');
    ##    $(element).modal('hide');
    ##    $('#bounce-window #bounce-content').show();
    ##  };
    ##
    ##  $('.cropper-oragin').cropper_plugin(options={}, callback=callback);
    <!-- 圖片裁切 -->

    function show_upload_image_cropper(){
        $('#upload_preview_image_file').modal('show');
    }

    var Table = new MakeTable({
        'dom_search_btn': $('.btn-search'),
        'dom_table_tbody': $('.image_list'),
        'ajax_search_url':  "${ request.route_url('api.image') }",
        'ajax_csrf_token':csrfToken,
        'table_model': function (obj, page) {
            //開始產生頁面資料
            var $list = obj['image_list'];
            var html_str = '';
            //var data_table_row = [];
              console.log(obj)
            $.each($list, function (i, item) {
                item.index = (i + 1 + ((page - 1) * 10));
                var tb_row = " <tr>" +
                        "<td>" + item.index + "</td>" +
                        "<td><img src='"+ "${ request.static_path('beefun:static/uploads')}" + item.url + "' alt='" + item.description+ "' style='width:200px;'></td>" +
                        "<td>" + item.name + "</td>" +
                        "<td>" + item.description+ "</td>" +
                        "<td>" +
                        "<button class='btn blue btn-add update_btn' onclick=\"show_update('"+item.image_id+"')\">修改</button>"+
                        "<button class='btn blue btn-add delete_btn' onclick=\"delete_image('"+item.image_id+"')\">刪除</button></td>"+
                        "</tr>";
                html_str += tb_row;
            });
            $('.image_list').append(html_str);//Table頁面資料繪製
        }
    });
    Table.start();

    function show_upload_image_file() {
        $('#upload_image_file').modal('show');
    }

    function show_update(image_id){
         // 讀取 image
         var url = "${ request.route_url('api.image.info', image_id='')}"+image_id;
         ajax(url, "GET", null, $('.update_btn'), function(response){
             console.log(response);
             if (response['status']) {
                  var item = response['response']['image'];
                  $('#update_image_form').find("input[name='image_id']").val(item.image_id);
                  $('#update_image_form').find("input[name='name']").val(item.name);
                  $('#update_image_form').find("input[name='description']").val(item.description);
                  $('#update_image').modal('show');
              } else {
                  alert(response['message']);
              }
         });
    }

    function delete_image(image_id){
        if(confirm("${ _(u'確認要刪除圖片') }")){
             var url = "${ request.route_url('api.image.info', image_id='')}"+image_id;
             ajax(url, "Delete", null, $('.delete_btn'), function(response){
                 console.log(response);
                 if (response['status']) {
                      $('.btn-search').click()
                  } else {
                      alert(response['message']);
                  }
             });
         }
     }

    $('#upload_image_file_form').submit(function(e) { e.preventDefault(); }).validate({
          ignore: ":hidden",
          errorElement: 'div', //default input error message container
          errorClass: 'help-block', // default input error message class
          focusInvalid: true, // do not focus the last invalid input
          rules: {
              image: {
                  required: true,
              },
              description: {
                  maxlength: 250,
              }

          },
          messages: {
              account: {
                  required: "請輸入帳號"
              },
              description:{
                  maxlength: "最長不可以超過 250 個字"
              }

          },
          highlight: function (element) { // hightlight error inputs
              $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
          },
          success: function (label) {
              label.closest('.form-group').removeClass('has-error');
              label.remove();
          },
          errorPlacement: function (error, element) {
              error.insertAfter(element);
          },
          submitHandler:function(form){
              var form_json = {};
              $(form).serializeArray().map(function(v, i){
                  if(v['name'] != "image")
                   form_json[v['name']] = v['value'];
              });
              console.log("form_json:",form_json);

              var form_data = new FormData();
              for(var k in form_json){
                  form_data.append(k, form_json[k]);
              }
              // 儲存圖片
              form_data.append('image_file', $('#upload_image_file_form').find("input[name='image']")[0].files[0])

              var url = "${ request.route_url('api.image') }";
              ajax(url, "POST", form_data, $('.registory_btn'), function(response){
                  if (response['status']) {
                      console.log(response);
                      $('#upload_image_file').modal('hide');
                      //清除
                      $('#upload_image_file_form').find("input[name='description']").val("");
                      $('.btn-search').click()
                  } else {
                      alert(response['message']);
                  }
              });

          }
     });

    $('#upload_preview_image_file_form').submit(function(e) { e.preventDefault(); }).validate({
          ignore: ":hidden",
          errorElement: 'div', //default input error message container
          errorClass: 'help-block', // default input error message class
          focusInvalid: true, // do not focus the last invalid input
          rules: {
              image: {
                  required: true,
              },
              description: {
                  maxlength: 250,
              }

          },
          messages: {
              account: {
                  required: "請輸入帳號"
              },
              description:{
                  maxlength: "最長不可以超過 250 個字"
              }

          },
          highlight: function (element) { // hightlight error inputs
              $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
          },
          success: function (label) {
              label.closest('.form-group').removeClass('has-error');
              label.remove();
          },
          errorPlacement: function (error, element) {
              error.insertAfter(element);
          },
          submitHandler:function(form){
              var form_json = {};
              $(form).serializeArray().map(function(v, i){
                  if(v['name'] != "image")
                   form_json[v['name']] = v['value'];
              });
              console.log("form_json:",form_json);

              var form_data = new FormData();
              for(var k in form_json){
                  form_data.append(k, form_json[k]);
              }
              // 儲存圖片
              form_data.append('image_file', $('#upload_preview_image_file_form').find("input[name='image']")[0].files[0])

              var url = "${ request.route_url('api.image') }";
              ajax(url, "POST", form_data, $('.registory_btn'), function(response){
                  if (response['status']) {
                      console.log(response);
                      $('#upload_preview_image_file').modal('hide');
                      //清除
                      $('#upload_preview_image_file_form').find("input[name='description']").val("");
                      $('.btn-search').click()
                  } else {
                      alert(response['message']);
                  }
              });

          }
     });

    $('#update_image_form').submit(function(e) { e.preventDefault(); }).validate({
          ignore: ":hidden",
          errorElement: 'div', //default input error message container
          errorClass: 'help-block', // default input error message class
          focusInvalid: true, // do not focus the last invalid input
          rules: {
              name: {
                  maxlength: 128,
              },
              description: {
                  maxlength: 250,
              }
          },
          messages: {
              name :{
                  maxlength: "最長不可以超過 128 個字"
              },
              description:{
                  maxlength: "最長不可以超過 250 個字"
              }
          },
          highlight: function (element) { // hightlight error inputs
              $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
          },
          success: function (label) {
              label.closest('.form-group').removeClass('has-error');
              label.remove();
          },
          errorPlacement: function (error, element) {
              error.insertAfter(element);
          },
          submitHandler:function(form){
              var form_json = {};
              $(form).serializeArray().map(function(v, i){
                   form_json[v['name']] = v['value'];
              });
              console.log("form_json:",form_json);

              var form_data = new FormData();
              for(var k in form_json){
                  form_data.append(k, form_json[k]);
              }
              var url = "${ request.route_url('api.image.info', image_id='') }"+form_json['image_id'];
              var method = "POST";
              ajax(url, method, form_data, $('.update_btn'), function(response){
                  if (response['status']) {
                      console.log(response);
                      $('#update_image').modal('hide');
                      //清除
                      $('#update_image_form').find("input[name='image_id']").val("");
                      $('#update_image_form').find("input[name='name']").val("");
                      $('#update_image_form').find("input[name='description']").val("");
                      $('.btn-search').click()
                  } else {
                      alert(response['message']);
                  }
              });

          }
     });

    function ajax(url, method, form_data, btn, callback){
        $.ajax({
            url: url,
            type: method,
            data: form_data,
            contentType: false,
            processData: false,
            headers: {'X-CSRF-Token': csrfToken},
            beforeSend: function () {
                $(btn).attr('disabled', true);
            },
            error: function (xhr) {
                $(btn).attr('disabled', true);
                alert('Ajax request 發生錯誤');
             },
            success: function (response) {
                callback(response);
            },
            complete: function () {
                $(btn).attr('disabled', false);
            }
        });
    }

</script>
</html>