# coding=utf-8
from __future__ import unicode_literals
import calendar
from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime, Float
from sqlalchemy_utils import UUIDType
from ...base.meta_module import (Base, TimestampTable, GUID, uuid4)

class Images(Base):
    """圖片"""
    __tablename__ = 't_images'
    image_id = Column('f_image_id', GUID, primary_key=True, default=uuid4, doc=u"圖片編號")
    filename = Column('f_file_name', String(128), nullable=False, doc=u'檔案名稱(hash值)')
    filepath = Column('f_file_path', String(32), nullable=True, doc=u'儲存位置')
    description = Column('f_description', String(256), nullable=True, default='', doc=u'圖片描述')
    alt = Column('f_alt', String(256), nullable=True, default='', doc=u'圖片alt')
    position = Column('f_position', Integer, nullable=True, default=0, doc=u'圖片位置')
    name = Column('f_name', String(128), nullable=False, default='', doc=u'圖片名稱')
    width = Column('f_width', Integer, nullable=True, doc=u'圖片寬度')
    height = Column('f_height', Integer, nullable=True, doc=u'圖片長度')
    type = Column('f_type', String(64), nullable=True, doc=u'檔案類型')
    size_KB = Column('f_size', Integer, nullable=True, doc=u'圖片大小(KB)')

    create_by = Column('f_create_by', UUIDType(binary=False), doc=u'創建者')
    created = Column('f_created', DateTime, nullable=False, default=datetime.utcnow(), doc=u'創建日期時間')

    def __repr__(self):
        return '<Image object (filename={0})>'.format(self.filename)

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    @classmethod
    def __likeattribute__(cls, key_word):
        map_args = [i for i in cls.__dict__.keys() if key_word in i and i[:1] != '_']
        return map_args[0] if map_args else None

    def __json__(self, detail=False):
        d = {
            'image_id': str(self.image_id),
            'url': '{}{}'.format(self.filepath, self.filename),
            'name': self.name,
            'alt': self.alt,
            'position': self.position,
            'description': self.description,
            'width': self.width,
            'height': self.height,
            'type': self.type
        }
        if detail:
            d['filename'] = self.filename
            d['filepath'] = self.filepath
            d['size_KB'] = self.size_KB
            d['filename'] = self.filename
            d['created'] = calendar.timegm(self.created.timetuple())

        return d