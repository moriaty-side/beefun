# coding=utf8
from __future__ import unicode_literals

import formencode
from formencode import ForEach, validators


class UploadImageFormValid(formencode.Schema):
    """
    圖片上傳
    """
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    image_file = ForEach(validators.FieldStorageUploadConverter())
    image_url = ForEach(validators.String(not_empty=False))
    image_base64 = ForEach(validators.String(not_empty=False))
    other_file = ForEach(validators.FieldStorageUploadConverter())

    name = validators.String(not_empty=False, strip=True)
    description = validators.String(not_empty=False, strip=True)

    width = validators.Int(not_empty=False)
    height = validators.Int(not_empty=False)

    watermark_opacity = validators.Number(not_empty=False) # 浮水印 0:顯示 1.不顯示 0~1
    watermark_text = validators.String(not_empty=False)  # 浮水印文字
    watermark_color = validators.String(not_empty=False) # rgb (255, 238, 169)

    # i18n
    # image_i18n_data = validators.String(not_empty=False)
    # language_id = validators.String(not_empty=False)
    # description = validators.String(not_empty=False)
    # alt = validators.String(not_empty=False)


class UpdateImageFormValid(formencode.Schema):
    """
    圖片更新
    """
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    name = validators.String(not_empty=False, strip=True)
    description = validators.String(not_empty=False, strip=True)

class SearchImageFormValid(formencode.Schema):
    """
    圖片查詢
    """
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    filename = validators.String(not_empty=False, strip=True)
    name = validators.String(not_empty=False, strip=True)
    type = validators.String(not_empty=False, strip=True)
    status = validators.String(not_empty=False, strip=True)

    order_by = validators.String(not_empty=False)
    page = validators.Int(not_empty=False)
    limit = validators.Int(not_empty=False)



