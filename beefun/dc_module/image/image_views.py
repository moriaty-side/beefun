# coding=utf8
from __future__ import unicode_literals
import os
import io
import json
import uuid
import magic
import base64
import shutil
import requests
import hashlib
import transaction
from PIL import Image
from . import image_form as form
from ...base.base_view import BaseView
from ...lib.my_exception import MyException
from .image_service import ImageService


class ImagesPageView(BaseView):

    def __init__(self, request):
        super(ImagesPageView, self).__init__(request)
        repository_path = self.request.registry.settings['repository_path']
        self.image_service = ImageService(repository_path, self.session)

    def manager(self):
        """
        圖片管理頁面
        """
        _ = self.localizer

        return {}

class ImagesJsonView(BaseView):

    def __init__(self, request):
        super(ImagesJsonView, self).__init__(request)
        repository_path = self.request.registry.settings['repository_path']
        self.image_service = ImageService(repository_path, self.session)

    def search(self):
        """
        取得圖片
        :return:
        """
        _ = self.localizer

        # 檢查輸入參數
        json_data = form.SearchImageFormValid.to_python(self.request_params)

        # 分頁機制
        current_page = json_data.get('page', 1)
        limit = json_data.get('limit', 20)
        json_data['offset'] = (current_page - 1) * limit
        json_data['limit'] = limit
        json_data['order_by'] = [('name', -1)]
        json_data['show_count'] = True

        with transaction.manager:
            img_json_list, count = self.image_service.get_list(**json_data)

        return {'response': {'image_list': [im.__json__() for im in img_json_list],
                             'total_count': count,
                             'total_page': (int(count / limit) + 1 if count % limit else 0)},
                'message': _(u'圖片搜尋成功')}

    def upload(self):
        """
        上傳圖片
        :return:
        """
        _ = self.localizer

        # 檢查輸入參數
        json_data = form.UploadImageFormValid.to_python(self.request_params)

        # 如果有 watermark_color 轉換格式
        if json_data.get('watermark_color', None) is not None:
            json_data['watermark_color'] = tuple(int(n) for n in json_data['watermark_color'].split(','))

        # 回傳資料
        response = []

        # 上傳檔案，多筆
        if json_data.get('image_file') is not []:
            for image in json_data.get('image_file'):
                # if json_data.get('length'):
                #     if json_data.get('length') != image.bytes_read:
                #         raise MyException(message=u'檢查圖片長度錯誤')
                try:
                    # 上傳檔案
                    image_obj = self.image_service.upload_file(image=image,
                                                               member=self.account_id,
                                                               **json_data)
                    response.append(image_obj.__json__())

                except MyException as e:
                    raise e
                except Exception as e:
                    raise MyException(code=2405, message=e)

        # 上傳URL，多筆
        if json_data.get('image_url') is not []:
            for image_url in json_data.get('image_url'):
                try:
                    image_obj = self.image_service.upload_url(img_url=image_url,
                                                              member=self.account_id,
                                                              **json_data)
                    response.append(image_obj.__json__())
                except MyException as e:
                    raise e
                except Exception as e:
                    raise MyException(code=2406, message=e)

        # 上傳Base64，多筆
        if json_data.get('image_base64') is not []:
            for image_base64_str in json_data.get('image_base64'):
                # 反解Json Format
                image_base64_json = json.loads(image_base64_str)
                if image_base64_json.get('data'):
                    # 如檢查檔案正確，才儲存
                    try:
                        image_obj = self.image_service.upload_base64(image_base64_json=image_base64_json,
                                                                     member=self.account_id,
                                                                     **json_data)
                        response.append(image_obj.__json__())
                    except MyException as e:
                        raise e
                    except Exception as e:
                        raise MyException(code=2407, message=e)

        # 上傳其他檔案，多筆
        if json_data.get('other_file') is not []:
            for input_file in json_data.get('other_file'):
                try:
                    file_obj = self.image_service.upload_other(input_file=input_file,
                                                               member=self.account_id)
                    response.append(file_obj.__json__())

                except MyException as e:
                    raise e
                except Exception as e:
                    raise MyException(code=2407, message=e)

        return {'response': {'image_list': response},
                'message': _(u'圖片上傳成功')}

    def get(self):
        """
        圖片讀取
        :return:
        """
        _ = self.localizer

        image_obj = self.image_service.get_by_id(image_id=self.request.matchdict['image_id'],
                                                 check=True)

        return {'response': {'image': image_obj.__json__()},
                'message': _(u'圖片讀取成功')}

    def edit(self):
        """
        圖片修改
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.UpdateImageFormValid().to_python(self.request_params)

        with transaction.manager:
            self.image_service.update_by_id(image_id=self.request.matchdict['image_id'],
                                            **request_data)

        image_obj = self.image_service.get_by_id(image_id=self.request.matchdict['image_id'])

        return {'response': {'image': image_obj.__json__()},
                'message': _(u'圖片修改成功')}


    def delete(self):
        """
        刪除圖片
        :return:
        """
        _ = self.localizer

        image_obj = self.image_service.get_by_id(image_id=self.request.matchdict['image_id'],
                                                 check=True)

        with transaction.manager:
            delete_status = self.image_service.delete(image_obj)

        return {'response': delete_status,
                'message': _(u'圖片刪除成功')}