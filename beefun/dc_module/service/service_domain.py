# coding=utf8
from sqlalchemy import (Column, Integer, String, DateTime, Text, ForeignKey)
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from ...base.meta_module import (Base, TimestampTable, GUID, uuid4)

DeclarativeBase = declarative_base()

class Service(Base, TimestampTable):
    """
    產品 資料表
    """
    __tablename__ = 't_service'

    service_id = Column('f_service_id', GUID, primary_key=True, default=uuid4, doc=u"產品ID")

    language = Column('f_language', String(12), index=True, nullable=False, default="zh_Hant", doc=u"多國語系語系")

    title = Column('f_title', String, nullable=True, doc=u'標題')

    category_id = Column('f_category_id', ForeignKey('t_category.f_category_id'), nullable=False, doc=u"分類")

    main_image_id = Column('f_main_image_id', ForeignKey('t_images.f_image_id'), nullable=False, doc=u"封面圖片")

    # product_image_id = Column('f_product_image_id', ForeignKey('t_images.f_image_id'), nullable=False, doc=u"產品圖")

    light_image_id = Column('f_light_image_id', ForeignKey('t_images.f_image_id'), nullable=True, doc=u"照度圖")

    sequence = Column('f_sequence', Integer, nullable=False, default=0, doc=u'排序')


    light_mode = Column('f_light_mode', String, nullable=True, doc=u'光源模式')




    _status = Column('f_status', Integer, nullable=False, default=10,
                     info={"show": 10, "hiden": 20, "delete": 40},
                     doc=u"狀態:10.顯示 , 20.不顯示, 40.封存")








    def __repr__(self):
        return '<ProductionObject (product_id={0})>'.format(self.product_id)

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @hybrid_property
    def popular(self):
        if hasattr(self.__class__, '_popular'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._popular.info
            for k in info_dic.keys():
                if info_dic[k] == self._popular:
                    return k

    @popular.setter
    def popular(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._popular.info.get(value)
            if not v:
                raise Exception('popular column input value {} is error'.format(value))
            self._popular = v
        else:
            raise Exception('popular column input value_type {} is error')

    @popular.expression
    def popular(cls):
        return cls._popular

    @hybrid_property
    def popular_type(self):
        if hasattr(self.__class__, '_popular_type'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._popular_type.info
            for k in info_dic.keys():
                if info_dic[k] == self._popular_type:
                    return k

    @popular_type.setter
    def popular_type(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._popular_type.info.get(value)
            if not v:
                raise Exception('popular_type column input value {} is error'.format(value))
            self._popular_type = v
        else:
            raise Exception('popular_type column input value_type {} is error')

    @popular_type.expression
    def popular_type(cls):
        return cls._popular_type

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    def __json__(self, show_main_image=False, show_llluminance_image=False, show_category=False, show_product_imgaes=False):
        d = {'product_id': str(self.product_id),
             'language': self.language,
             'title': self.title,
             'sequence': self.sequence,
             'category_id': str(self.category_id),
             # 'product_image_id': str(self.product_image_id),
             'main_image_id': str(self.main_image_id),
             'light_image_id': str(self.light_image_id),
             'llluminance_image_id': str(self.llluminance_image_id),
             'popular': self.popular,
             'status': self.status,
             'light_mode': self.light_mode,
             'lamp_size': self.lamp_size,
             'installation_method': self.installation_method,
             'input_voltage': self.input_voltage,
             'watts': self.watts,
             'lighting_effect': self.lighting_effect,
             'luminous_flux': self.luminous_flux,
             'color_temperature': self.color_temperature,
             'beam_angle': self.beam_angle,
             'power_factor': self.power_factor,
             'color_rendering': self.color_rendering,
             'applicable_environment': self.applicable_environment,
             'waterproof_and_dustproof': self.waterproof_and_dustproof,
             'material_weight': self.material_weight,
             'color_form_f_one': self.color_form_f_one,
             'color_form_f_sec': self.color_form_f_sec,
             'color_form_c_one': self.color_form_c_one,
             'color_form_c_sec': self.color_form_c_sec,
             'popular_type': self.popular_type,
             'content': self.content,
             'product_specification': self.product_specification,
             }

        if show_main_image:
            d['main_image'] = self.main_image.__json__() if self.main_image else {}

            d['light_image'] = self.light_image.__json__() if self.light_image else {}

        if show_llluminance_image:
            d['llluminance_image'] = self.llluminance_image.__json__() if self.llluminance_image else {}

        if show_category:
            d['category'] = self.category.__json__() if self.category else {}

        if show_product_imgaes:
            d['product_images'] = [pi.__json__(show_image=True) for pi in self.product_images]

        return d
