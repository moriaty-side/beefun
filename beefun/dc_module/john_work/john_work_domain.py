# coding=utf8
import uuid
from sqlalchemy import (Column, Integer, String, DateTime, Text, ForeignKey)
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from ...base.meta_module import (Base, TimestampTable, GUID, uuid4, now_func)

DeclarativeBase = declarative_base()


class Client(Base, TimestampTable):
    """
    客戶資料表單
    """
    __tablename__ = 'client'
    client_id = Column('client_id', GUID, primary_key=True, default=uuid4, doc=u"文章ID")

    client_name = Column('client_name', String, nullable=True, doc=u'站點名稱')

    client_code = Column('client_code', String, nullable=True, doc=u'名稱')

    line_id = Column('line_id', String, unique=True, doc=u'站點名稱')

    _status = Column('status', Integer, nullable=False, default=10,
                     info={"show": 10, "hiden": 20, "delete": 40},
                     doc=u"狀態:10.上架,20.下架,40:封存")

    def __repr__(self):
        return '<ClientObject (client_id={0})>'.format(self.client_id)

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    def __json__(self):
        d = {'client_id': str(self.client_id),
             'client_name': str(self.client_name),
             'client_code': str(self.client_code),
             'line_id': str(self.line_id)
             }

        return d

class RichMenu(Base, TimestampTable):
    """
    RichMenu資料表單
    """
    __tablename__ = 'rich_menu'
    menu_id = Column('menu_id', GUID, primary_key=True, default=uuid4, doc=u"角色流水編號")

    name = Column('name_id', String(512), doc=u"name")

    rich_menu_id = Column('rich_menu_id', String(512), doc=u"rich_menu_id")

    _status = Column('status', Integer, nullable=False, default=10,
                     info={"show": 10, "auth": 15, "hiden": 20, "delete": 400},
                     doc=u"狀態:10.開通,15.驗證中,20.關閉,400.刪除")

    def __repr__(self):
        return '<RelativesObject (menu_id={0})>'.format(self.menu_id)

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    def __json__(self):
        d = {
            'menu_id': str(self.menu_id),
            'name': self.name,
            'status': str(self.status),
            'rich_menu_id': self.rich_menu_id,
        }


        return d

class Case(Base, TimestampTable):
    """
    案件資料表單
    """
    __tablename__ = 'case'
    case_id = Column('case_id', GUID, primary_key=True, default=uuid4, doc=u"文章ID")

    line_id = Column('line_id', ForeignKey('client.line_id'), nullable=False, doc=u"分類")

    case_name = Column('case_name', String, nullable=True, doc=u'站點名稱')

    _status = Column('status', Integer, nullable=False, default=10,
                     info={"show": 10, "hiden": 20, "delete": 40},
                     doc=u"狀態:10.上架,20.下架,40:封存")

    client = relationship("Client")

    def __repr__(self):
        return '<CaseObject (case_id={0})>'.format(self.case_id)

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    def __json__(self):
        d = {'case_id': str(self.case_id),
             'case_name': str(self.case_name),
             'line_id': str(self.line_id),
             'status': str(self.status)
             }

        d['client'] = self.client.__json__() if self.client else {}

        return d

class Identity(Base, TimestampTable):
    """
    案件資料表單
    """
    __tablename__ = 'identity'
    identity_id = Column('identity_id', GUID, primary_key=True, default=uuid4, doc=u"文章ID")

    client_id = Column('client_id', ForeignKey('client.client_id'), nullable=False, doc=u"分類")

    identity_name = Column('identity_name', String, nullable=True, doc=u'身份名稱')

    identity_code = Column('identity_code', String, nullable=True, doc=u'身份代碼')

    password = Column('password', String, nullable=True, doc=u'密碼')

    _status = Column('status', Integer, nullable=False, default=10,
                     info={"show": 10, "hiden": 20, "delete": 40},
                     doc=u"狀態:10.上架,20.下架,40:封存")

    client = relationship("Client")
    identity_connection = relationship("IdentityConnection")

    def __repr__(self):
        return '<identityObject (identity_id={0})>'.format(self.identity_id)

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    def __json__(self):
        d = {'identity_id': str(self.identity_id),
             'client_id': str(self.client_id),
             'identity_name': str(self.identity_name),
             'identity_code': str(self.identity_code),
             'status': str(self.status)
             }
        d['identity_connection'] = [s.__json__() for s in self.identity_connection]
        d['client'] = self.client.__json__() if self.client else {}

        return d

class Connection(Base, TimestampTable):
    """
    案件資料表單
    """
    __tablename__ = 'connection'
    connection_id = Column('connection_id', GUID, primary_key=True, default=uuid4, doc=u"文章ID")

    client_id = Column('client_id', ForeignKey('client.client_id'), nullable=False, doc=u"分類")

    connection_name = Column('connection_name', String, nullable=True, doc=u'聯絡名稱')

    connection_info = Column('connection_info', String, nullable=True, doc=u'聯絡內容')

    _status = Column('status', Integer, nullable=False, default=10,
                     info={"show": 10, "hiden": 20, "delete": 40},
                     doc=u"狀態:10.上架,20.下架,40:封存")

    _type = Column('type', Integer, nullable=False, default=10,
                     info={"line": 10, "git": 20, "qrcode": 30, "phone": 40},
                     doc=u"狀態:10.上架,20.下架,40:封存")

    client = relationship("Client")

    def __repr__(self):
        return '<ConnectionObject (connection_id={0})>'.format(self.connection_id)

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @hybrid_property
    def type(self):
        if hasattr(self.__class__, '_type'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._type.info
            for k in info_dic.keys():
                if info_dic[k] == self._type:
                    return k

    @type.setter
    def type(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._type.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._type = v
        else:
            raise Exception('status column input value_type {} is error')

    @type.expression
    def type(cls):
        return cls._type

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    def __json__(self):
        d = {'connection_id': str(self.connection_id),
             'client_id': str(self.client_id),
             'connection_name': str(self.connection_name),
             'connection_info': str(self.connection_info),
             'status': str(self.status),
             'type': str(self.type)
             }

        d['client'] = self.client.__json__() if self.client else {}

        return d

class IdentityConnection(Base, TimestampTable):
    """
    案件資料表單
    """
    __tablename__ = 'identity_connection'
    identity_connection_id = Column('identity_connection_id', GUID, primary_key=True, default=uuid4, doc=u"文章ID")

    identity_id = Column('identity_id', ForeignKey('identity.identity_id'), nullable=False, doc=u"分類")

    connection_id = Column('connection_id', ForeignKey('connection.connection_id'), nullable=False, doc=u"分類")

    connection = relationship("Connection")

    def __repr__(self):
        return '<IdentityConnectionObject (identity_connection_id={0})>'.format(self.identity_connection_id)

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    def __json__(self):
        d = {'identity_connection_id': str(self.identity_connection_id),
             'identity_id': str(self.identity_id),
             'connection_id': str(self.connection_id)
             }

        d['connection'] = self.connection.__json__() if self.connection else {}

        return d