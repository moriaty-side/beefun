<!DOCTYPE html>
<html lang="zh-TW">
<head>
    <title></title>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta content="#{description}" name="description">
    <meta content="" name="author">
    <link href="${ request.static_path('beefun:static/dist/assets/favicon.ico')}" rel="icon">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet">
    <style>
        .logo {
            margin-top: 10px;
            margin-bottom: 30px;
        }

    </style>
    <link href="${ request.static_path('beefun:static/dist/css/index.css')}" rel="stylesheet">
</head>
<body>
<div class="container-fluid privacy py-4">
    <div class="row privacy-content">
        <div class="col-12 text-center logo"><img alt="" src="${ request.static_path('beefun:static/dist/images/logo.png')}"></div>
        <div class="col-12 text-center">
            <div class="title text-primary">報修表單</div>
        </div>
        <div class="col-12">


              <form id="send_form">
                    <div class="form-group mb-4">
                    <label class="font-weight-bold">客戶名稱<span class="text-danger">*</span></label>
                    <div class="form-row">

                      <div class="col-9">
                        <input type="text" class="form-control" id="client_name" name="client_name" placeholder="客戶名稱">
                      </div>
                    </div>
                  </div>
              </form>

              <div class="row">
                <div class="col">
                  <div class="btn btn-primary rounded-pill btn-block send_mobile">送出</div>
                </div>
              </div>

        </div>

    </div>
</div>
<script src="${ request.static_path('beefun:static/assets/global/plugins/jquery.min.js')}"></script>
  <script src="${ request.static_path('beefun:static/dist/js/index.js')}"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
    <script src="${ request.static_path('beefun:static/assets/jquery.serialize-object.js')}"
            type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/custom_validation.js')}"
            type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="${ request.static_path('beefun:static/dist/js/index.js')}"></script>
<script src="https://static.line-scdn.net/liff/edge/2.1/sdk.js"></script>
<script src="liff-starter.js"></script>
<script>

      const windowHeight = window.innerHeight;

      var user_id;

      function initializeApp() {
          liff.getProfile().then(function (profile) {
              const userId = profile.userId;
              user_id = userId;
              $('#fname').val(userId);
              const name = profile.displayName;
              const pictureUrl = profile.pictureUrl;
              const statusMessage = profile.statusMessage;
          }).catch(function (error) {
              console.log('error', error);
          });
      }

          ##  測試用 code
                   ##  1655586468-zdXkQ3Za

          ##  正式用 code
                   ##  1655082439-GnLQ2Bvd

      initializeLiff('${line_code}');

      function initializeLiff(myLiffId) {
          liff.init({
              liffId: myLiffId
          }).then(() => {
              // start to use LIFF's api
              initializeApp();
          }).catch((err) => {
              document.getElementById("liffAppContent").classList.add('hidden');
              document.getElementById("liffInitErrorMessage").classList.remove('hidden');
          });
      }

var csrfToken = "${request.session.get_csrf_token()}";

        function ajax(url, method, form_data, btn, callback) {
                    $.ajax({
                        url: url,
                        type: method,
                        data: form_data,
                        contentType: false,
                        processData: false,
                        headers: { 'X-CSRF-Token': csrfToken },
                        beforeSend: function() {
                            $(btn).attr('disabled', true);
                        },
                        error: function(xhr) {
                            $(btn).attr('disabled', true);
                            alert("${ _(u'Ajax request 發生錯誤') }");
                        },
                        success: function(response) {
                            callback(response);
                        },
                        complete: function() {
                            $(btn).attr('disabled', false);
                        }
                    });
                }


        $('#send_form').submit(function(e) { e.preventDefault(); }).validate({
                    ignore: ":hidden",
                    errorElement: 'div', //default input error message container
                    errorClass: 'form-control-feedback', // default input error message class
                    focusInvalid: true, // do not focus the last invalid input
                    rules: {
                        client_name: {
                            required: true
                        },


                    },
                    messages: {
                        client_name: {
                            required: "${ _(u'客戶名稱為必填值')}"
                        },

                    },
                    onfocusout: false,
                    invalidHandler: function(form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            validator.errorList[0].element.focus();
                        }
                    } ,
                    highlight: function (element) { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-danger'); // set error class to the control group
                    },
                    success: function (label) {
                        label.closest('.form-group').removeClass('has-danger');
                        label.remove();
                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element);
                    },
                    submitHandler: function(form) {
                        var add_form_vals = new FormData();

                        $('#send_form').serializeArray().map(function (v, i) {

                            add_form_vals.append(v['name'],  v['value']);

                        });

                        add_form_vals.append('line_id', user_id);

                        var url = "${ request.route_url('api.attendant.get_verification_code')}";
                        var method = "POST";


                        ajax(url, method, add_form_vals, $('#save-btn'), function(response) {
                                    ##  console.log(response);
                                     if (response['status'] ==true) {


                                     } else {
                                         alert(response['message']);
                                     }
                        });

                    }
                });

        $(document).on('click', '.send_mobile', function () {

            $('#send_form').submit();


        });


    </script>
</body>
</html>