<%inherit file="beefun:templates/backend/master.mako"/>

<%block name="css">
    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css"/>
    <!-- 圖片預覽 -->

</%block>

<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span class="m-font-898b96">${ _('案件列表')}</span> <i class="fa fa-caret-right"></i>

                    </h3>
                </div>
                <div>

                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="case_list" data-parent="case">
            <!--begin::Portlet-->
            <div class="languages hide"></div>
            <div class="m-portlet">

                <!--Begin::Section-->
                <!--begin::Form-->

                <form class="m-form m-form--fit m-form--label-align-right" id="search_form">
                    <!--Begin::Section-->
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-6 offset-3 mb-15px">




                                <table class="table m-table m-table--head-bg-success">
                                    <thead>
                                    <tr>

                                        <th> ${ _(u'項目')}</th>
                                        <th> ${ _(u'客戶')} </th>
                                        <th>
                                            <!--操作-->&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody id="case_tbody">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>
</%block>



<%block name="script">
    <!-- 圖片預覽 -->
    <script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/readimage.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/make_table/make_table.js')}" type="text/javascript"></script>
    <!-- 圖片預覽 -->
    <script>
        $('.se1').select2();
        var csrfToken = "${request.session.get_csrf_token()}";
        var language ='${ request.registry.settings.get('available_langs', [['zh_Hant', u'繁體中文']])[0][0] }';


        $(document).ready(function (e) {

            Table.start();
        });


        var Table = new MakeTable({
            'dom_search_form': $('form#search_form'),
            'dom_search_btn': $('i.la.la-search'),
            'dom_table_tbody': $('#case_tbody'),

            'init_search_parameter': '',

            'ajax_search_url':  "${ request.route_url('api.case.list_backend') }",
            'ajax_csrf_token':csrfToken,
            'table_model': function (obj, page) {
                //開始產生頁面資料
                var $list = obj['case_list'];
                var html_str = '';
                //var data_table_row = [];

                $.each($list, function (i, item) {
                    item.index = (i + 1 + ((page - 1) * 10));
                    var status=""

                    var tb_row = " <tr class='obj_index'>" ;
                    tb_row+="<td>";
                    tb_row+="<div class='title'>"+item.case_name+"</div>";

                    tb_row+="</td>";
                    tb_row+="<td>";
                    tb_row+="<div class='title'>"+item.client.client_name+"</div>";

                    tb_row+="</td>";
                    tb_row+='<td align="right">';
                    tb_row+='<button type="button" class="btn btn-accent m-btn m-btn--icon btn-sm m-btn--icon-only sav_cont hide"><i class="fa fa-check"></i></button>';
                    tb_row+='<button type="button" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only edit_cont"><i class="fa fa-edit"></i></button>';

                    tb_row+='</td>';
                    tb_row += "</tr>";
                    html_str += tb_row;
                });
                $('#case_tbody').empty()
                $('#case_tbody').append(html_str);//Table頁面資料繪製
            }
        });

        ajax_reload = Table;

        function ajax(url, method, form_data, btn, callback){
            $.ajax({
                url: url,
                type: method,
                data: form_data,
                ##  dataType: 'JSON',
                contentType: false,
                processData: false,
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    if (btn) btn.attr("disabled", true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                    if (btn) btn.attr("disabled", false)
                },
                success: function (response) {
                    callback(response);
                },
                complete: function () {
                    if (btn) btn.attr("disabled", false);
                }
            });
        }
    </script>
</%block>


