<%inherit file="beefun:templates/backend/master.mako"/>

<%block name="css">
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/allen_assets/cropper/cropper.min.css') }">
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/allen_assets/cropper/cropper_plugin.css') }">
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/allen_assets/diyUpload/css/webuploader.css') }">
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/allen_assets/diyUpload/css/diyUpload.css') }">
    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css" />
</%block>

<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span class="m-font-898b96">${ _(u'身份管理')}</span> <i class="fa fa-caret-right"></i>
                         <span>${ _(u'新增')}</span>
                    </h3>
                </div>
                <div>
                    <button type="button" class="btn btn-metal m-btn m-btn--outline-2x btn-sm mr-5px" onclick="javascript:window.open('${ request.route_url('page.product.list') }','_self')">
                    <span>
                                        <i class="fa fa-close"></i>
                                        <span>
                                            ${ _(u'取消')}
                                        </span>
                    </span>
                    </button>
                    <button type="button" class="btn btn-success m-btn m-btn--outline-2x btn-sm" id="create_btn">
                    <span>
                                        <i class="fa fa-check"></i>
                                        <span>
                                            ${ _(u'新增存檔')}
                                        </span>
                    </span>
                    </button>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="client_list" data-parent="case">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right create_form" id="create_form">
                    <!--Begin::Section-->
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12">


                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'名稱')}
                                    </label>
                                    <div class="col-8">
                                        <input class="form-control" placeholder="" id="connection_name" name="connection_name">
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'內容')}
                                    </label>
                                    <div class="col-8">
                                        <input class="form-control" placeholder="" id="connection_info" name="connection_info">
                                    </div>
                                </div>

                           <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'型態')}
                                    </label>
                                    <div class="col-8 col-md-4">

                                        <div class="class-a">
                                            <select class="form-control se1" id="type" name="type">
                                                 <option value="line">line</option>
                                                 <option value="git">git</option>
                                                 <option value="qrcode">qrcode</option>
                                                 <option value="phone">phone</option>
                                            </select>

                                        </div>

                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>

</%block>



<%block name="script">


    <script src="${ request.static_url('beefun:static/html_fronted_work/backstage/assets/my_js/jquery.serialize-object.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/allen_assets/cropper/cropper.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/allen_assets/cropper/cropper_plugin.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>

    <script src="${ request.static_path('beefun:static/assets/other/custom_validation.js')}"
            type="text/javascript"></script>




    <script type="text/javascript" src="${ request.static_path('beefun:static/assets/other/jscolor.js') }"></script>

    <script>
        var csrfToken = "${request.session.get_csrf_token()}";
        $('.se1').select2();

        $(document).ready(function (e) {

        });

            ## 新增消息表單驗證
          $('.create_form').validate({
            ignore: "",
            errorElement: 'div', //default input error message container
            errorClass: 'form-control-feedback', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {

                connection_name: {
                    required: true,
                    byteRangeLength:60
                }
            },
            messages: {

                connection_name: {
                    required: "標題為必填值",
                    byteRangeLength:"公司名稱最多輸入字數為60(中文算2個字)"
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-danger'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-danger');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            }
        });

            ## 創建登錄公司事件
        $(document).on('click', '#create_btn', function () {
            var event_btn = $(this);
            var $valid = $('.create_form').valid();

            if (!$valid ) {
                return false
            }
            var form_data = new FormData();
            var url = "${ request.route_url('api.connection')}";


            form_data.append('connection_name', $('#connection_name').val());
            form_data.append('connection_info', $('#connection_info').val());
            form_data.append('client_id', '${client_obj.get('client_id')}');
            form_data.append('type', $('#type').val());
            ajax(url, 'POST', form_data, event_btn, function(data){
                        if (event_btn) event_btn.attr("disabled", false);

                        if (data['status']) {
                                     window.location = '${ request.route_url("page.connection.backend.list", client_id='') }' + '${client_obj.get('client_id')}';
                        } else {
                            alert('${_(u"創建失敗")}')
                        }
            });

        });

        function ajax(url, method, form_data, btn, callback){
            $.ajax({
                url: url,
                type: method,
                data: form_data,
                ##  dataType: 'JSON',
                contentType: false,
                processData: false,
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    if (btn) btn.attr("disabled", true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                    if (btn) btn.attr("disabled", false)
                },
                success: function (response) {
                    callback(response);
                },
                complete: function () {
                    if (btn) btn.attr("disabled", false);
                }
            });
        }


    </script>

</%block>


