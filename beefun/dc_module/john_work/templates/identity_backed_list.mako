<%inherit file="beefun:templates/backend/master.mako"/>

<%block name="css">
    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css"/>
    <!-- 圖片預覽 -->

</%block>

<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span class="m-font-898b96">${client_obj.get('client_name')}</span> <i class="fa fa-caret-right"></i>
                        <span class="m-font-898b96">${ _('身份列表')}</span> <i class="fa fa-caret-right"></i>

                    </h3>
                </div>
                    <button type="button" class="btn btn-brand m-btn m-btn--outline-2x btn-sm" onclick="javascript:window.open('${ request.route_url("page.identity.backend.create", client_id='') }${client_obj.get('client_id')}','_self')">
                        <span>
                                        <i class="fa fa-plus"></i>
                                        <span>
                                            ${ _(u'新增')}
                                        </span>
                        </span>
                    </button>
            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="client_list" data-parent="case">
            <!--begin::Portlet-->
            <div class="languages hide"></div>
            <div class="m-portlet">

                <!--Begin::Section-->
                <!--begin::Form-->
                <button hidden type="button"
                        class="btn btn-warning m-btn m-btn--outline-2x btn-sm mr-5px btn-search">
									<span>
										<i class="fa fa-search"></i>
										<span>
											搜尋
										</span>
									</span>
                </button>
                <form class="m-form m-form--fit m-form--label-align-right" id="search_form">
                    <!--Begin::Section-->
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-6 offset-3 mb-15px">




                                <table class="table m-table m-table--head-bg-success">
                                    <thead>
                                    <tr>


                                        <th> ${ _(u'名稱')} </th>
                                        <th> ${ _(u'聯絡方式列表')} </th>
                                        <th>
                                            <!--操作-->&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody id="client_tbody">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>
</%block>



<%block name="script">
    <!-- 圖片預覽 -->
    <script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/readimage.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/make_table/make_table.js')}" type="text/javascript"></script>
    <!-- 圖片預覽 -->
    <script>
        $('.se1').select2();
        var csrfToken = "${request.session.get_csrf_token()}";
        var language ='${ request.registry.settings.get('available_langs', [['zh_Hant', u'繁體中文']])[0][0] }';


        $(document).ready(function (e) {

            Table.start();
        });
        //新增分類


        var Table = new MakeTable({
            'dom_search_form': $('form#search_form'),
            'dom_search_btn': $('i.la.la-search'),
            'dom_table_tbody': $('#client_tbody'),

            'init_search_parameter': '',

            'ajax_search_url':  '${ request.route_url("api.identity.list", client_id='') }' + '${client_obj.get('client_id')}',
            'ajax_csrf_token':csrfToken,
            'table_model': function (obj, page) {
                //開始產生頁面資料
                var $list = obj['identity_list'];
                var html_str = '';
                //var data_table_row = [];

                $.each($list, function (i, item) {
                    item.index = (i + 1 + ((page - 1) * 10));
                    var status=""


                    var tb_row = " <tr class='obj_index'>" ;


                    tb_row+="<td>";
                    tb_row+="<div class='title'>"+item.identity_name+"</div>";

                    tb_row+="</td>";

                    tb_row+="<td>";
                    tb_row+="<div class='title'>";
                    $.each(item.identity_connection, function (i, item) {
                        tb_row+=item.connection.connection_name+"-"+item.connection.type+"</br>";
                    })
                    tb_row+="</div>";

                    tb_row+="</td>";

                    tb_row+='<td align="right">';

                    tb_row+='<button type="button" data-id="' + item['identity_id'] + '" class="btn btn-brand m-btn m-btn--outline-2x btn-sm btn_connection_backed_list">身份修改</button>';

                    tb_row+='</td>';
                    tb_row += "</tr>";
                    html_str += tb_row;
                });
                $('#client_tbody').empty()
                $('#client_tbody').append(html_str);//Table頁面資料繪製
            }
        });

        ajax_reload = Table;

       $(document).on('click', '.btn_identity_backed_list', function () {
          window.location = '${ request.route_url("page.identity.backend.list", client_id='') }' + $(this).data('id');;
      });

             $(document).on('click', '.btn_connection_backed_list', function () {
          window.location = '${ request.route_url("page.connection.backend.list", client_id='') }' + $(this).data('id');;
      });


        function ajax(url, method, form_data, btn, callback){
            $.ajax({
                url: url,
                type: method,
                data: form_data,
                ##  dataType: 'JSON',
                contentType: false,
                processData: false,
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    if (btn) btn.attr("disabled", true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                    if (btn) btn.attr("disabled", false)
                },
                success: function (response) {
                    callback(response);
                },
                complete: function () {
                    if (btn) btn.attr("disabled", false);
                }
            });
        }
    </script>
</%block>


