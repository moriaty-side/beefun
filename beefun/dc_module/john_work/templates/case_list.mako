<!DOCTYPE html>
<html lang="zh-TW">
<head>
    <title></title>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta content="#{description}" name="description">
    <meta content="" name="author">
    <link href="${ request.static_path('beefun:static/dist/assets/favicon.ico')}" rel="icon">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet">
    <style>
        .logo {
            margin-top: 10px;
            margin-bottom: 30px;
        }

    </style>
    <link href="${ request.static_path('beefun:static/dist/css/index.css')}" rel="stylesheet">
</head>
<body>
<div class="container-fluid privacy py-4">
    <div class="row privacy-content">
        <div class="col-12 text-center logo"><img alt="" src="${ request.static_path('beefun:static/dist/images/logo.png')}"></div>
        <div class="col-12 text-center">
            <div class="title text-primary">案件追蹤</div>
        </div>
        <div class="col-12">

        </div>

    </div>
</div>
<script src="${ request.static_path('beefun:static/assets/global/plugins/jquery.min.js')}"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="${ request.static_path('beefun:static/dist/js/index.js')}"></script>
<script src="https://static.line-scdn.net/liff/edge/2.1/sdk.js"></script>
<script src="liff-starter.js"></script>
<script>

      const windowHeight = window.innerHeight;

      var user_id;

      function initializeApp() {
          liff.getProfile().then(function (profile) {
              const userId = profile.userId;
              user_id = userId;
              $('#fname').val(userId);
              const name = profile.displayName;
              const pictureUrl = profile.pictureUrl;
              const statusMessage = profile.statusMessage;
          }).catch(function (error) {
              console.log('error', error);
          });
      }

          ##  測試用 code
                   ##  1655586468-zdXkQ3Za

          ##  正式用 code
                   ##  1655082439-GnLQ2Bvd

      initializeLiff('${line_code}');

      function initializeLiff(myLiffId) {
          liff.init({
              liffId: myLiffId
          }).then(() => {
              // start to use LIFF's api
              initializeApp();
          }).catch((err) => {
              document.getElementById("liffAppContent").classList.add('hidden');
              document.getElementById("liffInitErrorMessage").classList.remove('hidden');
          });
      }

      $(document).on('click', '.go_to_next', function () {
          window.location = '${ request.route_url("frontend.page.attendant_verification", line_id='') }' + user_id;
      });


    </script>
</body>
</html>