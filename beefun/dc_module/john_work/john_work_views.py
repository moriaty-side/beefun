# coding=utf8
from __future__ import unicode_literals
import transaction, json
from . import john_work_form as form
from ...base.base_view import BaseView
from .john_work_service import ClientService, CaseService, RichMenuService,IdentityService,IdentityConnectionService,ConnectionService
import requests
import uuid

from linebot import LineBotApi, WebhookHandler

from linebot.models import *

# 測試用設定

line_bot_api_code = "flfZdCgM8gGIFNE1m3gpvPLOpgvGnEg0ySH8h7JwSz2OxQOIyaqqGIy/T4a2yR5JBlVjnlqnyN7HAaJJe2OdPA2uh2oLP67ZrsydmFBGHJkR/0WLSX88mHPZC4egiUp6X6jKcBAfOcaK8QrIv7SORQdB04t89/1O/w1cDnyilFU="

line_bot_api = LineBotApi(
    'flfZdCgM8gGIFNE1m3gpvPLOpgvGnEg0ySH8h7JwSz2OxQOIyaqqGIy/T4a2yR5JBlVjnlqnyN7HAaJJe2OdPA2uh2oLP67ZrsydmFBGHJkR/0WLSX88mHPZC4egiUp6X6jKcBAfOcaK8QrIv7SORQdB04t89/1O/w1cDnyilFU=')
handler = WebhookHandler('eede4ac7dec6062c24bf92d6c4774e91')

class ClientPageView(BaseView):
    def __init__(self, request):
        super(ClientPageView, self).__init__(request)
        self.client_service = ClientService(self.session, logger=self.logger)

    def list(self):
        _ = self.localizer
        return {}
    def list_backend(self):
        _ = self.localizer
        return {}

    def create(self):
        _ = self.localizer


        return {}

    def update(self):
        _ = self.localizer
        client_obj = self.client_service.get_by_id(client_id=self.request.matchdict['client_id'],
                                               check=True)

        return {'client_obj': client_obj.__json__(detail=True)}

class CasePageView(BaseView):
    def __init__(self, request):
        super(CasePageView, self).__init__(request)
        self.case_service = CaseService(self.session, logger=self.logger)

    def list(self):
        _ = self.localizer
        return {}

    def list_backend(self):
        _ = self.localizer
        return {}

    def create(self):
        _ = self.localizer


        return {}

    # def update(self):
    #     _ = self.localizer
    #     client_obj = self.client_service.get_by_id(client_id=self.request.matchdict['client_id'],
    #                                            check=True)
    #
    #     return {'client_obj': client_obj.__json__(detail=True)}

class IdentityPageView(BaseView):
    def __init__(self, request):
        super(IdentityPageView, self).__init__(request)
        self.identity_service = IdentityService(self.session, logger=self.logger)
        self.connection_service = ConnectionService(self.session, logger=self.logger)
        self.client_service = ClientService(self.session, logger=self.logger)
        self.identity_connection_service = IdentityConnectionService(self.session, logger=self.logger)

    def list(self):
        _ = self.localizer
        client_obj = self.client_service.get_by_id(client_id=self.request.matchdict['client_id'],
                                               check=True)

        return {'client_obj': client_obj.__json__(detail=True)}

    def list_backend(self):
        _ = self.localizer
        client_obj = self.client_service.get_by_id(client_id=self.request.matchdict['client_id'],
                                               check=True)

        return {'client_obj': client_obj.__json__()}

    def create(self):
        _ = self.localizer

        client_obj = self.client_service.get_by_id(client_id=self.request.matchdict['client_id'],
                                               check=True)
        request_data = {}
        request_data['client_id'] = uuid.UUID(self.request.matchdict['client_id'])

        # 搜尋帳號
        connection_list, total_count = self.connection_service.get_list(show_count=True, **request_data)

        return {'client_obj': client_obj.__json__(),
                'connection_list': [a.__json__() for a in connection_list]
                }

    def update(self):
        _ = self.localizer
        identity_obj = self.identity_service.get_by_id(identity_id=self.request.matchdict['identity_id'],
                                               check=True)

        return {'identity_obj': identity_obj.__json__(detail=True)}

class ConnectionPageView(BaseView):
    def __init__(self, request):
        super(ConnectionPageView, self).__init__(request)
        self.connection_service = ConnectionService(self.session, logger=self.logger)
        self.client_service = ClientService(self.session, logger=self.logger)
        self.identity_connection_service = IdentityConnectionService(self.session, logger=self.logger)

    def list(self):
        _ = self.localizer
        print(self.request.matchdict['client_id'])
        client_obj = self.client_service.get_by_id(client_id=self.request.matchdict['client_id'],
                                               check=True)

        print(client_obj)

        return {'client_obj': client_obj.__json__(detail=True)}

    def list_backend(self):
        _ = self.localizer
        client_obj = self.client_service.get_by_id(client_id=self.request.matchdict['client_id'],
                                               check=True)

        return {'client_obj': client_obj.__json__()}

    def create(self):
        _ = self.localizer

        client_obj = self.client_service.get_by_id(client_id=self.request.matchdict['client_id'],
                                               check=True)

        return {'client_obj': client_obj.__json__()}

    def update(self):
        _ = self.localizer
        connection_obj = self.connection_service.get_by_id(connection_id=self.request.matchdict['connection_id'],
                                               check=True)

        return {'connection_obj': connection_obj.__json__(detail=True)}

class ClientJsonView(BaseView):
    def __init__(self, request):
        super(ClientJsonView, self).__init__(request)
        self.client_service = ClientService(self.session, logger=self.logger)
        self.richmenu_service = RichMenuService(self.session, logger=self.logger)

    def notify_test(self):
        """
        創建標籤
        """
        _ = self.localizer

        token = "VodbvDDlQe6flSxiMRIKEDoNnQLXubpg1PH8fSy59mv"

        headers = {
            "Authorization": "Bearer " + token,
            "Content-Type": "application/x-www-form-urlencoded"
        }

        msg = str("案件號：0009988")

        payload = {'message': msg}
        r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)

        return {'response': {},
                'message': _(u'News創建成功')}

    def create(self):
        """
        創建標籤
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.ClientCreateForm().to_python(self.request_params)



        with transaction.manager:
            client_obj = self.client_service.create(**request_data)

            ##richmenu = self.richmenu_service.get_by(name='client_menu')

            ##rich_menu_id = richmenu.rich_menu_id

            ##line_bot_api.link_rich_menu_to_user(request_data.get('line_id'), rich_menu_id)

        return {'response': {'client': client_obj.__json__()},
                'message': _(u'News創建成功')}

    def rich_menu_def(self):
        """
        創建標籤
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.ClientDefForm().to_python(self.request_params)

        richmenu = self.richmenu_service.get_by(name='all_menu')

        rich_menu_id = richmenu.rich_menu_id

        line_bot_api.link_rich_menu_to_user(request_data.get('line_id'), rich_menu_id)

        return {'response': '',
                'message': _(u'News創建成功')}

    def rich_menu_client(self):
        """
        創建標籤
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.ClientDefForm().to_python(self.request_params)

        richmenu = self.richmenu_service.get_by(name='client_menu')

        rich_menu_id = richmenu.rich_menu_id

        line_bot_api.link_rich_menu_to_user(request_data.get('line_id'), rich_menu_id)

        return {'response': '',
                'message': _(u'News創建成功')}

    def search(self):
        """
        搜尋標籤
        """

        # 檢查輸入參數
        request_data = form.ClientSearchForm().to_python(self.request_params)

        # 分頁機制
        current_page = request_data.get('page', 1)
        limit = request_data.get('limit', 10)
        request_data['offset'] = (current_page - 1) * limit
        request_data['limit'] = limit
        request_data['order_by'] = [('created', -1)]

        # 搜尋帳號
        client_list, total_count = self.client_service.get_list(show_count=True, **request_data)

        return {'response': {'client_list': [a.__json__() for a in client_list],
                             'total_count': total_count,
                             'total_page': (int(total_count / limit) + 1 if total_count % limit else 0)
                             },
                'message': u'News搜尋成功'}
    #
    # def get(self):
    #     """
    #     讀取標籤
    #     """
    #
    #     # 用 category_id 取得分類
    #     web_obj = self.web_service.get_by_id(web_id=self.request.matchdict['web_id'],
    #                                            check=True)
    #
    #     return {'response': {'web': web_obj.__json__()},
    #             'message': 'web讀取成功'}
    #
    # def edit(self):
    #     """
    #     編輯標籤
    #     """
    #
    #     # 檢查輸入參數
    #     request_data = form.WebEditForm().to_python(self.request_params)
    #
    #     # 用 category_id 取得分類
    #     web_obj = self.web_service.get_by_id(web_id=self.request.matchdict['web_id'],
    #                                            check=True)
    #
    #     # 更新帳號
    #     with transaction.manager:
    #         web_obj = self.web_service.update(old_obj=web_obj,
    #                                             user_id=self.account_id,
    #                                             **request_data)
    #
    #     return {'response': {'web': web_obj.__json__()},
    #             'message': u'Web修改成功'}
    #
    # def delete(self):
    #     """
    #     刪除標籤
    #     """
    #
    #     # 用 tag_id 取得分類
    #     web_obj = self.web_service.get_by_id(web_id=self.request.matchdict['web_id'],
    #                                            check=True)
    #     # 更新 status 達到軟刪除
    #     with transaction.manager:
    #         self.web_service.delete(old_obj=web_obj)
    #
    #     return {'message': u'Web刪除成功'}

class CaseJsonView(BaseView):
    def __init__(self, request):
        super(CaseJsonView, self).__init__(request)
        self.case_service = CaseService(self.session, logger=self.logger)
        self.client_service = ClientService(self.session, logger=self.logger)


    def create(self):
        """
        創建標籤
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.CaseCreateForm().to_python(self.request_params)
        client_obj = self.client_service.get_by(line_id=request_data.get('line_id'))
        with transaction.manager:
            case_obj = self.case_service.create(**request_data)

            token = "VodbvDDlQe6flSxiMRIKEDoNnQLXubpg1PH8fSy59mv"

            headers = {
                "Authorization": "Bearer " + token,
                "Content-Type": "application/x-www-form-urlencoded"
            }

            msg = str("案件名："+str(case_obj.case_name)+",業主："+str(client_obj.client_name))

            payload = {'message': msg}
            r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)

        return {'response': {'case': case_obj.__json__()},
                'message': _(u'News創建成功')}

    def search(self):
        """
        搜尋標籤
        """

        # 檢查輸入參數
        request_data = form.CaseSearchForm().to_python(self.request_params)

        # 分頁機制
        current_page = request_data.get('page', 1)
        limit = request_data.get('limit', 10)
        request_data['offset'] = (current_page - 1) * limit
        request_data['limit'] = limit
        request_data['order_by'] = [('created', -1)]

        # 搜尋帳號
        case_list, total_count = self.case_service.get_list(show_count=True, **request_data)

        return {'response': {'case_list': [a.__json__() for a in case_list],
                             'total_count': total_count,
                             'total_page': (int(total_count / limit) + 1 if total_count % limit else 0)
                             },
                'message': u'News搜尋成功'}

    def search_f(self):
        """
        搜尋標籤
        """

        # 檢查輸入參數
        request_data = form.CaseSearchForm().to_python(self.request_params)

        request_data['line_id'] = self.request.matchdict['line_id']

        # 分頁機制
        current_page = request_data.get('page', 1)
        limit = request_data.get('limit', 10)
        request_data['offset'] = (current_page - 1) * limit
        request_data['limit'] = limit
        request_data['order_by'] = [('created', -1)]

        # 搜尋帳號
        case_list, total_count = self.case_service.get_list(show_count=True, **request_data)

        return {'response': {'case_list': [a.__json__() for a in case_list],
                             'total_count': total_count,
                             'total_page': (int(total_count / limit) + 1 if total_count % limit else 0)
                             },
                'message': u'News搜尋成功'}
    #
    # def get(self):
    #     """
    #     讀取標籤
    #     """
    #
    #     # 用 category_id 取得分類
    #     web_obj = self.web_service.get_by_id(web_id=self.request.matchdict['web_id'],
    #                                            check=True)
    #
    #     return {'response': {'web': web_obj.__json__()},
    #             'message': 'web讀取成功'}
    #
    # def edit(self):
    #     """
    #     編輯標籤
    #     """
    #
    #     # 檢查輸入參數
    #     request_data = form.WebEditForm().to_python(self.request_params)
    #
    #     # 用 category_id 取得分類
    #     web_obj = self.web_service.get_by_id(web_id=self.request.matchdict['web_id'],
    #                                            check=True)
    #
    #     # 更新帳號
    #     with transaction.manager:
    #         web_obj = self.web_service.update(old_obj=web_obj,
    #                                             user_id=self.account_id,
    #                                             **request_data)
    #
    #     return {'response': {'web': web_obj.__json__()},
    #             'message': u'Web修改成功'}
    #
    # def delete(self):
    #     """
    #     刪除標籤
    #     """
    #
    #     # 用 tag_id 取得分類
    #     web_obj = self.web_service.get_by_id(web_id=self.request.matchdict['web_id'],
    #                                            check=True)
    #     # 更新 status 達到軟刪除
    #     with transaction.manager:
    #         self.web_service.delete(old_obj=web_obj)
    #
    #     return {'message': u'Web刪除成功'}

class IdentityJsonView(BaseView):
    def __init__(self, request):
        super(IdentityJsonView, self).__init__(request)
        self.identity_service = IdentityService(self.session, logger=self.logger)
        self.client_service = ClientService(self.session, logger=self.logger)
        self.identity_connection_service = IdentityConnectionService(self.session, logger=self.logger)

    def create(self):
        """
        創建標籤
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.IdentityCreateForm().to_python(self.request_params)
        with transaction.manager:
            Identity_obj = self.identity_service.create(**request_data)

            for connection in request_data.get('connection_list'):

                print('------------------')
                print(connection)
                print('------------------')

                connection_data = {
                    'identity_id': Identity_obj.identity_id,
                    'connection_id': connection
                }

                self.identity_connection_service.create(**connection_data)

        return {'response': {'Identity': Identity_obj.__json__()},
                'message': _(u'News創建成功')}

    def search(self):
        """
        搜尋標籤
        """

        # 檢查輸入參數
        request_data = form.IdentitySearchForm().to_python(self.request_params)
        request_data['client_id'] = uuid.UUID(self.request.matchdict['client_id'])
        # 分頁機制
        current_page = request_data.get('page', 1)
        limit = request_data.get('limit', 10)
        request_data['offset'] = (current_page - 1) * limit
        request_data['limit'] = limit
        request_data['order_by'] = [('created', -1)]

        # 搜尋帳號
        identity_list, total_count = self.identity_service.get_list(show_count=True, **request_data)

        return {'response': {'identity_list': [a.__json__() for a in identity_list],
                             'total_count': total_count,
                             'total_page': (int(total_count / limit) + 1 if total_count % limit else 0)
                             },
                'message': u'News搜尋成功'}

    def search_connection(self):
        """
        搜尋標籤
        """

        # 檢查輸入參數
        request_data = form.IdentityConnectionSearchForm().to_python(self.request_params)

        # 搜尋帳號
        identity_connection_list, total_count = self.identity_connection_service.get_list(show_count=True, **request_data)

        return {'response': {'identity_connection_list': [a.__json__() for a in identity_connection_list]
                             },
                'message': u'News搜尋成功'}

    def identity_login(self):
        """
        搜尋標籤
        """

        # 檢查輸入參數
        request_data = form.IdentityLoginForm().to_python(self.request_params)

        # 搜尋帳號
        client_obj = self.client_service.get_by(client_name=request_data['client_name'])

        request_data_ind = {
            'client_id':client_obj.client_id,
            'password': request_data['password']
        }

        identity = self.identity_service.get_by(**request_data_ind)

        return {'response': {'identity': str(identity.identity_id)
                             },
                'message': u'News搜尋成功'}

    #
    # def get(self):
    #     """
    #     讀取標籤
    #     """
    #
    #     # 用 category_id 取得分類
    #     web_obj = self.web_service.get_by_id(web_id=self.request.matchdict['web_id'],
    #                                            check=True)
    #
    #     return {'response': {'web': web_obj.__json__()},
    #             'message': 'web讀取成功'}
    #
    def edit(self):
        """
        編輯標籤
        """

        # 檢查輸入參數
        request_data = form.IdentityUpdateForm().to_python(self.request_params)

        # 用 category_id 取得分類
        identity_obj = self.identity_service.get_by_id(identity_id=self.request.matchdict['identity_id'],
                                               check=True)

        # 更新帳號
        with transaction.manager:
            identity_obj = self.identity_service.update(old_obj=identity_obj,
                                                user_id=self.account_id,
                                                **request_data)

        return {'response': {'identity': identity_obj.__json__()},
                'message': u'Web修改成功'}
    #
    # def delete(self):
    #     """
    #     刪除標籤
    #     """
    #
    #     # 用 tag_id 取得分類
    #     web_obj = self.web_service.get_by_id(web_id=self.request.matchdict['web_id'],
    #                                            check=True)
    #     # 更新 status 達到軟刪除
    #     with transaction.manager:
    #         self.web_service.delete(old_obj=web_obj)
    #
    #     return {'message': u'Web刪除成功'}

class ConnectionJsonView(BaseView):
    def __init__(self, request):
        super(ConnectionJsonView, self).__init__(request)
        self.connection_service = ConnectionService(self.session, logger=self.logger)
        self.client_service = ClientService(self.session, logger=self.logger)
        self.identity_connection_service = IdentityConnectionService(self.session, logger=self.logger)


    def create(self):
        """
        創建標籤
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.ConnectionCreateForm().to_python(self.request_params)

        with transaction.manager:
            connection_obj = self.connection_service.create(**request_data)

        return {'response': {'connection': connection_obj.__json__()},
                'message': _(u'News創建成功')}

    def search(self):
        """
        搜尋標籤
        """

        # 檢查輸入參數
        request_data = form.ConnectionSearchForm().to_python(self.request_params)
        request_data['client_id'] = uuid.UUID(self.request.matchdict['client_id'])

        # 分頁機制
        current_page = request_data.get('page', 1)
        limit = request_data.get('limit', 10)
        request_data['offset'] = (current_page - 1) * limit
        request_data['limit'] = limit
        request_data['order_by'] = [('created', -1)]

        # 搜尋帳號
        connection_list, total_count = self.connection_service.get_list(show_count=True, **request_data)

        return {'response': {'connection_list': [a.__json__() for a in connection_list],
                             'total_count': total_count,
                             'total_page': (int(total_count / limit) + 1 if total_count % limit else 0)
                             },
                'message': u'News搜尋成功'}

    #
    # def get(self):
    #     """
    #     讀取標籤
    #     """
    #
    #     # 用 category_id 取得分類
    #     web_obj = self.web_service.get_by_id(web_id=self.request.matchdict['web_id'],
    #                                            check=True)
    #
    #     return {'response': {'web': web_obj.__json__()},
    #             'message': 'web讀取成功'}
    #
    def edit(self):
        """
        編輯標籤
        """

        # 檢查輸入參數
        request_data = form.ConnectionUpdateForm().to_python(self.request_params)

        # 用 category_id 取得分類
        connection_obj = self.connection_service.get_by_id(connection_id=self.request.matchdict['connection_id'],
                                               check=True)

        # 更新帳號
        with transaction.manager:
            connection_obj = self.connection_service.update(old_obj=connection_obj,
                                                user_id=self.account_id,
                                                **request_data)

        return {'response': {'connection': connection_obj.__json__()},
                'message': u'Web修改成功'}
    #
    # def delete(self):
    #     """
    #     刪除標籤
    #     """
    #
    #     # 用 tag_id 取得分類
    #     web_obj = self.web_service.get_by_id(web_id=self.request.matchdict['web_id'],
    #                                            check=True)
    #     # 更新 status 達到軟刪除
    #     with transaction.manager:
    #         self.web_service.delete(old_obj=web_obj)
    #
    #     return {'message': u'Web刪除成功'}

class RichMenuJsonView(BaseView):
    def __init__(self, request):
        super(RichMenuJsonView, self).__init__(request)
        self.richmenu_service = RichMenuService(self.session, logger=self.logger)

    def rich_menu_setting(self):
        """
        創建標籤
        """
        _ = self.localizer

        rich_menu_to_create = RichMenu(
            size=RichMenuSize(width=2500, height=843),
            selected=False,
            name="richmenu",  # display name
            chat_bar_text="菜單 \0",
            areas=[
                RichMenuArea(  # 快速治療
                    bounds=RichMenuBounds(x=0, y=0, width=2500, height=843),
                    action=URIAction(label='註冊', uri='https://liff.line.me/1657557034-ODgqgVl4'))
            ]
        )
        # try:
        #     line_bot_api.delete_rich_menu(ar_id)
        # except:
        #     pass

        rich_menu_id = line_bot_api.create_rich_menu(
            rich_menu=rich_menu_to_create)

        ## 本機端
        # with open('/Users/nagi/python_project/company_pro/iris/iris/static/image/all_menu.png', 'rb') as f:
        #         line_bot_api.set_rich_menu_image(rich_menu_id, 'image/jpeg', f)

        ## 遠端
        url_set = 'https://api-data.line.me/v2/bot/richmenu/' + rich_menu_id + '/content'
        headers = {
            "Authorization": "Bearer " + line_bot_api_code,
            "Content-Type": "image/jpeg"}
        # with open(g_setting.get('all_menu'), 'rb') as f:
        #         line_bot_api.set_rich_menu_image(rich_menu_id, 'image/jpeg', f)
        req = requests.request('POST', url_set, data=open("/Users/nagi/python_project/company_pro/beefun/beefun/static/image/點我註冊.jpg", 'rb'),
                               headers=headers)

        headers = {
            "Authorization": "Bearer " + line_bot_api_code,
            "Content-Type": "application/json"}

        url_set = 'https://api.line.me/v2/bot/user/all/richmenu/' + rich_menu_id

        req = requests.request('POST', url_set,
                               headers=headers)

        menu_dic = {
            'name': 'all_menu',
            'rich_menu_id': rich_menu_id
        }
        with transaction.manager:

            self.richmenu_service.create(**menu_dic)





        rich_menu_to_create = RichMenu(
            size=RichMenuSize(width=2500, height=843),
            selected=False,
            name="richmenu",  # display name
            chat_bar_text="客戶 \0",
            areas=[
                RichMenuArea(  # 快速治療
                    bounds=RichMenuBounds(x=0, y=0, width=1250, height=843),
                    action=URIAction(label='案件新增', uri='https://liff.line.me/1657557034-mXALAeZD')),
                RichMenuArea(  # 日記
                    bounds=RichMenuBounds(x=1250, y=0, width=1250, height=843),
                    action=URIAction(label='案件追蹤', uri='https://liff.line.me/1657557034-ywxMxGab'))
            ]
        )

        rich_menu_id = line_bot_api.create_rich_menu(
            rich_menu=rich_menu_to_create)

        ## 本機端
        # with open('/Users/nagi/python_project/company_pro/iris/iris/static/image/all_menu.png', 'rb') as f:
        #         line_bot_api.set_rich_menu_image(rich_menu_id, 'image/jpeg', f)

        ## 遠端
        url_set = 'https://api-data.line.me/v2/bot/richmenu/' + rich_menu_id + '/content'
        headers = {
            "Authorization": "Bearer " + line_bot_api_code,
            "Content-Type": "image/jpeg"}

        req = requests.request('POST', url_set, data=open("/Users/nagi/python_project/company_pro/beefun/beefun/static/image/客戶菜單.jpg", 'rb'),
                               headers=headers)


        menu_dic = {
            'name': 'client_menu',
            'rich_menu_id': rich_menu_id
        }
        with transaction.manager:
            self.richmenu_service.create(**menu_dic)


        return {'response': rich_menu_id,
                'message': _(u'News創建成功')}




