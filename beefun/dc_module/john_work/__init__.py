from __future__ import unicode_literals

from . import john_work_views as views
from ...base.context import AdminContext
from ...lib.api import return_format
from ...lib.page import return_page_format


def includeme(config):
    # Page
    config.add_route('page.case.list', '/page/case/list', factory=AdminContext)
    config.add_view(
        views.CasePageView, attr='list',
        route_name='page.case.list',
        renderer='templates/case_list.mako',
        decorator=return_page_format

    )
    config.add_route('page.case.create', '/page/case/create', factory=AdminContext)
    config.add_view(
        views.CasePageView, attr='create',
        route_name='page.case.create',
        renderer='templates/create_case.mako',
        decorator=return_page_format

    )

    config.add_route('page.client.create', '/page/client/create', factory=AdminContext)
    config.add_view(
        views.ClientPageView, attr='create',
        route_name='page.client.create',
        renderer='templates/create_back_client.mako',
        decorator=return_page_format

    )

    config.add_route('page.client.update', '/page/client/update/{identity_id}', factory=AdminContext)
    config.add_view(
        views.ClientPageView, attr='update',
        route_name='page.client.update',
        renderer='templates/update_client.mako',
        decorator=return_page_format
    )

    config.add_route('page.case.backend.list', '/page/case/backend/list', factory=AdminContext)
    config.add_view(
        views.CasePageView, attr='list_backend',
        route_name='page.case.backend.list',
        renderer='templates/case_backed_list.mako',
        decorator=return_page_format

    )

    config.add_route('page.client.backend.list', '/page/client/backend/list', factory=AdminContext)
    config.add_view(
        views.ClientPageView, attr='list_backend',
        route_name='page.client.backend.list',
        renderer='templates/client_backed_list.mako',
        decorator=return_page_format

    )

    config.add_route('page.identity.backend.list', '/page/identity/backend/list/{client_id}', factory=AdminContext)
    config.add_view(
        views.IdentityPageView, attr='list_backend',
        route_name='page.identity.backend.list',
        renderer='templates/identity_backed_list.mako',
        decorator=return_page_format
    )

    config.add_route('page.identity.backend.create', '/page/identity/backend/create/{client_id}', factory=AdminContext)
    config.add_view(
        views.IdentityPageView, attr='create',
        route_name='page.identity.backend.create',
        renderer='templates/identity_backed_create.mako',
        decorator=return_page_format
    )

    config.add_route('page.identity.backend.update', '/page/identity/backend/update/{identity_id}', factory=AdminContext)
    config.add_view(
        views.IdentityPageView, attr='update',
        route_name='page.identity.backend.update',
        renderer='templates/identity_backed_update.mako',
        decorator=return_page_format
    )

    config.add_route('page.connection.backend.list', '/page/connection/backend/list/{client_id}', factory=AdminContext)
    config.add_view(
        views.ConnectionPageView, attr='list_backend',
        route_name='page.connection.backend.list',
        renderer='templates/connection_backed_list.mako',
        decorator=return_page_format
    )

    config.add_route('page.connection.backend.create', '/page/connection/backend/create/{client_id}', factory=AdminContext)
    config.add_view(
        views.IdentityPageView, attr='create',
        route_name='page.connection.backend.create',
        renderer='templates/connection_backed_create.mako',
        decorator=return_page_format
    )

    config.add_route('page.connection.backend.update', '/page/connection/backend/update/{connection_id}',
                     factory=AdminContext)
    config.add_view(
        views.IdentityPageView, attr='update',
        route_name='page.connection.backend.update',
        renderer='templates/connection_backed_update.mako',
        decorator=return_page_format
    )

    # config.add_route('page.client.update', '/page/client/update/{client_id}', factory=AdminContext)
    # config.add_view(
    #     views.ClientPageView, attr='update',
    #     route_name='page.client.update',
    #     renderer='templates/client_edit.mako',
    #     decorator=return_page_format
    # )

    # API
    # news create
    config.add_route('api.client.notify', '/api/client/notify', factory=AdminContext)
    config.add_view(
        views.ClientJsonView, attr='notify_test',
        route_name='api.client.notify',
        request_method='GET',
        decorator=return_format,
        renderer='json'

    )

    config.add_route('api.rich_menu.setting', '/api/rich_menu/setting', factory=AdminContext)
    config.add_view(
        views.RichMenuJsonView, attr='rich_menu_setting',
        route_name='api.rich_menu.setting',
        request_method='GET',
        decorator=return_format,
        renderer='json'

    )


    config.add_route('api.client', '/api/client', factory=AdminContext)
    config.add_view(
        views.ClientJsonView, attr='create',
        route_name='api.client',
        request_method='POST',
        decorator=return_format,
        renderer='json'

    )
    config.add_route('api.client.list', '/api/client/list', factory=AdminContext)
    config.add_view(
        views.ClientJsonView, attr='search',
        route_name='api.client.list',
        request_method='GET',
        decorator=return_format,
        renderer='json'
    )

    config.add_route('api.client.rich_menu_def', '/api/client/rich_menu_def', factory=AdminContext)
    config.add_view(
        views.ClientJsonView, attr='rich_menu_def',
        route_name='api.client.rich_menu_def',
        request_method='POST',
        decorator=return_format,
        renderer='json'

    )

    config.add_route('api.client.rich_menu_client', '/api/client/rich_menu_client', factory=AdminContext)
    config.add_view(
        views.ClientJsonView, attr='rich_menu_client',
        route_name='api.client.rich_menu_client',
        request_method='POST',
        decorator=return_format,
        renderer='json'

    )

    config.add_route('api.identity', '/api/identity', factory=AdminContext)
    config.add_view(
        views.IdentityJsonView, attr='create',
        route_name='api.identity',
        request_method='POST',
        decorator=return_format,
        renderer='json'

    )

    config.add_route('api.identity.list', '/api/identity/list/{client_id}', factory=AdminContext)
    config.add_view(
        views.IdentityJsonView, attr='search',
        route_name='api.identity.list',
        request_method='GET',
        decorator=return_format,
        renderer='json'

    )

    config.add_route('api.identity.login', '/api/identity/login', factory=AdminContext)
    config.add_view(
        views.IdentityJsonView, attr='identity_login',
        route_name='api.identity.login',
        request_method='POST',
        decorator=return_format,
        renderer='json'

    )

    config.add_route('api.connection', '/api/connection', factory=AdminContext)
    config.add_view(
        views.ConnectionJsonView, attr='create',
        route_name='api.connection',
        request_method='POST',
        decorator=return_format,
        renderer='json'

    )

    config.add_route('api.connection.list', '/api/connection/list/{client_id}', factory=AdminContext)
    config.add_view(
        views.ConnectionJsonView, attr='search',
        route_name='api.connection.list',
        request_method='GET',
        decorator=return_format,
        renderer='json'

    )


    # config.add_view(
    #     views.ClientJsonView, attr='search',
    #     route_name='api.client',
    #     request_method='GET',
    #     decorator=return_format,
    #     renderer='json'
    #
    # )
    #
    # config.add_route('api.client.detail', '/api/client/{client_id}', factory=AdminContext)
    # config.add_view(
    #     views.ClientJsonView, attr='get',
    #     route_name='api.client.detail',
    #     request_method='GET',
    #     decorator=return_format,
    #     renderer='json'
    # )
    #
    # config.add_view(
    #     views.ClientJsonView, attr='edit',
    #     route_name='api.client.detail',
    #     request_method='POST',
    #     decorator=return_format,
    #     renderer='json'
    #
    # )

    config.add_route('api.case', '/api/case', factory=AdminContext)
    config.add_view(
        views.CaseJsonView, attr='create',
        route_name='api.case',
        request_method='POST',
        decorator=return_format,
        renderer='json'

    )

    config.add_route('api.case.list_backend', '/api/case/list_backend', factory=AdminContext)
    config.add_view(
        views.CaseJsonView, attr='search',
        route_name='api.case.list_backend',
        request_method='GET',
        decorator=return_format,
        renderer='json'

    )

    config.add_route('api.case.list_f', '/api/case/list/{line_id}', factory=AdminContext)
    config.add_view(
        views.CaseJsonView, attr='search_f',
        route_name='api.case.list_f',
        request_method='GET',
        decorator=return_format,
        renderer='json'

    )





