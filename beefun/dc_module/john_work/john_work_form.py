# coding=utf8
from __future__ import unicode_literals

import formencode, uuid
from formencode import validators


class ClientCreateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    client_name = validators.String()
    line_id = validators.String()

class ClientDefForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    line_id = validators.String()

class CaseCreateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    identity_name = validators.String()
    line_id = validators.String()


class ClientSearchForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True

    line_id = validators.String()

    page = validators.Int(not_empty=True, min=1)  # 第幾頁
    limit = validators.Int(not_empty=True, min=1)  # 一頁幾筆

class CaseSearchForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True

    line_id = validators.String()

    page = validators.Int(not_empty=True, min=1)  # 第幾頁
    limit = validators.Int(not_empty=True, min=1)  # 一頁幾筆

class IdentityCreateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    identity_name = validators.String()
    password = validators.String()
    client_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    connection_list = validators.Wrapper(convert_to_python=lambda a: [uuid.UUID(b.strip()) for b in
                                                                  a.replace('\'', '').replace('[', '').replace(']',
                                                                                                               '').replace(
                                                                      '\"', '').replace('\\', '').split(',') if
                                                                  len(b) > 0])

class IdentityLoginForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    client_name = validators.String()
    password = validators.String()

class IdentityUpdateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    identity_name = validators.String()
    identity_id = validators.String()

class IdentitySearchForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True

    client_id = validators.String()

    page = validators.Int(not_empty=True, min=1)  # 第幾頁
    limit = validators.Int(not_empty=True, min=1)  # 一頁幾筆

class IdentityConnectionSearchForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True

    identity_id = validators.String()


class ConnectionCreateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    connection_name = validators.String()
    connection_info = validators.String()
    type = validators.String()
    client_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')


class ConnectionUpdateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    connection_name = validators.String()
    connection_info = validators.String()
    connection_id = validators.String()


class ConnectionSearchForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True

    client_id = validators.String()

    page = validators.Int(not_empty=True, min=1)  # 第幾頁
    limit = validators.Int(not_empty=True, min=1)  # 一頁幾筆





