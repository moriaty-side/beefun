# coding=utf-8
from __future__ import unicode_literals

import uuid, datetime
from .john_work_domain import Client, Case, RichMenu,Identity,Connection,IdentityConnection
from ..category.category_domain import Category
from sqlalchemy import func,or_
from ...base.base_service import EMPTY_DIC, BaseService
from ...lib.my_exception import MyException

class ClientService(BaseService):

    TABLE = Client

    def __init__(self, session, logger=None):
        super(ClientService, self).__init__(session, logger)
        self.table_args = self.TABLE.__getattributes__()

    def create(self, **data):
        # 不可用參數
        un_available_args = []
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        create_data = {key: data[key] for key in available_args if key in data}

        create_data['client_id'] = uuid.uuid4()
        create_data['client_code'] = str(uuid.uuid4())
        # 資料創建
        try:
            return self._create(**create_data)
        except Exception as e:
            raise MyException(code=3001, message=e)

    def update(self, old_obj, **data):
        # 不可用參數
        un_available_args = ['client_id']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=3002, message=e)

    def update_by_id(self, client_id, **data):
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['client_id']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 抓取對應物件
        old_obj = self.get_by_id(client_id)
        # 選填關聯參數
        old_obj, data = self._relationship_obj_ud(old_obj, data)
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=3002, message=e)

    def get_by(self, **data):
        try:
            return self._get_by(**data)
        except Exception as e:
            raise MyException(code=3004, message=e)

    def get_by_id(self, client_id, check=False):
        id = client_id if isinstance(client_id, uuid.UUID) else uuid.UUID(client_id)
        obj = self._get_by(client_id=id)
        if check and not obj:
            raise MyException(code=3004)
        return obj

    def delete(self, old_obj):
        try:
            return self._delete(old_obj)
        except Exception as e:
            raise MyException(code=3006, message=e)

    def delete_by_id(self, client_id):
        obj = self.get_by_id(client_id=client_id, check=True)
        return self.delete(obj)

    def get_list(self, status=None, focus=None, order_by=EMPTY_DIC, group_by=EMPTY_DIC, language=None,
                 title=None, category_id=None, offset=0, limit=None, show_count=False, **search_condition):

        # 可不用參數
        un_available_args = ['']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        filter_data = {key: search_condition[key]
                       for key in available_args if key in search_condition}

        # 查詢條件製成
        query = self.session.query(self.TABLE)
        query = query.filter(self.TABLE.status != 40)
        try:

            # status
            if status is not None:
                status_value = self.TABLE._status.info.get(status, None)
                if status_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._status == status_value)

            return self._get_list(query, order_by, group_by,
                                  offset, limit, show_count, **filter_data)

        except Exception as e:
            raise MyException(code=3003, message=e)

class CaseService(BaseService):

    TABLE = Case

    def __init__(self, session, logger=None):
        super(CaseService, self).__init__(session, logger)
        self.table_args = self.TABLE.__getattributes__()

    def create(self, **data):
        # 不可用參數
        un_available_args = []
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        create_data = {key: data[key] for key in available_args if key in data}

        create_data['case_id'] = uuid.uuid4()
        # 資料創建
        try:
            return self._create(**create_data)
        except Exception as e:
            raise MyException(code=3001, message=e)

    def update(self, old_obj, **data):
        # 不可用參數
        un_available_args = ['case_id']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=3002, message=e)

    def update_by_id(self, web_id, **data):
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['case_id']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 抓取對應物件
        old_obj = self.get_by_id(web_id)
        # 選填關聯參數
        old_obj, data = self._relationship_obj_ud(old_obj, data)
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=3002, message=e)

    def get_by(self, **data):
        try:
            return self._get_by(**data)
        except Exception as e:
            raise MyException(code=3004, message=e)

    def get_by_id(self, case_id, check=False):
        id = case_id if isinstance(case_id, uuid.UUID) else uuid.UUID(case_id)
        obj = self._get_by(case_id=id)
        if check and not obj:
            raise MyException(code=3004)
        return obj

    def delete(self, old_obj):
        try:
            return self._delete(old_obj)
        except Exception as e:
            raise MyException(code=3006, message=e)

    def delete_by_id(self, case_id):
        obj = self.get_by_id(case_id=case_id, check=True)
        return self.delete(obj)

    def get_list(self, status=None, focus=None, order_by=EMPTY_DIC, group_by=EMPTY_DIC, language=None,
                 title=None, line_id=None, offset=0, limit=None, show_count=False, **search_condition):

        # 可不用參數
        un_available_args = ['']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        filter_data = {key: search_condition[key]
                       for key in available_args if key in search_condition}

        # 查詢條件製成
        query = self.session.query(self.TABLE)
        query = query.filter(self.TABLE.status != 40)
        if line_id!=None:
            query = query.filter(self.TABLE.line_id == line_id)
        try:

            # status
            if status is not None:
                status_value = self.TABLE._status.info.get(status, None)
                if status_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._status == status_value)

            return self._get_list(query, order_by, group_by,
                                  offset, limit, show_count, **filter_data)

        except Exception as e:
            raise MyException(code=3003, message=e)

class IdentityService(BaseService):

    TABLE = Identity

    def __init__(self, session, logger=None):
        super(IdentityService, self).__init__(session, logger)
        self.table_args = self.TABLE.__getattributes__()

    def create(self, **data):
        # 不可用參數
        un_available_args = []
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        create_data = {key: data[key] for key in available_args if key in data}

        create_data['identity_id'] = uuid.uuid4()
        create_data['identity_code'] = str(uuid.uuid4())
        # 資料創建
        try:
            return self._create(**create_data)
        except Exception as e:
            raise MyException(code=3001, message=e)

    def update(self, old_obj, **data):
        # 不可用參數
        un_available_args = ['identity_id']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=3002, message=e)

    def update_by_id(self, identity_id, **data):
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['identity_id']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 抓取對應物件
        old_obj = self.get_by_id(identity_id)
        # 選填關聯參數
        old_obj, data = self._relationship_obj_ud(old_obj, data)
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=3002, message=e)

    def get_by(self, **data):
        try:
            return self._get_by(**data)
        except Exception as e:
            raise MyException(code=3004, message=e)

    def get_by_id(self, identity_id, check=False):
        id = identity_id if isinstance(identity_id, uuid.UUID) else uuid.UUID(identity_id)
        obj = self._get_by(identity_id=id)
        if check and not obj:
            raise MyException(code=3004)
        return obj

    def delete(self, old_obj):
        try:
            return self._delete(old_obj)
        except Exception as e:
            raise MyException(code=3006, message=e)

    def delete_by_id(self, identity_id):
        obj = self.get_by_id(identity_id=identity_id, check=True)
        return self.delete(obj)

    def get_list(self, status=None, focus=None, order_by=EMPTY_DIC, group_by=EMPTY_DIC, language=None,
                 title=None, client_id=None, offset=0, limit=None, show_count=False, **search_condition):

        # 可不用參數
        un_available_args = ['']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        filter_data = {key: search_condition[key]
                       for key in available_args if key in search_condition}

        # 查詢條件製成
        query = self.session.query(self.TABLE)

        query = query.filter(self.TABLE.client_id == client_id)

        try:

            # status
            if status is not None:
                status_value = self.TABLE._status.info.get(status, None)
                if status_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._status == status_value)

            return self._get_list(query, order_by, group_by,
                                  offset, limit, show_count, **filter_data)

        except Exception as e:
            raise MyException(code=3003, message=e)

class ConnectionService(BaseService):

    TABLE = Connection

    def __init__(self, session, logger=None):
        super(ConnectionService, self).__init__(session, logger)
        self.table_args = self.TABLE.__getattributes__()

    def create(self, **data):
        # 不可用參數
        un_available_args = []
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        create_data = {key: data[key] for key in available_args if key in data}

        create_data['connection_id'] = uuid.uuid4()
        # 資料創建
        try:
            return self._create(**create_data)
        except Exception as e:
            raise MyException(code=3001, message=e)

    def update(self, old_obj, **data):
        # 不可用參數
        un_available_args = ['connection_id']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=3002, message=e)

    def update_by_id(self, connection_id, **data):
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['connection_id']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 抓取對應物件
        old_obj = self.get_by_id(connection_id)
        # 選填關聯參數
        old_obj, data = self._relationship_obj_ud(old_obj, data)
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=3002, message=e)

    def get_by(self, **data):
        try:
            return self._get_by(**data)
        except Exception as e:
            raise MyException(code=3004, message=e)

    def get_by_id(self, connection_id, check=False):
        id = connection_id if isinstance(connection_id, uuid.UUID) else uuid.UUID(connection_id)
        obj = self._get_by(connection_id=id)
        if check and not obj:
            raise MyException(code=3004)
        return obj

    def delete(self, old_obj):
        try:
            return self._delete(old_obj)
        except Exception as e:
            raise MyException(code=3006, message=e)

    def delete_by_id(self, connection_id):
        obj = self.get_by_id(connection_id=connection_id, check=True)
        return self.delete(obj)

    def get_list(self, status=None, focus=None, order_by=EMPTY_DIC, group_by=EMPTY_DIC, language=None,
                 title=None, client_id=None, offset=0, limit=None, show_count=False, **search_condition):

        # 可不用參數
        un_available_args = ['']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        filter_data = {key: search_condition[key]
                       for key in available_args if key in search_condition}

        # 查詢條件製成
        query = self.session.query(self.TABLE)

        query = query.filter(self.TABLE.client_id == client_id)

        try:

            # status
            if status is not None:
                status_value = self.TABLE._status.info.get(status, None)
                if status_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._status == status_value)

            return self._get_list(query, order_by, group_by,
                                  offset, limit, show_count, **filter_data)

        except Exception as e:
            raise MyException(code=3003, message=e)

class IdentityConnectionService(BaseService):

    TABLE = IdentityConnection

    def __init__(self, session, logger=None):
        super(IdentityConnectionService, self).__init__(session, logger)
        self.table_args = self.TABLE.__getattributes__()

    def create(self, **data):
        # 不可用參數
        un_available_args = []
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        create_data = {key: data[key] for key in available_args if key in data}

        create_data['identity_connection_id'] = uuid.uuid4()
        # 資料創建
        try:
            return self._create(**create_data)
        except Exception as e:
            raise MyException(code=3001, message=e)

    def update(self, old_obj, **data):
        # 不可用參數
        un_available_args = ['identity_connection_id']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=3002, message=e)

    def update_by_id(self, web_id, **data):
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['identity_connection_id']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 抓取對應物件
        old_obj = self.get_by_id(web_id)
        # 選填關聯參數
        old_obj, data = self._relationship_obj_ud(old_obj, data)
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=3002, message=e)

    def get_by(self, **data):
        try:
            return self._get_by(**data)
        except Exception as e:
            raise MyException(code=3004, message=e)

    def get_by_id(self, identity_connection_id, check=False):
        id = identity_connection_id if isinstance(identity_connection_id, uuid.UUID) else uuid.UUID(identity_connection_id)
        obj = self._get_by(identity_connection_id=id)
        if check and not obj:
            raise MyException(code=3004)
        return obj

    def delete(self, old_obj):
        try:
            return self._delete(old_obj)
        except Exception as e:
            raise MyException(code=3006, message=e)

    def delete_by_id(self, identity_connection_id):
        obj = self.get_by_id(identity_connection_id=identity_connection_id, check=True)
        return self.delete(obj)

    def get_list(self, status=None, focus=None, order_by=EMPTY_DIC, group_by=EMPTY_DIC, language=None,
                 title=None, identity_id=None, offset=0, limit=None, show_count=False, **search_condition):

        # 可不用參數
        un_available_args = ['']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        filter_data = {key: search_condition[key]
                       for key in available_args if key in search_condition}

        # 查詢條件製成
        query = self.session.query(self.TABLE)

        query = query.filter(self.TABLE.identity_id == identity_id)
        try:

            # status

            return self._get_list(query, order_by, group_by,
                                  offset, limit, show_count, **filter_data)

        except Exception as e:
            raise MyException(code=3003, message=e)

class RichMenuService(BaseService):
    TABLE = RichMenu

    def __init__(self, session, logger=None):
        super(RichMenuService, self).__init__(session, logger=logger)

    def create(self, user_id=None, **data):
        """
        必填資料
            account: 帳號
        其他資料
            **data: 其他資料
        """
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['menu_id']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 資料過濾
        create_data = {key: data[key] for key in available_args if key in data}

        create_data['menu_id'] = uuid.uuid4()
        # 當有使用者id時
        if user_id:
            create_data['create_by'] = uuid.UUID(user_id)
            create_data['update_by'] = uuid.UUID(user_id)
        # 資料創建
        try:
            return self._create(**create_data)
        except Exception as e:
            raise MyException(code=1001, message=e)

    def update(self, old_obj, user_id=None, **data):
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['menu_id']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}
        # 當有使用者id時
        if user_id:
            update_data['update_by'] = uuid.UUID(user_id)

        # 資料更新
        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=1003, message=e)

    def get_by_id(self, obj_id, check=False):
        """
        讀取帳號編號
        :param obj_id:
        :param check:
        :return:
        """
        id = obj_id if isinstance(obj_id, uuid.UUID) else uuid.UUID(obj_id)
        account = self._get_by(menu_id=id)
        if check and not account:
            raise MyException(code=1005)

        return account


    def get_by(self, **data):
        try:
            return self._get_by(**data)
        except Exception as e:
            raise MyException(code=1004, message=e)

    def get_list(self, order_by=EMPTY_DIC, group_by=EMPTY_DIC,
                 offset=0, limit=None, show_count=False, name=None, account=None,
                 status=None, check_mechanism='filtering', **search_condition):

        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 可不用參數
        un_available_args = []
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]

        # 查詢條件製成
        query = self.session.query(self.TABLE)

        if status is not None:
            status_value = self.TABLE._status.info.get(status, None)
            if status_value is None:
                raise MyException(code=901, message='{0}-{1}'.format(self.TABLE.__name__, 'Status'))
            query = query.filter(self.TABLE._status == status_value)
        else:
            query = query.filter(self.TABLE.status != 400)

        # 資料篩選器
        if check_mechanism == 'filtering':
            # 資料過濾機制
            search_condition = {key: search_condition[key] for key in available_args if key in search_condition}
        elif check_mechanism == 'defence':
            # 資料防護機制
            for key in search_condition:
                if key in un_available_args:
                    raise Exception('input query args is Disable in {0} '.format(self.TABLE.__name__))
                elif key not in available_args:
                    raise Exception('input query args is error in {0} '.format(self.TABLE.__name__))

        try:

            return self._get_list(query, order_by, group_by,
                                  offset, limit, show_count, **search_condition)

        except Exception as e:
            raise MyException(code=1004, message=e)
