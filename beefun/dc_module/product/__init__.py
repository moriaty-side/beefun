from __future__ import unicode_literals

from . import product_views as views
from ...base.context import AdminContext
from ...lib.api import return_format
from ...lib.page import return_page_format


def includeme(config):
    #Page
    config.add_route('page.product', '/page/product', factory=AdminContext)
    config.add_view(
        views.ProductPageView, attr='single_manage',
        route_name='page.product',
        renderer='templates/product.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )

    config.add_route('page.product.list', '/page/product/list', factory=AdminContext)
    config.add_view(
        views.ProductPageView, attr='list',
        route_name='page.product.list',
        renderer='templates/product_list.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )

    config.add_route('page.product.create', '/page/product/create', factory=AdminContext)
    config.add_view(
        views.ProductPageView, attr='create',
        route_name='page.product.create',
        renderer='templates/product_create.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )

    config.add_route('page.product.update', '/page/product/update/{product_id}', factory=AdminContext)
    config.add_view(
        views.ProductPageView, attr='update',
        route_name='page.product.update',
        renderer='templates/product_edit.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )



    # API
    # product create
    config.add_route('api.product', '/api/product', factory=AdminContext)
    config.add_view(
        views.ProductJsonView, attr='create',
        route_name='api.product',
        renderer='json',
        request_method='POST',
        decorator=return_format,
        
        permission='login',
        
    )
    config.add_view(
        views.ProductJsonView, attr='search',
        route_name='api.product',
        request_method='GET',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    # 多筆儲存
    config.add_route('api.product.multi.set', '/api/product/multi_set', factory=AdminContext)
    config.add_view(
        views.ProductJsonView, attr='multi_set',
        route_name='api.product.multi.set',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_route('api.product.detail', '/api/product/{product_id}', factory=AdminContext)
    config.add_view(
        views.ProductJsonView, attr='get',
        route_name='api.product.detail',
        request_method='GET',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_view(
        views.ProductJsonView, attr='edit',
        route_name='api.product.detail',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_view(
        views.ProductJsonView, attr='delete',
        route_name='api.product.detail',
        request_method='DELETE',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )
