import transaction
from ...base.base_test import BaseTest, dummy_request
from .product_domain import Product, ProductImage
from .product_views import ProductJsonView as View

from ..category.category_service import CategoryService
from ..image.image_service import ImageService

class ProductAPITest(BaseTest):

    def setUp(self):
        super(ProductAPITest, self).setUp()
        self.init_database()
        self.image_service = ImageService(repository_path='', session=self.session)
        self.category_service = CategoryService(session=self.session)

    def create(self, content='test'):
        with transaction.manager:
            image = self.get_or_create_image()
            category = self.get_or_create_category()
            product_obj = Product(content=content,
                                  main_image_id=image.image_id,
                                  category_id=category.category_id)
            self.session.add(product_obj)
            product_obj = self.session.query(Product).filter(Product.content == content).first()

        self.create_product_image(product_id=product_obj.product_id)

        return product_obj

    def create_product_image(self, product_id, description="123", image_type=10):
        with transaction.manager:
            product_image = self.get_or_create_image(name='image')
            product_image_obj = ProductImage(image_id=product_image.image_id,
                                             product_id=product_id,
                                             description=description,
                                             _image_type=image_type)

            self.session.add(product_image_obj)
        return product_image_obj

    def get_or_create_category(self, name="測試"):
        category = self.category_service.get_by(name=name)
        if not category:
            with transaction.manager:
                category = self.category_service.create(name=name)
        return category

    def get_or_create_image(self, name="測試"):
        image = self.image_service.get_by(name=name)
        if not image:
            with transaction.manager:
                image = self.image_service.create(name=name, filename='test.jpg')
        return image


    def test_create(self):
        """
        測試 創建產品
        """
        content = 'test'
        main_image = self.get_or_create_image(name="main_image")
        llluminance_image = self.get_or_create_image(name="llluminance_image")
        work_image = self.get_or_create_image(name='image')
        category = self.get_or_create_category()
        request = dummy_request(self.session)
        request.params = {'content': content,
                          'main_image_id': str(main_image.image_id),
                          'llluminance_image_id': str(llluminance_image.image_id),
                          'category_id': str(category.category_id),
                          'product_images': [{'image_id': str(work_image.image_id),
                                              'description': "123",
                                              'image_type': "thumb"}],
                          }

        view = View(request)
        response = view.create()
        self.assertEqual(response.get('message'), '產品創建成功')

    def test_search(self):
        """
        測試 創建產品
        """
        request = dummy_request(self.session)
        view = View(request)
        resp = view.search()
        self.assertEqual(resp.get('message'), '產品搜尋成功')
        self.assertEqual(resp.get('response', {}).get('total_count'), 0)
        self.assertEqual(resp.get('response', {}).get('total_page'), 0)
        self.assertEqual(len(resp.get('response', {}).get('product_list', [])), 0)

    def test_get(self):
        """
        測試 讀取產品
        """
        product = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'product_id': str(product.product_id)}
        view = View(request)
        response = view.get()
        self.assertEqual(response.get('message'), '產品讀取成功')

    def test_update(self):
        """
        測試 更新產品
        """
        content = "CCC"
        product = self.create(content="BBB")
        product_image = self.get_or_create_image(name='aaa')
        request = dummy_request(self.session)
        request.matchdict = {'product_id': str(product.product_id)}
        request.params = {'content': content,
                          'product_images': [{'image_id': str(product_image.image_id),
                                              'description': "aaa",
                                              'image_type': "thumb"}]}
        view = View(request)
        response = view.edit()
        self.assertEqual(response.get('message'), '產品修改成功')
        self.assertEqual(response.get('response', {}).get('product', {}).get('content'), content)

    def test_delete(self):
        """
        測試 更新產品
        """
        product = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'product_id': str(product.product_id)}
        view = View(request)
        response = view.delete()
        self.assertEqual(response.get('message'), '刪除產品成功')

