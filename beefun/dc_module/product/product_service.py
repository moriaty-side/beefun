# coding=utf-8
from __future__ import unicode_literals

import uuid
from .product_domain import Product
from ...base.base_service import EMPTY_DIC, NOT_SET, BaseService
from ...lib.my_exception import MyException
from sqlalchemy import func,or_

class ProductService(BaseService):

    TABLE = Product

    def __init__(self, session, logger=None):
        super(ProductService, self).__init__(session,logger)
        self.table_args = self.TABLE.__getattributes__()

    def create(self, **data):
        # 不可用參數
        un_available_args = ['product_images']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        create_data = {key: data[key] for key in available_args if key in data}

        # 資料創建
        try:
            return self._create(**create_data)
        except Exception as e:
            raise MyException(code=8201, message=e)

    def update(self, old_obj, **data):
        # 不可用參數
        un_available_args = ['product_id', 'product_images']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=8203, message=e)

    def update_by_id(self, product_id, **data):
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['product_id', 'product_images']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 抓取對應物件
        old_obj = self.get_by_id(product_id)
        # 選填關聯參數
        old_obj, data = self._relationship_obj_ud(old_obj, data)
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=8203, message=e)

    def get_by_sequence_max(self,language,popular_type):
        """
        抓取物件依據 article_id
        :param article_id:
        :return: article_obj
        """

        query =  self.session.query(func.max(self.TABLE.sequence))
        # max_date = self.session.query(func.max(self.TABLE.sort)).filter(self.TABLE.category_id == category_id).first()
        if popular_type=="product_light":
            set=10
        else:
            set=11

        query=query.filter(self.TABLE.popular_type == set)
        max_date = query.filter(self.TABLE.language == language).scalar()

        # query = self.session.query(func.max(self.TABLE.sort)).filter(self.TABLE.category_id == category_id)

        # query = query.filter(self.TABLE.category_id == category_id)
        # query = query(func.max(self.TABLE.sort))

        return max_date

    def get_by_sequence_minus_one(self, language,number_set):
        """
        抓取物件依據 article_id
        :param article_id:
        :return: article_obj
        """
        query = self.session.query(self.TABLE).filter(self.TABLE.language == language).filter(self.TABLE.sequence > number_set).update({self.TABLE.sequence: self.TABLE.sequence - 1})
        # query.commit()
        # max_date = self.session.query(func.max(self.TABLE.sort)).filter(self.TABLE.category_id == category_id).first()

        # max_date = self.session.query(self.TABLE.language == language)
        # max_date = self.session.query(self.TABLE.sequence>number_set)
        # query = self.session.query(func.max(self.TABLE.sort)).filter(self.TABLE.category_id == category_id)

        # query = query.filter(self.TABLE.category_id == category_id)
        # query = query(func.max(self.TABLE.sort))

        return None

    def get_by(self, **data):
        try:
            return self._get_by(**data)
        except Exception as e:
            raise MyException(code=8204, message=e)

    def get_by_id(self, product_id, check=False):
        id = product_id if isinstance(product_id, uuid.UUID) else uuid.UUID(product_id)
        obj = self._get_by(product_id=id)
        if check and not obj:
            raise MyException(code=8205)
        return obj


    def delete(self, old_obj):
        try:
            return self._delete(old_obj)
        except Exception as e:
            raise MyException(code=8206, message=e)

    def delete_by_id(self, product_id):
        obj = self.get_by_id(product_id=product_id, check=True)
        return self.delete(obj)

    def get_count(self, status=None, category_ids=None):

        # 查詢條件製成
        query = self.session.query(self.TABLE)

        try:
            # status
            if status is not None:
                status_value = self.TABLE._status.info.get(status, None)
                if status_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._status == status_value)
            else:
                status_value = self.TABLE._status.info.get('show', None)
                query = query.filter(self.TABLE._status == status_value)

            # category_ids
            if category_ids is not None:
                query = query.filter(self.TABLE.category_id.in_(category_ids))

            return query.count()

        except Exception as e:
            raise MyException(code=8204, message=e)


    def get_list(self, status=None, popular=None, order_by=EMPTY_DIC, group_by=EMPTY_DIC,
                 category_ids=None, product_ids=None, excloude_product_id=None, title=None,
                 offset=0, limit=None, show_count=False, **search_condition):

        # 可不用參數
        un_available_args = ['']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        filter_data = {key: search_condition[key]
                       for key in available_args if key in search_condition}

        # 查詢條件製成
        query = self.session.query(self.TABLE)
        query = query.filter(self.TABLE.status != 40)
        try:
            #title
            if title is not None:
                query = query.filter(self.TABLE.title.ilike('%{}%'.format(title)))

            # status
            if status is not None:
                status_value = self.TABLE._status.info.get(status, None)
                if status_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._status == status_value)


            # popular
            if popular is not None:
                popular_value = self.TABLE._popular.info.get(popular, None)
                if popular_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._popular == popular_value)

            # category_ids
            if category_ids is not None:
                query = query.filter(self.TABLE.category_id.in_(category_ids))

            #product_ids
            if product_ids is not None:
                query = query.filter(self.TABLE.product_id.in_(product_ids))

            if excloude_product_id is not None:
                query = query.filter(self.TABLE.product_id != excloude_product_id)

            return self._get_list(query, order_by, group_by,
                                  offset, limit, show_count, **filter_data)

        except Exception as e:
            raise MyException(code=8204, message=e)



