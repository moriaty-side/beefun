# coding=utf8
from sqlalchemy import (Column, Integer, String, DateTime, Text, ForeignKey)
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from ...base.meta_module import (Base, TimestampTable, GUID, uuid4)

DeclarativeBase = declarative_base()

class Product(Base, TimestampTable):
    """
    產品 資料表
    """
    __tablename__ = 't_product'

    product_id = Column('f_product_id', GUID, primary_key=True, default=uuid4, doc=u"產品ID")

    language = Column('f_language', String(12), index=True, nullable=False, default="zh_Hant", doc=u"多國語系語系")

    title = Column('f_title', String, nullable=True, doc=u'標題')

    category_id = Column('f_category_id', ForeignKey('t_category.f_category_id'), nullable=False, doc=u"分類")

    main_image_id = Column('f_main_image_id', ForeignKey('t_images.f_image_id'), nullable=False, doc=u"封面圖片")

    # product_image_id = Column('f_product_image_id', ForeignKey('t_images.f_image_id'), nullable=False, doc=u"產品圖")

    light_image_id = Column('f_light_image_id', ForeignKey('t_images.f_image_id'), nullable=True, doc=u"照度圖")

    sequence = Column('f_sequence', Integer, nullable=False, default=0, doc=u'排序')


    light_mode = Column('f_light_mode', String, nullable=True, doc=u'光源模式')

    lamp_size = Column('f_lamp_size', String, nullable=True, doc=u'燈具尺寸')

    installation_method = Column('f_installation_method', String, nullable=True, doc=u'安裝方式')

    input_voltage = Column('f_input_voltage', String, nullable=True, doc=u'輸入電壓')

    watts = Column('f_watts', String, nullable=True, doc=u'瓦數')

    lighting_effect = Column('f_lighting_effect', String, nullable=True, doc=u'燈具光效')

    luminous_flux = Column('f_luminous_flux', String, nullable=True, doc=u'光通量')

    color_temperature = Column('f_color_temperature', String, nullable=True, doc=u'色溫')

    beam_angle = Column('f_beam_angle', String, nullable=True, doc=u'光束角')

    color_rendering = Column('f_color_rendering', String, nullable=True, doc=u'演色指數')

    applicable_environment = Column('f_applicable_environment', String, nullable=True, doc=u'適用環境')

    waterproof_and_dustproof = Column('f_waterproof_and_dustproof', String, nullable=True, doc=u'防水防塵等級')

    power_factor = Column('f_power_factor', String, nullable=True, doc=u'防水防塵等級')

    material_weight = Column('f_material_weight', String, nullable=True, doc=u'材質重量')

    color_form_f_one = Column('f_color_form_f_one', String, nullable=True, doc=u'表格顏色（表主）')

    color_form_f_sec = Column('f_color_form_f_sec', String, nullable=True, doc=u'表格顏色（表副）')

    color_form_c_one = Column('f_color_form_c_one', String, nullable=True, doc=u'表格顏色（內主）')

    color_form_c_sec = Column('f_color_form_c_sec', String, nullable=True, doc=u'表格顏色（內副）')



    llluminance_image_id = Column('f_llluminance_image_id', ForeignKey('t_images.f_image_id'), nullable=True, doc=u"照度圖片")

    _popular = Column('f_popular', Integer, nullable=False, default=10,
                      info={"show": 10, "hiden": 20},
                      doc=u"主打產品 :10.顯示 , 20.不顯示")

    _popular_type = Column('f_popular_type', Integer, nullable=False, default=10,
                           info={"product_light": 10, "product_green": 11},
                           doc=u"產品類型 :10.燈具 , 11.綠建材")

    _status = Column('f_status', Integer, nullable=False, default=10,
                     info={"show": 10, "hiden": 20, "delete": 40},
                     doc=u"狀態:10.顯示 , 20.不顯示, 40.封存")

    content = Column('f_content', String, doc=u'產品描述')

    product_specification = Column('f_product_specification', String, doc=u'產品規格')

    category = relationship("Category")

    main_image = relationship("Images", foreign_keys=[main_image_id])

    # product_image = relationship("Images", foreign_keys=[product_image_id])

    light_image = relationship("Images", foreign_keys=[light_image_id])

    llluminance_image = relationship("Images", foreign_keys=[llluminance_image_id])

    product_images = relationship("ProductImage")

    def __repr__(self):
        return '<ProductionObject (product_id={0})>'.format(self.product_id)

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @hybrid_property
    def popular(self):
        if hasattr(self.__class__, '_popular'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._popular.info
            for k in info_dic.keys():
                if info_dic[k] == self._popular:
                    return k

    @popular.setter
    def popular(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._popular.info.get(value)
            if not v:
                raise Exception('popular column input value {} is error'.format(value))
            self._popular = v
        else:
            raise Exception('popular column input value_type {} is error')

    @popular.expression
    def popular(cls):
        return cls._popular

    @hybrid_property
    def popular_type(self):
        if hasattr(self.__class__, '_popular_type'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._popular_type.info
            for k in info_dic.keys():
                if info_dic[k] == self._popular_type:
                    return k

    @popular_type.setter
    def popular_type(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._popular_type.info.get(value)
            if not v:
                raise Exception('popular_type column input value {} is error'.format(value))
            self._popular_type = v
        else:
            raise Exception('popular_type column input value_type {} is error')

    @popular_type.expression
    def popular_type(cls):
        return cls._popular_type

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    def __json__(self, show_main_image=False, show_llluminance_image=False, show_category=False, show_product_imgaes=False):
        d = {'product_id': str(self.product_id),
             'language': self.language,
             'title': self.title,
             'sequence': self.sequence,
             'category_id': str(self.category_id),
             # 'product_image_id': str(self.product_image_id),
             'main_image_id': str(self.main_image_id),
             'light_image_id': str(self.light_image_id),
             'llluminance_image_id': str(self.llluminance_image_id),
             'popular': self.popular,
             'status': self.status,
             'light_mode': self.light_mode,
             'lamp_size': self.lamp_size,
             'installation_method': self.installation_method,
             'input_voltage': self.input_voltage,
             'watts': self.watts,
             'lighting_effect': self.lighting_effect,
             'luminous_flux': self.luminous_flux,
             'color_temperature': self.color_temperature,
             'beam_angle': self.beam_angle,
             'power_factor': self.power_factor,
             'color_rendering': self.color_rendering,
             'applicable_environment': self.applicable_environment,
             'waterproof_and_dustproof': self.waterproof_and_dustproof,
             'material_weight': self.material_weight,
             'color_form_f_one': self.color_form_f_one,
             'color_form_f_sec': self.color_form_f_sec,
             'color_form_c_one': self.color_form_c_one,
             'color_form_c_sec': self.color_form_c_sec,
             'popular_type': self.popular_type,
             'content': self.content,
             'product_specification': self.product_specification,
             }

        if show_main_image:
            d['main_image'] = self.main_image.__json__() if self.main_image else {}

            d['light_image'] = self.light_image.__json__() if self.light_image else {}

        if show_llluminance_image:
            d['llluminance_image'] = self.llluminance_image.__json__() if self.llluminance_image else {}

        if show_category:
            d['category'] = self.category.__json__() if self.category else {}

        if show_product_imgaes:
            d['product_images'] = [pi.__json__(show_image=True) for pi in self.product_images]

        return d

class ProductImage(Base):
    """
    產品圖片 資料表
    """

    __tablename__ = 't_product_image'

    product_image_id = Column('f_product_image_id', GUID, primary_key=True, default=uuid4, doc=u"工程實績圖片ID")

    product_id = Column('f_product_id', ForeignKey('t_product.f_product_id'), nullable=False, doc=u"工程實績ID")

    image_id = Column('f_image_id', ForeignKey('t_images.f_image_id'), nullable=False, doc=u"圖片ID")

    sequence = Column('f_sequence', Integer, nullable=False, default=0, doc=u'排序')

    description = Column('f_description', String(256), nullable=False, doc=u'描述')

    _image_type = Column('f_image_type', Integer, nullable=False, default=10,
                         info={"thumb": 10, "content": 20},
                         doc=u"圖片類型:10.縮圖, 20.內文圖")

    image = relationship("Images")
    work = relationship("Product") #, backref="work_images"

    def __repr__(self):
        return '<ProdcutImageObject (product_image_id={0})>'.format(self.product_image_id)

    @hybrid_property
    def image_type(self):
        if hasattr(self.__class__, '_image_type'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._image_type.info
            for k in info_dic.keys():
                if info_dic[k] == self._image_type:
                    return k

    @image_type.setter
    def image_type(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._image_type.info.get(value)
            if not v:
                raise Exception('image_type column input value {} is error'.format(value))
            self._image_type = v
        else:
            raise Exception('image_type column input value_type {} is error')

    @image_type.expression
    def image_type(cls):
        return cls._image_type

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    def __json__(self, show_image=False):
        d = {'product_image_id': str(self.product_image_id),
             'description': self.description,
             'product_id': str(self.product_id),
             'sequence': self.sequence,
             'image_id': self.image_id,
             'image_type': self.image_type,
             }

        if show_image:
            d['image'] = self.image.__json__() if self.image else {}

        return d