# coding=utf8
from __future__ import unicode_literals
import uuid

import formencode
from formencode import validators, ForEach

class ProductImageCreateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    product_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    description = validators.String(not_empty=True)
    image_type = validators.String(not_empty=True)


class ProductImageUpdateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    product_image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    product_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    description = validators.String(not_empty=True)
    image_type = validators.String(not_empty=True)


class ProductCreateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    language = validators.String(not_empty=True)
    category_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    main_image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    product_image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    light_image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    llluminance_image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')

    popular = validators.String(not_empty=False)
    popular_type = validators.String(not_empty=False)
    status = validators.String(not_empty=False)
    content = validators.String(not_empty=False)
    product_specification = validators.String(not_empty=False)
    light_mode = validators.String(not_empty=False)
    lamp_size = validators.String(not_empty=False)
    installation_method = validators.String(not_empty=False)
    title = validators.String(not_empty=False)
    input_voltage = validators.String(not_empty=False)
    watts = validators.String(not_empty=False)
    lighting_effect = validators.String(not_empty=False)
    luminous_flux = validators.String(not_empty=False)
    color_temperature = validators.String(not_empty=False)
    beam_angle = validators.String(not_empty=False)
    power_factor = validators.String(not_empty=False)
    color_rendering = validators.String(not_empty=False)
    applicable_environment = validators.String(not_empty=False)
    waterproof_and_dustproof = validators.String(not_empty=False)
    material_weight = validators.String(not_empty=False)
    color_form_f_one = validators.String(not_empty=False)
    color_form_f_sec = validators.String(not_empty=False)
    color_form_c_one = validators.String(not_empty=False)
    color_form_c_sec = validators.String(not_empty=False)

    product_images = ForEach(ProductImageCreateForm)


class ProductSearchForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    language = validators.String(not_empty=True)
    content = validators.String(not_empty=True)
    category_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    popular_type = validators.String(not_empty=True)
    status = validators.String(not_empty=False)

    page = validators.Int(not_empty=True, min=1)  # 第幾頁
    limit = validators.Int(not_empty=True, min=1)  # 一頁幾筆


class ProductEditForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    language = validators.String(not_empty=True)
    category_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    main_image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    product_image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    light_image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    llluminance_image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    popular = validators.String(not_empty=True)
    popular_type = validators.String(not_empty=True)
    status = validators.String(not_empty=True)
    content = validators.String(not_empty=True)

    product_specification = validators.String(not_empty=False)
    light_mode = validators.String(not_empty=False)
    lamp_size = validators.String(not_empty=False)
    installation_method = validators.String(not_empty=False)
    title = validators.String(not_empty=False)
    input_voltage = validators.String(not_empty=False)
    watts = validators.String(not_empty=False)
    power_factor = validators.String(not_empty=False)
    lighting_effect = validators.String(not_empty=False)
    luminous_flux = validators.String(not_empty=False)
    color_temperature = validators.String(not_empty=False)
    beam_angle = validators.String(not_empty=False)
    color_rendering = validators.String(not_empty=False)
    applicable_environment = validators.String(not_empty=False)
    waterproof_and_dustproof = validators.String(not_empty=False)
    material_weight = validators.String(not_empty=False)
    color_form_f_one = validators.String(not_empty=False)
    color_form_f_sec = validators.String(not_empty=False)
    color_form_c_one = validators.String(not_empty=False)
    color_form_c_sec = validators.String(not_empty=False)

    product_images = ForEach(ProductImageCreateForm)
