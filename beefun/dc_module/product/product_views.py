# coding=utf8
from __future__ import unicode_literals
import transaction, json
from . import product_form as form
from ...base.base_view import BaseView
from .product_service import ProductService
from .product_image_service import ProductImageService
from ..category.category_service import CategoryService
import uuid

class ProductPageView(BaseView):
    def __init__(self, request):
        super(ProductPageView, self).__init__(request)
        self.category_service = CategoryService(session=self.session, logger=self.logger)
        self.product_service = ProductService(session=self.session, logger=self.logger)
        self.product_image_service = ProductImageService(session=self.session, logger=self.logger)

    def manage(self):
        _ = self.localizer
        return {}

    def create(self):
        _ = self.localizer
        category_list = self.category_service.get_list(type="news")

        return {'category_list': [c.__json__() for c in category_list]}

    def update(self):
        _ = self.localizer
        _ = self.localizer
        category_list = self.category_service.get_list(type="news")
        product_obj = self.product_service.get_by_id(product_id=self.request.matchdict['product_id'],
                                               check=True)

        request_data={}
        request_data['product_id'] = uuid.UUID(self.request.matchdict['product_id'])

        product_image_obj, total_count = self.product_image_service.get_list(show_count=True, **request_data)
        # category_list = self.category_service.get_list(type="news")

        return {'product_obj': product_obj.__json__(show_main_image=True,
                                                      show_category=True),
                'product_image_obj': [a.__json__(show_image=True) for a in product_image_obj],
                'category_list': [c.__json__() for c in category_list]}

    def list(self):
        _ = self.localizer

        return {}

class ProductJsonView(BaseView):
    def __init__(self, request):
        super(ProductJsonView, self).__init__(request)
        self.product_service = ProductService(session=self.session, logger=self.logger)
        self.product_image_service = ProductImageService(session=self.session, logger=self.logger)

    def create(self):
        """
        創建產品
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.ProductCreateForm().to_python(self.request_params)
        request_json_list = json.loads(self.request_params.get('product_images_list', '{}').replace('\r\n', '\\r\\n'))

        if self.product_service.get_by_sequence_max(language=request_data.get('language'),popular_type=request_data.get('popular_type'))==None:
            request_data['sequence'] = 1
        else:
            request_data['sequence'] = self.product_service.get_by_sequence_max(language=request_data.get('language'),popular_type=request_data.get('popular_type'))+1

        # 創建產品
        with transaction.manager:
            product_obj = self.product_service.create(**request_data)

            # 創建 產品 圖片集
            for product_image in request_json_list:
                product_image['product_id'] = product_obj.product_id
                product_image['image_id'] = uuid.UUID(product_image['image_id'])
                self.product_image_service.create(**product_image)

        return {'response': {'product': product_obj.__json__()},
                'message': _(u'產品創建成功')}

    def search(self):
        """
        搜尋產品
        """

        # 檢查輸入參數
        request_data = form.ProductSearchForm().to_python(self.request_params)

        # # 分頁機制
        current_page = request_data.get('page', 1)
        limit = request_data.get('limit', 10)
        request_data['offset'] = (current_page - 1) * limit
        request_data['limit'] = limit
        request_data['order_by'] = [('created', -1)]
        if request_data.get('popular_type') == "product_light":
            request_data['popular_type'] = 10
        else:
            request_data['popular_type'] = 11

        # 搜尋產品
        product_list, total_count = self.product_service.get_list(show_count=True, **request_data)

        return {'response': {'product_list': [a.__json__(show_main_image=True,
                                                      show_category=True,) for a in product_list],
                             'total_count': total_count,
                             'total_page': (int(total_count / limit) + 1 if total_count % limit else 0)
                             },
                'message': u'產品搜尋成功'}

    def get(self):
        """
        讀取產品
        """

        # 用 product_id 取得 產品
        product_obj = self.product_service.get_by_id(product_id=self.request.matchdict['product_id'],
                                                     check=True)

        return {'response': {'product': product_obj.__json__(show_main_image=True,
                                                             show_llluminance_image=True,
                                                             show_category=True,
                                                             show_product_imgaes=True)},
                'message': '產品讀取成功'}

    def edit(self):
        """
        編輯產品
        """

        # 檢查輸入參數
        request_data = form.ProductEditForm().to_python(self.request_params)
        request_json_list = json.loads(self.request_params.get('product_images_list', '{}').replace('\r\n', '\\r\\n'))
        # 更新產品
        with transaction.manager:
            # 用 product_id 取得 產品
            product_obj = self.product_service.get_by_id(product_id=self.request.matchdict['product_id'],
                                                         check=True)

            if request_data.get('status') == "delete":
                product_obj_sequence = product_obj.sequence

                request_data['sequence'] = 0

            if request_data.get('status') == "delete":
                self.product_service.get_by_sequence_minus_one(language=request_data.get('language'),
                                                            number_set=product_obj_sequence)

            product_obj = self.product_service.update(old_obj=product_obj,
                                                      user_id=self.account_id,
                                                      **request_data)

            if request_data.get('status') != "delete":
                if len(request_json_list)!=0:
                    request_data_img={}
                    request_data_img['product_id'] = uuid.UUID(self.request.matchdict['product_id'])
                    product_image_obj, total_count = self.product_image_service.get_list(show_count=True, **request_data_img)

                    for product_image in product_image_obj:
                        self.product_image_service.delete_by_id(product_image_id=product_image.product_image_id)

                    # 創建工程實績圖片集
                    for product_image in request_json_list:
                        product_image['product_id'] = product_obj.product_id
                        product_image['image_id'] = uuid.UUID(product_image['image_id'])
                        self.product_image_service.create(**product_image)

            # # 更新 工程實績 圖片集
            # old_product_image_id_list = [str(wi.product_image_id) for wi in product_obj.product_images]
            #
            # new_product_image_id_list = []
            # for wi in request_data.get('product_images', []):
            #     # 如果有 product_image_id 代表已有資料，更新，否則創建
            #     if wi.get('product_image_id'):
            #         work_image_obj = self.product_image_service.update_by_id(**wi)
            #         new_product_image_id_list.append(str(work_image_obj.product_image_id))
            #     else:
            #         wi['product_id'] = product_obj.product_id
            #         work_image_obj = self.product_image_service.create(**wi)
            #         new_product_image_id_list.append(str(work_image_obj.product_image_id))
            #
            # del_list = list(set(old_product_image_id_list) - set(new_product_image_id_list))
            # # 刪除
            # for product_image_id in del_list:
            #     self.product_image_service.delete_by_id(product_image_id=product_image_id)


        return {'response': {'product': product_obj.__json__()},
                'message': u'產品修改成功'}

    def delete(self):
        """
        刪除產品
        """

        # 用 product_id 取得 工程實績
        product_obj = self.product_service.get_by_id(product_id=self.request.matchdict['product_id'],
                                               check=True)
        # 更新 status 達到軟刪除
        with transaction.manager:
            product_obj = self.product_service.update(old_obj=product_obj, status="delete")

        return {'response': {'product': product_obj.__json__()},
                'message': u'刪除產品成功'}

    def multi_set(self):
        """
        一次處理多筆
        :return:
        """
        _ = self.localizer

        # 檢查輸入參數
        request_json_list = json.loads(self.request_params.get('json_data', '{}').replace('\r\n', '\\r\\n'))

        response = []
        with transaction.manager:
            for work_data in request_json_list:
                work_obj = self.product_service.update_by_id(**work_data)
                response.append(work_obj.__json__())

        return {'response': {'work_list': response},
                'message': _(u'banner 創建更新成功')}

