<%inherit file="beefun:templates/backend/master.mako"/>

<%block name="css">
    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css" />
    <!-- 圖片預覽 -->

</%block>

<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span class="m-font-898b96">${ _(u'產品管理')}</span> <i class="fa fa-caret-right"></i>
                        <span>${ _(u'產品列表')}</span>
                    </h3>
                </div>
                <div>
                    <button type="button" class="btn btn-warning m-btn m-btn--outline-2x btn-sm mr-5px btn-search">
                    <span>
                                        <i class="fa fa-search"></i>
                                        <span>
                                            ${ _(u'搜尋')}
                                        </span>
                    </span>
                    </button>
                    <button type="button" class="btn btn-success m-btn m-btn--outline-2x btn-sm save_btn">
                    <span>
                                        <i class="fa fa-check"></i>
                                        <span>
                                            ${ _(u'序列存檔')}
                                        </span>
                    </span>
                    </button>
                    <button type="button" class="btn btn-brand m-btn m-btn--outline-2x btn-sm" onclick="javascript:window.open('${ request.route_url('page.product.create') }','_self')">
                        ## <button type="button" class="btn btn-brand m-btn m-btn--outline-2x btn-sm" onclick="javascript:window.open(" { request.route_url('page.news.create') }",'_self')">
                        <span>
                                        <i class="fa fa-plus"></i>
                                        <span>
                                            ${ _(u'新增')}
                                        </span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="product_list" data-parent="product">
            <!--begin::Portlet-->
            <div class="languages hide"></div>
            <div class="m-portlet">
                <div class="m-portlet__body">
                    <ul class="nav nav-tabs" role="tablist">
                        %for index, language_list in enumerate(request.registry.settings.get('available_langs',[])):
                            %if index == 0:
                                <li class="nav-item">
                                    <a class="nav-link active" id="${ language_list[0] }" data-toggle="tab" href="#m_tabs_1_${index}">${ language_list[1] }</a>
                                </li>
                            %else:
                                <li class="nav-item">
                                    <a class="nav-link" id="${ language_list[0] }" data-toggle="tab" href="#m_tabs_1_${index}">${ language_list[1] }</a>
                                </li>
                            %endif
                        %endfor
                    </ul>
                    <div class="tab-content">
                        %for index, language_list in enumerate(request.registry.settings.get('available_langs',[])):
                            %if index == 0:
                                <div class="tab-pane active" id="m_tabs_1_${index}" role="tabpanel">
                                    <!--load work_list-1.html-->
                                </div>
                            %else:
                                <div class="tab-pane" id="m_tabs_1_${index}" role="tabpanel">
                                    <!--load work_list-3.html-->
                                </div>
                            %endif
                        %endfor
                    </div>
                </div>
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right" id="search_form">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12 col-lg-6 col-xl-4">
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4">
                                        ${ _(u'分類')}
                                    </label>
                                    <div class="col-8">
                                        <div class="input-group m-input-group ">
                                            <select class="form-control se1 selectOne" id="popular_type" name="popular_type">
                                                <option value="product_light">${ _(u'燈具')}</option>
                                                <option value="product_green">${ _(u'綠建材')}</option>
                                            </select>
                                        </div>
                                        <div class="input-group m-input-group mt-7px class-a">
                                            <select class="form-control se1 " id="category_id" name="category_id">

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4">
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4">
                                        ${ _(u'產品名稱')}
                                    </label>
                                    <div class="col-8">
                                        <input type="text" class="form-control m-input" placeholder="" name="title" id="title">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4">
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4">
                                        ${ _(u'狀態')}
                                    </label>
                                    <div class="col-8">
                                        <select class="form-control se1" name="status" id="status">
                                            <option value="">${ _(u'All')}</option>
                                            <option value="show">${ _(u'上架')}</option>
                                            <option value="hiden">${ _(u'下架')}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
                <!--end::Form-->
                <!--Begin::Section-->
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-12">
                            <table class="table m-table m-table--head-bg-success">
                                <thead>
                                <tr>
                                    <th>
                                        ${ _(u'排序')}
                                    </th>
                                    <th>
                                        ${ _(u'主打')}
                                    </th>
                                    <th>
                                        ${ _(u'分類')}
                                    </th>
                                    <th>
                                        ${ _(u'產品名稱')}
                                    </th>
                                    <th>
                                        ${ _(u'狀態')}
                                    </th>
                                    <th>
                                        <!--操作-->
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="product_list">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--End::Section-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>
</%block>



<%block name="script">
    <!-- 圖片預覽 -->
    <script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/readimage.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/make_table/make_table.js')}" type="text/javascript"></script>
    <!-- 圖片預覽 -->
    <script>




        var csrfToken = "${request.session.get_csrf_token()}";
        $('.se1').select2();
        var language='${ request.registry.settings.get('available_langs', [['zh_Hant', u'繁體中文']])[0][0] }';

        $(document).on('click', '.nav-link', function (e) {


            $('#product_list').empty();
            Table.search_parameter='&language='+$(this).attr('id');
            $('.btn-search').click();

            var type_set = $('#popular_type').val();

            var url = '${ request.route_url('api.category')}?page=1&language='+$('.nav-link.active').attr('id')+'&type='+type_set;
            LoadObj(url);


        });

        var Table = new MakeTable({
            'dom_search_form': $('form#search_form'),
            'dom_search_btn': $('.btn-search'),
            'dom_table_tbody': $('.product_list'),
            'init_search_parameter': '&language='+language,
            'ajax_search_url':  "${ request.route_url('api.product') }",
            'ajax_csrf_token':csrfToken,
            'table_model': function (obj, page) {
                //開始產生頁面資料
                var $list = obj['product_list'];
                var html_str = '';
                //var data_table_row = [];

                $.each($list, function (i, item) {
                    item.index = (i + 1 + ((page - 1) * 10));

                    var status="";


                    switch (item.status) {
                        case "show":
                            status = "${ _(u'開通')}";
                            break;
                        case "hiden":
                            status = "${ _(u'關閉')}";
                            break;
                        case "delete":
                            status = "${ _(u'刪除')}";
                            break;
                    }




                    var tb_row = " <tr class='obj_index'>" ;
                    tb_row += "<td>"+"<input type='text' class='form-control input-mini number_set' value='"+item.sequence+"'>"+"</td>";
                    tb_row +="<td>";
                    tb_row +='<label class="m-checkbox">';
                    if(item.popular=="show"){
                        tb_row +='<input class="check"  type="checkbox" checked="">';
                    }else{
                        tb_row +='<input class="check" type="checkbox">';
                    }

                    tb_row +='<span></span>';
                    tb_row +='</label>';
                    tb_row +="</td>";
                    tb_row +="<td>";
                    tb_row +='<ul class="ulli-no">';
                    if(item.popular_type=='product_light'){
                        tb_row +='<li>燈具</li>';
                    }else{
                        tb_row +='<li>綠建材</li>';
                    }

                    tb_row +='<li>'+item.category.name+'</li>';
                    tb_row +='</ul>';

                    tb_row +="</td>";

                    tb_row +="<td>"+item.title+"</td>";
                    tb_row +="<td>";
                    if(status=="${ _(u'開通')}") {
                        tb_row+='<span class="m-badge m-badge--info m-badge--wide m-badge--rounded m--font-bolder">${ _(u'上架')}</span>';
                    }else{
                        tb_row+='<span class="m-badge m-badge--metal m-badge--wide m-badge--rounded m--font-bolder">${ _(u'下架')}</span>';
                    }
                    tb_row +="<input type='text' value='"+item.product_id+"'  class='form-control product_id hide'>";
                    tb_row +="</td>";
                    tb_row +='<td align="right">';
                    tb_row +='<button type="button" class="btn btn-outline-danger m-btn m-btn--outline-2x btn-sm"'+ 'onclick="javascript:window.open('+"'${ request.route_url('page.product.update',product_id='') }"+item.product_id+"','_self'"+')"><span><i class="fa fa-edit"></i><span>${ _(u'編輯')}</span></span></button>';
                    tb_row +='<button type="button" class="btn btn-outline-metal m-btn m-btn--outline-2x btn-sm del_btn"><span><i class="la la-eye-slash"></i><span>${ _(u'刪除')}</span></span></button>';



                    tb_row +="</td>";
                    tb_row += "</tr>";
                    html_str += tb_row;
                });
                $('#product_list').empty();
                $('#product_list').append(html_str);//Table頁面資料繪製
            }
        });


        $(document).on('change', '#popular_type', function (e) {
            var type_set = $('#popular_type').val();

            var url = '${ request.route_url('api.category')}?page=1&language='+$('.nav-link.active').attr('id')+'&type='+type_set;
            LoadObj(url);

        });

        $(document).on('change', '#category_type', function (e) {
            type_set = $('#category_type').val();
            $('#product_category_tbody').empty();
            Table.search_parameter='&language=en&type='+type_set;
            Table.start();
        });




        $(document).on('click', '.del_btn', function (e) {
            var url = "${ request.route_url('api.product.detail',product_id="")}"+$(this).closest(".obj_index").find('.product_id').val();
            var form_data = new FormData();

            form_data.append('status','delete');
            form_data.append('language',$('.nav-link.active').attr('id'));


            ajax(url, "POST", form_data, null, function (response) {
                if (response['status']) {

                    alert("${_('儲存成功')}");
                    ##  $('#product_list').empty();
                    ##  Table.start();
                     $('.btn-search').click();
                } else {
                    alert(response['message']);
                }
            });
        });


        $(document).on('change', '#type', function (e) {

            if($('.nav-link.active').text()=='英文'){
                language='en'
            }else if($('.nav-link.active').text()=='繁體中文'){
                language='zh_Hant'

            }else if($('.nav-link.active').text()=='越南文'){
                language='vi'

            }else{
                language='id'

            }

            type_set = $('#type').val();
            if($('#type').val()=='light'){
                var url = '${ request.route_url('api.category')}?page=1&language='+language+'&type=product_light';
            }else{
                var url = '${ request.route_url('api.category')}?page=1&language='+language+'&type=product_green';
            }

            LoadObj(url);

        });





        //讀取分類
        function LoadObj(url) {
            //設定已選擇確認列表
            $.ajax({
                type: "GET",
                url: url,
                dataType: 'JSON',
                headers: {'X-CSRF-Token': csrfToken},
                success: function (response) {

                    if (response['status'] == true) {
                        append_obj_list(response)
                    } else {
                        if(response['message']=='驗證錯誤'){
                            location.reload();
                        }
                    }
                }
            });
        }
        function append_obj_list(obj) {
            //預設清空類表內部

            $('#category_id').empty();

            //設定後端回傳格式



            var list = obj['response']['category_list'];

            //設定html內容
            var html = '<option value="">請選擇</option>';
            for (var i = 0; i < list.length; i++) {

                html += "<option value='"+list[i].category_id +"'>"+list[i].name+"</option>";

            }
            $('#category_id').append(html)

        }


        $(document).ready(function (e) {

            var search = location.search;
            if(search!=""){
                if(location.search.split("&")[0].split("=")[1]=="product_light"){
                    var title="燈具";
                    var lang = "product_light";
                    $("#popular_type").val("product_light")
                    $("#select2-popular_type-container").attr("title",title)
                    $("#select2-popular_type-container").text(title)

                }else if(location.search.split("&")[0].split("=")[1]=="product_green"){
                    var title="綠建材";
                    var lang = "product_green";
                    $("#popular_type").val("product_green")
                    $("#select2-popular_type-container").attr("title",title)
                    $("#select2-popular_type-container").text(title)
                }else{
                    var title="燈具"
                    var lang = "product_light";
                    $("#popular_type").val("product_light")
                    $("#select2-popular_type-container").attr("title",title)
                    $("#select2-popular_type-container").text(title)
                }
                $('.nav-link').removeClass('active');
                if(location.search.split("&")[1].split("=")[1]=='zh_Hant'){
                    var lang_set = 'zh_Hant'
                    Table.search_parameter='&language='+lang_set;
                    $("#zh_Hant").addClass('active')
                }else if(location.search.split("&")[1].split("=")[1]=='en'){
                    var lang_set = 'en'
                    Table.search_parameter='&language='+lang_set;
                    $("#en").addClass('active')
                }else if(location.search.split("&")[1].split("=")[1]=='vi'){
                    var lang_set = 'vi'
                    Table.search_parameter='&language='+lang_set;
                    $("#vi").addClass('active')
                }else if(location.search.split("&")[1].split("=")[1]=='id'){
                    var lang_set = 'id'
                    Table.search_parameter='&language='+lang_set;
                    $("#id").addClass('active')

                }else{
                    var lang_set = 'zh_Hant'
                    Table.search_parameter='&language='+lang_set;
                    $("#zh_Hant").addClass('active')
                }
                var url = '${ request.route_url('api.category')}?page=1&language='+lang_set+'&type='+lang;
            }else{

                var url = '${ request.route_url('api.category')}?page=1&language='+$('.nav-link.active').attr('id')+'&type=product_light';
            }


            Table.search_parameter='&language='+$('.nav-link.active').attr('id');




            LoadObj(url);
            $('.btn-search').click();




        });

        $(document).on('change', '.check', function (e) {
            var url = "${ request.route_url('api.product.detail',product_id="")}"+$(this).closest(".obj_index").find('.product_id').val();
            var form_data = new FormData();
            if(this.checked){
                form_data.append('popular', 'show');
            }else{
                form_data.append('popular','hiden');
            }
            ajax(url, "POST", form_data, null, function (response) {
                if (response['status']) {

                    alert("${_('儲存成功')}");
                    $('.btn-search').click();
                } else {
                    alert(response['message']);
                }
            });
        });



        function ajax(url, method, form_data, btn, callback){
            $.ajax({
                url: url,
                type: method,
                data: form_data,
                ##  dataType: 'JSON',
                contentType: false,
                processData: false,
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    if (btn) btn.attr("disabled", true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                    if (btn) btn.attr("disabled", false)
                },
                success: function (response) {
                    callback(response);
                },
                complete: function () {
                    if (btn) btn.attr("disabled", false);
                }
            });
        }

        $(document).on('click','.save_btn',function(){
            var event_btn = $(this);

            var product_data_list = [];
            var number_set_check = [];
            //第一層
            $('.obj_index').each(function(i, v){
                if(parseInt($(v).find('.number_set').val())==0){
                    alert('請勿輸入0');
                    return false;
                }
                var work_data = {
                    'product_id': $(v).find('.product_id').val(),
                    'sequence': $(v).find('.number_set').val()
                }
                product_data_list.push(work_data);
                number_set_check.push($(v).find('.number_set').val());
            });



            var number_set_check_onleny=number_set_check.filter(function (el, i, arr) {
                return arr.indexOf(el) === i;
            });

            if(product_data_list.length==number_set_check_onleny.length){
                product_save(event_btn, product_data_list);
            }else{
                alert("${ _(u'序列有重複，請修正！！')}");
            }

            return false;

        });

        function product_save(event_btn, product_data_list){
            var url = "${ request.route_url('api.product.multi.set')}";
            var work_data_form = new FormData();
            work_data_form.append('json_data', JSON.stringify(product_data_list))

            ajax(url, 'POST', work_data_form, event_btn, function(data){
                if (event_btn) event_btn.attr("disabled", false);

                if (data['status']) {
                    $('.btn-search').click();
                } else {
                    alert('${_(u"創建失敗")}')
                }
            });
        }




    </script>

</%block>


