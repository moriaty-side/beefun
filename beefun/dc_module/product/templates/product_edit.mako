<%inherit file="beefun:templates/backend/master.mako"/>

<%block name="css">
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/allen_assets/cropper/cropper.min.css') }">
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/allen_assets/cropper/cropper_plugin.css') }">
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/allen_assets/diyUpload/css/webuploader.css') }">
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/allen_assets/diyUpload/css/diyUpload.css') }">
    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css" />
</%block>

<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span class="m-font-898b96">${ _(u'產品管理管理')}</span> <i class="fa fa-caret-right"></i>
                        <span class="m-font-898b96">${ _(u'產品列表')}</span> <i class="fa fa-caret-right"></i> <span>${ _(u'修改')}</span>
                    </h3>
                </div>
                <div>
                    <button type="button" class="btn btn-metal m-btn m-btn--outline-2x btn-sm mr-5px" onclick="javascript:window.open('${ request.route_url('page.product.list') }','_self')">
                    <span>
                                        <i class="fa fa-close"></i>
                                        <span>
                                            ${ _(u'取消')}
                                        </span>
                    </span>
                    </button>
                    <button type="button" class="btn btn-success m-btn m-btn--outline-2x btn-sm" id="create_btn">
                    <span>
                                        <i class="fa fa-check"></i>
                                        <span>
                                            ${ _(u'修改存檔')}
                                        </span>
                    </span>
                    </button>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="product_list" data-parent="product">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right create_form" id="create_form">
                    <!--Begin::Section-->
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'平台')}
                                    </label>
                                    <div class="col-8 col-md-4">
                                        <select class="form-control se1" id="language" name="language" disabled="disabled">


                                            %if product_obj.get('language')=='zh_Hant':
                                                <option value="${product_obj.get('language')}">${ _(u'繁體中文')}</option>
                                            %elif product_obj.get('language')=='en':
                                                <option value="${product_obj.get('language')}">${ _(u'英文')}</option>
                                            %elif product_obj.get('language')=='vi':
                                                <option value="${product_obj.get('language')}">${ _(u'越南文')}</option>
                                            %else:
                                                <option value="${product_obj.get('language')}">${ _(u'印尼文')}</option>
                                            %endif
                                        </select>
                                        ##                                         <div id="title-error" class="my_error_category hide" style="color: red; font-size:0.85rem;">標題為必填值</div>
                                    </div>
                                </div>
                                 <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'狀態')}
                                    </label>
                                    <div class="col-8 col-md-4">
                                        <select class="form-control se1" id="status" name="status" >




                                            <option value="show" ${ 'selected' if product_obj.get('status') == 'show' else '' }>${ _(u'上架')}</option>

                                            <option value="hiden" ${ 'selected' if product_obj.get('status') == 'hiden' else '' }>${ _(u'下架')}</option>

                                        </select>

                                    </div>

                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                        ${ _(u'主打')}
                                    </label>
                                    <div class="col-8 col-md-4">
                                        <label class="m-checkbox">
                                            <input type="checkbox" ${ 'checked' if product_obj.get('popular') == 'show' else '' } id="popular">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'分類')}
                                    </label>
                                    <div class="col-8 col-md-4">
                                        <div class="m-form__control inline-block selectOne mt-7px">
                                            %if product_obj.get('popular_type')=='product_light':
                                                <label class="m-radio" >
                                                    <input type="radio" class="type" id="category_type" name="optionsRadios" value="product_light" checked="checked" disabled="disabled">
                                                    燈具
                                                    <span></span>
                                                </label>
                                                <label class="m-radio">
                                                    <input type="radio" class="type" id="category_type" name="optionsRadios" value="product_green" disabled="disabled">
                                                    綠建材
                                                    <span></span>
                                                </label>
                                            %else:
                                                <label class="m-radio" >
                                                    <input type="radio" class="type" id="category_type" name="optionsRadios" value="product_light" disabled="disabled">
                                                    燈具
                                                    <span></span>
                                                </label>
                                                <label class="m-radio">
                                                    <input type="radio" class="type" id="category_type" name="optionsRadios" value="product_green" checked="checked" disabled="disabled">
                                                    綠建材
                                                    <span></span>
                                                </label>
                                            %endif

                                        </div>
                                        <div class="class-a" >
                                            <select class="form-control se1" id="category_id" name="category_id">

                                            </select>
                                            <div id="title-error" class="my_error_category hide" style="color: red; font-size:0.85rem;">${ _(u'標題為必填值')}</div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'產品名稱')}
                                    </label>
                                    <div class="col-8">
                                        <input class="form-control" placeholder="" id="title" name="title" value="${product_obj.get('title')}">
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'列表圖')}
                                    </label>
                                    <div class="col-8">
                                        <div class="cropper_upload_btn obj_here" data-fk="" data-cut="false"></div>
                                        <div class="help-block">${ _(u'尺寸')}：700x500</div>
                                        <div id="title-error" class=" my_error_image hide" style="color: red; font-size:0.85rem;">${ _(u'圖片為必填值')}</div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                        <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'照片')}
                                    </label>
                                    <div class="col-8 obj_index">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail text-l hide" style="width: 384px; height: 178px;">
                                                <img src="http://www.placehold.it/384x178/EFEFEF/AAAAAA&amp;text=1920x890" alt="">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail hide" style="max-width: 384px; max-height: 178px;"><img src="" alt="" style="width: 100%;">
                                            </div><div><span class="btn default btn-file">
                                            <span class="fileinput-new"> ${ _(u'選擇圖片')} </span>
                                            <span class="fileinput-exists"> ${ _(u'選擇圖片')}</span>
                                            <input type="file" class="image_file" onchange="readImage(this,700,500);"></span>
                                            <a href="#" class="btn red fileinput-exists btn_del_pic hide" data-dismiss="fileinput"> ${ _(u'移除')} </a></div></div>
                                    </div>

                                </div>
                                 <div class="form-group m-form__group">
                                      <label class="col-form-label col-4 col-md-2">


                                    </label>
                                    <div class="">
                                         ${ _(u'尺寸')}：700x500
                                    </div>

                                </div>
                                <div class="form-group m-form__group">
                                    <div class="col-8 offset-4 offset-md-2">
                                        <table class="table m-table m-table--head-bg-success">
                                            <thead>
                                            <tr>
                                                <th>
                                                    ${ _(u'排序')}
                                                </th>
                                                <th>
                                                    ${ _(u'縮圖')}
                                                </th>
                                                <th>
                                                    ${ _(u'圖片說明')}
                                                </th>
                                                <th>
                                                    <!--操作-->
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody id="image_list">
                                                %for image_obj in product_image_obj:
                                                    <tr class="image_obj">
                                                        <td scope="row">
                                                            <input type="text" class="form-control input-mini image_number_set" value="${image_obj.get('sequence')}">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control input-mini hide image_id" value="${image_obj.get('image').get('image_id')}">
                                                            <img src="${ request.static_path('beefun:static/uploads')}${image_obj.get('image').get('url')}" alt="" style="max-width: 384px; max-height: 178px;">
                                                        </td>
                                                        <td>
                                                            <textarea class="form-control description" rows="5" style="resize:none;" placeholder="">${image_obj.get('description')}</textarea>
                                                        </td>
                                                        <td align="right">
                                                            <button type="button" class="btn btn-outline-metal m-btn m-btn--outline-2x btn-sm del_image_btn"><span><i class="la la-eye-slash"></i><span>封存</span></span></button>
                                                        </td>
                                                    </tr>

                                                %endfor
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                            <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'產品描述')}
                                    </label>
                                    <div class="col-8">
                                        <div class="summernote" id="content">${product_obj.get('content')|n}</div>
                                                                                <div id="title-error" class=" my_error_content hide" style="color: red; font-size:0.85rem;">${ _(u'描述為必填值')}</div>
                                    </div>
                                </div>
                                <div class="cont-a ${ 'hide' if product_obj.get('popular_type') == 'product_green' else '' }">
                                    <div class="form-group m-form__group">
                                        <label class="col-form-label col-4 col-md-2">
                                            ${ _(u'產品規格')}
                                        </label>
                                        <div class="col-8 child-cont">
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'光源模式')}
                                                </label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="light_mode" value="${product_obj.get('light_mode')}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'燈具尺寸')}
                                                </label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="lamp_size" value="${product_obj.get('lamp_size')}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'安裝方式')}
                                                </label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="installation_method" value="${product_obj.get('installation_method')}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'輸入電壓')}
                                                </label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="input_voltage" value="${product_obj.get('input_voltage')}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'瓦數')}
                                                </label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="watts" value="${product_obj.get('watts')}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'功率因素')}
                                                </label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="power_factor" value="${product_obj.get('power_factor')}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'燈具光效')}
                                                </label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="lighting_effect" value="${product_obj.get('lighting_effect')}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'光通量')}
                                                </label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="luminous_flux" value="${product_obj.get('luminous_flux')}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'色溫')}
                                                </label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="color_temperature" value="${product_obj.get('color_temperature')}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'光束角')}
                                                </label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="beam_angle" value="${product_obj.get('beam_angle')}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'演色指數')}
                                                </label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="color_rendering" value="${product_obj.get('color_rendering')}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'適用環境')}
                                                </label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="applicable_environment" value="${product_obj.get('applicable_environment')}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'防水防塵等級')}
                                                </label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="waterproof_and_dustproof" value="${product_obj.get('waterproof_and_dustproof')}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'材質重量')}
                                                </label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="material_weight" value="${product_obj.get('material_weight')}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label class="col-form-label col-4 col-md-2">
                                            ${ _(u'表格顏色')}
                                        </label>
                                        <div class="col-8 child-cont">
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'表頭')}
                                                </label>
                                                <div class="col-8 col-md-4">
                                                    <div class="form-group m-form__group">
                                                        <label class="col-form-label col-4 col-md-2">
                                                            ${ _(u'主')}
                                                        </label>
                                                        <div class="col-4">
                                                            <input name="color1" class="jscolor" value="${product_obj.get('color_form_f_one')}" id="color_form_f_one">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group">
                                                        <label class="col-form-label col-4 col-md-2">
                                                            ${ _(u'副')}
                                                        </label>
                                                        <div class="col-4">
                                                            <input name="color1" class="jscolor" value="${product_obj.get('color_form_f_sec')}" id="color_form_f_sec">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label class="col-form-label col-4 col-md-2">
                                                    ${ _(u'內容')}
                                                </label>
                                                <div class="col-8 col-md-4">
                                                    <div class="form-group m-form__group">
                                                        <label class="col-form-label col-4 col-md-2">
                                                            ${ _(u'主')}
                                                        </label>
                                                        <div class="col-4">
                                                            <input name="color1" class="jscolor" value="${product_obj.get('color_form_c_one')}" id="color_form_c_one">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group">
                                                        <label class="col-form-label col-4 col-md-2">
                                                            ${ _(u'副')}
                                                        </label>
                                                        <div class="col-4">
                                                            <input name="color1" class="jscolor" value="${product_obj.get('color_form_c_sec')}" id="color_form_c_sec">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label class="col-form-label col-4 col-md-2">
                                            ${ _(u'照度圖')}
                                        </label>
                                        <div class="col-8">
                                            <div class="cropper_upload_btn_image obj_here" data-fk="" data-cut="false"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cont-b ${ 'hide' if product_obj.get('popular_type') == 'product_light' else '' }">
                                    <div class="form-group m-form__group">
                                        <label class="col-form-label col-4 col-md-2">
                                            ${ _(u'產品規格')}
                                        </label>
                                        <div class="col-8">
                                            %if product_obj.get('popular_type')=='product_light':
                                                <div class="summernote" id="product_specification"></div>
                                            %else:
                                                <div class="summernote" id="product_specification">${product_obj.get('product_specification')|n}</div>
                                            %endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>

</%block>



<%block name="script">


    <script src="${ request.static_url('beefun:static/html_fronted_work/backstage/assets/my_js/jquery.serialize-object.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/allen_assets/cropper/cropper.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/allen_assets/cropper/cropper_plugin.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>

    <script src="${ request.static_path('beefun:static/assets/other/custom_validation.js')}"
            type="text/javascript"></script>




    <script type="text/javascript" src="${ request.static_path('beefun:static/assets/other/jscolor.js') }"></script>

    <script>
        var csrfToken = "${request.session.get_csrf_token()}";
        $('.se1').select2();

        $('.summernote').summernote({
            height: 500,// set editor height
        });

        $(document).on('change', '#category_type', function (e) {
            type_set = $(this).val();
            $('#category_id').empty();
            $("#language").val()
            var url = '${ request.route_url('api.category')}?page=1&language='+$("#language").val()+'&type='+type_set;
            LoadObj(url);
        });

            ##  //切換
        ##  $('.selectOne').on('change','input[type="radio"]',function(){
        ##      $('.cont-a').toggleClass('hide');
        ##      $('.cont-b').toggleClass('hide');
        ##      $('.se1').select2();
        ##  })


        //讀取分類
        function LoadObj(url) {
            //設定已選擇確認列表
            $.ajax({
                type: "GET",
                url: url,
                dataType: 'JSON',
                headers: {'X-CSRF-Token': csrfToken},
                success: function (response) {

                    if (response['status'] == true) {
                        append_obj_list(response)
                    } else {
                        if(response['message']=='驗證錯誤'){
                            location.reload();
                        }
                    }
                }
            });
        }
        function append_obj_list(obj) {
            //預設清空類表內部

            $('#category_id').empty();

            //設定後端回傳格式



            var list = obj['response']['category_list'];

            //設定html內容
            var html = '';
            for (var i = 0; i < list.length; i++) {

                if(list[i].category_id=="${product_obj.get('category_id')}"){
                    html += "<option value='"+list[i].category_id +"' selected='selected' >"+list[i].name+"</option>";
                }else{
                    html += "<option value='"+list[i].category_id +"'>"+list[i].name+"</option>";
                }

            }
            $('#category_id').append(html)

        }

        $(document).ready(function (e) {

            if($('#category_type')[0].checked==true){
                var url = '${ request.route_url('api.category')}?page=1&language='+$("#language").val()+'&type=product_light';
            }else{
                var url = '${ request.route_url('api.category')}?page=1&language='+$("#language").val()+'&type=product_green';
            }


            LoadObj(url);

            // 裁切圖片功能
            var upload_callback = function(return_data, element) {
                var new_image_link = '${ request.static_path('beefun:static/uploads')}'+return_data["url"];
                $('.cropper_upload_btn').find('img').attr('src', new_image_link);
                $('.cropper_upload_btn').find('img').attr('image_id', return_data['image_id']);
                $('.cropper_upload_btn').parent().parent().removeClass('has-error')

                $('.cropper_upload_btn').find('span.help-block').remove();


                ## $('#bounce-window').find('.cropper-oragin.active-cropper img').attr('src', new_image_link).closest('.active-cropper').removeClass('active-cropper');
               $(element).modal('hide');
                ## $('#bounce-window #bounce-content').show();
            }
            // 裁切圖片功能
            var upload_callback_700 = function(return_data, element) {
                var new_image_link = '${ request.static_path('beefun:static/uploads')}'+return_data["url"];
                $('.cropper_upload_btn_700').find('img').attr('src', new_image_link);
                $('.cropper_upload_btn_700').find('img').attr('image_id', return_data['image_id']);
                $('.cropper_upload_btn_700').parent().parent().removeClass('has-error')

                $('.cropper_upload_btn_700').find('span.help-block').remove();


                ## $('#bounce-window').find('.cropper-oragin.active-cropper img').attr('src', new_image_link).closest('.active-cropper').removeClass('active-cropper');
               $(element).modal('hide');
                ## $('#bounce-window #bounce-content').show();
            }
            // 裁切圖片功能
            var upload_callback_image = function(return_data, element) {
                var new_image_link = '${ request.static_path('beefun:static/uploads')}'+return_data["url"];
                $('.cropper_upload_btn_image').find('img').attr('src', new_image_link);
                $('.cropper_upload_btn_image').find('img').attr('image_id', return_data['image_id']);
                $('.cropper_upload_btn_image').parent().parent().removeClass('has-error')

                $('.cropper_upload_btn_image').find('span.help-block').remove();


                ## $('#bounce-window').find('.cropper-oragin.active-cropper img').attr('src', new_image_link).closest('.active-cropper').removeClass('active-cropper');
               $(element).modal('hide');
                ## $('#bounce-window #bounce-content').show();
            }
            $('.cropper_upload_btn').cropper_plugin(options = {
                url: "${request.route_url('api.image')}",
                image_path: 'https://www.placehold.it/700x500/EFEFEF/AAAAAA&text=700x500',
                image_ratio: '700/500',
                csrfToken: csrfToken
            }, callback = upload_callback);

            $('.cropper_upload_btn_700').cropper_plugin(options = {
                url: "${request.route_url('api.image')}",
                image_path: 'https://www.placehold.it/700x500/EFEFEF/AAAAAA&text=700x500',
                image_ratio: '700/500',
                csrfToken: csrfToken
            }, callback = upload_callback_700);

            $('.cropper_upload_btn_image').cropper_plugin(options = {
                url: "${request.route_url('api.image')}",
                image_path: 'https://www.placehold.it/350x250/EFEFEF/AAAAAA&text=image',
                image_ratio: '330/235',
                csrfToken: csrfToken
            }, callback = upload_callback_image);


            % if product_obj.get('main_image_id'):
                $('.cropper_upload_btn').find('img').attr('src', "${ request.static_path('beefun:static/uploads')}${ product_obj.get('main_image').get('url')}" );
                $('.cropper_upload_btn').find('img').attr('image_id', "${ product_obj.get('main_image_id')}" );
            % endif

            % if product_obj.get('product_image_id'):
                $('.cropper_upload_btn_700').find('img').attr('src', "${ request.static_path('beefun:static/uploads')}${ product_obj.get('product_image').get('url')}" );
                $('.cropper_upload_btn_700').find('img').attr('image_id', "${ product_obj.get('product_image_id')}" );
            % endif

            % if product_obj.get('light_image_id') != 'None':
                $('.cropper_upload_btn_image').find('img').attr('src', "${ request.static_path('beefun:static/uploads')}${ product_obj.get('light_image').get('url')}" );
                $('.cropper_upload_btn_image').find('img').attr('image_id', "${ product_obj.get('light_image_id')}" );
            % endif




        });

            ## 新增消息表單驗證
          $('.create_form').validate({
            ignore: "",
            errorElement: 'div', //default input error message container
            errorClass: 'form-control-feedback', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                category_id: {
                    required: true,
                },
                title: {
                    required: true,
                    byteRangeLength:60
                }
            },
            messages: {
                category_id: {
                    required: "分類為必填值",
                },
                title: {
                    required: "標題為必填值",
                    byteRangeLength:"公司名稱最多輸入字數為60(中文算2個字)"
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-danger'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-danger');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            }
        });

            ## 創建登錄公司事件
        $(document).on('click', '#create_btn', function () {
            var event_btn = $(this);
            var $valid = $('.create_form').valid();
            if($($('.my_image_data')[0]).attr('image_id')==undefined){
                $($('.my_error_image')[0]).removeClass('hide');
                return false
            }else{
                $($('.my_error_image')[0]).addClass('hide');
            }

             if($('#content').summernote('code')=="<p><br></p>"){
                 alert('描述為必填值')
                 $('.my_error_content').removeClass('hide');
                return false
            }else{
                 $('.my_error_content').removeClass('hide');

            }




            if (!$valid ) {
                return false
            }
            var form_data = new FormData();
            var url = "${ request.route_url('api.product.detail',product_id="")}${product_obj.get('product_id')}";

            form_data.append('main_image_id', $($('.my_image_data')[0]).attr('image_id'));
            ##  form_data.append('product_image_id', $($('.my_image_data')[1]).attr('image_id'));
            form_data.append('title', $('#title').val());

            form_data.append('language', $('#language').val());
            form_data.append('category_id', $('#category_id').val());
            form_data.append('content', $('#content').summernote('code'));
            form_data.append('status', $('#status').val());

            if($("#popular")[0].checked==true){
                form_data.append('popular', 'show');
            }else{
                form_data.append('popular', 'hiden');
            }

            if($("#category_type")[0].checked==true){
                form_data.append('popular_type', 'product_light');
            }else{
                form_data.append('popular_type', 'product_green');
            }

            if($('#category_type')[0].checked==true){
                if($($('.my_image_data')[1]).attr('image_id')!=undefined){
                    form_data.append('light_image_id', $($('.my_image_data')[1]).attr('image_id'));
                }
                form_data.append('light_mode',$("#light_mode").val());
                form_data.append('lamp_size',$("#lamp_size").val());
                form_data.append('installation_method',$("#installation_method").val());
                form_data.append('input_voltage',$("#input_voltage").val());
                form_data.append('watts',$("#watts").val());
                form_data.append('lighting_effect',$("#lighting_effect").val());
                form_data.append('luminous_flux',$("#luminous_flux").val());
                form_data.append('color_temperature',$("#color_temperature").val());
                form_data.append('beam_angle',$("#beam_angle").val());
                form_data.append('color_rendering',$("#color_rendering").val());
                form_data.append('applicable_environment',$("#applicable_environment").val());
                form_data.append('power_factor',$("#power_factor").val());
                form_data.append('waterproof_and_dustproof',$("#waterproof_and_dustproof").val());
                form_data.append('material_weight',$("#material_weight").val());
                form_data.append('color_form_f_one',$("#color_form_f_one").val());
                form_data.append('color_form_f_sec',$("#color_form_f_sec").val());
                form_data.append('color_form_c_one',$("#color_form_c_one").val());
                form_data.append('color_form_c_sec',$("#color_form_c_sec").val());


            }else{
                form_data.append('product_specification', $('#product_specification').summernote('code'));
            }

            var image_data_list = [];
            var number_set_check = [];
            //第一層
            $('.image_obj').each(function(i, v){
                var image_data = {
                    'image_id': $(v).find('.image_id').val(),
                    'description':$(v).find('.description').val(),
                    'sequence': $(v).find('.image_number_set').val()
                }
                image_data_list.push(image_data);
                number_set_check.push($(v).find('.image_number_set').val());
            });
            var number_set_check_onleny=number_set_check.filter(function (el, i, arr) {
                return arr.indexOf(el) === i;
            });
            if(image_data_list.length!=0){
                if(image_data_list.length==number_set_check_onleny.length){
                    form_data.append('product_images_list', JSON.stringify(image_data_list));

                    ajax(url, 'POST', form_data, event_btn, function(data){
                        if (event_btn) event_btn.attr("disabled", false);

                        if (data['status']) {
                            window.location = '${ request.route_url("page.product.list") }?type='+data['response'].product.popular_type+'&language='+data['response'].product.language;
                        } else {
                            alert('${_(u"創建失敗")}')
                        }
                    });
                }else{
                    alert("${ _(u'序列有重複，請修正！！')}");
                }
            }else{
                alert("${ _(u'請上傳產品圖！！')}");
            }




        });

        function ajax(url, method, form_data, btn, callback){
            $.ajax({
                url: url,
                type: method,
                data: form_data,
                ##  dataType: 'JSON',
                contentType: false,
                processData: false,
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    if (btn) btn.attr("disabled", true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                    if (btn) btn.attr("disabled", false)
                },
                success: function (response) {
                    callback(response);
                },
                complete: function () {
                    if (btn) btn.attr("disabled", false);
                }
            });
        }

        //------------判斷圖片尺寸------------------//
        function readImage(file,img_w,img_h) {
            var src  =$(file).parents('.fileinput').find('.fileinput-preview.thumbnail img').attr('src'),
                    input = file,
                    exsits=$(file).parents('.fileinput').hasClass('fileinput-exists');
            btn_check = $(file).closest('.obj_index');


            var file_id= file.id,
                    imageFile = file.files[0];

            var reader = new FileReader();
            var image  = new Image();
            reader.readAsDataURL(imageFile);
            var w = 0,h=0;
            reader.onload = function(_file) {
                image.src    = _file.target.result;
                image.onload = function() {
                    w = this.width,
                            h = this.height;
                    // t = file.type,
                    // n = file.name,
                    // s = ~~(file.size/1024) +'KB';


                    if(w !=img_w || h !=img_h){

                        if(!exsits){

                            $(file).parents('.fileinput').addClass('fileinput-new').removeClass('fileinput-exists');
                        }

                        $(input).parents('.fileinput').find('.fileinput-preview.thumbnail img').attr('src',src);
                        alert("${ _(u'請選擇尺寸為')} "+img_w+' X '+img_h+"${ _(u' 的圖片')}");
                        $('#banner_tbody').empty();
                        Table.start();

                    }else {


                        upload_image(function (status, image_data) {
                            if (status) {
                                $('.image_file').val('')
                                              $('.fileinput').addClass('fileinput-new');
                              $('.fileinput').removeClass('fileinput-exists');

                                var set_img = "";

                                var form_data = new FormData();

                                form_data.append('image_id', image_data["image_id"]);

                                set_img += "<tr class='image_obj'>";
                                set_img += '<td scope="row">';
                                if ($('.image_number_set').length == 0) {
                                    set_img += '<input type="text" class="form-control input-mini image_number_set" value="1">';
                                } else {
                                    var set = parseInt($($('.image_number_set')[$('.image_number_set').length - 1]).val()) + 1;
                                    set_img += '<input type="text" class="form-control input-mini image_number_set" value="' + set + '">';
                                }
                                set_img += '</td>';
                                set_img += '<td>';
                                set_img += '<input type="text" class="form-control input-mini hide image_id" value="' + image_data["image_id"] + '">';
                                set_img += '<img src="' + '${ request.static_path('beefun:static/uploads')}' + image_data["url"] + '" alt="" style="max-width: 384px; max-height: 178px;">';
                                set_img += '</td>';
                                set_img += '<td>';
                                set_img += '<textarea class="form-control description" rows="5" style="resize:none;" placeholder="">' + image_data["name"] + '</textarea>';
                                set_img += '</td>';
                                set_img += '<td align="right">';

                                set_img += '<button type="button" class="btn btn-outline-metal m-btn m-btn--outline-2x btn-sm del_image_btn"><span><i class="la la-eye-slash"></i><span>封存</span></span></button>';
                                set_img += '</td>';


                                set_img += "</tr>";

                                $('#image_list').append(set_img);

                                ##  ajax(url, "POST", form_data, null, function (response) {
                            ##      if (response['status']) {

                                ##          alert("${_('儲存成功')}");
                            ##          $('#banner_tbody').empty();
                            ##          Table.start();
                            ##      } else {
                            ##          alert(response['message']);
                            ##      }
                            ##  });
                        }
                        });
                    }
                }
            };
            image.onerror= function() {
                alert('Invalid file type: '+ file.type);
            };





        }


        $(document).on('click', '.del_image_btn', function () {
            $(this).closest('.image_obj').remove();


        });


        function upload_image(callback){
            // 上傳圖片
            if (btn_check.find('.image_file')[0].files[0] == undefined){
                var image_id = btn_check.find('.logo_image').attr('image_id');
                if(image_id){
                    callback(true, {'image_id': image_id});
                }else{
                    alert("${ _(u'圖片上傳錯誤')}");
                    callback(false, null);
                }
            }else{
                var image_form_data = new FormData();
                image_form_data.append('image_file', btn_check.find('.image_file')[0].files[0]);
                var upload_image_url = "${ request.route_url('api.image') }";
                ajax(upload_image_url, "POST", image_form_data, $('.create_btn'), function(response){
                    if (response['status']) {

                        callback(true, response['response']['image_list'][0])
                    } else {
                        alert(response['message']);
                    }
                });
            }
        }



    </script>

</%block>


