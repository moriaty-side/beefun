# coding=utf8
from __future__ import unicode_literals

from sqlalchemy import (
    Column,
    Integer,
    Float,
    String,
    ARRAY,
    ForeignKey,
    DateTime
)
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
from ...base.meta_module import (Base, TimestampTable, GUID, uuid4)


class Member(Base, TimestampTable):
    """
    分類資料表
    """
    __tablename__ = 't_member'
    member_id = Column('f_member_id', GUID, primary_key=True, default=uuid4, doc=u"分類編號")

    name = Column('f_name', String(512), nullable=False, doc=u"line_id")

    line_id = Column('f_line_id', String(512), doc=u"line_id")

    notify_token = Column('f_notify_token', String, nullable=False, doc=u"notify_token(通知)")

    remark = Column('f_remark', String(512), doc=u'備註')

    _status = Column('f_status', Integer, nullable=False, default=10,
                     info={"show": 10, "hiden": 11, "delete": 12},
                     doc=u"狀態:10.顯示,11.隱藏,12:封存")

    def __repr__(self):
        return '<Object (member_id={0})>'.format(self.member_id)

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @classmethod
    def __getattributes__(cls):
        return [i[1:] if i[:1] == '_' else i for i in cls.__dict__.keys() if
                i[:1] != '_' or i == '_update_user_id' or i == '_create_user_id']

    @classmethod
    def __likeattribute__(cls, key_word):
        map_args = [i for i in cls.__dict__.keys() if key_word in i and i[:1] != '_']
        return map_args[0] if map_args else None

    def __json__(self):
        d = {
            'member_id': str(self.member_id),
            'line_id': self.line_id,
            'status': self.status,
        }

        return d


class MemberAttention(Base, TimestampTable):
    """
    分類資料表
    """
    __tablename__ = 't_member_attention'
    attention_id = Column('f_attention_id', GUID, primary_key=True, default=uuid4, doc=u"分類編號")

    currency_id = Column('f_currency_id', ForeignKey('t_currency.f_currency_id'), nullable=False, doc=u"幣別")

    sec_currency_id = Column('f_sec_currency_id', ForeignKey('t_currency.f_currency_id'), nullable=False, doc=u"幣別")

    stock_id = Column('f_stock_id', ForeignKey('t_stock.f_stock_id'), nullable=False, doc=u"股票")

    member_id = Column('f_member_id', ForeignKey('t_member.f_member_id'), nullable=False, doc=u"會員")

    line_id = Column('f_line_id', String(512), doc=u"line_id(便於通知)")

    notify_token = Column('f_notify_token', String, doc=u"notify_token(通知)")

    # new_buy_in_price = Column('f_new_buy_in_price', String, nullable=False, doc=u'最新買價')
    #
    # new_sell_out_price = Column('f_new_sell_out_price', String, nullable=False, doc=u'最新賣價')

    notice_buy_in_price = Column('f_notice_buy_in_price', String, nullable=False, doc=u'最新買價')

    notice_sell_out_price = Column('f_notice_sell_out_price', String, nullable=False, doc=u'最新賣價')

    _status = Column('f_status', Integer, nullable=False, default=10,
                     info={"stock": 10, "currency": 11},
                     doc=u"狀態:10.顯示,11.隱藏,12:封存")

    currency = relationship("Currency", foreign_keys="[MemberAttention.currency_id]")

    sec_currency = relationship("Currency", foreign_keys="[MemberAttention.sec_currency_id]")

    stock = relationship("Stock")

    def __repr__(self):
        return '<Object (attention_id={0})>'.format(self.attention_id)

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @classmethod
    def __getattributes__(cls):
        return [i[1:] if i[:1] == '_' else i for i in cls.__dict__.keys() if
                i[:1] != '_' or i == '_update_user_id' or i == '_create_user_id']

    @classmethod
    def __likeattribute__(cls, key_word):
        map_args = [i for i in cls.__dict__.keys() if key_word in i and i[:1] != '_']
        return map_args[0] if map_args else None

    def __json__(self):
        d = {
            'attention_id': str(self.attention_id),
            'status': self.status,
        }

        return d
