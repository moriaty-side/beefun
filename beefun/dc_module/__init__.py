from __future__ import unicode_literals

def includeme(config):

    config.include('.banner')

    config.include('.news')

    config.include('.video')

    config.include('.category')

    config.include('.account')

    config.include('.optimization')

    config.include('.tag')

    config.include('.product')

    config.include('.image')
    config.include('.web')
    config.include('.john_work')
    
    
    

    