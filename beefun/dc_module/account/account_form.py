# coding=utf8
from __future__ import unicode_literals

import formencode
from formencode import validators, ForEach


class LoginForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    account = validators.Email(not_empty=True)
    password = validators.String(not_empty=True)
    came_from = validators.String(not_empty=True)

    facebook_id = validators.String(not_empty=True)
    google_id = validators.String(not_empty=True)

    account_id = validators.String(not_empty=True, strip=True)
    email_token = validators.String(not_empty=True, strip=True)
    name = validators.String(not_empty=True, strip=True)

class AccountCreateForm(formencode.Schema):
    """使用者建立格式"""
    allow_extra_fields = True
    ignore_key_missing = True

    account = validators.Email(not_empty=True, strip=True)
    name = validators.String(not_empty=True, strip=True)
    password = validators.String(not_empty=True, strip=True)



class AccountEditForm(formencode.Schema):
    """AccountEdit 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    name = validators.String(not_empty=True, strip=True)
    status = validators.String(not_empty=True, strip=True)

    old_password = validators.String(not_empty=True, strip=True)
    new_password = validators.String(not_empty=True, strip=True)


class AccountSearchForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    account = validators.Email(not_empty=True, strip=True)
    name = validators.String(not_empty=True, strip=True)
    status = validators.String(not_empty=True, strip=True)
    page = validators.Int(not_empty=True, min=1)  # 第幾頁
    limit = validators.Int(not_empty=True, min=1)  # 一頁幾筆


class InviteForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    emails = ForEach(validators.Email(not_empty=True, strip=True))

class RegistryForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    email_token = validators.String(not_empty=True, strip=True)
