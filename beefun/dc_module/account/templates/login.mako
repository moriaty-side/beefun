<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			${ _(u'JMLED')}
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<!--end::Web font -->
        <!--begin::Base Styles -->
		<link href="${ request.static_path('beefun:static/assets/vendors/base/vendors.bundle.css') }" rel="stylesheet" type="text/css" />
		<link href="${ request.static_path('beefun:static/assets/demo/default/base/style.bundle.css') }" rel="stylesheet" type="text/css" />
		<link href="${ request.static_path('beefun:static/assets/main.css') }" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="${ request.static_path('beefun:static/assets/demo/default/media/img/logo/favicon.ico') }" />
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url(${ request.static_path('beefun:static/assets/app/media/img//bg/bg-1.jpg')});">
				<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#">
								<img src="${ request.static_path('beefun:static/assets/app/media/img//logos/logo-1.png') }">
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">
                                    % if len(account_id) > 0:
									${ _(u'註冊')}
									% else:
									${ _(u'登入')}
                                    % endif
								</h3>
							</div>
							<form class="m-login__form m-form" id="login_form" action="${ request.route_url('api.account.login')}" method="POST">
								<div class="form-group m-form__group">
                                    % if len(account_id) > 0:
                                    <input class="form-control m-input" type="text" name="account_id" value="${account_id}" hidden>
                                    <input class="form-control m-input" type="text" name="email_token" value="${email_token}" hidden>
                                    <input class="form-control m-input" type="email" name="account" value="${account}" disabled>
                                    % else:
                                    <input class="form-control m-input" type="email" placeholder="account" name="account">
                                    % endif
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
								</div>
								<div class="row m-login__form-sub">
									<div class="col m--align-left m-login__form-left">
										<label class="m-checkbox  m-checkbox--light">
											<input type="checkbox" name="remember">
											${ _(u'記住我')}
											<span></span>
										</label>
									</div>
								</div>
								<div class="m-login__form-action">
									<button type="submit" id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">
										% if len(account_id) > 0:
                                        ${ _(u'註冊')}
                                        % else:
                                        ${ _(u'登入')}
                                        % endif
									</button>
								</div>
							</form>
						</div>
                        
                        
					</div>
				</div>
			</div>
		</div>
		<!-- end:: Page -->
    	<!--begin::Base Scripts -->
		<script src="${ request.static_path('beefun:static/assets/vendors/base/vendors.bundle.js') }" type="text/javascript"></script>
		<script src="${ request.static_path('beefun:static/assets/demo/default/base/scripts.bundle.js') }" type="text/javascript"></script>
		<!--end::Base Scripts -->
        <!--begin::Page Snippets -->
## 		<script src="${ request.static_path('beefun:static/assets/snippets/pages/user/login.js') }" type="text/javascript"></script>
		<!--end::Page Snippets -->
        <script src="${ request.static_path('beefun:static/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}" type="text/javascript"></script>

        

        

        <script>
             var csrfToken = "${request.session.get_csrf_token()}";

             $('#login_form').submit(function(e) { e.preventDefault(); }).validate({
                 ignore: ":hidden",
                 errorElement: 'div', //default input error message container
                 errorClass: 'help-block', // default input error message class
                 focusInvalid: true, // do not focus the last invalid input
                 rules: {
                     account: {
                         required: true,
                     },
                     password: {
                         required: true,
                         minlength: 5
                     }
                 },
                 messages: {
                     account: {
                         required: "${ _(u'請輸入Email')}"
                     },
                     password: {
                         required: "${ _(u'請輸入密碼')}",
                         minlength: "${ _(u'密碼長度不能小於5個字母')}"
                     }
                 },
                 highlight: function (element) { // hightlight error inputs
                     $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                 },
                 success: function (label) {
                     label.closest('.form-group').removeClass('has-error');
                     label.remove();
                 },
                 errorPlacement: function (error, element) {
                     error.insertAfter(element);
                 },
                 submitHandler:function(form){
                     var form_json = {};
                     $(form).serializeArray().map(function(v, i){
                          form_json[v['name']] = v['value'];
                     });
                     console.log("form_json:",form_json);

                     var form_data = new FormData();
                     for(var k in form_json){
                         form_data.append(k, form_json[k]);
                     }
                     login(form_data, $('.btn'));
                 }
            });

             function login(form_data, btn){
                $.ajax({
                    url: "${ request.route_url('api.account.login') }",
                    type: 'POST',
                    data: form_data,
                    contentType: false,
                    processData: false,
                    headers: {'X-CSRF-Token': csrfToken},
                    beforeSend: function () {
                        $(btn).attr('disabled', true);
                    },
                    error: function (xhr) {
                        $(btn).attr('disabled', true);
                        alert("${ _(u'Ajax request 發生錯誤')}");
                     },
                    success: function (response) {
                        if (response['status']) {
                            console.log(response);
                            window.location=response['response'];
                        } else {
                            alert(response['message']);
                        }
                    },
                    complete: function () {
                        $(btn).attr('disabled', false);
                    }
                });
            }

        </script>

	</body>
	<!-- end::Body -->
</html>
