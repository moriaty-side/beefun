<%inherit file="beefun:templates/backend/master.mako"/>
<%block name="css">

</%block>
<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span>${ _(u'帳號管理')}</span>
                    </h3>
                </div>
                <div>
                    <button type="button" class="btn btn-warning m-btn m-btn--outline-2x btn-sm mr-5px btn-search">
									<span>
										<i class="fa fa-search"></i>
										<span>
											${ _(u'搜尋')}
										</span>
									</span>
								</button>
                    <button type="button" class="btn btn-warning m-btn m-btn--outline-2x btn-sm mr-5px btn-invite" id="btn-invite_form">
									<span>
										<i class="fa fa-paper-plane"></i>
										<span>
											${ _(u'邀請')}
										</span>
									</span>
								</button>
                    <button type="button" class="btn btn-brand m-btn m-btn--outline-2x btn-sm" data-toggle="modal" id="btn-add_form">
                                    <span>
                                        <i class="fa fa-plus"></i>
                                        <span>
                                            ${ _(u'新增')}
                                        </span>
                                    </span>
                    </button>
                </div>

            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="account_list" data-parent="account">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right" id="search_form">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12 col-lg-6 col-xl-4">
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-3">
                                        ${ _(u'E-Mail')}
                                    </label>
                                    <div class="col-9">
                                        <input type="text" class="form-control params" name="account">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4">
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-3">
                                        ${ _(u'姓名')}
                                    </label>
                                    <div class="col-9">
                                        <input type="text" class="form-control params" name="name">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->

                <!--Begin::Section-->
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-12">
                            <table class="table m-table m-table--head-bg-success">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        ${ _(u'E-Mail')}
                                    </th>
                                    <th>
                                        ${ _(u'姓名')}
                                    </th>
                                    <th>
                                        ${ _(u'狀態')}
                                    </th>
                                    <th><!--操作--></th>
                                </tr>
                                </thead>
                                <tbody class="account_list">
                                </tbody>
                            </table>

                            <!--頁碼-->
                            <div>
                                <ul class="pagination" id="page-selection">
                                </ul>
                            </div>
                            <!--頁碼  end-->
                        </div>

                    </div>
                </div>
                <!--End::Section-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>
    <!--begin::Modal 新增-->
    <div class="modal fade" id="add-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        ${ _(u'新增使用者')}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">
                                    &times;
                                </span>
                    </button>
                </div>
                <div class="modal-body">
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="add_form">
                        <div class="m-portlet__body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group m-form__group">
                                        <label class="col-form-label col-4">
                                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                            ${ _(u'E-Mail')}
                                        </label>
                                        <div class="col-8">
                                            <input class="form-control" name="account">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label class="col-form-label col-4">
                                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                            ${ _(u'姓名')}
                                        </label>
                                        <div class="col-8">
                                            <input class="form-control" name="name">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label class="col-form-label col-4">
                                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                            ${ _(u'密碼')}
                                        </label>
                                        <div class="col-8">
                                            <input id="re-password" type="password" class="form-control" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label class="col-form-label col-4">
                                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                            ${ _(u'密碼確認')}
                                        </label>
                                        <div class="col-8">
                                            <input type="password" class="form-control" name="password_again">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-metal m-btn m-btn--outline-2x btn-sm"
                            data-dismiss="modal">
                                <span>
                                    <i class="fa fa-close"></i>
                                    <span>
                                        ${ _(u'關閉')}
                                    </span>
                                </span>
                    </button>
                    <button type="button" class="btn btn-outline-success m-btn m-btn--outline-2x btn-sm"
                            id="add_form_submit">
                                <span>
                                    <i class="fa fa-check"></i>
                                    <span>
                                        ${ _(u'確認新增')}
                                    </span>
                                </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!--end::Modal 新增-->
    <!--begin::Modal 修改-->
    <div class="modal fade" id="edit-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        ${ _(u'修改使用者')}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">
                                    &times;
                                </span>
                    </button>
                </div>
                <div class="modal-body">
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="edit_form">
                        <div class="m-portlet__body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group m-form__group">
                                        <label class="col-form-label col-4">
                                            ${ _(u'狀態')}
                                        </label>
                                        <div class="col-8">
                                            <select class="form-control se1" name="status">
                                                <option value="show">
                                                    ${ _(u'開通')}
                                                </option>
                                                <option value="hiden">
                                                    ${ _(u'關閉')}
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label class="col-form-label col-4">
                                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                            ${ _(u'E-Mail')}
                                        </label>
                                        <div class="col-8">
                                             <input type="hidden" class="edit_form_account_id" name="account_id">
                                            <span class="cont-set edit_form_account"></span>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label class="col-form-label col-4">
                                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                            ${ _(u'姓名')}
                                        </label>
                                        <div class="col-8">
                                            <input class="form-control edit_form_name" name="name" value="">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group password-set hide">
                                        <label class="col-form-label col-4">
                                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                            ${ _(u'密碼')}
                                        </label>
                                        <div class="col-8">
                                            <input id="edit-re-password" type="password" class="form-control" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group password-set hide">
                                        <label class="col-form-label col-4">
                                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                            ${ _(u'密碼確認')}
                                        </label>
                                        <div class="col-8">
                                            <input type="password" class="form-control" name="password_again">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-metal m-btn m-btn--outline-2x btn-sm"
                            data-dismiss="modal">
                                <span>
                                    <i class="fa fa-close"></i>
                                    <span>
                                        ${ _(u'關閉')}
                                    </span>
                                </span>
                    </button>
                    <button type="button" class="btn btn-outline-danger m-btn m-btn--outline-2x btn-sm password-btn">
                                <span>
                                    <i class="fa fa-key"></i>
                                    <span>
                                        ${ _(u'重設密碼')}
                                    </span>
                                </span>
                    </button>
                    <button type="button" class="btn btn-outline-success m-btn m-btn--outline-2x btn-sm edit_form_sumbit">
                                <span>
                                    <i class="fa fa-check"></i>
                                    <span>
                                        ${ _(u'確認修改')}
                                    </span>
                                </span>
                    </button>

                </div>
            </div>
        </div>
    </div>
    <!--end::Modal 修改-->
    <!--begin::Modal 邀請-->
    <div class="modal fade" id="invite-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        ${ _(u'邀請使用者')}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">
                                    &times;
                                </span>
                    </button>
                </div>
                <div class="modal-body">
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="invite_form">
                        <div class="m-portlet__body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group m-form__group">
                                        <label class="col-form-label col-4">
                                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                            ${ _(u'E-Mail')}
                                        </label>
                                        <div class="col-8">
                                             <input type="email" class="" name="emails">
                                            <span class="cont-set"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-metal m-btn m-btn--outline-2x btn-sm"
                            data-dismiss="modal">
                                <span>
                                    <i class="fa fa-close"></i>
                                    <span>
                                        ${ _(u'關閉')}
                                    </span>
                                </span>
                    </button>
                    <button type="button" class="btn btn-outline-success m-btn m-btn--outline-2x btn-sm" id="invite_form_submit">
                                <span>
                                    <i class="fa fa-check"></i>
                                    <span>
                                        ${ _(u'確認')}
                                    </span>
                                </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!--end::Modal 邀請-->
</%block>
<%block name="script">
    <script src="${ request.static_path('beefun:static/assets/global/plugins/jquery-bootpag/jquery.bootpag.min.js') }"
        type="text/javascript"></script>

    <script src="${ request.static_path('beefun:static/assets/make_table/make_table.js')}"
            type="text/javascript"></script>

     <script src="${ request.static_path('beefun:static/assets/other/custom_validation.js')}"
            type="text/javascript"></script>
    <script>
        $('.se1').select2();
        var csrfToken = "${request.session.get_csrf_token()}";
        ## 新增使用者表單驗證
        $('#add_form').validate({
            ignore: ":hidden",
            errorElement: 'div', //default input error message container
            errorClass: 'form-control-feedback', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                account: {
                    required: true,
                    isEmail: true,
                    maxlength: 120
                },
                name:{
                    required: true,
                    byteRangeLength:60
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                password_again: {
                    equalTo: "#re-password",
                    minlength: 6,
                    maxlength: 20
                }
            },
            messages: {
                account: {
                    required: "${ _(u'E-Mail為必填值')}",
                    email: "${ _(u'E-Mail格式必須為正確格式')}",
                    maxlength: "${ _(u'E-Mail字數最多為120')}"
                },
                name:{
                    required:"${ _(u'姓名為必填值')}",
                    byteRangeLength:"${ _(u'姓名最多輸入字數為60(中文算2個字)')}"
                },
                password: {
                    required: "${ _(u'密碼為必填值')}",
                    minlength: "${ _(u'密碼字數最少為6')}",
                    maxlength: "${ _(u'密碼字數最多為20')}"
                },
                password_again: {
                    equalTo: "${ _(u'確認密碼必須與輸入密碼相同')}",
                    minlength: "${ _(u'確認密碼字數最少為6')}",
                    maxlength: "${ _(u'確認密碼字數最多為20')}"
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-danger'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-danger');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            }
        });
        ## 修改表單驗證
        $('#edit_form').validate({
            ignore: ":hidden",
            errorElement: 'div', //default input error message container
            errorClass: 'form-control-feedback', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                name:{
                    required:true,
                    byteRangeLength:60
                },
                password: {
                    required:true,
                    minlength: 6,
                    maxlength: 20
                },
                password_again: {
                    minlength: 6,
                    maxlength: 20,
                    equalTo: "#edit-re-password"
                }
            },
            messages: {
                name:{
                    required:"${ _(u'姓名為必填值')}",
                    byteRangeLength:"${ _(u'姓名最多輸入字數為60(中文算2個字)')}"
                },
                password: {
                     required: "${ _(u'密碼為必填值')}",
                    minlength: "${ _(u'密碼字數最少為6')}",
                    maxlength: "${ _(u'密碼字數最多為20')}"
                },
                password_again: {
                    minlength: "${ _(u'確認密碼字數最少為6')}",
                    maxlength: "${ _(u'確認密碼字數最多為20')}",
                    equalTo: "${ _(u'確認密碼必須與輸入密碼相同')}"
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-danger'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-danger');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            }
        });
        ## 修改表單驗證
        $('#invite_form').validate({
            ignore: ":hidden",
            errorElement: 'div', //default input error message container
            errorClass: 'form-control-feedback', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                emails:{
                    required:true,
                    email: true
                }
            },
            messages: {
                emails:{
                    required:"${ _(u'Email為必填值')}",
                    email: "${ _(u'請輸入Email格式')}"
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-danger'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-danger');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            }
        });

        var account_list_table = new MakeTable({
            'dom_search_form': $('form#search_form'),
            'dom_table_tbody': $('.account_list'),
            'ajax_csrf_token': csrfToken,
            'init_search_parameter': '&limit=10',
            'ajax_search_url': "${ request.route_url('api.account')}",
            'table_model': function (obj, page) {
                //開始產生頁面資料
                var $list = obj['account_list'];
                var user_id = obj['user_id'];
                $('#hid_user_id').val(user_id);
                var html_str = '';
                $('.list_row').remove();
                $.each($list, function (i, item) {
                    item.index = (i + 1 + ((page - 1) * 10));
                    item['name_str'] = (item['name'] != null) ? item['name'] : "";
                    var tb_row = '<tr>' +
                            '<td scope="row">' + item.index + '</td>' +
                            '<td>' + item['account'] + '</td>'+
                            '<td>' + item['name_str'] + '</td>';
                    if (item['status'] == 'show') {
                        tb_row += '<td><span class="m-badge m-badge--info m-badge--wide m-badge--rounded m--font-bolder">${ _(u"開通")}</span></td>';
                    }
                    else if(item['status'] == 'auth') {
                        tb_row += '<td><span class="m-badge m-badge--metal m-badge--wide m-badge--rounded m--font-bolder">${ _(u"邀請中")}</span></td>';
                    }else{
                        tb_row += '<td><span class="m-badge m-badge--metal m-badge--wide m-badge--rounded m--font-bolder">${ _(u"關閉")}</span></td>';
                    }
                    tb_row += '<td align="right">' +
                            '<button type="button" data-id="' + item['account_id'] + '" class="btn btn-outline-danger m-btn m-btn--outline-2x btn-sm btn_edit_user" data-toggle="modal" data-target="#edit-user">' +
                            '<span>' +
                            '<i class="fa fa-edit"></i>' +
                            '<span>' +
                            '                                                                ${ _(u"修改")}' +
                            '</span>' +
                            '</span>' +
                            '</button>' +
                            '<button type="button" data-id="' + item['account_id'] + '" class="btn btn-outline-metal m-btn m-btn--outline-2x btn-sm btn_delete_user">' +
                            '<span><i class="la la-trash"></i>' +
                            '<span>' +
                            '                                                                ${ _(u"刪除")}' +
                            '</span>' +
                            '</span></button>' +
                            '</td>' +
                            '</tr>';
                    html_str += tb_row;
                });
                $('.account_list').append(html_str);//Table頁面資料繪製
            }
        });
        account_list_table.start();
        ajax_reload = account_list_table;
            ## 打開新增使用者按鈕時
            $(document).on('click', '#add_user_btn', function () {
            $('#add_form').validate().resetForm();
        });
        ## 新增
        $(document).on('click', '#add_form_submit', function () {
            var $valid = $('#add_form').valid();
            if (!$valid) {
                return false
            }
            var datas = $('#add_form').serializeArray();
            $.ajax({
                url: "${ request.route_url('api.account')}",
                type: 'POST',
                data: datas,
                dataType: 'json',
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    $('.btn').attr('disabled', true);
                },
                error: function (xhr) {
                    alert('${ _(u"Ajax request 發生錯誤")}');
                },
                success: function (response) {
                    if (response['status']) {
                        $('.btn').attr('disabled', false);
                        ##  alert(response['message']);
                        $('.btn-search').click();
                        $('#add-user').modal('hide');

                    } else {
                        $('.btn').attr('disabled', false);
                        alert(response['message']);
                        $(".button-next").show();
                    }
                },
                complete: function () {
                    $(".button-previous").show();
                    ajax_reload.start();
                }
            });
        });

        ## 邀請
        $(document).on('click', '#invite_form_submit', function () {
            var $valid = $('#invite_form').valid();
            if (!$valid) {
                return false
            }
            var datas = $('#invite_form').serializeArray();
            $.ajax({
                url: "${ request.route_url('api.account.invite')}",
                type: 'POST',
                data: datas,
                dataType: 'json',
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    $('.btn').attr('disabled', true);
                },
                error: function (xhr) {
                    alert('${ _(u"Ajax request 發生錯誤")}');
                },
                success: function (response) {
                    if (response['status']) {
                        $('.btn').attr('disabled', false);
                         alert(response['message']);
                        $('.btn-search').click();
                        $('#invite-user').modal('hide');

                    } else {
                        $('.btn').attr('disabled', false);
                        alert(response['message']);
                        $(".button-next").show();
                    }
                },
                complete: function () {
                    $(".button-previous").show();
                    ajax_reload.start();
                }
            });
        });

        ## 打開重設密碼form
        $(document).on('click', '.btn_reset_pwd', function () {
            $('#reset_pwd_form')[0].reset();
            var account_id = $(this).data('id');
            $('.reset_form_account_id').val(account_id);
        });

        ## 刪除使用者
        $(document).on('click', '.btn_delete_user', function () {
            if (!confirm("${ _(u'是否刪除此使用者')}")) {
                return false
            }
            var account_id = $(this).data('id');
            $.ajax({
                url: "${ request.route_url('api.account.detail', account_id='')}"+account_id,
                type: 'DELETE',
                data: {'account_id': account_id},
                dataType: 'json',
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    $('.btn').attr('disabled', true);
                },
                error: function (xhr) {
                    alert('${ _(u"Ajax request 發生錯誤")}');
                },
                success: function (response) {
                    if (response['status']) {
                        $('.btn').attr('disabled', false);
                        alert(response['message']);
                        $('.btn-search').click();
                        $('#re-password').modal('hide');
                    } else {
                        $('.btn').attr('disabled', false);
                        alert(response['message']);
                        $(".button-next").show();
                    }
                },
                complete: function () {
                    $(".button-previous").show();
                     ajax_reload.start();
                }
            });
        });

        ## 修改使用者按鈕
        $(document).on('click', '.btn_edit_user', function () {
            $('.password-set').addClass('hide');
            $('#edit_form').validate().resetForm();
            var datas = {'account_id': $(this).data('id')};
            $.ajax({
                url: "${ request.route_url('api.account.detail', account_id='')}"+$(this).data('id'),
                type: 'GET',
                data: datas,
                dataType: 'json',
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {

                },
                error: function (xhr) {
                    alert('${ _(u"Ajax request 發生錯誤")}');
                },
                success: function (response) {
                    if (response['code'] == '200' && response['status'] == true) {
                        $('.btn').attr('disabled', false);
                        $('.edit_form_account').text(response['response']['account']['account']);
                        $('.edit_form_name').val(response['response']['account']['name']);
                        $('.edit_form_account_id').val(response['response']['account']['account_id']);
                        $('#edit_form select[name="status"]').val(response['response']['account']['status']).trigger('change');

                        var user_id = "${request.account_id}";
                        if (user_id == response['response']['account']['account_id']) {
                            $('#edit_form select[name="status"]').attr('disabled', true);
                        }
                        else {
                            $('#edit_form select[name="status"]').attr('disabled', false);
                        }

                    } else {
                        $('.btn').attr('disabled', false);
                        alert(response['message']);
                        $(".button-next").show();
                    }
                },
                complete: function () {
                    $(".button-previous").show();
                }
            });
        });

        ## 修改使用者-確認修改
        $(document).on('click', '.edit_form_sumbit', function () {
            const $valid = $('#edit_form').valid();
            if (!$valid) {
                return false
            }
            var edit_form_array = $('#edit_form').serializeArray();
            var datas = {};
            jQuery.map(edit_form_array, function (n, i) {
                if (n.value)
                    datas[n.name] = n.value.trim();
            });
            $.ajax({
                url: "${ request.route_url('api.account.detail', account_id='')}"+datas['account_id'],
                type: 'POST',
                data: datas,
                dataType: 'json',
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    $('.btn').attr('disabled', true);
                },
                error: function (xhr) {
                    alert('${ _(u"Ajax request 發生錯誤")}');
                },
                success: function (response) {
                    if (response['code'] == '200' && response['status'] == true) {
                        $('.btn').attr('disabled', false);
                        ##  alert(response['message']);
                        $('.btn-search').click();
                        $('#edit-user').modal('hide');

                    } else {
                        $('.btn').attr('disabled', false);
                        alert(response['message']);
                        $(".button-next").show();
                    }
                },
                complete: function () {
                    $(".button-previous").show();
                    ajax_reload.start();
                }
            });
        });

        ## 修改密碼
        $(document).on('click', '.password-btn', function() {
            if($('.password-set').hasClass('hide')){
                      $('.password-set').removeClass('hide');
            }
            else{
                  $('.password-set').addClass('hide');
                  $('#edit_form input[name="password"],#edit_form input[name="password_again"]').val('');
                  $('#edit_form').valid();
            }
        });

        ## 搜尋 enter 事件
        $('.params').on('keydown', function (e) {
            if (e.which == 13) {
                ajax_reload.dom_search_btn.click();
                e.preventDefault();
            }
        });

        ## 新增form 開啟事件
        $(document).on('click','#btn-add_form',function(){
             $('#add-user').modal('show');
             $('#add_form').validate().resetForm();
        });

        ## 邀請form 開啟事件
        $(document).on('click','#btn-invite_form',function(){
             $('#invite-user').modal('show');
             $('#invite_form').validate().resetForm();
        });

    </script>
</%block>