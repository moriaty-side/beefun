from ...base.base_test import BaseTest, dummy_request
from .account_domain import Account
from .account_views import AccountView

class AccountAPITest(BaseTest):

    def setUp(self):
        super(AccountAPITest, self).setUp()
        self.init_database()

    def create_account(self, account='user@gmail.com', name='user', password='123456'):
        account_obj = Account(account=account, name=name, password=password)
        self.session.add(account_obj)
        account = self.session.query(Account).filter(Account.account == account).first()
        return account

    def test_create_account(self):
        """
        測試 創建帳號
        """
        account = 'admin@gmail.com'
        name = 'admin'
        password = '123456'
        request = dummy_request(self.session)
        request.params = {'account': account, 'name': name, 'password': password}
        account_view = AccountView(request)
        account_resp = account_view.create()
        self.assertEqual(account_resp.get('message'), '創建帳號成功')

    def test_search_account(self):
        """
        測試 創建帳號
        """
        request = dummy_request(self.session)
        account_view = AccountView(request)
        account_resp_list = account_view.search()
        self.assertEqual(account_resp_list.get('message'), '帳號搜尋成功')
        self.assertEqual(account_resp_list.get('response', {}).get('total_count'), 0)
        self.assertEqual(account_resp_list.get('response', {}).get('total_page'), 0)
        self.assertEqual(len(account_resp_list.get('response', {}).get('account_list', [])), 0)

    def test_get_account(self):
        """
        測試 讀取帳號
        """
        account = self.create_account()
        request = dummy_request(self.session)
        request.matchdict = {'account_id': str(account.account_id)}
        account_view = AccountView(request)
        account_resp = account_view.get()
        self.assertEqual(account_resp.get('message'), '讀取帳號成功')

    def test_update_account(self):
        """
        測試 更新帳號
        """
        account = self.create_account()
        request = dummy_request(self.session)
        request.matchdict = {'account_id': str(account.account_id)}
        name = '123'
        request.params = {'name': name}
        account_view = AccountView(request)
        account_resp = account_view.edit()
        self.assertEqual(account_resp.get('message'), '帳號修改成功')
        self.assertEqual(account_resp.get('response', {}).get('account', {}).get('name'), name)

    def test_delete_account(self):
        """
        測試 更新帳號
        """
        account = self.create_account()
        request = dummy_request(self.session)
        request.matchdict = {'account_id': str(account.account_id)}
        account_view = AccountView(request)
        account_resp = account_view.delete()
        self.assertEqual(account_resp.get('message'), '刪除帳號成功')

    def test_passing_view(self):
        account = self.create_account()
        account_view = AccountView(request=dummy_request(self.session))
        info = account_view.search()
        self.assertEqual(info.get('message'), '帳號搜尋成功')
        self.assertEqual(info.get('response', {}).get('total_page'), 1)
        self.assertEqual(len(info.get('response', {}).get('account_list')), 1)
