# coding=utf8
from __future__ import unicode_literals
from pyramid.httpexceptions import HTTPFound
from pyramid.url import urlencode
import transaction
import json
import datetime
from pyramid.security import remember, forget
from pyramid.response import Response
from pyramid.csrf import check_csrf_token
from pyramid.i18n import make_localizer

from . import account_form as form
from ...base.base_view import BaseView
from ...lib.my_exception import MyException
from .account_service import AccountService
from ...lib.i18n import Localizer
from ...utils import load_app_cfg

import urllib
import uuid
from jwcrypto import jwk
from ...lib.jwt import jwe_genertor, jwe_decode
from ...lib.mail import send_mail_by_mailgun_html

class HomeView(BaseView):
    def __init__(self, request):
        super(HomeView, self).__init__(request)
        self.account_service = AccountService(self.session)
        self.request = request

    def home(self):
        """頁面首頁"""
        _ = self.localizer
        return {}

    def login(self):
        """超級管理員登入"""
        _ = self.localizer
        request_data = form.RegistryForm().to_python(self.request.params)
        account = ''
        account_id = ''
        if request_data.get('email_token'):
            account_id = self.email_token_check(state_secret=self.settings.get('state_secret'),
                                                email_token=request_data.get('email_token'),
                                                redis_client=self.settings.get('redis_client'))
            # 取得帳號
            account_obj = self.account_service.get_by_id(obj_id=uuid.UUID(account_id), check=True)
            account = account_obj.account
            account_id = str(account_obj.account_id)

        return {'account_id': account_id,
                'account': account,
                'email_token': request_data.get('email_token', '')}

    def account_list(self):
        """帳號"""
        _ = self.localizer
        return {}

    def locale(self):
        """
        語系設定
        :return:
        """
        if self.request.GET['language']:
            language = self.request.GET['language']
            response = Response()
            response.set_cookie('_LOCALE_',
                                value=language,
                                max_age=31536000)  # max_age = year

        localizer = make_localizer(self.request.GET['language'], ['jmled/locale'])
        _ = Localizer(localizer, 'jmled')

        self.settings['available_langs'] = [[language[0], _(language[1])]
                                            for index, language in enumerate(load_app_cfg().get('available_langs', []))]

        return HTTPFound(location=self.request.route_url('backend.page.home'),
                         headers=response.headers)

    def logout(self):
        """超級管理員登出"""
        _ = self.localizer
        referrer = self.request.referrer
        my_url = self.request.route_url('page.logout')
        if referrer == my_url or not referrer:
            referrer = self.request.route_url('page.login')
        came_from = self.request.params.get('came_from', referrer)
        headers = forget(self.request)
        return HTTPFound(location=came_from, headers=headers)

    def email_token_check(self, state_secret, email_token, redis_client=None):
        """
        Email 驗證碼 檢查
        :return:
        """
        key = jwk.JWK.from_json(state_secret)
        token_data = jwe_decode(encrypted_token_string=email_token,
                                key=key)

        # # 檢查Reids
        if redis_client:
            token = redis_client.get(email_token)
            if not token:
                raise MyException(code=1009)
        #     else:
        #         redis_client.delete(email_token)

        # 檢查有效日期
        expire_time = token_data.get('expire_time')
        if datetime.datetime.utcnow() > datetime.datetime.fromtimestamp(expire_time):
            raise MyException(code=1009)

        return token_data.get('account_id')



class AccountView(BaseView):
    def __init__(self, request):
        super(AccountView, self).__init__(request)
        self.account_service = AccountService(self.session)

    def login(self):
        """
        帳號登入
        """
        #  檢查CSRF Token
        # check_csrf_token(self.request)

        # 檢查輸入參數
        request_data = form.LoginForm().to_python(self.request.params)

        # 讀取帳號
        if request_data.get('account_id'):
            # 取得帳號
            account_obj = self.account_service.get_by_id(obj_id=uuid.UUID(request_data.get('account_id')), check=True)
            with transaction.manager:
                account_obj = self.account_service.update(old_obj=account_obj,
                                                          password=request_data.get('password'),
                                                          status='show')
                # 刪除 email_token
                redis_client = self.settings.get('redis_client')
                redis_client.delete(request_data.get('email_token'))
        else:
            account_obj = self.account_service.get_by(account=request_data.get('account'))

        if not account_obj:
            raise MyException(code=1012)

        # 比對帳號密碼
        if not account_obj.validate_password(request_data.get('password')):
            raise MyException(code=1012)

        # 檢查帳號狀態
        if account_obj.status != 'show':
            raise MyException(code=1007)

        # 記錄 Session
        remember_id = '{}@account'.format(str(account_obj.account_id))
        self.request.session['account_data'] = account_obj.__json__()
        remember(self.request, remember_id)

        

        return {'response': self.request.route_url('backend.page.home')}

    

    def logout(self):
        """
        登出
        :return:
        """
        _ = self.localizer
        # 如果有用session
        self.request.session.invalidate()
        headers = forget(self.request)

        self.request.response.headerlist.extend(headers)
        login_url = self.request.route_url('backend.page.login')
        return HTTPFound(location=login_url)

    def create(self):
        """
        # URL: domain/api/account
        # 創建帳號
        - 檢查輸入參數
        - 檢查帳號是否重複
        - 創建帳號
        # 參數說明
            :request:
            :parma account   : 帳號信箱  | String 格式 必填
            :parma password  : 使用者密碼 | String 格式 必填
            :parma name      : 使用者暱稱 | String 格式 必填

            :return:
            response: {account_id: account_id}
            message: 創建帳號成功
        """
        #  檢查CSRF Token
        check_csrf_token(self.request)

        # 檢查輸入參數
        request_data = form.AccountCreateForm.to_python(self.request_params)

        # 檢查帳號是否重複
        account_obj = self.account_service.get_by(account=request_data.get('account'))
        if account_obj:
            raise MyException(code=1002)

        # 創建帳號
        with transaction.manager:
            account = self.account_service.create(user_id=self.account_id, **request_data)

        return {'response': {'account': account.__json__()},
                'message': '創建帳號成功'}

    def search(self):
        """
        # URL: domain/api/account
        # 搜尋帳號
        - 檢查輸入參數
        - 分頁機制
        - 搜尋帳號
        # 參數說明
            :request:
            :parma account   : 帳號信箱         | String 格式 選填
            :parma name      : 使用者暱稱        | String 格式 選填
            :parma status    : 帳號狀態| String 格式 選填
                               ("show": 1, "hiden": 2, "delete": 400)
            :parma limit     : 一次讀取筆數，預設10   | number 格式 選填
            :parma offset    : 從哪一筆開始搜尋，預設0 | number 格式 選填

            :return:
            response: {account_list: account_list,
                       total_count: total_count,
                       total_page: total_page}
            message: 搜尋帳號成功
        """
        #  檢查CSRF Token
        check_csrf_token(self.request)

        # 檢查輸入參數
        request_data = form.AccountSearchForm().to_python(self.request_params)

        # 分頁機制
        current_page = request_data.get('page', 1)
        limit = request_data.get('limit', 10)
        request_data['offset'] = (current_page - 1) * limit
        request_data['limit'] = limit
        request_data['order_by'] = [('created', -1)]

        # 搜尋帳號
        account_list, total_count = self.account_service.get_list(show_count=True,
                                                                  except_delete=True,
                                                                  **request_data)

        return {'response': {'account_list': [a.__json__() for a in account_list],
                             'total_count': total_count,
                             'total_page': (int(total_count / limit) + 1 if total_count % limit else 0)},
                'message': u'帳號搜尋成功'}

    def get(self):
        """
        # URL: domain/api/account/account_id
        # 讀取帳號
        - 讀取帳號
        # 參數說明
            :request:
            :parma account_id   : 帳號編號  | String 格式 必填

            :return:
            response: {account: {}}
            message: 讀取帳號成功
        """
        #  檢查CSRF Token
        check_csrf_token(self.request)

        # 讀取帳號
        account_obj = self.account_service.get_by_id(obj_id=self.request.matchdict['account_id'],
                                                     check=True)

        return {'response': {'account': account_obj.__json__()},
                'message': '讀取帳號成功'}

    def edit(self):
        """
        # URL: domain/api/account/{account_id}
        # 搜尋帳號
        - 檢查輸入參數
        - 用 account_id 取得帳號
        - 更新帳號
        # 參數說明
            :request:
            :parma account   : 帳號信箱         | String 格式 選填
            :parma name      : 使用者暱稱        | String 格式 選填
            :parma status    : 帳號狀態| String 格式 選填
                               ("show": 1, "hiden": 2, "delete": 400)
            :parma limit     : 一次讀取筆數，預設10   | number 格式 選填
            :parma offset    : 從哪一筆開始搜尋，預設0 | number 格式 選填

            :return:
            response: {account: account_data}
            message: 帳號修改成功
        """
        #  檢查CSRF Token
        check_csrf_token(self.request)

        # 檢查輸入參數
        request_data = form.AccountEditForm().to_python(self.request_params)

        # 用 account_id 取得帳號
        account_obj = self.account_service.get_by_id(obj_id=self.request.matchdict['account_id'],
                                                     check=True)

        # 檢查是否為更新密碼
        if request_data.get('old_password'):
            if not account_obj.validate_password(request_data.get('old_password')):
                raise MyException(code=1006)
            else:
                request_data['password'] = request_data.get('new_password')

        # 更新帳號
        with transaction.manager:
            account_obj = self.account_service.update(old_obj=account_obj,
                                                      user_id=self.account_id,
                                                      **request_data)

        return {'response': {'account': account_obj.__json__()},
                'message': u'帳號修改成功'}

    def delete(self):
        """
        # URL: domain/admin/api/account/delete
        # 搜尋帳號
        - 用 account_id 取得帳號
        - 更新 status 達到軟刪除
        # 參數說明
            :request:
            :parma account   : 帳號信箱         | String 格式 選填
            :parma name      : 使用者暱稱        | String 格式 選填
            :parma status    : 帳號狀態| String 格式 選填
                               ("show": 1, "hiden": 2, "delete": 400)
            :parma limit     : 一次讀取筆數，預設10   | number 格式 選填
            :parma offset    : 從哪一筆開始搜尋，預設0 | number 格式 選填

            :return:
            response: {account: account_data}
            message: 帳號修改成功
        """
        #  檢查CSRF Token
        check_csrf_token(self.request)

        # 用 account_id 取得帳號
        account_obj = self.account_service.get_by_id(obj_id=self.request.matchdict['account_id'],
                                                     check=True)
        # 更新 status 達到軟刪除
        with transaction.manager:
            self.account_service.update(old_obj=account_obj, status='delete')

        return {'message': u'刪除帳號成功'}

    def invite(self):
        """
        寄送註冊邀請信
        :return:
        """
        #  檢查CSRF Token
        check_csrf_token(self.request)

        # 檢查輸入參數
        request_data = form.InviteForm().to_python(self.request.params)
        account_exist = []

        # 寄送邀請信
        for email in request_data.get('emails', []):

            # 檢查email 是否註冊過
            account = self.account_service.get_by(account=email)

            if not account:
                # 創建帳號
                with transaction.manager:
                    account_data = {'account': email,
                                    'name': email.split('@')[0],
                                    'status': 'auth'}
                    account = self.account_service.create(user_id=self.member_id, **account_data)
            else:
                account_exist.append(account.account)

            # 創建驗證碼，並存在redis，設定過期時間
            jwt_content = {
                "account_id": str(account.account_id),
                "expire_time": int((datetime.datetime.utcnow() + datetime.timedelta(days=3)).timestamp()),#有效時間
            }

            key = jwk.JWK.from_json(self.settings.get('state_secret'))
            token = jwe_genertor(json.dumps(jwt_content), key)

            # 產生邀請驗證url
            url = self.request.route_url('backend.page.login')
            url = url + '?' + urllib.parse.urlencode({'email_token': token})

            # 拼裝
            content = request_data.get('content', '請點該連結開通帳號')
            content += (u'<a href="{0}" style="font-size:24px;color:red;">開通連結</a>').format(url)
            send_mail_by_mailgun_html(request=self.request,
                                      subject=request_data.get('subject', '邀請信'),
                                      email=email,
                                      content=content)

            # 記在 Redis
            if self.request.registry.settings.get('redis_client'):
                redis_client = self.request.registry.settings.get('redis_client')
                redis_client.set(token, json.dumps(jwt_content), ex=60*60*24*3)

        if len(account_exist) > 0:
            message = u'Email已註冊過:{}'.format(','.join(account_exist))
        else:
            message = u'寄送邀請信成功'

        return {'message': message}

    
    



