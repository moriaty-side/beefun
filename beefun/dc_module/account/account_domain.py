# coding=utf8
import uuid
from sqlalchemy import (Column, Integer, String)
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.ext.declarative import declarative_base

from ...base.meta_module import (Base, TimestampTable, GUID, uuid4)
from ...lib.function import salt_password

DeclarativeBase = declarative_base()

class Account(Base, TimestampTable):
    """
    Account資料表單
    """
    __tablename__ = 't_admin_account'
    account_id = Column('f_account_id', GUID, primary_key=True, default=uuid4, doc=u"角色流水編號")

    account = Column('f_account', String(256), nullable=False, unique=True, doc=u"帳號")
    name = Column('f_name', String(200), nullable=False, doc=u"使用者名稱")
    _password = Column('f_password', String(255), nullable=True, doc=u"密碼")

    login = Column('f_login', String(512), doc=u"上次登入的裝置名稱")
    login_ip = Column('f_login_ip', String(512), doc=u"上次登入的IP")
    remark = Column('f_remark', String(512), doc=u"備註")
    _status = Column('f_status', Integer, nullable=False, default=10,
                     info={"show": 10, "auth": 15, "hiden": 20, "delete": 400},
                     doc=u"狀態:10.開通,15.驗證中,20.關閉,400.刪除")

    def __repr__(self):
        return '<AccountObject (account_id={0})>'.format(self.account_id)

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @hybrid_property
    def password(self):
        if hasattr(self.__class__, '_password'):
            return self._password

    @password.setter
    def password(self, value):
        hash_name, salt, hashed_password = salt_password(password=value)
        str_password = hash_name + u"$" + salt.decode("utf-8") + u"$" + hashed_password
        self._password = str_password

    @password.expression
    def password(cls):
        return cls._password

    @hybrid_method
    def validate_password(self, check_password):
        """
        認證密碼
        """
        hash_name, salt, hashed_password = self.password.split('$')
        _, _, input_hashed_password = salt_password(check_password, salt.encode("utf-8"), hash_name)
        return input_hashed_password == hashed_password

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    def __json__(self, show_password=False):
        d = {
            'account_id': str(self.account_id),
            'account': self.account,
            'name': self.name,
            'status': str(self.status),
            'login_ip': self.login_ip,
        }
        if show_password:
            d['password'] = self.password

        return d

