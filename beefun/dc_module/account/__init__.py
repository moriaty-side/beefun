from __future__ import unicode_literals

from . import account_views as views
from ...lib.api import return_format
from ...lib.page import return_page_format
from ...base.context import AdminContext


def includeme(config):
    # Page Route
    config.add_route('backend.page.home', '/', factory=AdminContext)
    config.add_view(
        views.HomeView, attr='home',
        route_name='backend.page.home',
        renderer='templates/home.mako',
        decorator=return_page_format,
        permission='login',
    )

    config.add_route('backend.page.account_list', '/page/account/list', factory=AdminContext)
    config.add_view(
        views.HomeView, attr='account_list',
        route_name='backend.page.account_list',
        renderer='templates/account_list.mako',
        decorator=return_page_format,
        
        permission='login',
        

    )

    config.add_route('backend.page.login', '/login')
    config.add_view(
        views.HomeView, attr='login',
        route_name='backend.page.login',
        renderer='templates/login.mako',
        decorator=return_page_format,
    )

    config.add_route('backend.page.locale', '/locale', factory=AdminContext)
    config.add_view(
        views.HomeView, attr='locale',
        route_name='backend.page.locale',
        request_method='GET',
        permission='login',
        decorator=return_page_format,
    )

    # API  Route
    config.add_route('api.account.login', '/api/account/login')
    config.add_view(
        views.AccountView, attr='login',
        route_name='api.account.login',
        request_method='POST',
        decorator=return_format,
        renderer='json'
    )

    config.add_route('api.account.logout', '/api/account/logout')
    config.add_view(
        views.AccountView, attr='logout',
        route_name='api.account.logout',
        request_method='GET',
        # decorator=return_format,
        # renderer='json'
    )

    config.add_route('api.account', '/api/account', factory=AdminContext)
    config.add_view(
        views.AccountView, attr='create',
        route_name='api.account',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission = 'login',
        
    )

    config.add_view(
        views.AccountView, attr='search',
        route_name='api.account',
        request_method='GET',
        decorator=return_format,
        renderer='json',
        
        permission = 'login',
        
    )

    config.add_route('api.account.detail', '/api/account/{account_id}', factory=AdminContext)
    config.add_view(
        views.AccountView, attr='get',
        route_name='api.account.detail',
        request_method='GET',
        decorator=return_format,
        renderer='json',
        
        permission = 'login',
        
    )

    config.add_view(
        views.AccountView, attr='edit',
        route_name='api.account.detail',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission = 'login',
        
    )

    config.add_view(
        views.AccountView, attr='delete',
        route_name='api.account.detail',
        request_method='DELETE',
        decorator=return_format,
        renderer='json',
        
        permission = 'login',
        
    )


    # 註冊邀請信
    config.add_route('api.account.invite', '/api/account_invite')
    config.add_view(
        views.AccountView, attr='invite',
        route_name='api.account.invite',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission = 'login',
        
    )

    
    