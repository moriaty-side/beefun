# coding=utf-8
from __future__ import unicode_literals

import uuid, datetime
from sqlalchemy import func,or_
from .banner_domain import Banner
from ...base.base_service import EMPTY_DIC, BaseService
from ...lib.my_exception import MyException

class BannerService(BaseService):

    TABLE = Banner

    def __init__(self, session, logger=None):
        super(BannerService, self).__init__(session,logger)
        self.table_args = self.TABLE.__getattributes__()

    def create(self, account_id=None, **data):
        # 不可用參數
        un_available_args = []
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        create_data = {key: data[key] for key in available_args if key in data}

        create_data['banner_id'] = uuid.uuid4()
        # 當有account_id時
        if account_id:
            create_data['create_by'] = account_id
            create_data['update_by'] = account_id

        # 資料創建
        try:
            return self._create(**create_data)
        except Exception as e:
            raise MyException(code=9001, message=e)

    def update(self, old_obj, account_id=None, **data):
        # 不可用參數
        un_available_args = ['banner_id']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}
        # 當有account_id時
        if account_id:
            update_data['update_by'] = account_id

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=9002, message=e)

    def update_by_id(self, banner_id, **data):
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['banner_id']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 抓取對應物件
        old_obj = self.get_by_id(banner_id)
        # 選填關聯參數
        old_obj, data = self._relationship_obj_ud(old_obj, data)
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=9002, message=e)

    def get_by(self, **data):
        try:
            return self._get_by(**data)
        except Exception as e:
            raise MyException(code=9004, message=e)

    
    def get_by_id(self, banner_id, check=False):
        id = banner_id if isinstance(banner_id, uuid.UUID) else uuid.UUID(banner_id)
        obj = self._get_by(banner_id=id)
        if check and not obj:
            raise MyException(code=9004)
        return obj

    def delete(self, old_obj):
        try:
            return self._delete(old_obj)
        except Exception as e:
            raise MyException(code=9006, message=e)

    def delete_by_id(self, banner_id):
        obj = self.get_by_id(banner_id=banner_id, check=True)
        return self.delete(obj)

    def get_list(self, status=None, type=None, order_by=EMPTY_DIC, group_by=EMPTY_DIC, title=None, language=None,
                 link_type=None, ad_set=None,
                 offset=0, limit=None, show_count=False, **search_condition):

        # 可不用參數
        un_available_args = ['']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        filter_data = {key: search_condition[key]
                       for key in available_args if key in search_condition}

        # 查詢條件製成
        query = self.session.query(self.TABLE)

        try:
            if title is not None:
                query = query.filter(self.TABLE.title.ilike('%{}%'.format(title)))
            
            # status
            if status is not None:
                status_value = self.TABLE._status.info.get(status, None)
                if status_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._status == status_value)
            else:
                status_value = self.TABLE._status.info.get('delete', None)
                query = query.filter(self.TABLE.status!=status_value)

            # type
            if type is not None:
                type_value = self.TABLE._type.info.get(type, None)
                if type_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._type == type_value)

            # link_type
            if link_type is not None:
                link_type_value = self.TABLE._link_type.info.get(link_type, None)
                if link_type_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._link_type == link_type_value)

            # ad_set
            if ad_set is not None:
                ad_set_value = self.TABLE._ad_set.info.get(ad_set, None)
                if ad_set_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._ad_set == ad_set)


            return self._get_list(query, order_by, group_by,
                                  offset, limit, show_count, **filter_data)

        except Exception as e:
            raise MyException(code=9003, message=e)



