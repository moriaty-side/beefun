<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css"/>
    <!-- 圖片預覽 -->

    <!-- DatetimePick -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.css" />
    <!-- DatetimePick -->

    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-tagsinput/bootstrap-tagsinput.css')}" rel="stylesheet" type="text/css" />
    <link href="${ request.static_path('beefun:static/assets/other/jquery.multiple.select/multiple-select.css')}" rel="stylesheet" type="text/css" />

</head>
<body>

    <!--portlet-body-->

    <div class="row">
        <!-- BEGIN: Subheader -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span class="m-font-898b96">Banner管理</span> <i class="fa fa-caret-right"></i>
                        <span>新增</span>
                    </h3>
                </div>
                <div class="col-md-offset-8">
                    <button type="button" class="btn btn-metal m-btn m-btn--outline-2x btn-sm mr-5px" onclick="javascript:window.open('${ request.route_url('page.banner.list') }','_self')">
                                    <span><i class="fa fa-close"></i><span>取消</span></span>
                    </button>
                    <button type="button" class="btn btn-success m-btn m-btn--outline-2x btn-sm" id="update_btn">
                                    <span><i class="fa fa-check"></i><span>確認更新</span></span>
                    </button>
                </div>

            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="row">
            <!--begin::Portlet-->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 20px 20px">
                <!--begin::Form-->
                <form id="banner_update_form">
                    <!--Begin::Section-->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label class="col-form-label col-md-4 col-md-2">

                                </label>
                                      <div class="m-form__control inline-block" style="color: red">
                                        如果您選擇關閉的分類或標籤，前台將不顯示。
                                      <input type='text' class="form-control" name="banner_id" value="${banner_obj.get('banner_id')}" style="display: none;"/>
                                    </div>
                            </div>
                            <div class="form-group has-danger row">
                                <label class="col-form-label col-md-4 col-md-2">
                                    狀態
                                </label>
                                <div class="col-md-3">
                                    <select class="form-control"  name="status">
                                        % if banner_obj.get('status') == 'show':
                                            <option value="show" selected>上架</option>
                                            <option value="hiden">下架</option>
                                        % else:
                                            <option value="show">上架</option>
                                            <option value="hiden" selected>下架</option>
                                        % endif

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-4 col-md-2">
                                    <span class="m--font-danger">*</span>Banner Title
                                </label>
                                <div class="col-md-4">
                                    <input type='text' class="form-control" name="title" value="${banner_obj.get('title')}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-4 col-md-2">
                                    Banner Description
                                </label>
                                <div class="col-md-4">
                                    <input type='text' class="form-control" name="description" value="${banner_obj.get('description','')}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-4 col-md-2">
                                    <span class="m--font-danger">*</span>Banner 類型
                                </label>
                                <div class="col-md-4">
                                    <div class="m-form__group--inline">
                                        <div class="m-form__control inline-block w15p" id="ad-class">
                                            <select class="form-control" name="type">
                                                % if banner_obj.get('type') == 'banner':
                                                    <option value="banner" selected>輪播Banner</option>
                                                    <option value="ad">AD</option>
                                                % else:
                                                    <option value="banner">輪播Banner</option>
                                                    <option value="ad" selected>AD</option>
                                                % endif
                                            </select>
                                        </div>
                                        <div class="m-form__control inline-block w15p hide" id="ad-side">
                                            <select class="form-control se1" name="ad_set">
                                                % for v, s in {'one': _(u'廣告一號版位'), 'two': _(u'廣告二號版位'), 'three': _(u'廣告三號版位'), 'four': _(u'廣告四號版位')}.items():
                                                    % if banner_obj.get('ad_set') == v:
                                                        <option value="${v}" selected>${s}</option>
                                                    % else:
                                                        <option value="${v}">${s}</option>
                                                    % endif
                                                % endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">*</span>圖片
                                </label>
                                <div class="col-md-8">
                                    <div class="fileinput fileinput-exists" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 344px; height: 178px;">
                                            <img src="http://www.placehold.it/344x178/EFEFEF/AAAAAA&amp;text=378x80" alt=""/>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 344px; max-height: 178px;">
                                            % if banner_obj.get('image'):
                                                <img src="${ request.static_path('beefun:static/uploads')+banner_obj.get('image', {}).get('url')}" alt="" image_id="${banner_obj.get('image', {}).get('image_id')}"/>
                                            % else:
                                                <img src="" alt="">
                                            % endif

                                        </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> 選擇圖片 </span>
                                                <span class="fileinput-exists"> 更換</span>
                                                <input type="file" name="image" onchange="readImage(this,378,80);">
                                            </span>
                                            <a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"> 移除 </a>
                                        </div>
                                    </div>
                                    <div class="help-block">尺寸：378x80</div>
                                </div>
                            </div>
                            <div class="form-group has-danger row">
                                <label class="col-form-label col-4 col-md-2">
                                    上架時間
                                </label>
                                <div class="col-md-5 input-group date datetimepicker1">
                                    <input type='text' class="form-control" name="start_time" value="${banner_obj.get('start_time')}"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group has-danger row">
                                <label class="col-form-label col-4 col-md-2">
                                    下架時間
                                </label>
                                <div class="col-md-5 input-group date datetimepicker1">
                                    <input type='text' class="form-control" name="end_time" value="${banner_obj.get('end_time')}"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-4 col-md-2">
                                    網址
                                </label>
                                <div class="col-md-8 mt-7px">
                                    <div class="m-radio-inline">
                                        <label class="m-radio">
                                            <input type="radio" name="weblink" value="os">
                                            外站網址
                                            <span></span>
                                        </label>
                                        <label class="m-radio">
                                            <input type="radio" name="weblink" value="is">
                                            內站網址
                                            <span></span>
                                        </label>
                                    </div>
                                    <div id="os">
                                        <input class="form-control" placeholder="請包含http://https://" name="link" value="${banner_obj.get('link')}">
                                    </div>
                                    <div id="is">
                                        <div class="m-form__group--inline">
                                            <div class="m-form__control inline-block w140">
                                                <select class="form-control se1" id="isSelect" >
                                                    <option value="is1" selected="">分類</option>
                                                    <option value="is2">標籤</option>
                                                    <option value="is3">keyword</option>
                                                </select>
                                            </div>
                                            <div class="inline-block">
                                                <div id="divis1" class="m-form__control w15p is-in">
                                                    <select class="form-control se1" name="category">
                                                    </select>
                                                </div>
                                                <div id="divis2" class="is-in">
                                                    <select class="w200" multiple="multiple" data-placeholder="Select..." name="tag">

                                                    </select>
                                                </div>
                                                <div id="divis3" class="is-in">
                                                    <input class="form-control w35p" name="keyword">
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group has-danger row">
                                <label class="col-form-label col-4 col-md-2">
                                    圖片SEO
                                </label>
                                <div class="col-md-8">
                                    <table class="table table-noline">
                                        <tbody>
                                        <tr>
                                            <td>ALT：</td>
                                            <td><input type="text" class="form-control m-input" placeholder="" name="ALT" value="${banner_obj.get('image_alt', '')}"></td>
                                        </tr>
                                        <tr>
                                            <td>標題：</td>
                                            <td><input type="text" class="form-control m-input" placeholder="" name="TITLE" value="${banner_obj.get('image_title', '')}"></td>
                                        </tr>
                                        <tr>
                                            <td>簡述：</td>
                                            <td><textarea class="form-control" rows="5" style="resize:none;" name="description" value="${banner_obj.get('image_description', '')}"></textarea></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>
    <!--portlet-body end-->

</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js"></script>

<script src="${ request.static_path('beefun:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
<!-- 圖片預覽 -->
<script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
<script src="${ request.static_path('beefun:static/assets/other/readimage.js') }" type="text/javascript"></script>
<!-- 圖片預覽 -->

<!-- DatetimePick -->
## https://eonasdan.github.io/bootstrap-datetimepicker/
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<!-- DatetimePick -->

<script src="${ request.static_path('beefun:static/assets/other/bootstrap-tagsinput/bootstrap-tagsinput.js')}" type="text/javascript"></script>
<script src="${ request.static_path('beefun:static/assets/other/jquery.multiple.select/jquery.multiple.select.js')}" type="text/javascript"></script>

<script>

    var csrfToken = "${request.session.get_csrf_token()}";

    $(function () {
        ##  $('.datetimepicker1').datetimepicker();
        $('.datetimepicker1').datetimepicker({
            format: "YYYY-MM-DDThh:mm:ss",
            defaultDate:new Date()
        });
    });

    //網址選擇
    $(document).ready(function(){




         $("input[name='weblink']").change(function () {
             if ($("input[name='weblink']:checked").val() =='is') {
                 $('#is').show();
                 $('#os').hide();
                 $('#preview-btn').show();
             } else {
                 $('#is').hide();
                 $('#os').show();
                 $('#preview-btn').hide();
             }
         });

         % if banner_obj.get('type_link') == 'out':
            $($("input[name='weblink']")[1]).attr('checked',true);
            $('#is').show();
            $('#os').hide();
        % else:
            $($("input[name='weblink']")[0]).attr('checked',true);
            $('#is').hide();
            $('#os').show();
        % endif

        % if banner_obj.get('link_type')=='cate':
            $("#divis2").hide();
            $("#divis3").hide();
        %elif  banner_obj.get('link_type')=='tag':
            $("#divis1").hide();
            $("#divis3").hide();

        %elif  banner_obj.get('link_type')=='Keyword':
            $("#divis2").hide();
            $("#divis1").hide();

        %else:
            $("#divis2").hide();
            $("#divis3").hide();
        % endif

        $("#isSelect").change(function(){
            var selectedValue = $(this).val();
            $(".is-in").hide();
            $("#div" + selectedValue).show();
        });
        // 驅動 banner_create_form submit
        $("#update_btn").click(function(){
            $('#banner_update_form').submit();
        })
    });

    $('#banner_update_form').submit(function(e) { e.preventDefault(); }).validate({
        ignore: ":hidden",
        errorElement: 'div', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        rules: {
            title: {
                required: true,
            },
            type: {
                required: true,
            },
            ad_set: {
                required: true,
            },
            link: {
                required: true,
            }
        },
        messages: {
            title: {
                required: "請輸入標題"
            },
            type: {
                required: "請輸入類型"
            },
            ad_set: {
                required: "請輸入廣告"
            },
            link: {
                required: "請輸入連結"
            }

        },
        highlight: function(element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function(error, element) {
            ##  error.insertAfter(element);
        },
        submitHandler: function(form) {
            var form_json = {};
            $(form).serializeArray().map(function(v, i) {
                form_json[v['name']] = v['value'];
            });
            console.log("form_json:", form_json);

            var form_data = new FormData();
            for (var k in form_json) {
                form_data.append(k, form_json[k]);
            }
            var banner_id = $('input[name="banner_id"]').val();
            upload_image(function(status, image_data){
                // 上傳圖片是否正確
                if (status){
                    form_data.append('image_id', image_data['image_id']);
                    var url = "${ request.route_url('api.banner.detail', banner_id='') }"+banner_id;
                    var method = "POST";
                    ajax(url, method, form_data, $('.update_btn'), function(response) {
                        if (response['status']) {
                            console.log(response);
                            location.href = "${ request.route_url('page.banner.list')}"
                        } else {
                            alert(response['message']);
                        }
                    });
                }
            });
        }
    });
    function upload_image(callback){
        // 上傳圖片
        if ($('#banner_update_form').find("input[name='image']")[0].files[0] == undefined){
            var image_id = $($('#banner_update_form').find("img")[1]).attr('image_id');
            if(image_id){
                callback(true, {'image_id': image_id});
            }else{
                alert('圖片上傳錯誤');
                callback(false, null);
            }
        }else{
            var image_form_data = new FormData();
            image_form_data.append('image_file', $('#banner_update_form').find("input[name='image']")[0].files[0])
            var upload_image_url = "${ request.route_url('api.image') }";
            ajax(upload_image_url, "POST", image_form_data, $('.update_btn'), function(response){
                  if (response['status']) {
                      console.log(response);
                      callback(true, response['response']['image_list'][0])
                  } else {
                      alert(response['message']);
                  }
            });
        }
    }

    function ajax(url, method, form_data, btn, callback) {
        $.ajax({
            url: url,
            type: method,
            data: form_data,
            contentType: false,
            processData: false,
            headers: { 'X-CSRF-Token': csrfToken },
            beforeSend: function() {
                $(btn).attr('disabled', true);
            },
            error: function(xhr) {
                $(btn).attr('disabled', true);
                alert('Ajax request 發生錯誤');
            },
            success: function(response) {
                callback(response);
            },
            complete: function() {
                $(btn).attr('disabled', false);
            }
        });
    }


</script>
</html>