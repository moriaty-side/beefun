<%inherit file="beefun:templates/backend/master.mako"/>

<%block name="css">
    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css" />
    <!-- 圖片預覽 -->

</%block>

<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span class="m-font-898b96">${ _(u'首頁管理')}</span> <i class="fa fa-caret-right"></i>
                        <span>${ _(u'Banner')}</span>
                    </h3>
                </div>
                
            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="banner" data-parent="home_set">
            <!--begin::Portlet-->
            <div class="languages hide"></div>
            <div class="m-portlet">
                
                <!--Begin::Section-->
                <div class="m-portlet__body">
                    <div class="row ">
                        <div class="col-12">

                            <div class="table-scrollable no-border">
                                <table class="table m-table m-table--head-bg-success" >
                                    <thead>
                                    <tr>
                                        
                                        <th> ${ _(u'顯示')}</th>
                                        

                                        <th> ${ _(u'圖片')} </th>
                                        <th> ${ _(u'ALT')}</th>
                                        <th><!--操作-->&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody id="banner_tbody">

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="4">
                                            <div>
                                                <a href="#" class="addtr btn btn-brand m-btn m-btn--outline-2x btn-sm"> <i class="fa fa-plus"></i> ${ _(u'新增Banner')}</a>
                                            </div>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End::Section-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>
</%block>



<%block name="script">
    <script src="${ request.static_path('beefun:static/assets/test.js') }"></script>
    <script type="text/xrender" id="tb_tr_row">
        <tr class="banner_tr">
          <td>
            <div class="table-cell">
                <label class="m-checkbox">
                    <input type="checkbox" checked>
                    <span></span>
                </label>
            </div>
            <div class="table-cell">
                <input type="text" class="form-control input-mini number_set" value="1" name="sequence">
            </div>
          </td>
          <td>
              <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail text-l" style="width: 384px; height: 178px;">
                  <img src="http://www.placehold.it/344x178/EFEFEF/AAAAAA&amp;text=1920x890" alt=""/>
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 384px; max-height: 178px;">
                  <img class="banner_image" src="" alt="">
                </div>
                <div>
                  <span class="btn default btn-file">
                    <span class="fileinput-new"> ${ _(u'選擇圖片')} </span>
                    <span class="fileinput-exists"> ${ _(u'更換')}</span>
                    <input type="file" name="..." onchange="readImage(this,1920,890);">
                  </span>
                  <a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"> ${ _(u'移除')} </a>
                </div>
            </div>
            <div class="help-block">尺寸：1920x890</div>
          </td>
          <td><input type="text" class="form-control" name="description"></td>
          <td align="right">
              <button type="button" class="btn btn-outline-metal m-btn m-btn--outline-2x btn-sm">
                <span>
                    <i class="la la-eye-slash"></i>
                    <span>
                        ${ _(u'封存')}
                    </span>
                </span>
            </button>
          </td>
        </tr>
    </script>
    <!-- 圖片預覽 -->
    <script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
    ##     <script src="${ request.static_path('beefun:static/assets/other/readimage.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/make_table/make_table.js')}" type="text/javascript"></script>
    <!-- 圖片預覽 -->
    <script>
        $('.se1').select2();
        var csrfToken = "${request.session.get_csrf_token()}";

        var btn_check;
        

        var Table = new MakeTable({
            'dom_search_btn': $('.btn-search'),
            'dom_table_tbody': $('.banner_list'),
            
            'init_search_parameter': '',
            
            'ajax_search_url':  "${ request.route_url('api.banner') }",
            'ajax_csrf_token':csrfToken,
            'table_model': function (obj, page) {
                //開始產生頁面資料
                var $list = obj['banner_list'];
                var html_str = '';
                //var data_table_row = [];
                ##  console.log(obj)
                $.each($list, function (i, item) {
                    item.index = (i + 1 + ((page - 1) * 10));
                    var status=""
                    switch (item.status) {
                        case "show":
                            status = "${ _(u'開通')}";
                            break;
                        case "hiden":
                            status = "${ _(u'關閉')}";
                            break;
                        case "delete":
                            status = "${ _(u'刪除')}";
                            break;
                    }
                    var tb_row = " <tr class='obj_index'>" +
                            "<td>"+"<div class='table-cell'>" + "<label class='m-checkbox'>";
                    if(status=="${ _(u'開通')}") {
                        tb_row+="<input class='check' type='checkbox' checked>"
                    }else{
                        tb_row+="<input class='check' type='checkbox'>"
                    }
                    tb_row +="<input type='text' value='"+item.banner_id+"'  class='form-control banner_id'>";
                    tb_row+="<span></span></label>";
                    
                    tb_row+="<td>";

                    if (item.image_id!='None'){
                        tb_row += "<div class='fileinput fileinput-exists' data-provides='fileinput'>";
                    }else{
                        tb_row += "<div class='fileinput fileinput-new' data-provides='fileinput'>";
                    }
                    tb_row+="<div class='fileinput-new thumbnail text-l' style='width: 384px; height: 178px;'>";
                    tb_row+="<img src='http://www.placehold.it/384x178/EFEFEF/AAAAAA&amp;text=1920x890' alt=''/>";
                    tb_row+="</div>";
                    tb_row+=" <div class='fileinput-preview fileinput-exists thumbnail' style='max-width: 384px; max-height: 178px;'>";
                    if (item.image_id!='None'){
                        tb_row += "<img src='"+"${ request.static_path('beefun:static/uploads')}"+item.image.url+"' alt='' style='width: 100%;'>";
                    }else{
                        tb_row += "<img src='' alt='' style='width: 100%;'>";
                    }
                    tb_row+="</div>";

                    tb_row+="<div>";
                    tb_row+="<span class='btn default btn-file'>";
                    tb_row+="<span class='fileinput-new'> ${ _(u'選擇圖片')} </span>";
                    tb_row+="<span class='fileinput-exists'> ${ _(u'更換')}</span>";
                    tb_row+=" <input type='file' class='image_file' onchange='readImage(this,1920,890);'>";
                    tb_row+="</span>";
                    tb_row+="<a href='#' class='btn red fileinput-exists btn_del_pic' data-dismiss='fileinput'> ${ _(u'移除')} </a>";
                    tb_row+="</div>";
                    tb_row+="</div><div class='help-block'>尺寸：1920x890</div></td>";
                    tb_row +="<td>"+"<input type='text' value='"+item.description+"' class='form-control description'>"+"</td>";
                    tb_row +=
                            "<td align='right'><button type='button' class='btn btn-outline-metal m-btn m-btn--outline-2x btn-sm del_btn'><span><span><i class='la la-eye-slash'></i>${ _(u'刪除')}</span></span></td>"
                    "</tr>";
                    html_str += tb_row;
                });
                $('#banner_tbody').empty()
                $('#banner_tbody').append(html_str);//Table頁面資料繪製
            }
        });

        $(document).ready(function (e) {
            
            Table.start();
        });

        $(document).on('blur', '.description', function () {
            var url = "${ request.route_url('api.banner.detail',banner_id='') }"+$(this).closest(".obj_index").find('.banner_id').val();
            var form_data = new FormData();
            form_data.append('description', $(this).val());
            ajax(url, "POST", form_data, null, function (response) {
                if (response['status']) {


                    $('#banner_tbody').empty();
                    Table.start();
                } else {
                    alert(response['message']);
                }
            });
        });

        $(document).on('click', '.btn_del_pic', function (e) {
            var url = "${ request.route_url('api.banner.detail',banner_id='') }"+$(this).closest(".obj_index").find('.banner_id').val();
            var form_data = new FormData();
            form_data.append('image_id', "");

            ajax(url, "POST", form_data, null, function (response) {
                if (response['status']) {


                    $('#banner_tbody').empty();
                    Table.start();
                } else {
                    alert(response['message']);
                }
            });
        });

        $(document).on('change', '.check', function (e) {
            var url = "${ request.route_url('api.banner.detail',banner_id='') }"+$(this).closest(".obj_index").find('.banner_id').val();
            var form_data = new FormData();
            if(this.checked){
                form_data.append('status', 'show');
            }else{
                form_data.append('status','hide');
            }
            ajax(url, "POST", form_data, null, function (response) {
                if (response['status']) {

                    $('#banner_tbody').empty();
                    Table.start();
                } else {
                    alert(response['message']);
                }
            });
        });

        $(document).on('click', '.del_btn', function (e) {
            var url = "${ request.route_url('api.banner.detail',banner_id='') }"+$(this).closest(".obj_index").find('.banner_id').val();
            var form_data = new FormData();
            form_data.append('status', 'delete');
            
            ajax(url, "POST", form_data, null, function (response) {
                if (response['status']) {
                    $('#banner_tbody').empty();
                    Table.start();
                } else {
                    alert(response['message']);
                }
            });
        });

        $(document).on('keydown', '.description', function (e) {
            if (e.which == 13) {
                var url = "${ request.route_url('api.banner.detail',banner_id='') }"+$(this).closest(".obj_index").find('.banner_id').val();
                var form_data = new FormData();
                form_data.append('description', $(this).val());
                ajax(url, "POST", form_data, null, function (response) {
                    if (response['status']) {
                        $('#banner_tbody').empty();
                        Table.start();
                    } else {
                        alert(response['message']);
                    }
                });
            }
        });

        //------------判斷圖片尺寸------------------//
        function readImage(file,img_w,img_h) {
            var src  =$(file).parents('.fileinput').find('.fileinput-preview.thumbnail img').attr('src'),
                    input = file,
                    exsits=$(file).parents('.fileinput').hasClass('fileinput-exists');
            btn_check = $(file).closest('.obj_index');
            ##  console.log(input,src,exsits);
            var file_id= file.id,
                imageFile = file.files[0];
            var reader = new FileReader();
            var image  = new Image();
            reader.readAsDataURL(imageFile);
            var w = 0,h=0;
            reader.onload = function(_file) {
                image.src    = _file.target.result;
                image.onload = function() {
                    w = this.width,
                            h = this.height;
                    // t = file.type,
                    // n = file.name,
                    // s = ~~(file.size/1024) +'KB';
                    // console.log(w+'x'+h);
                    if(w !=img_w || h !=img_h){
                        if(!exsits){
                            $(file).parents('.fileinput').addClass('fileinput-new').removeClass('fileinput-exists');
                        }
                        $(input).parents('.fileinput').find('.fileinput-preview.thumbnail img').attr('src',src);
                        alert("${ _(u'請選擇尺寸為')} "+img_w+' X '+img_h+"${ _(u' 的圖片')}");
                        $('#banner_tbody').empty();
                        Table.start();
                    }else{
                        upload_image(function (status, image_data) {
                            if (status) {
                                var url = "${ request.route_url('api.banner.detail',banner_id='') }" + $(file).closest(".obj_index").find('.banner_id').val();
                                var form_data = new FormData();
                                form_data.append('image_id', image_data["image_id"]);
                                ajax(url, "POST", form_data, null, function (response) {
                                    if (response['status']) {
                                        $('#banner_tbody').empty();
                                        Table.start();
                                    } else {
                                        alert(response['message']);
                                    }
                                });
                            }
                        });
                    }
                };
                image.onerror= function() {
                    alert('Invalid file type: '+ file.type);
                };
            };
        }

        function upload_image(callback){
            // 上傳圖片
            if (btn_check.find('.image_file')[0].files[0] == undefined){
                var image_id = btn_check.find('.logo_image').attr('image_id');
                if(image_id){
                    callback(true, {'image_id': image_id});
                }else{
                    alert("${ _(u'圖片上傳錯誤')}");
                    callback(false, null);
                }
            }else{
                var image_form_data = new FormData();
                image_form_data.append('image_file', btn_check.find('.image_file')[0].files[0]);
                var upload_image_url = "${ request.route_url('api.image') }";
                ajax(upload_image_url, "POST", image_form_data, $('.create_btn'), function(response){
                    if (response['status']) {
                        ##  console.log(response);
                        callback(true, response['response']['image_list'][0])
                    } else {
                        alert(response['message']);
                    }
                });
            }
        }

        //新增banner
        $('.addtr').click(function(){
            var event_btn = $(this);
            var url = "${ request.route_url('api.banner')}";
            var banner_data_form = new FormData();
            
            ajax(url, 'POST', banner_data_form, event_btn, function(data){
                if (event_btn) event_btn.attr("disabled", false);
                if (data['status']) {
                    $('#banner_tbody').empty();
                    Table.start();
                } else {
                    alert('${_(u"創建失敗")}')
                }
            });
        });

        $(document).on('click','.save_btn',function(){
            var event_btn = $(this);
            var banner_data_list = [];
            var number_set_check = [];
            //第一層
            $('.obj_index').each(function(i, v){
                if(parseInt($(v).find('.number_set').val())==0){
                    alert('請勿輸入0');
                    return false;
                }
                var banner_data = {
                    'banner_id': $(v).find('.banner_id').val(),
                    'sequence': $(v).find('.number_set').val()
                }
                banner_data_list.push(banner_data);
                number_set_check.push($(v).find('.number_set').val());
            });

            var number_set_check_onleny=number_set_check.filter(function (el, i, arr) {
                return arr.indexOf(el) === i;
            });
            if(banner_data_list.length==number_set_check_onleny.length){
                banner_save(event_btn, banner_data_list);
            }else{
                alert("${ _(u'序列有重複，請修正！！')}");
            }
            return false;
        });

        function banner_save(event_btn, banner_data_list){
            var url = "${ request.route_url('api.banner.multi.set')}";
            var banner_data_form = new FormData();
            banner_data_form.append('json_data', JSON.stringify(banner_data_list))
            ajax(url, 'POST', banner_data_form, event_btn, function(data){
                if (event_btn) event_btn.attr("disabled", false);

                if (data['status']) {
                    $('#banner_tbody').empty();
                    Table.start();
                } else {
                    alert('${_(u"創建失敗")}')
                }
            });
        }

        function ajax(url, method, form_data, btn, callback){
            $.ajax({
                url: url,
                type: method,
                data: form_data,
                ##  dataType: 'JSON',
                contentType: false,
                processData: false,
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    if (btn) btn.attr("disabled", true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                    if (btn) btn.attr("disabled", false)
                },
                success: function (response) {
                    callback(response);
                },
                complete: function () {
                    if (btn) btn.attr("disabled", false);
                }
            });
        }
    </script>
</%block>


