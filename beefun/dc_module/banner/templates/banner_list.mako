<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

</head>
<body>

<!--portlet-body-->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    <span>Banner管理</span>
                </h3>
            </div>
            <div>
                <button type="button" class="btn btn-brand m-btn m-btn--outline-2x btn-sm" onclick="javascript:window.open('${ request.route_url('page.banner.create') }','_self')">
                    <span><i class="fa fa-plus"></i><span>新增</span></span>
                </button>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <!--begin::Portlet-->
    <div class="m-content" data-menu="banner_list" data-parent="">
        <!--begin::Portlet-->
        <div class="m-portlet">
            <!--Begin::Section-->
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-12">
                        <table class="table m-table m-table--head-bg-success">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>縮圖</th>
                                <th>簡述</th>
                                <th>狀態</th>
                                <th><!--操作--></th>
                            </tr>
                            </thead>
                            <tbody class="banner_list">

                            </tbody>
                        </table>
                        <!--頁碼-->
                        <div>
                            <ul class="pagination" id="page-selection">
                            </ul>
                        </div>
                        <!--頁碼  end-->
                    </div>
                </div>
            </div>
            <!--End::Section-->
        </div>
        <!--end::Portlet-->
    </div>
    <!--end::Portlet-->
</div>
<!--portlet-body end-->

<div id="create_account" class="modal fade" role="dialog" aria-labelledby="myModalLabel10">
    <!--modal-dialog-->
    <div class="modal-dialog modal-lg">
        <!--modal-content-->
        <div class="modal-content">
            <div id="bounce-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"></button>
                    <h4 class="modal-title">創建帳號</h4>
                </div>
                <div class="modal-body">
                    <form action="#" class="form-horizontal" role="form" id="create_account_form">
                        <!--資料01-->
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label col-xs-4"><span class="required">*</span>帳號：</label>
                                    <div class="col-xs-7">
                                        <input type="text" class="form-control" placeholder="" name="account">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4"><span class="required">*</span>姓名：</label>
                                    <div class="col-xs-7">
                                        <input type="text" class="form-control" placeholder="" name="name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4"><span class="required">*</span>密碼：</label>
                                    <div class="col-xs-7">
                                        <input type="password" class="form-control" placeholder="" name="password" id="model_password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4"><span class="required">*</span>重複密碼：</label>
                                    <div class="col-xs-7">
                                        <input type="password" class="form-control" placeholder="" name="re_password">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer no-line">
                            <!-- <button type="submit" class="btn green-seagreen" data-dismiss="modal"><i class="fa fa-check"></i> 寄送邀請</button> -->
                            <button type="submit" class="btn btn-success uppercase pull-right" >創建帳號</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--modal-content end-->
    </div>
    <!--modal-dialog end-->
</div>
<div id="update_account" class="modal fade" role="dialog" aria-labelledby="myModalLabel10">
    <!--modal-dialog-->
    <div class="modal-dialog modal-lg">
        <!--modal-content-->
        <div class="modal-content">
            <div id="bounce-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"></button>
                    <h4 class="modal-title">更新帳號</h4>
                </div>
                <div class="modal-body">
                    <form action="#" class="form-horizontal" role="form" id="update_account_form">
                        <!--資料01-->
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label col-xs-4"><span class="required">*</span>帳號：</label>
                                    <div class="col-xs-7">
                                        <input type="text" class="form-control" placeholder="" name="account_id" style="display: none;">
                                        <input type="text" class="form-control" placeholder="" name="account" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4"><span class="required">*</span>姓名：</label>
                                    <div class="col-xs-7">
                                        <input type="text" class="form-control" placeholder="" name="name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4"><span class="required">*</span>狀態：</label>
                                    <div class="col-xs-7">
                                        <select class="form-control input-inline select2-multiple" data-placeholder="Select..." name="status">
                                            <option></option>
                                            <% status_code = {'show': '開通', 'hiden': '關閉', 'delete': '刪除'} %>
                                            % for status in ['show', 'hiden', 'delete']:
                                                <option value="${ status }">${ status_code[status] }</option>
                                            % endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer no-line">
                            <button type="submit" class="btn btn-success uppercase pull-right update_btn" >更新帳號</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--modal-content end-->
    </div>
    <!--modal-dialog end-->
</div>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js"></script>

<script src="${ request.static_path('beefun:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
<script src="${ request.static_path('beefun:static/assets/make_table/make_table.js')}" type="text/javascript"></script>

<script>

    var csrfToken = "${request.session.get_csrf_token()}";

    var Table = new MakeTable({
        'dom_search_btn': $('.btn-search'),
        'dom_table_tbody': $('.banner_list'),
        'ajax_search_url':  "${ request.route_url('api.banner') }",
        'ajax_csrf_token':csrfToken,
        'table_model': function (obj, page) {
            //開始產生頁面資料
            var $list = obj['banner_list'];
            var html_str = '';
            //var data_table_row = [];
            console.log(obj)
            $.each($list, function (i, item) {
                item.index = (i + 1 + ((page - 1) * 10));
                var status=""
                switch (item.status) {
                    case "show":
                        status = "開通";
                        break;
                    case "hiden":
                        status = "關閉";
                        break;
                    case "delete":
                        status = "刪除";
                        break;
                }
                var tb_row = " <tr>" +
                        "<td>" + item.index + "</td>" ;
                if (item.image){
                    tb_row += "<td><img src='${ request.static_path('beefun:static/uploads')}" + item.image.url + "' alt='" + item.image.alt+ "' style='width:200px;'></td>";
                }else{
                    tb_row += "<td><img src='http://www.placehold.it/344x178/EFEFEF/AAAAAA&amp;text=378x80' style='width:200px;'></td>";
                }
                tb_row += "<td>" + item.title + "</td>" +
                        "<td>" + status+ "</td>" +
                        "<td><button class='btn blue btn-add update_btn' onclick=\"show_update_banner('"+item['banner_id']+"')\">修改</button>"+
                        "</tr>";
                html_str += tb_row;
            });
            $('.banner_list').append(html_str);//Table頁面資料繪製
        }
    });
    Table.start();

    function show_update_banner(banner_id){
        var url = "${ request.route_url('page.banner.update', banner_id='') }"+banner_id ;
        location.href = url;
    }

</script>
</html>