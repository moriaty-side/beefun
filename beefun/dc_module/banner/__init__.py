from __future__ import unicode_literals

from . import banner_views as views
from ...base.context import AdminContext
from ...lib.api import return_format
from ...lib.page import return_page_format


def includeme(config):
    #Page
    config.add_route('page.banner.list', '/page/banner/list', factory=AdminContext)
    config.add_view(
        views.BannerPageView, attr='list',
        route_name='page.banner.list',
        renderer='templates/banner.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )
    config.add_route('page.banner.create', '/page/banner/create', factory=AdminContext)
    config.add_view(
        views.BannerPageView, attr='create',
        route_name='page.banner.create',
        renderer='banner_create.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )
    config.add_route('page.banner.update', '/page/banner/update/{banner_id}', factory=AdminContext)
    config.add_view(
        views.BannerPageView, attr='update',
        route_name='page.banner.update',
        renderer='banner_update.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )

    # API
    # banner create

    # 多筆儲存
    config.add_route('api.banner.multi.set', '/api/banner/multi_set', factory=AdminContext)
    config.add_view(
        views.BannerJsonView, attr='multi_set',
        route_name='api.banner.multi.set',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_route('api.banner', '/api/banner', factory=AdminContext)
    config.add_view(
        views.BannerJsonView, attr='create',
        route_name='api.banner',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_view(
        views.BannerJsonView, attr='search',
        route_name='api.banner',
        request_method='GET',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )




    config.add_route('api.banner.detail', '/api/banner/{banner_id}', factory=AdminContext)
    config.add_view(
        views.BannerJsonView, attr='get',
        route_name='api.banner.detail',
        request_method='GET',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_view(
        views.BannerJsonView, attr='edit',
        route_name='api.banner.detail',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_view(
        views.BannerJsonView, attr='delete',
        route_name='api.banner.detail',
        request_method='DELETE',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )






