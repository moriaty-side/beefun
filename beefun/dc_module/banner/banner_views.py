# coding=utf8
from __future__ import unicode_literals
import transaction, json
from . import banner_form as form
from ...base.base_view import BaseView
from .banner_service import BannerService
import uuid
class BannerPageView(BaseView):
    def __init__(self, request):
        super(BannerPageView, self).__init__(request)
        self.banner_service = BannerService(self.session)

    def list(self):
        _ = self.localizer
        return {}

    def create(self):
        _ = self.localizer
        return {'banner_obj': {'link_type': 'cate'}}

    def update(self):
        _ = self.localizer
        banner_obj = self.banner_service.get_by_id(banner_id=self.request.matchdict['banner_id'],
                                                   check=True)
        return {'banner_obj': banner_obj.__json__()}


class BannerJsonView(BaseView):
    def __init__(self, request):
        super(BannerJsonView, self).__init__(request)
        self.banner_service = BannerService(self.session)

    def create(self):
        """
        創建標籤
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.BannerCreateForm().to_python(self.request_params)
        request_data['description'] = ''

        
        with transaction.manager:
            banner_obj = self.banner_service.create(**request_data)

        return {'response': {'banner': banner_obj.__json__()},
                'message': _(u'Banner創建成功')}

    def search(self):
        """
        搜尋標籤
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.BannerSearchForm().to_python(self.request_params)

        # 分頁機制
        current_page = request_data.get('page', 1)
        limit = request_data.get('limit', 10)
        request_data['offset'] = (current_page - 1) * limit
        request_data['limit'] = limit
        request_data['order_by'] = [('created', -1)]

        # 搜尋帳號
        banner_list, total_count = self.banner_service.get_list(show_count=True, **request_data)

        return {'response': {'banner_list': [a.__json__(show_image=True) for a in banner_list],
                             'total_count': total_count,
                             'total_page': (int(total_count / limit) + 1 if total_count % limit else 0)
                             },
                'message': u'Banner搜尋成功'}

    def get(self):
        """
        讀取標籤
        """
        _ = self.localizer

        # 用 category_id 取得分類
        banner_obj = self.banner_service.get_by_id(banner_id=self.request.matchdict['banner_id'],
                                                   check=True)

        return {'response': {'banner': banner_obj.__json__()},
                'message': 'Banner讀取成功'}

    def edit(self):
        """
        編輯標籤
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.BannerEditForm().to_python(self.request_params)

        # 用 category_id 取得分類
        banner_obj = self.banner_service.get_by_id(banner_id=self.request.matchdict['banner_id'],
                                                   check=True)
        

        # 更新帳號
        with transaction.manager:
            
            banner_obj = self.banner_service.update(old_obj=banner_obj,
                                                    account_id=self.account_id,
                                                    **request_data)

        return {'response': {'banner': banner_obj.__json__()},
                'message': u'Banner修改成功'}

    def delete(self):
        """
        刪除標籤
        """
        _ = self.localizer

        # 用 tag_id 取得分類
        banner_obj = self.banner_service.get_by_id(banner_id=self.request.matchdict['banner_id'],
                                                   check=True)
        request_data = {'status': 'delete'}
        # 更新 status 達到軟刪除
        with transaction.manager:
            banner_obj = self.banner_service.update(old_obj=banner_obj,
                                                    account_id=self.account_id,
                                                    **request_data)
            self.banner_service.delete(old_obj=banner_obj)

        return {'message': u'Banner刪除成功'}

    def multi_set(self):
        """
        一次處理多筆
        :return:
        """
        _ = self.localizer

        # 檢查輸入參數
        request_json_list = json.loads(self.request_params.get('json_data', '{}').replace('\r\n', '\\r\\n'))

        response = []
        with transaction.manager:
            for banner_data in request_json_list:
                banner_obj = self.banner_service.update_by_id(**banner_data)
                response.append(banner_obj.__json__())

        return {'response': {'banner_list': response},
                'message': _(u'banner 創建更新成功')}

