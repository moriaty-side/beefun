import uuid
from sqlalchemy import (Column, Index, Integer, ForeignKey, String, DateTime)

from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.ext.declarative import declarative_base

from ..image.image_domain import Images
from ...base.meta_module import (Base, TimestampTable, GUID, uuid4)

DeclarativeBase = declarative_base()

class Banner(Base, TimestampTable):
    """
    Banner
    """
    __tablename__ = 't_banner'

    banner_id = Column('f_banner_id', GUID, default=uuid4, primary_key=True, doc=u'BannerID')
    
    image_id = Column('f_image_id', ForeignKey('t_images.f_image_id'), nullable=True, doc=u"網站圖片")
    
    link = Column('f_link', String(200), nullable=True, doc=u'連結')
    
    description = Column('f_description', String(200), nullable=True, doc=u'Banner描述')
    
    remark1 = Column('f_remark1', String(128), doc=u"預留欄位")

    remark2 = Column('f_remark2', String(128), doc=u"預留欄位")

    _status = Column('f_status', Integer, nullable=False, default=11,
                     info={"show": 10, "hiden": 11, "delete": 12},
                     doc=u"狀態:(10.顯示,11.隱藏,12.封存")
    
    image = relationship(Images)

    def __repr__(self):
        return '<BannerObject (banner_id={0})>'.format(self.banner_id)

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status
    
    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    def __json__(self, show_image=False):
        d = {'banner_id': str(self.banner_id),
             'image_id': str(self.image_id),
             'description': self.description,
             'link': str(self.link),
             'status': self.status,
             'remark1': self.remark1,
             'remark2': self.remark2,
             
             
             
             
             
             }

        if show_image:
            d['image'] = self.image.__json__() if self.image else {}

        return d