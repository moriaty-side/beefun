from ...base.base_test import BaseTest, dummy_request
from .banner_domain import Banner
from .banner_views import BannerJsonView

class BannerAPITest(BaseTest):

    def setUp(self):
        super(BannerAPITest, self).setUp()
        self.init_database()

    def create(self, description='test2'):
        banner_obj = Banner(description=description)
        self.session.add(banner_obj)
        banner_obj = self.session.query(Banner).filter(Banner.description == description).first()
        return banner_obj

    def test_create(self):
        """
        測試 創建帳號
        """
        title = 'test'
        request = dummy_request(self.session)
        request.params = {'title': title}
        view = BannerJsonView(request)
        response = view.create()
        self.assertEqual(response.get('message'), 'Banner創建成功')

    def test_search(self):
        """
        測試 創建帳號
        """
        request = dummy_request(self.session)
        view = BannerJsonView(request)
        banner_resp_list = view.search()
        self.assertEqual(banner_resp_list.get('message'), 'Banner搜尋成功')
        self.assertEqual(banner_resp_list.get('response', {}).get('total_count'), 0)
        self.assertEqual(banner_resp_list.get('response', {}).get('total_page'), 0)
        self.assertEqual(len(banner_resp_list.get('response', {}).get('banner_list', [])), 0)

    def test_get(self):
        """
        測試 讀取帳號
        """
        banner = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'banner_id': str(banner.banner_id)}
        view = BannerJsonView(request)
        response = view.get()
        self.assertEqual(response.get('message'), 'Banner讀取成功')

    def test_update(self):
        """
        測試 更新帳號
        """
        banner = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'banner_id': str(banner.banner_id)}
        description = '123'
        request.params = {'description': description}
        view = BannerJsonView(request)
        response = view.edit()
        self.assertEqual(response.get('message'), 'Banner修改成功')
        self.assertEqual(response.get('response', {}).get('banner', {}).get('description'), description)

    def test_delete(self):
        """
        測試 更新帳號
        """
        banner = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'banner_id': str(banner.banner_id)}
        view = BannerJsonView(request)
        response = view.delete()
        self.assertEqual(response.get('message'), 'Banner刪除成功')

