# coding=utf8
from __future__ import unicode_literals

import formencode, uuid
from formencode import validators


class BannerCreateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True
    
    image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    description = validators.String()
    link = validators.String()
    status = validators.String(not_empty=True)
    
    
    
class BannerSearchForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True
    
    
    
    status = validators.String(not_empty=True, strip=True)
    
    page = validators.Int(not_empty=True, min=1)  # 第幾頁
    limit = validators.Int(not_empty=True, min=1)  # 一頁幾筆


class BannerEditForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    banner_id = validators.String(not_empty=True)
    
    image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    description = validators.String()
    link = validators.String()
    status = validators.String(not_empty=True)
    
    
    
