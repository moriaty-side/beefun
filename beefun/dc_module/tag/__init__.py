from __future__ import unicode_literals

from . import tag_views as views
from ...base.context import AdminContext
from ...lib.api import return_format
from ...lib.page import return_page_format


def includeme(config):
    #Page
    config.add_route('page.tag', '/page/tag/list', factory=AdminContext)
    config.add_view(
        views.TagPageView, attr='single_manage',
        route_name='page.tag',
        renderer='templates/tag_list.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )

    # API
    # tag create
    config.add_route('api.tag', '/api/tag', factory=AdminContext)
    config.add_view(
        views.TagJsonView, attr='create',
        route_name='api.tag',
        renderer='json',
        request_method='POST',
        decorator=return_format,
        
        permission='login',
        
    )
    config.add_view(
        views.TagJsonView, attr='search',
        route_name='api.tag',
        request_method='GET',
        decorator=return_format,
        renderer='json',
        permission='login'
    )

    config.add_route('api.tag.detail', '/api/tag/{tag_id}', factory=AdminContext)
    config.add_view(
        views.TagJsonView, attr='get',
        route_name='api.tag.detail',
        request_method='GET',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_view(
        views.TagJsonView, attr='edit',
        route_name='api.tag.detail',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_view(
        views.TagJsonView, attr='delete',
        route_name='api.tag.detail',
        request_method='DELETE',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )
