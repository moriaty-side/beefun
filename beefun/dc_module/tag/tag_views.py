# coding=utf8
from __future__ import unicode_literals

import json
import transaction

from . import tag_form as form
from ...base.base_view import BaseView
from ...lib.my_exception import MyException
from .tag_service import TagService

class TagPageView(BaseView):
    def __init__(self, request):
        super(TagPageView, self).__init__(request)
        self.tag_service = TagService(self.session)

    def single_manage(self):
        _ = self.localizer
        return {}


class TagJsonView(BaseView):
    def __init__(self, request):
        super(TagJsonView, self).__init__(request)
        self.tag_service = TagService(self.session)

    def create(self):
        """
        創建標籤
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.TagCreateForm().to_python(self.request_params)

        with transaction.manager:
            tag_obj = self.tag_service.create(**request_data)

        return {'response': {'tag': tag_obj.__json__()},
                'message': _(u'標籤創建成功')}

    def search(self):
        """
        搜尋標籤
        """

        # 檢查輸入參數
        request_data = form.TagSearchForm().to_python(self.request_params)

        # 分頁機制
        current_page = request_data.get('page', 1)
        limit = request_data.get('limit', 10)
        request_data['offset'] = (current_page - 1) * limit
        request_data['limit'] = limit
        request_data['order_by'] = [('created', -1)]

        # 搜尋帳號
        category_list, total_count = self.tag_service.get_list(show_count=True, **request_data)

        return {'response': {'tag_list': [a.__json__() for a in category_list],
                             'total_count': total_count,
                             'total_page': (int(total_count / limit) + 1 if total_count % limit else 0)},
                'message': u'標籤搜尋成功'}

    def get(self):
        """
        讀取標籤
        """

        # 用 category_id 取得分類
        tag_obj = self.tag_service.get_by_id(tag_id=self.request.matchdict['tag_id'],
                                             check=True)

        return {'response': {'tag': tag_obj.__json__()},
                'message': '標籤讀取成功'}

    def edit(self):
        """
        編輯標籤
        """

        # 檢查輸入參數
        request_data = form.TagEditForm().to_python(self.request_params)

        # 用 category_id 取得分類
        tag_obj = self.tag_service.get_by_id(tag_id=self.request.matchdict['tag_id'],
                                             check=True)

        # 更新帳號
        with transaction.manager:
            tag_obj = self.tag_service.update(old_obj=tag_obj,
                                              user_id=self.account_id,
                                              **request_data)

        return {'response': {'tag': tag_obj.__json__()},
                'message': u'標籤修改成功'}

    def delete(self):
        """
        刪除標籤
        """

        # 用 tag_id 取得分類
        tag_obj = self.tag_service.get_by_id(tag_id=self.request.matchdict['tag_id'],
                                             check=True)
        # 更新 status 達到軟刪除
        with transaction.manager:
            self.tag_service.delete(old_obj=tag_obj)

        return {'message': u'刪除標籤成功'}
