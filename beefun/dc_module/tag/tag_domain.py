# coding=utf8
from __future__ import unicode_literals

from sqlalchemy import (
    Column,
    Integer,
    String,
)
from sqlalchemy.ext.hybrid import hybrid_property

from ...base.meta_module import (Base, TimestampTable, GUID, uuid4)

class Tag(Base, TimestampTable):
    """
    分類資料表
    """
    __tablename__ = 't_tag'
    tag_id = Column('f_tag_id', GUID, primary_key=True, default=uuid4, doc=u"標籤編號")
    
    name = Column('f_name', String, nullable=False, doc=u"名稱")

    _status = Column('f_status', Integer, nullable=False, default=200,
                     info={"show": 200, "hiden": 300, "delete": 400},
                     doc=u"分類狀態:(200.顯示,300.隱形,400.下架")

    _type = Column('f_type', Integer, nullable=False, default=10,
                   info={"article": 10, "product": 20, "program": 30, "other": 40},
                   doc=u"分類類型:(10.最新消息,20.產品資訊,30.工程時機,40.其他")

    remark = Column('f_remark', String(128), doc=u"預留欄位")

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @hybrid_property
    def type(self):
        if hasattr(self.__class__, '_type'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._type.info
            for k in info_dic.keys():
                if info_dic[k] == self._type:
                    return k

    @type.setter
    def type(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._type.info.get(value)
            if not v:
                raise Exception('type column input value {} is error'.format(value))
            self._type = v
        else:
            raise Exception('type column input value_type {} is error')

    @type.expression
    def type(cls):
        return cls._type

    def __repr__(self):
        return '<TagObject (tag_id={0})>'.format(self.tag_id)

    @classmethod
    def __getattributes__(cls):
        return [i[1:] if i[:1] == '_' else i for i in cls.__dict__.keys() if
                i[:1] != '_' or i == '_update_user_id' or i == '_create_user_id']

    @classmethod
    def __likeattribute__(cls, key_word):
        map_args = [i for i in cls.__dict__.keys() if key_word in i and i[:1] != '_']
        return map_args[0] if map_args else None

    def __json__(self):
        d = {
            'tag_id': str(self.tag_id),
            
            'name': self.name,
            'status': self.status,
            'type': self.type,
        }
        return d
