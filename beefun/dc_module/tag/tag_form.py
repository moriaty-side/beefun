# coding=utf8
from __future__ import unicode_literals

import formencode
from formencode import validators


class TagCreateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True
    
    name = validators.String(not_empty=True)
    status = validators.String(not_empty=True)
    type = validators.String(not_empty=True)


class TagSearchForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True
    
    name = validators.String(not_empty=True, strip=True)
    status = validators.String(not_empty=True, strip=True)
    type = validators.String(not_empty=True, strip=True)
    page = validators.Int(not_empty=True, min=1)  # 第幾頁
    limit = validators.Int(not_empty=True, min=1)  # 一頁幾筆


class TagEditForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True
    
    tag_id = validators.Int(not_empty=True)  # 分類id
    status = validators.String(not_empty=True)
    type = validators.String(not_empty=True)
    name = validators.String(not_empty=True)
