<%inherit file="aaa:templates/backend/master.mako"/>

<%block name="css">
    <!-- 圖片預覽 -->
    <link href="${ request.static_path('aaa:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css"/>
    <!-- 圖片預覽 -->

</%block>

<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span class="m-font-898b96">${ _(u'最新消息管理')}</span> <i class="fa fa-caret-right"></i>
                        <span>${ _(u'分類')}</span>
                    </h3>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="tag">
            <!--begin::Portlet-->
            <div class="languages hide"></div>
            <div class="m-portlet">
                <!--Begin::Section-->
                <!--begin::Form-->
                <button hidden type="button" class="btn btn-warning m-btn m-btn--outline-2x btn-sm mr-5px btn-search">
                    <span><i class="fa fa-search"></i><span>搜尋</span></span>
                </button>
                <form class="m-form m-form--fit m-form--label-align-right" id="search_form">
                    <!--Begin::Section-->
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-6 offset-3 mb-15px">
                                <a href="#" class="m-portlet__nav-link m-btn--pill">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" class="form-control m-input m-input--solid m-input--pill search" name="name" placeholder="Search...">
                                        <span class="m-input-icon__icon m-input-icon__icon--right">
                                            <span><i class="la la-search m--font-brand"></i></span>
                                        </span>
                                    </div>
                                </a>
                                <div class="form-group m-form__group mt-15px">
                                    <label class="col-form-label col-4">
                                    <span class="m--font-danger">*</span>
                                        ${ _(u'分類名稱')}：
                                    </label>
                                    <div class="col-8">
                                        <input type="text" class="form-control m-input" placeholder="" id="name">
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <button type="button" class="btn btn-brand m-btn m-btn--outline-2x btn-sm H-center tag_btn">
                                    <span><i class="fa fa-plus"></i><span>${ _(u'新增')}</span></span>
                                    </button>
                                </div>
                                <table class="table m-table m-table--head-bg-success">
                                    <thead>
                                    <tr>
                                        <th> ${ _('SN')}</th>
                                        <th> ${ _(u'名稱')} </th>
                                        <th> <!--操作-->&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tag_list">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>
</%block>
<%block name="script">
    <!-- 圖片預覽 -->
    <script src="${ request.static_path('aaa:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
    <script src="${ request.static_path('aaa:static/assets/other/readimage.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('aaa:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('aaa:static/assets/make_table/make_table.js')}" type="text/javascript"></script>
    <script>
        $('.se1').select2();
        var csrfToken = "${request.session.get_csrf_token()}";
        var language ='${ request.registry.settings.get('available_langs', [['zh_Hant', u'繁體中文']])[0][0] }';

        $(document).ready(function (e) {
            Table.start();
        });
        //新增分類
        $(document).on('click', '.tag_btn', function() {
            if($('#name').val()==""){
                alert('${_(u"請輸入名稱")}')
                return false
            }
             if($('#name').val().length>100){
                alert('${_(u"字數大於100")}')
                return false
            }
                   if( $('#name').val().match('[\(\)\<\>\/\?\!\+\~]')!=null){
                alert('${_(u"請勿輸入特殊字元")}')
                return false
            }
            var event_btn = $(this);
            var url = "${ request.route_url('api.tag')}";
            var category_data_form = new FormData();
            category_data_form.append('name', $('#name').val());
            if($('#name').val()!=""){
                ajax(url, 'POST', category_data_form, event_btn, function(data){
                if (event_btn) event_btn.attr("disabled", false);
                if (data['status']) {
                    $('#tag_list').empty();
                    $('#name').val("");
                    Table.start();
                } else {
                    alert('${_(u"創建失敗")}')
                }
            });
            }else{
                alert('${_(u"請輸入名稱")}')
            }
        });

        var Table = new MakeTable({
            'dom_search_form': $('form#search_form'),
            'dom_search_btn': $('i.la.la-search'),
            'dom_table_tbody': $('.tag_list'),
            'init_search_parameter': '',
            'ajax_search_url':  "${ request.route_url('api.tag') }",
            'ajax_csrf_token':csrfToken,
            'table_model': function (obj, page) {
                //開始產生頁面資料
                var $list = obj['tag_list'];
                var html_str = '';
                $.each($list, function (i, item) {
                    item.index = (i + 1 + ((page - 1) * 10));
                    var tb_row = " <tr class='obj_index'>" ;
                    tb_row += "<td>"+item.index+"</td>";
                    tb_row+="<td>";
                    tb_row +="<input type='text' value='"+item.tag_id+"' class='form-control tag_id' hidden>";
                    tb_row+="<div class='title'>"+item.name+"</div>";
                    tb_row+='<div class="into-name hide">';
                    tb_row+='<input class="edit-title form-control m-input edit_input" type="text" value="'+item.name+'">';
                    tb_row+='</div>';
                    tb_row+="</td>";
                    tb_row+='<td align="right">';
                    tb_row+='<button type="button" class="btn btn-accent m-btn m-btn--icon btn-sm m-btn--icon-only sav_cont hide"><i class="fa fa-check"></i></button>';
                    tb_row+='<button type="button" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only edit_cont"><i class="fa fa-edit"></i></button>';
                    tb_row+='</td>';
                    tb_row += "</tr>";
                    html_str += tb_row;
                });
                $('#tag_list').empty()
                $('#tag_list').append(html_str);//Table頁面資料繪製
            }
        });

        $(document).on('click', '.sav_cont', function (e) {
            if($(this).closest(".obj_index").find('.edit_input').val()==""){
                alert('${_(u"請輸入名稱")}')
                  Table.start();
                return false
            }
             if($(this).closest(".obj_index").find('.edit_input').val().length>100){
                alert('${_(u"字數大於100")}')
                  Table.start();
                return false
            }
             if( $(this).closest(".obj_index").find('.edit_input').val().match('[\(\)\<\>\/\?\!\~]')!=null){
                alert('${_(u"請勿輸入特殊字元")}')
                  Table.start();
                return false
            }
            var url = "${ request.route_url('api.tag.detail',tag_id='') }"+$(this).closest(".obj_index").find('.tag_id').val();
            var form_data = new FormData();
            form_data.append('name', $(this).closest(".obj_index").find('.edit_input').val());
            ajax(url, "POST", form_data, null, function (response) {
                if (response['status']) {
                    alert("${_('儲存成功')}");
                    $('#tag_list').empty();
                    Table.start();
                } else {
                    alert(response['message']);
                }
            });
        });
        ajax_reload = Table;

        //修改名稱事件
        $(document).on('click', '.btn.edit_cont', function() {
            $(this).parents('tr').find('.title').addClass('hide').siblings('.hide').removeClass('hide');
            $(this).addClass('hide').siblings('.hide').removeClass('hide');
        });

        $(document).on('click', '.btn.sav_cont', function() {
            $(this).addClass('hide').siblings('.hide').removeClass('hide');
            $(this).parents('tr').find('.into-name').addClass('hide').siblings('.hide').removeClass('hide');

            var name = $(this).parents('tr').find('.into-name input').val();
            $(this).parents('tr').find('.title').text(name);
        });

        $('.search').on('keydown', function (e) {
            if (e.which == 13) {
                $('#tag_list').empty();
                ajax_reload.dom_search_btn.click();
                e.preventDefault();
            }
        });

        function ajax(url, method, form_data, btn, callback){
            $.ajax({
                url: url,
                type: method,
                data: form_data,
                ##  dataType: 'JSON',
                contentType: false,
                processData: false,
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    if (btn) btn.attr("disabled", true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                    if (btn) btn.attr("disabled", false)
                },
                success: function (response) {
                    callback(response);
                },
                complete: function () {
                    if (btn) btn.attr("disabled", false);
                }
            });
        }

    </script>
</%block>