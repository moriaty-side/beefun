# coding=utf-8
from __future__ import unicode_literals

import uuid
from .tag_domain import Tag
from ...base.base_service import EMPTY_DIC, NOT_SET, BaseService
from ...lib.my_exception import MyException
from sqlalchemy import or_

class TagService(BaseService):

    TABLE = Tag

    def __init__(self, session, logger=None):
        super(TagService, self).__init__(session,logger)
        self.table_args = self.TABLE.__getattributes__()

    def create(self, **data):
        # 不可用參數
        un_available_args = []
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        create_data = {key: data[key] for key in available_args if key in data}

        # 資料創建
        try:
            return self._create(**create_data)
        except Exception as e:
            raise MyException(code=7001, message=e)

    def update(self, old_obj, **data):
        # 不可用參數
        un_available_args = ['tag_id']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=7003, message=e)

    def update_by_id(self, tag_id, **data):
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['tag_id']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 抓取對應物件
        old_obj = self.get_by_id(tag_id)
        # 選填關聯參數
        old_obj, data = self._relationship_obj_ud(old_obj, data)
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}

        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=7003, message=e)

    def get_by(self, **data):
        try:
            return self._get_by(**data)
        except Exception as e:
            raise MyException(code=7004, message=e)

    def get_by_id(self, tag_id, check=False):
        id = tag_id if isinstance(tag_id, uuid.UUID) else uuid.UUID(tag_id)
        obj = self._get_by(tag_id=id)
        if check and not obj:
            raise MyException(code=7005)
        return obj

    def delete(self, old_obj):
        try:
            return self._delete(old_obj)
        except Exception as e:
            raise MyException(code=7006, message=e)

    def delete_by_id(self, tag_id):
        obj = self.get_by_id(tag_id=tag_id, check=True)
        return self.delete(obj)

    def get_list(self, status=None, type=None, order_by=EMPTY_DIC, group_by=EMPTY_DIC, name=None,
                 language=None, offset=0, limit=None, show_count=False, **search_condition):

        # 可不用參數
        un_available_args = ['']
        # 可用參數
        available_args = [key for key in self.table_args if key not in un_available_args]
        # 資料過濾
        filter_data = {key: search_condition[key]
                       for key in available_args if key in search_condition}

        # 查詢條件製成
        query = self.session.query(self.TABLE)

        try:
            if name is not None:
                query = query.filter(self.TABLE.name.ilike('%{}%'.format(name)))

            #language
            if language is not None:
                query = query.filter(self.TABLE.language == language)

            # status
            if status is not None:
                status_value = self.TABLE._status.info.get(status, None)
                if status_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._status == status_value)

            # type
            if type is not None:
                type_value = self.TABLE._type.info.get(type, None)
                if type_value is None:
                    raise '{0} obj attribute is error '.format(self.TABLE.__name__)
                query = query.filter(self.TABLE._type == type_value)


            return self._get_list(query, order_by, group_by,
                                  offset, limit, show_count, **filter_data)

        except Exception as e:
            raise MyException(code=7004, message=e)



