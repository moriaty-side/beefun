from ...base.base_test import BaseTest, dummy_request
from .tag_domain import Tag
from .tag_views import TagJsonView as View

class TagAPITest(BaseTest):

    def setUp(self):
        super(TagAPITest, self).setUp()
        self.init_database()

    def create(self, name='test2', type=10):
        tag_obj = Tag(name=name, _type=type)
        self.session.add(tag_obj)
        tag_obj = self.session.query(Tag).filter(Tag.name == name).filter(Tag.type == type).first()
        return tag_obj

    def test_create(self):
        """
        測試 創建帳號
        """
        name = 'test'
        request = dummy_request(self.session)
        request.params = {'name': name}
        view = View(request)
        response = view.create()
        self.assertEqual(response.get('message'), '標籤創建成功')

    def test_search(self):
        """
        測試 創建帳號
        """
        request = dummy_request(self.session)
        view = View(request)
        category_resp_list = view.search()
        self.assertEqual(category_resp_list.get('message'), '標籤搜尋成功')
        self.assertEqual(category_resp_list.get('response', {}).get('total_count'), 0)
        self.assertEqual(category_resp_list.get('response', {}).get('total_page'), 0)
        self.assertEqual(len(category_resp_list.get('response', {}).get('tag_list', [])), 0)

    def test_get(self):
        """
        測試 讀取帳號
        """
        tag = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'tag_id': str(tag.tag_id)}
        view = View(request)
        response = view.get()
        self.assertEqual(response.get('message'), '標籤讀取成功')

    def test_update(self):
        """
        測試 更新帳號
        """
        tag = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'tag_id': str(tag.tag_id)}
        name = '123'
        request.params = {'name': name}
        view = View(request)
        response = view.edit()
        self.assertEqual(response.get('message'), '標籤修改成功')
        self.assertEqual(response.get('response', {}).get('tag', {}).get('name'), name)

    def test_delete(self):
        """
        測試 更新帳號
        """
        tag = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'tag_id': str(tag.tag_id)}
        view = View(request)
        response = view.delete()
        self.assertEqual(response.get('message'), '刪除標籤成功')

