# coding=utf8
from __future__ import unicode_literals

from sqlalchemy import (
    Column,
    Integer,
    String,
    ARRAY,
    ForeignKey,
    DateTime
)
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
from ...base.meta_module import (Base, TimestampTable, GUID, uuid4)


class StockDateTimePrice(Base, TimestampTable):
    """
    分類資料表
    """
    __tablename__ = 't_stock_datetime_price'
    d_stock_price_id = Column('f_d_stock_price_id', GUID, primary_key=True, default=uuid4, doc=u"分類編號")

    stock_id = Column('f_stock_id', ForeignKey('t_stock.f_stock_id'), nullable=False, doc=u"幣別")

    datetime_price = Column('f_datetime_price', DateTime, nullable=False, doc=u"當下匯率")

    n_sequence = Column('f_n_sequence', Integer, default=0, doc=u"匯率序列")

    amplitude = Column('f_amplitude', String(512), doc=u'幅度')

    _status = Column('f_status', Integer, nullable=False, default=13,
                     info={"rise": 10, "decline": 11, "flat": 12, "error": 13},
                     doc=u"狀態:10.上升,11.下降,12:持平,13:無資料")

    stock = relationship("Stock")

    def __repr__(self):
        return '<Object (d_stock_price_id={0})>'.format(self.d_stock_price_id)

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @classmethod
    def __getattributes__(cls):
        return [i[1:] if i[:1] == '_' else i for i in cls.__dict__.keys() if
                i[:1] != '_' or i == '_update_user_id' or i == '_create_user_id']

    @classmethod
    def __likeattribute__(cls, key_word):
        map_args = [i for i in cls.__dict__.keys() if key_word in i and i[:1] != '_']
        return map_args[0] if map_args else None

    def __json__(self):
        d = {
            'd_stock_price_id': str(self.d_stock_price_id),
            'datetime_price': str(self.datetime_price),
            'status': self.status,
        }

        return d
