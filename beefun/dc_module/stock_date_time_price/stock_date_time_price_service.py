# coding=utf-8
from __future__ import unicode_literals

import uuid
from .stock_date_time_price_domain import StockDateTimePrice
from ...lib.my_exception import MyException
from ...base.base_service import EMPTY_DIC, BaseService, now_func


class StockDateTimePriceService(BaseService):
    TABLE = StockDateTimePrice

    def __init__(self, session, logger=None):
        super(StockDateTimePriceService, self).__init__(session, logger=logger)

    def create(self, user_id=None, **data):
        """
        必填資料
            currency: 帳號
        其他資料
            **data: 其他資料
        """
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['d_stock_price_id']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 資料過濾
        create_data = {key: data[key] for key in available_args if key in data}

        create_data['d_stock_price_id'] = uuid.uuid4()
        # 當有使用者id時
        if user_id:
            create_data['create_by'] = uuid.UUID(user_id)
            create_data['update_by'] = uuid.UUID(user_id)
        # 資料創建
        try:
            return self._create(**create_data)
        except Exception as e:
            raise MyException(code=1001, message=e)

    def update(self, old_obj, user_id=None, **data):
        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 不可用參數
        un_available_args = ['d_stock_price_id']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]
        # 資料過濾
        update_data = {key: data[key] for key in available_args if key in data}
        # 當有使用者id時
        if user_id:
            update_data['update_by'] = uuid.UUID(user_id)

        update_data['updated'] = now_func()

        # 資料更新
        try:
            return self._update(old_obj, **update_data)
        except Exception as e:
            raise MyException(code=1003, message=e)

    def get_by_id(self, obj_id, check=False):
        """
        讀取帳號編號
        :param obj_id:
        :param check:
        :return:
        """
        id = obj_id if isinstance(obj_id, uuid.UUID) else uuid.UUID(obj_id)
        account = self._get_by(d_stock_price_id=id)
        if check and not account:
            raise MyException(code=1005)

        return account


    def get_by(self, **data):
        try:
            return self._get_by(**data)
        except Exception as e:
            raise MyException(code=1004, message=e)

    def get_list(self, order_by=EMPTY_DIC, group_by=EMPTY_DIC,
                 offset=0, limit=None, show_count=False, name=None, account=None,
                 status=None, check_mechanism='filtering', **search_condition):

        # 物件元素
        class_args = self.TABLE.__getattributes__()
        # 可不用參數
        un_available_args = ['password']
        # 可用參數
        available_args = [key for key in class_args if key not in un_available_args]

        # 查詢條件製成
        query = self.session.query(self.TABLE)

        # if account is not None:
        #     like_str = '%{0}%'.format(account)
        #     query = query.filter(self.TABLE.account.ilike(like_str))
        #
        # if name is not None:
        #     name_str = '%{0}%'.format(name)
        #     query = query.filter(self.TABLE.name.ilike(name_str))

        if status is not None:
            status_value = self.TABLE._status.info.get(status, None)
            if status_value is None:
                raise MyException(code=901, message='{0}-{1}'.format(self.TABLE.__name__, 'Status'))
            query = query.filter(self.TABLE._status == status_value)
        else:
            query = query.filter(self.TABLE.status != 400)

        # 資料篩選器
        if check_mechanism == 'filtering':
            # 資料過濾機制
            search_condition = {key: search_condition[key] for key in available_args if key in search_condition}
        elif check_mechanism == 'defence':
            # 資料防護機制
            for key in search_condition:
                if key in un_available_args:
                    raise Exception('input query args is Disable in {0} '.format(self.TABLE.__name__))
                elif key not in available_args:
                    raise Exception('input query args is error in {0} '.format(self.TABLE.__name__))

        try:

            return self._get_list(query, order_by, group_by,
                                  offset, limit, show_count, **search_condition)

        except Exception as e:
            raise MyException(code=1004, message=e)
