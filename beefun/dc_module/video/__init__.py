from __future__ import unicode_literals

from . import video_views as views
from ...base.context import AdminContext
from ...lib.api import return_format
from ...lib.page import return_page_format


def includeme(config):
    #Page
    config.add_route('page.video', '/page/video/{language}', factory=AdminContext)
    config.add_view(
        views.ViedoPageView, attr='single_manage',
        route_name='page.video',
        renderer='templates/video.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )

    # API
    # video create
    config.add_route('api.video', '/api/video', factory=AdminContext)
    config.add_view(
        views.VideoJsonView, attr='create',
        route_name='api.video',
        renderer='json',
        request_method='POST',
        decorator=return_format,
        
        permission='login',
        
    )
    config.add_view(
        views.VideoJsonView, attr='search',
        route_name='api.video',
        request_method='GET',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_route('api.video.detail', '/api/video/{video_id}', factory=AdminContext)
    config.add_view(
        views.VideoJsonView, attr='get',
        route_name='api.video.detail',
        request_method='GET',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_view(
        views.VideoJsonView, attr='edit',
        route_name='api.video.detail',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_view(
        views.VideoJsonView, attr='delete',
        route_name='api.video.detail',
        request_method='DELETE',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )
