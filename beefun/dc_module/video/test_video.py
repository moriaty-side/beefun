import transaction
from ...base.base_test import BaseTest, dummy_request
from .video_domain import Viedo
from .video_views import VideoJsonView as View

from ..image.image_service import ImageService

class TagAPITest(BaseTest):

    def setUp(self):
        super(TagAPITest, self).setUp()
        self.init_database()
        self.image_service = ImageService(repository_path='', session=self.session)

    def create(self, link_play="https://google.com", link_more="https://yahoo.com"):
        image = self.get_or_create_image()
        video_obj = Viedo(image_id=image.image_id, link_play=link_play, link_more=link_more)
        self.session.add(video_obj)
        video_obj = self.session.query(Viedo).filter(Viedo.link_play==link_play).first()
        return video_obj
    
    def get_or_create_image(self):
        image = self.image_service.get_by(name="測試")
        if not image:
            with transaction.manager:
                self.image_service.create(name="測試", filename='test.jpg')
            image = self.image_service.get_by(name="測試")
        return image
        

    def test_create(self):
        """
        測試 創建帳號
        """
        link_play = 'https://youtube.com'
        image = self.get_or_create_image()
        request = dummy_request(self.session)
        request.params = {'link_play': link_play,
                          'image_id': str(image.image_id)}
        view = View(request)
        response = view.create()
        self.assertEqual(response.get('message'), '影片創建成功')

    def test_search(self):
        """
        測試 創建帳號
        """
        request = dummy_request(self.session)
        view = View(request)
        category_resp_list = view.search()
        self.assertEqual(category_resp_list.get('message'), '影片搜尋成功')
        self.assertEqual(category_resp_list.get('response', {}).get('total_count'), 0)
        self.assertEqual(category_resp_list.get('response', {}).get('total_page'), 0)
        self.assertEqual(len(category_resp_list.get('response', {}).get('tag_list', [])), 0)

    def test_get(self):
        """
        測試 讀取帳號
        """
        video = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'video_id': str(video.video_id)}
        view = View(request)
        response = view.get()
        self.assertEqual(response.get('message'), '影片讀取成功')

    def test_update(self):
        """
        測試 更新帳號
        """
        video = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'video_id': str(video.video_id)}
        link_play = 'https://facebook.com'
        request.params = {'link_play': link_play}
        view = View(request)
        response = view.edit()
        self.assertEqual(response.get('message'), '影片修改成功')
        self.assertEqual(response.get('response', {}).get('video', {}).get('link_play'), link_play)

    def test_delete(self):
        """
        測試 更新帳號
        """
        video = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'video_id': str(video.video_id)}
        view = View(request)
        response = view.delete()
        self.assertEqual(response.get('message'), '刪除影片成功')

