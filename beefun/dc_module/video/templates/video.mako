<%inherit file="beefun:templates/backend/master.mako"/>

<%block name="css">
    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css" />
    <!-- 圖片預覽 -->

</%block>

<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span class="m-font-898b96">${ _(u'首頁管理')}</span> <i class="fa fa-caret-right"></i>
                        <span>${ _(u'影片')}</span>
                    </h3>
                </div>
                <div>
                    <button type="button" class="btn btn-success m-btn m-btn--outline-2x btn-sm" id="video_save">
                    <span>
                                        <i class="fa fa-check"></i>
                                        <span>
                                            ${ _(u'存檔')}
                                        </span>
                    </span>
                    </button>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="video" data-parent="home_set">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__body">
                    <ul class="nav nav-tabs" role="tablist">
                        %for index, language_list in enumerate(request.registry.settings.get('available_langs',[])):
                            %if set == language_list[0]:
                                <li class="nav-item">
                                    <a class="nav-link active" id="${ language_list[0] }" data-toggle="tab" href="#m_tabs_1_${index}">${ language_list[1] }</a>
                                </li>
                            %else:
                                <li class="nav-item">
                                    <a class="nav-link" id="${ language_list[0] }" data-toggle="tab" href="#m_tabs_1_${index}">${ language_list[1] }</a>
                                </li>
                            %endif
                        %endfor
                    </ul>
                    <div class="tab-content">
                        %for index, language_list in enumerate(request.registry.settings.get('available_langs',[])):
                            %if index == 0:
                                <div class="tab-pane active" id="m_tabs_1_${index}" role="tabpanel">
                                    <!--load work_list-1.html-->
                                </div>
                            %else:
                                <div class="tab-pane" id="m_tabs_1_${index}" role="tabpanel">
                                    <!--load work_list-3.html-->
                                </div>
                            %endif
                        %endfor
                    </div>
                </div>
                <!--Begin::Section-->
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right create_form">
                    <!--Begin::Section-->
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'圖片')}
                                    </label>
                                    <div class="col-8">
                                        <div class="fileinput fileinput-${'exists' if video_obj.get('image_id') else 'new'}" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 231px; height: 175px;">
                                                <img src="http://www.placehold.it/231x175/EFEFEF/AAAAAA&amp;text=330x250" alt=""/>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 231px; max-height: 175px;">
                                                %if video_obj.get('image_id'):
                                                    <img class="logo_image" src="${ request.static_path('beefun:static/uploads')}${ video_obj.get('image',{}).get('url') }" alt="" image_id="${video_obj.get('image',{}).get('image_id')}" style="max-height: 178px;"/>
                                                %else:
                                                    <img class="logo_image" src="" alt="">
                                                %endif
                                            </div>
                                            <div>
                                                    <span class="btn default btn-file">
                                                            <span class="fileinput-new"> ${ _(u'選擇圖片')} </span>
                                                    <span class="fileinput-exists"> ${ _(u'更換')}</span>
                                                    <input type="file" name="video" onchange="readImage(this,330,250);">
                                                    </span>
                                                <a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"> ${ _(u'移除')} </a>
                                            </div>
                                        </div>
                                        <div class="help-block">${ _(u'尺寸')}：330*250</div>
                                    </div>
                                </div>
                                <input class="form-control hide" placeholder="" value="${ video_obj.get('video_id',"") }" name="video_id" id="video_id">
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                            <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'播放影片連結')}
                                    </label>
                                    <div class="col-8">
                                        <input class="form-control" placeholder="請包含http://https://" value="${ video_obj.get('link_play',"") }" name="link_play">
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                            <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'更多影片連結')}
                                    </label>
                                    <div class="col-8">
                                        <input class="form-control" placeholder="請包含http://https://" value="${ video_obj.get('link_more',"") }" name="link_more">
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                        ${ _(u'關閉時間')}
                                    </label>
                                    <div class="col-8">
                                        <div class="m-form__group--inline">
                                            <div class="m-form__control inline-block">
                                                <input class="form-control" placeholder="" value="${ video_obj.get('closing',"") }" name="closing">
                                            </div>
                                            <div class="m-form__control inline-block">
                                                ${ _(u'分')}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>
</%block>



<%block name="script">


    <!-- 圖片預覽 -->
    <script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/readimage.js') }" type="text/javascript"></script>
    <!-- 圖片預覽 -->
    <script src="${ request.static_path('beefun:static/assets/other/custom_validation.js')}" type="text/javascript"></script>
    <script>
        $('.se1').select2();
        var csrfToken = "${request.session.get_csrf_token()}";

        var language='${ request.registry.settings.get('available_langs', [['zh_Hant', u'繁體中文']])[0][0] }';

            ##新增公司表單驗證
$('.create_form').validate({
            ignore: "",
            errorElement: 'div', //default input error message container
            errorClass: 'form-control-feedback', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                link_play: {
                    url: true,
                    required: true,
                    byteRangeLength: 512
                },
                link_more: {
                    url: true,
                    required: true,
                    byteRangeLength: 512
                },
                closing: {
                    number: true,
                    max: 999
                },


            },
            messages: {
                link_play: {
                    required: "${ _(u'影片連結為必填值')}",
                    url: "${ _(u'影片連結需為正確的網址')}",
                    byteRangeLength: "${ _(u'影片連結最多輸入字數為512')}"
                },
                link_more: {
                    required: "${ _(u'影片連結為必填值')}",
                    url: "${ _(u'影片連結需為正確的網址')}",
                    byteRangeLength: "${ _(u'影片連結最多輸入字數為512')}"
                },
                closing: {
                    number: "${ _(u'時間需為數字格式')}",
                    max: "${ _(u'最多為999分鐘')}"
                },


            },

            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-danger'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-danger');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element);
            }
        });

        $(document).on('click', '.nav-link', function (e) {
            window.open('${ request.route_url('page.video', language='') }'+$(this).attr('id'),'_self')
        });





        // 儲存圖片
        $(document).on('click', '#video_save', function() {
            const $valid = $('.create_form').valid();
            if (!$valid) {
                return false;
            }
            var save_btn = $(this)
            upload_image(function(status, image_data) {
                if (status) {
                    if($('#video_id').val()==""){
                        var url = "${ request.route_url('api.video') }";
                    }else{
                        var url = "${ request.route_url('api.video.detail',video_id="") }"+$('#video_id').val();
                    }






                    var form_data = new FormData();

                    form_data.append('image_id', image_data["image_id"]);

                        form_data.append('language',$('.nav-link.active').attr('id'));

                    form_data.append('link_play', $('input[name="link_play"]').val());
                    form_data.append('link_more', $('input[name="link_more"]').val());
                    form_data.append('closing', $('input[name="closing"]').val());

                    ajax(url, "POST", form_data, save_btn, function(response) {
                        if (response['status']) {

                            alert("${_('儲存成功')}");
                            location.reload();
                        } else {
                            alert(response['message']);
                        }
                    });
                }
            });
        });

        function upload_image(callback) {
            // 上傳圖片
            if ($('input[name="video"]')[0].files == null || $('input[name="video"]')[0].files.length == 0) {
                var image_id = $('.logo_image').attr('image_id');
                if (image_id) {
                    callback(true, { 'image_id': image_id });
                } else {
                    alert('圖片上傳錯誤');
                    callback(false, null);
                }
            } else {
                var image_form_data = new FormData();
                image_form_data.append('image_file', $('input[name="video"]')[0].files[0]);
                var upload_image_url = "${ request.route_url('api.image') }";
                ajax(upload_image_url, "POST", image_form_data, $('.create_btn'), function(response) {
                    if (response['status']) {

                        callback(true, response['response']['image_list'][0])
                    } else {
                        alert(response['message']);
                    }
                });
            }
        }

        function ajax(url, method, form_data, btn, callback) {

            $.ajax({
                url: url,
                type: method,
                data: form_data,
                contentType: false,
                processData: false,

                headers: { 'X-CSRF-Token': csrfToken },
                beforeSend: function() {
                    $(btn).attr('disabled', true);
                },
                error: function(xhr) {
                    $(btn).attr('disabled', true);
                    alert('Ajax request 發生錯誤');
                },
                success: function(response) {
                    callback(response);
                },
                complete: function() {

                    $(btn).attr('disabled', false);
                }
            });
        }
    </script>


</%block>


