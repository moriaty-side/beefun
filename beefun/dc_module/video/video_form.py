# coding=utf8
from __future__ import unicode_literals
import uuid

import formencode
from formencode import validators

class VideoCreateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True

    image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    language = validators.String(not_empty=True)
    link_play = validators.String(not_empty=True)
    link_more = validators.String(not_empty=True)
    closing = validators.Int(not_empty=False)
    status = validators.String(not_empty=True)


class VideoSearchForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True

    language = validators.String(not_empty=True)
    link_play = validators.String(not_empty=True)
    link_more = validators.String(not_empty=True)
    status = validators.String(not_empty=True)

    page = validators.Int(not_empty=True, min=1)  # 第幾頁
    limit = validators.Int(not_empty=True, min=1)  # 一頁幾筆


class VideoEditForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True

    image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    language = validators.String(not_empty=True)
    link_play = validators.String(not_empty=True)
    link_more = validators.String(not_empty=True)
    closing = validators.Int(not_empty=False)
    status = validators.String(not_empty=True)
