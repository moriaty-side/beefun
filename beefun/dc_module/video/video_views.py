# coding=utf8
from __future__ import unicode_literals
import transaction
from . import video_form as form
from ...base.base_view import BaseView
from .video_service import VideoService

class ViedoPageView(BaseView):
    def __init__(self, request):
        super(ViedoPageView, self).__init__(request)
        self.video_service = VideoService(self.session)

    def single_manage(self):
        _ = self.localizer
        request_data = {}
        request_data['language'] = self.request.matchdict['language']
        # 搜尋帳號
        video_list, total_count = self.video_service.get_list(show_count=True, **request_data)
        if total_count != 0:
            video_obj = video_list[0].__json__(show_image=True)
        else:
            video_obj = {}

        return {'video_obj': video_obj,
                'set': request_data['language']
                }


class VideoJsonView(BaseView):
    def __init__(self, request):
        super(VideoJsonView, self).__init__(request)
        self.video_service = VideoService(self.session)

    def create(self):
        """
        創建影片
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.VideoCreateForm().to_python(self.request_params)

        with transaction.manager:
            video_obj = self.video_service.create(**request_data)

        # video_obj = self.video_service.get_by(**request_data)

        return {'response': {'video': video_obj.__json__()},
                'message': _(u'影片創建成功')}

    def search(self):
        """
        搜尋影片
        """

        # 檢查輸入參數
        request_data = form.VideoSearchForm().to_python(self.request_params)

        # 分頁機制
        current_page = request_data.get('page', 1)
        limit = request_data.get('limit', 10)
        request_data['offset'] = (current_page - 1) * limit
        request_data['limit'] = limit
        request_data['order_by'] = [('created', -1)]

        # 搜尋帳號
        video_list, total_count = self.video_service.get_list(show_count=True, **request_data)

        return {'response': {'video_list': [a.__json__() for a in video_list],
                             'total_count': total_count,
                             'total_page': (int(total_count / limit) + 1 if total_count % limit else 0)
                             },
                'message': u'影片搜尋成功'}

    def get(self):
        """
        讀取影片
        """

        # 用 category_id 取得分類
        video_obj = self.video_service.get_by_id(video_id=self.request.matchdict['video_id'],
                                                 check=True)

        return {'response': {'video': video_obj.__json__(show_image=True)},
                'message': '影片讀取成功'}

    def edit(self):
        """
        編輯影片
        """

        # 檢查輸入參數
        request_data = form.VideoEditForm().to_python(self.request_params)

        # 用 category_id 取得分類
        video_obj = self.video_service.get_by_id(video_id=self.request.matchdict['video_id'],
                                                 check=True)

        # 更新帳號
        with transaction.manager:
            video_obj = self.video_service.update(old_obj=video_obj,
                                                  user_id=self.account_id,
                                                  **request_data)

        return {'response': {'video': video_obj.__json__()},
                'message': u'影片修改成功'}

    def delete(self):
        """
        刪除影片
        """

        # 用 video_id 取得分類
        video_obj = self.video_service.get_by_id(video_id=self.request.matchdict['video_id'],
                                                 check=True)
        # 刪除
        with transaction.manager:
            self.video_service.delete(old_obj=video_obj)

        return {'message': u'刪除影片成功'}
