# coding=utf8
import uuid
from sqlalchemy import (Column, Integer, String, ForeignKey)
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from ...base.meta_module import (Base, TimestampTable, GUID, uuid4)
DeclarativeBase = declarative_base()

class Viedo(Base, TimestampTable):
    """
    影片 資料表單
    """
    __tablename__ = 't_video'
    video_id = Column('f_video_id', GUID, primary_key=True, default=uuid4, doc=u"影片ID")

    language = Column('f_language', String(12), index=True, nullable=False, default="zh_Hant", doc=u"多國語系語系")

    image_id = Column('f_image_id', ForeignKey('t_images.f_image_id'), nullable=False, doc=u"縮圖")

    link_play = Column('f_link_play', String(256), nullable=False, default='', doc=u'播放影片連結')

    link_more = Column('f_link_more', String(256), nullable=False, default='', doc=u'更多影片連結')

    closing = Column('f_closing', Integer, nullable=False, default=0, doc=u'關閉時間')

    _type = Column('f_type', Integer, nullable=False, default=10,
                   info={"news": 10},
                   doc=u"類別:(10.最新消息")

    _status = Column('f_status', Integer, nullable=False, default=10,
                     info={"show": 10, "hiden": 20, "delete": 40},
                     doc=u"分類狀態:(10.顯示,20.隱形,40.下架")

    image = relationship("Images")

    def __repr__(self):
        return '<ViedoObject (video_id={0})>'.format(self.video_id)

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @hybrid_property
    def type(self):
        if hasattr(self.__class__, '_type'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._type.info
            for k in info_dic.keys():
                if info_dic[k] == self._type:
                    return k

    @type.setter
    def type(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._type.info.get(value)
            if not v:
                raise Exception('type column input value {} is error'.format(value))
            self._type = v
        else:
            raise Exception('type column input value_type {} is error')

    @type.expression
    def type(cls):
        return cls._type

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    def __json__(self, show_image=False):
        d = {'video_id': str(self.video_id),
             'language': self.language,
             'image_id': str(self.image_id),
             'link_play': self.link_play,
             'link_more': self.link_more,
             'closing': self.closing,
             'status': self.status}

        if show_image:
            d['image'] = self.image.__json__() if self.image else {}
        return d

