<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css"/>
    <!-- 圖片預覽 -->

    <!-- DatetimePick -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.css" />
    <!-- DatetimePick -->

    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-tagsinput/bootstrap-tagsinput.css')}" rel="stylesheet" type="text/css" />
    <link href="${ request.static_path('beefun:static/assets/other/jquery.multiple.select/multiple-select.css')}" rel="stylesheet" type="text/css" />

    <!-- summernote -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote.css" />
    <!-- summernote -->
</head>
<body>

    <!--portlet-body-->

    <div class="row">
        <!-- BEGIN: Subheader -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span class="m-font-898b96">文章管理</span> <i class="fa fa-caret-right"></i>
                        <span>修改</span>
                    </h3>
                </div>
                <div class="col-md-offset-8">
                    <button type="button" class="btn btn-metal m-btn m-btn--outline-2x btn-sm mr-5px" onclick="javascript:window.open('${ request.route_url('page.news.list') }','_self')">
                                    <span><i class="fa fa-close"></i><span>取消</span></span>
                    </button>
                    <button type="button" class="btn btn-success m-btn m-btn--outline-2x btn-sm" id="create_btn">
                                    <span><i class="fa fa-check"></i><span>確認修改</span></span>
                    </button>
                </div>

            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="row">
            <!--begin::Portlet-->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 20px 20px">
                <!--begin::Form-->
                <form id="news_update_form">
                    <!--Begin::Section-->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group has-danger row">
                                <label class="col-form-label col-md-4 col-md-2">
                                    狀態
                                </label>
                                <div class="col-md-3">
                                    <select class="form-control"  name="status">
                                    %if news_obj['status'] == 'show':
                                        <option value="show" selected>上架</option>
                                        <option value="hiden">下架</option>
                                    %else:
                                        <option value="show">上架</option>
                                        <option value="hiden" selected>下架</option>
                                    %endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group has-danger row">
                                <label class="col-form-label col-md-4 col-md-2">
                                    分類
                                </label>
                                <div class="col-md-3">
                                    <select class="form-control"  name="category_id">
                                        <option value=""></option>
                                        %for category in category_list:
                                            %if news_obj['category_id'] == category['category_id']:
                                                <option value="${category['category_id']}" selected>${category['name']}</option>
                                            %else:
                                                <option value="${category['category_id']}">${category['name']}</option>
                                            %endif

                                        %endfor
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-4 col-md-2">
                                    <span class="m--font-danger">*</span>Title
                                </label>
                                <div class="col-md-4">
                                    <input type='text' class="form-control" name="title" value="${news_obj.get('title', '')}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-4 col-md-2">
                                    排序
                                </label>
                                <div class="col-md-4">
                                    <input type='number' class="form-control" name="sequence" value="${news_obj.get('sequence', '')}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-4 col-md-2">
                                    簡介
                                </label>
                                <div class="col-md-4">
                                    <input type='text' class="form-control" name="intro" value="${news_obj.get('intro', '')}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-4 col-md-2">
                                    內容
                                </label>
                                <div class="col-md-4">
                                    <div id="summernote">${news_obj.get('content', '')}</div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">*</span>圖片
                                </label>
                                <div class="col-md-8">
                                    <div class="fileinput fileinput-exists" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 210px; height: 100px;">
                                            <img src="http://www.placehold.it/210x100/EFEFEF/AAAAAA&amp;text=210x100" alt=""/>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 210px; max-height: 100px;">
                                            % if news_obj.get('image'):
                                                <img src="${ request.static_path('beefun:static/uploads')+news_obj.get('image', {}).get('url')}" alt="" image_id="${news_obj.get('image', {}).get('image_id')}"/>
                                            % else:
                                                <img src="" alt="">
                                            % endif
                                        </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> 選擇圖片 </span>
                                                <span class="fileinput-exists"> 更換</span>
                                                <input type="file" name="image" onchange="readImage(this,210,100);">
                                            </span>
                                            <a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"> 移除 </a>
                                        </div>
                                    </div>
                                    <div class="help-block">尺寸：378x80</div>
                                </div>
                            </div>
                            <div class="form-group has-danger row">
                                <label class="col-form-label col-4 col-md-2">
                                    日期時間
                                </label>
                                <div class="col-md-5 input-group date datetimepicker1">
                                    <input type='text' class="form-control" name="date" value="${news_obj.get('date', '')}"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>
    <!--portlet-body end-->

</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js"></script>

<script src="${ request.static_path('beefun:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
<!-- 圖片預覽 -->
<script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
<script src="${ request.static_path('beefun:static/assets/other/readimage.js') }" type="text/javascript"></script>
<!-- 圖片預覽 -->

<!-- DatetimePick -->
## https://eonasdan.github.io/bootstrap-datetimepicker/
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<!-- DatetimePick -->

<!-- summernote -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote.js"></script>
<!-- summernote -->

<script>

    var csrfToken = "${request.session.get_csrf_token()}";


    $(function () {
        ##  $('.datetimepicker1').datetimepicker();
        $('.datetimepicker1').datetimepicker({
            format: "YYYY-MM-DDThh:mm:ss",
            defaultDate:new Date()
        });
    });

    //網址選擇
    $(document).ready(function(){
        // Summernote 初始化
        $('#summernote').summernote();

        // 驅動 banner_create_form submit
        $("#create_btn").click(function(){
            $('#news_update_form').submit();
        })
    });

    $('#news_update_form').submit(function(e) { e.preventDefault(); }).validate({
        ignore: ":hidden",
        errorElement: 'div', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        rules: {
            title: {
                required: true,
            },
            category_id: {
                required: true,
            },
            focus: {
                required: true,
            },
            sequence: {
                required: true,
            },
            date: {
                required: true,
            },
            intro: {
                required: true,
            }
        },
        messages: {
            title: {
                required: "請輸入標題"
            },
            category_id: {
                required: "請輸入類型"
            },
            focus: {
                required: "請輸入廣告"
            },
            sequence: {
                required: "請輸入連結"
            },
            date: {
                required: "請輸入日期"
            },
            intro: {
                required: "請輸入簡介"
            }

        },
        highlight: function(element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function(error, element) {
            ##  error.insertAfter(element);
        },
        submitHandler: function(form) {
            var form_json = {};
            $(form).serializeArray().map(function(v, i) {
                form_json[v['name']] = v['value'];
            });
            console.log("form_json:", form_json);

            form_json['content'] = $('#summernote').summernote('code');

            var form_data = new FormData();
            for (var k in form_json) {
                form_data.append(k, form_json[k]);
            }

            upload_image(function(status, image_data){
                // 上傳圖片是否正確
                if (status){
                    form_data.append('image_id', image_data['image_id']);
                    var url = "${ request.route_url('api.news.detail', news_id=news_obj.get('news_id')) }";
                    var method = "POST";
                    ajax(url, method, form_data, $('.create_btn'), function(response) {
                        if (response['status']) {
                            console.log(response);
                            location.href = "${ request.route_url('page.news.list')}"
                        } else {
                            alert(response['message']);
                        }
                    });
                }
            });
        }
    });
    function upload_image(callback){
        // 上傳圖片
        if ($('#news_update_form').find("input[name='image']")[0].files[0] == undefined){
            var image_id = $($('#news_update_form').find("img")[1]).attr('image_id');
            if(image_id){
                callback(true, {'image_id': image_id});
            }else{
                alert('圖片上傳錯誤');
                callback(false, null);
            }

        }else{
            var image_form_data = new FormData();
            image_form_data.append('image_file', $('#news_update_form').find("input[name='image']")[0].files[0])
            var upload_image_url = "${ request.route_url('api.image') }";
            ajax(upload_image_url, "POST", image_form_data, $('.create_btn'), function(response){
                  if (response['status']) {
                      console.log(response);
                      callback(true, response['response']['image_list'][0])
                  } else {
                      alert(response['message']);
                  }
            });
        }
    }

    function ajax(url, method, form_data, btn, callback) {
        $.ajax({
            url: url,
            type: method,
            data: form_data,
            contentType: false,
            processData: false,
            headers: { 'X-CSRF-Token': csrfToken },
            beforeSend: function() {
                $(btn).attr('disabled', true);
            },
            error: function(xhr) {
                $(btn).attr('disabled', true);
                alert('Ajax request 發生錯誤');
            },
            success: function(response) {
                callback(response);
            },
            complete: function() {
                $(btn).attr('disabled', false);
            }
        });
    }


</script>
</html>