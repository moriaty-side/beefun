<%inherit file="beefun:templates/backend/master.mako"/>

<%block name="css">
    <!-- BEGIN PAGE STYLES -->
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/assets/cropper/cropper.min.css') }">
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/assets/cropper/cropper_plugin.css') }">
    <!-- END PAGE STYLES -->
</%block>

<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span class="m-font-898b96">${ _(u'最新消息管理')}</span> <i class="fa fa-caret-right"></i>
                        <span class="m-font-898b96">${ _(u'消息列表')}</span> <i class="fa fa-caret-right"></i> <span>${ _(u'新增')}</span>
                    </h3>
                </div>
                <div>
                    <button type="button" class="btn btn-metal m-btn m-btn--outline-2x btn-sm mr-5px" onclick="javascript:window.open('${ request.route_url('page.news.list') }','_self')">
                    <span>
                                        <i class="fa fa-close"></i>
                                        <span>
                                            ${ _(u'取消')}
                                        </span>
                    </span>
                    </button>
                    <button type="button" class="btn btn-success m-btn m-btn--outline-2x btn-sm" id="create_btn">
                    <span>
                                        <i class="fa fa-check"></i>
                                        <span>
                                            ${ _(u'新增存檔')}
                                        </span>
                    </span>
                    </button>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="news_list" data-parent="news">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right create_form" id="news_create">
                    <!--Begin::Section-->
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                        ${ _(u'日期')}
                                    </label>
                                    <div class="col-8 col-md-4">
                                        <input type="text" class="form-control" id="m_datepicker_1" value="" name="date" data-date-format="yyyy/mm/dd" />
                                    </div>
                                </div>
                                <div class="form-group m-form__group has-danger">
                                    <label class="col-form-label col-4 col-md-2">
                                        ${ _(u'焦點')}
                                    </label>
                                    <div class="col-8">
                                        <label class="m-radio">
                                            <input type="radio" name="focus" class="focus" id="class1" value="show" checked="checked">
                                            ${ _(u'顯示')}
                                            <span></span>
                                        </label>
                                        <label class="m-radio">
                                            <input type="radio" name="focus" class="focus" id="class2" value="hide">
                                            ${ _(u'不顯示')}
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'分類')}
                                    </label>
                                    <div class="col-8 col-md-4">
                                        <select class="form-control se1" id="category" name="category_id">
                                            ##                                             <option value="S1">分類Ａ</option>
##                                             <option value="S2">分類Ｂ</option>
                                        </select>
                                        <div id="title-error" class="my_error_category hide" style="color: red; font-size:0.85rem;">${ _(u'標題為必填值')}</div>
                                    </div>

                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'標題')}
                                    </label>
                                    <div class="col-8">
                                        <input class="form-control title" placeholder="" id="title" name="title">
                                        ##                                         <div id="title-error" class="my_error hide" style="color: red">標題為必填值</div>
                                    </div>

                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'列表圖')}
                                    </label>
                                    <div class="col-8">
                                        <div class="cropper_upload_btn obj_here" data-fk="" data-cut="false"></div>
                                        <div class="help-block">${ _(u'尺寸')}：330x235</div>
                                        <div id="title-error" class=" my_error_image hide" style="color: red; font-size:0.85rem;">${ _(u'圖片為必填值')}</div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                        ${ _(u'引言')}
                                    </label>
                                    <div class="col-8">
                                        <textarea class="form-control" rows="5" style="resize:none;" placeholder="" id="intro" name="intro"></textarea>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                        <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'內容')}
                                    </label>
                                    <div class="col-8">
                                        <div class="summernote" name="content"></div>
                                        <div id="title-error" class=" my_error_content hide" style="color: red; font-size:0.85rem;">${ _(u'內容為必填值')}</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>

</%block>



<%block name="script">
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="${ request.static_path('beefun:static/assets/jquery.serialize-object.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/cropper/cropper.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/cropper/cropper_plugin.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/custom_validation.js')}" type="text/javascript"></script>
    <script>
        var csrfToken = "${request.session.get_csrf_token()}";
        $('.se1').select2();

        

        function append_obj_list(obj) {
            //預設清空類表內部
            $('#category').empty();
            //設定後端回傳格式
            var list = obj['response']['category_list'];

            //設定html內容
            var html = '';
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].category_id +"'>"+list[i].name+"</option>";
            }
            $('#category').append(html)
        }

        $(document).ready(function (e) {
            
            // 裁切圖片功能
            var upload_callback = function(return_data, element) {
                var new_image_link = '${ request.static_path('beefun:static/uploads')}'+return_data["url"];
                $('.cropper_upload_btn').find('img').attr('src', new_image_link);
                $('.cropper_upload_btn').find('img').attr('image_id', return_data['image_id']);
                $('.cropper_upload_btn').parent().parent().removeClass('has-error')

                $('.cropper_upload_btn').find('span.help-block').remove();
                $(element).modal('hide');
            }
            $('.cropper_upload_btn').cropper_plugin(options = {
                url: "${request.route_url('api.image')}",
                image_path: 'https://www.placehold.it/330x235/EFEFEF/AAAAAA&text=330x235',
                image_ratio: '330/235',
                csrfToken: csrfToken
            }, callback = upload_callback);


        });

        $('.summernote').summernote({
            height: 500, // set editor height
        });

        // 上傳裁切圖片
        var image_filename = '';
        var app_site_id = 1;
        var banner_info_id = 1;

        $(document).on('change', '.btn-upload-input', function() {
            var $oragin = $(this).closest('.cropper-oragin');
            $oragin.addClass('active-cropper');
            var ratio = eval($oragin.attr('data-ratio'));
            var cut = ($oragin.attr('data-cut') == 'true');
            var $input = $(this).find('input')

            // 這邊用base64裁圖會變慢 所以要先上傳圖片回傳網址裁圖
            var file = $(this).find('input')[0].files[0];
            image_filename = file.name;

            var reader = new FileReader();
            reader.onloadend = function() {
                // base64
                var url = reader.result;
                // callback
                var $cropper = $('#cropper-window #bounce-content').find('.cropper-image');
                $cropper.attr('src', url);
                $cropper.attr('data-src', url);
                $cropper.cropper('destroy')
                $cropper.cropper({
                    aspectRatio: ratio,
                    viewMode: 3,
                    // 關閉圖片縮放
                    zoomable: false,
                    // 關閉一開始就有剪裁筐
                    // autoCrop:false,
                    minContainerWidth: 2,
                    minContainerHeight: 1,
                });
                $input.val('');
                if (!cut) {
                    $('#cropper-window').find('.cropper-btn-save').hide();
                } else {
                    $('#cropper-window').find('.cropper-btn-save').show();
                }
                $('#cropper-window').modal('show');
                // end callback
            }
            reader.readAsDataURL(file);
        })

        ## 新增消息表單驗證
        $('.create_form').validate({
            ignore: "",
            errorElement: 'div', //default input error message container
            errorClass: 'form-control-feedback', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                category_id: {
                    required: true,
                },
                title: {
                    required: true,
                    byteRangeLength:60
                }
            },
            messages: {
                category_id: {
                    required: "分類為必填值",
                },
                title: {
                    required: "標題為必填值",
                    byteRangeLength:"公司名稱最多輸入字數為60(中文算2個字)"
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-danger'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-danger');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            }
        });

        ## 創建登錄公司事件
        $(document).on('click', '#create_btn', function () {
            var event_btn = $(this);
            var $valid = $('.create_form').valid();
            if($($('.my_image_data')[0]).attr('image_id')==undefined){
                $('.my_error_image').removeClass('hide');
                return false
            }else{
                $('.my_error_image').addClass('hide');
            }

            if($('.summernote').summernote('code')=="<p><br></p>"){
                $('.my_error_content').removeClass('hide');
                return false
            }else{
                $('.my_error_content').addClass('hide');
            }

            if (!$valid ) {
                return false
            }
            var form_data = new FormData();
            var url = "${ request.route_url('api.news')}";

            form_data.append('image_id', $($('.my_image_data')[0]).attr('image_id'));
            form_data.append('title', $('#title').val());
            form_data.append('date', $('#m_datepicker_1').val());
            if($('.focus')[0].checked==true){
                form_data.append('focus',  $($('.focus')[0]).val());
            }else{
                form_data.append('focus',  $($('.focus')[1]).val());
            }

            form_data.append('intro', $('#intro').val());
            form_data.append('language', $('#language').val());
            form_data.append('content', $('.summernote').summernote('code'));
            form_data.append('category_id', $('#category').val());

            ajax(url, 'POST', form_data, event_btn, function(data){
                if (event_btn) event_btn.attr("disabled", false);

                if (data['status']) {
                    window.location = '${ request.route_url("page.news.list") }?language='+data['response'].news.language;;
                } else {
                    alert('${_(u"創建失敗")}')
                }
            });

        });

        function ajax(url, method, form_data, btn, callback){
            $.ajax({
                url: url,
                type: method,
                data: form_data,
                ##  dataType: 'JSON',
                contentType: false,
                processData: false,
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    if (btn) btn.attr("disabled", true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                    if (btn) btn.attr("disabled", false)
                },
                success: function (response) {
                    callback(response);
                },
                complete: function () {
                    if (btn) btn.attr("disabled", false);
                }
            });
        }

    </script>

</%block>


