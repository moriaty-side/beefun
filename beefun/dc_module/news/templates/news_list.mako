<%inherit file="beefun:templates/backend/master.mako"/>

<%block name="css">
    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css" />
    <!-- 圖片預覽 -->

</%block>

<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        <span class="m-font-898b96">${ _(u'最新消息管理')}</span> <i class="fa fa-caret-right"></i>
                        <span>${ _(u'消息列表')}</span>
                    </h3>
                </div>
                <div>
                    <button type="button" class="btn btn-warning m-btn m-btn--outline-2x btn-sm mr-5px btn-search">
                    <span>
                                        <i class="fa fa-search"></i>
                                        <span>
                                            ${ _(u'搜尋')}
                                        </span>
                    </span>
                    </button>

                    <button type="button" class="btn btn-brand m-btn m-btn--outline-2x btn-sm" onclick="javascript:window.open('${ request.route_url('page.news.create') }','_self')">
                    <span>
                                        <i class="fa fa-plus"></i>
                                        <span>
                                            ${ _(u'新增')}
                                        </span>
                    </span>
                    </button>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="news_list" data-parent="news">
            <!--begin::Portlet-->
            <div class="languages hide"></div>
            <div class="m-portlet">
                
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right" id="search_form">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12 col-lg-6 col-xl-4">
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4">
                                        ${ _(u'廠商名稱')}
                                    </label>
                                    <div class="col-8">

                                           <input type="text" class="form-control m-input" placeholder="" name="title">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4">
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4">
                                        ${ _(u'廠商介紹')}
                                    </label>
                                    <div class="col-8">
                                        <input type="text" class="form-control m-input" placeholder="" name="title">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4">
                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4">
                                        ${ _(u'狀態')}
                                    </label>
                                    <div class="col-8">
                                        <select class="form-control se1" name="status">
                                            <option value="">${ _(u'All')}</option>
                                            <option value="show">${ _(u'上架')}</option>
                                            <option value="hiden">${ _(u'下架')}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
                <!--end::Form-->
                <!--Begin::Section-->
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-12">
                            <table class="table m-table m-table--head-bg-success">
                                <thead>
                                <tr>


                                    <th>
                                        ${ _(u'廠商名稱')}
                                    </th>
                                    <th>
                                        ${ _(u'廠商介紹')}
                                    </th>
                                    <th>
                                        ${ _(u'狀態')}
                                    </th>
                                    <th>
                                        <!--操作-->
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="news_list">
                                    <tr class='obj_index'>



                    <td>廠商名稱</td>

                    <td>廠商介紹</td>
               <td>

                       <span class="m-badge m-badge--info m-badge--wide m-badge--rounded m--font-bolder">${ _(u'上架')}</span>



                  </td>

                    <td align="right">
                   <button type="button" class="btn btn-outline-danger m-btn m-btn--outline-2x btn-sm"'+ 'onclick="javascript:window.open('+"'${ request.route_url('page.news.update',news_id='') }"+item.news_id+"','_self'"+')"><span><i class="fa fa-edit"></i><span>${ _(u'編輯')}</span></span></button>
                   <button type="button" class="btn btn-outline-danger m-btn m-btn--outline-2x btn-sm" onclick="javascript:window.open('${ request.route_url('page.rule.create') }')"><span><i class="fa fa-edit"></i><span>${ _(u'發點規則（開啟才顯示）')}</span></span></button>
                   <button type="button" class="btn btn-outline-metal m-btn m-btn--outline-2x btn-sm del_btn"><span><i class="la la-eye-slash"></i><span>${ _(u'刪除')}</span></span></button>

              </td>
              </tr>
                                </tbody>
                            </table>
                            <!--頁碼-->

                            <!--頁碼  end-->
                        </div>
                    </div>
                </div>
                <!--End::Section-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>
</%block>

<%block name="script">
    <!-- 圖片預覽 -->
    <script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/readimage.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/make_table/make_table.js')}" type="text/javascript"></script>
    <!-- 圖片預覽 -->
    <script>
        var csrfToken = "${request.session.get_csrf_token()}";
        var language='${ request.registry.settings.get('available_langs', [['zh_Hant', u'繁體中文']])[0][0] }';
        $('.se1').select2();

        
        $(document).on('click', '.nav-link', function (e) {
            
            $('#news_list').empty();
            Table.start();
        });

        var Table = new MakeTable({
            'dom_search_form': $('form#search_form'),
            'dom_search_btn': $('.btn-search'),
            'dom_table_tbody': $('.news_list'),
            
            'init_search_parameter': '',
            
            'ajax_search_url':  "${ request.route_url('api.news') }",
            'ajax_csrf_token':csrfToken,
            'table_model': function (obj, page) {
                //開始產生頁面資料
                var $list = obj['news_list'];
                var html_str = '';

                $.each($list, function (i, item) {
                    item.index = (i + 1 + ((page - 1) * 10));
                    var focus="";
                    var status="";
                    switch (item.focus) {
                        case "show":
                            focus = "${ _(u'開通')}";
                            break;
                        case "hiden":
                            focus = "${ _(u'關閉')}";
                            break;
                        case "delete":
                            focus = "${ _(u'刪除')}";
                            break;
                    }

                    switch (item.status) {
                        case "show":
                            status = "${ _(u'開通')}";
                            break;
                        case "hiden":
                            status = "${ _(u'關閉')}";
                            break;
                        case "delete":
                            status = "${ _(u'刪除')}";
                            break;
                    }

                    var tb_row = " <tr class='obj_index'>" ;
                    tb_row += "<td>"+"<input type='text' class='form-control input-mini number_set' value='"+item.sequence+"'>"+"</td>";
                    tb_row +="<td>"+"<label class='m-checkbox'>";
                    if(focus=="${ _(u'開通')}") {
                        tb_row+="<input class='check' type='checkbox' checked>"
                    }else{
                        tb_row+="<input class='check' type='checkbox'>"
                    }
                    tb_row +="<input type='text' value='"+item.news_id+"'  class='form-control news_id'>";
                    tb_row+="<span></span>"+"</label>";
                    tb_row +="</td>";

                    tb_row +="<td>"+item.category.name+"</td>";

                    tb_row +="<td>"+item.title+"</td>";
                    tb_row +="<td>";
                    if(status=="${ _(u'開通')}") {
                        tb_row+='<span class="m-badge m-badge--info m-badge--wide m-badge--rounded m--font-bolder">${ _(u'上架')}</span>';
                    }else{
                        tb_row+='<span class="m-badge m-badge--metal m-badge--wide m-badge--rounded m--font-bolder">${ _(u'下架')}</span>';
                    }
                    tb_row +="</td>";

                    tb_row +='<td align="right">';
                    tb_row +='<button type="button" class="btn btn-outline-danger m-btn m-btn--outline-2x btn-sm"'+ 'onclick="javascript:window.open('+"'${ request.route_url('page.news.update',news_id='') }"+item.news_id+"','_self'"+')"><span><i class="fa fa-edit"></i><span>${ _(u'編輯')}</span></span></button>';
                    tb_row +='<button type="button" class="btn btn-outline-metal m-btn m-btn--outline-2x btn-sm del_btn"><span><i class="la la-eye-slash"></i><span>${ _(u'刪除')}</span></span></button>';

                    tb_row +="</td>";
                    tb_row += "</tr>";
                    html_str += tb_row;
                });
                $('#news_list').empty();
                $('#news_list').append(html_str);//Table頁面資料繪製
            }
        });


        $(document).on('change', '.check', function (e) {
            var url = "${ request.route_url('api.news.detail',news_id="")}"+$(this).closest(".obj_index").find('.news_id').val();
            var form_data = new FormData();
            if(this.checked){
                form_data.append('focus', 'show');
            }else{
                form_data.append('focus','hide');
            }
            ajax(url, "POST", form_data, null, function (response) {
                if (response['status']) {
                    alert("${_('儲存成功')}");
                    $('#news_list').empty();
                    Table.start();
                } else {
                    alert(response['message']);
                }
            });
        });

        $(document).on('click', '.del_btn', function (e) {
            var url = "${ request.route_url('api.news.detail',news_id="")}"+$(this).closest(".obj_index").find('.news_id').val();
            var form_data = new FormData();

            form_data.append('status','delete');
            
            ajax(url, "POST", form_data, null, function (response) {
                if (response['status']) {
                    alert("${_('儲存成功')}");
                    $('#news_list').empty();
                    Table.start();
                } else {
                    alert(response['message']);
                }
            });
        });


        function append_obj_list(obj) {
            //預設清空類表內部
            $('#category_id').empty();

            //設定後端回傳格式
            var list = obj['response']['category_list'];

            //設定html內容
            var html = '<option value="">請選擇</option>';
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].category_id +"'>"+list[i].name+"</option>";
            }
            $('#category_id').append(html)
        }

        function ajax(url, method, form_data, btn, callback){
            $.ajax({
                url: url,
                type: method,
                data: form_data,
                ##  dataType: 'JSON',
                contentType: false,
                processData: false,
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    if (btn) btn.attr("disabled", true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                    if (btn) btn.attr("disabled", false)
                },
                success: function (response) {
                    callback(response);
                },
                complete: function () {
                    if (btn) btn.attr("disabled", false);
                }
            });
        }

        $(document).on('click','.save_btn',function(){
            var event_btn = $(this);
            var news_data_list = [];
            var number_set_check = [];
            //第一層
            $('.obj_index').each(function(i, v){
                if(parseInt($(v).find('.number_set').val())==0){
                    alert('請勿輸入0');
                    return false;
                }
                var new_data = {
                    'news_id': $(v).find('.news_id').val(),
                    'sequence': $(v).find('.number_set').val()
                }
                news_data_list.push(new_data);
                number_set_check.push($(v).find('.number_set').val());
            });

            var number_set_check_onleny=number_set_check.filter(function (el, i, arr) {
                return arr.indexOf(el) === i;
            });

            if(news_data_list.length==number_set_check_onleny.length){
                news_save(event_btn, news_data_list);
            }else{
                alert("${ _(u'序列有重複，請修正！！')}");
            }

            return false;
        });

        function news_save(event_btn, category_data_list){
            var url = "${ request.route_url('api.news.multi.set')}";
            var category_data_form = new FormData();
            category_data_form.append('json_data', JSON.stringify(category_data_list))

            ajax(url, 'POST', category_data_form, event_btn, function(data){
                if (event_btn) event_btn.attr("disabled", false);

                if (data['status']) {
                    $('#news_list').empty();
                    Table.start();
                } else {
                    alert('${_(u"創建失敗")}')
                }
            });
        }

    </script>

</%block>


