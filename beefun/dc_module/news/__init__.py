from __future__ import unicode_literals

from . import news_views as views
from ...base.context import AdminContext
from ...lib.api import return_format
from ...lib.page import return_page_format


def includeme(config):
    #Page
    config.add_route('page.news.list', '/page/news/list', factory=AdminContext)
    config.add_view(
        views.NewsPageView, attr='list',
        route_name='page.news.list',
        renderer='templates/news_list.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )
    config.add_route('page.news.create', '/page/news/create', factory=AdminContext)
    config.add_view(
        views.NewsPageView, attr='create',
        route_name='page.news.create',
        renderer='templates/test_create.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )

    config.add_route('page.rule.create', '/page/rule/create', factory=AdminContext)
    config.add_view(
        views.NewsPageView, attr='create_2',
        route_name='page.rule.create',
        renderer='templates/point_rule.mako',
        decorator=return_page_format,

        permission='login',

    )

    config.add_route('page.news.update', '/page/news/update/{news_id}', factory=AdminContext)
    config.add_view(
        views.NewsPageView, attr='update',
        route_name='page.news.update',
        renderer='templates/news_edit.mako',
        decorator=return_page_format,
        
        permission='login',
        
    )

    # API
    # news create
    config.add_route('api.news', '/api/news', factory=AdminContext)
    config.add_view(
        views.NewsJsonView, attr='create',
        route_name='api.news',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )
    config.add_view(
        views.NewsJsonView, attr='search',
        route_name='api.news',
        request_method='GET',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    # 多筆儲存
    config.add_route('api.news.multi.set', '/api/news/multi_set', factory=AdminContext)
    config.add_view(
        views.NewsJsonView, attr='multi_set',
        route_name='api.news.multi.set',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_route('api.news.detail', '/api/news/{news_id}', factory=AdminContext)
    config.add_view(
        views.NewsJsonView, attr='get',
        route_name='api.news.detail',
        request_method='GET',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_view(
        views.NewsJsonView, attr='edit',
        route_name='api.news.detail',
        request_method='POST',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )

    config.add_view(
        views.NewsJsonView, attr='delete',
        route_name='api.news.detail',
        request_method='DELETE',
        decorator=return_format,
        renderer='json',
        
        permission='login',
        
    )
