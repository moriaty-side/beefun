# coding=utf8
from __future__ import unicode_literals
import transaction, json
from . import news_form as form
from ...base.base_view import BaseView
from .news_service import NewsService

from ..category.category_service import CategoryService


class NewsPageView(BaseView):
    def __init__(self, request):
        super(NewsPageView, self).__init__(request)
        self.news_service = NewsService(self.session, logger=self.logger)
        
        self.category_service = CategoryService(session=self.session, logger=self.logger)
        
    def list(self):
        _ = self.localizer
        return {}

    def create(self):
        _ = self.localizer
        
        category_list = self.category_service.get_list(type="news")
        return {'category_list': [c.__json__() for c in category_list]}

    def create_2(self):
        _ = self.localizer

        category_list = self.category_service.get_list(type="news")
        return {'category_list': [c.__json__() for c in category_list]}
        

    def update(self):
        _ = self.localizer
        news_obj = self.news_service.get_by_id(news_id=self.request.matchdict['news_id'],
                                               check=True)
        
        category_list = self.category_service.get_list(type="news")

        return {'news_obj': news_obj.__json__(show_image=True, detail=True),
                'category_list': [c.__json__() for c in category_list]}
        



class NewsJsonView(BaseView):
    def __init__(self, request):
        super(NewsJsonView, self).__init__(request)
        self.news_service = NewsService(self.session, logger=self.logger)

    def create(self):
        """
        創建標籤
        """
        _ = self.localizer

        # 檢查輸入參數
        request_data = form.NewsCreateForm().to_python(self.request_params)
        
        with transaction.manager:
            news_obj = self.news_service.create(**request_data)

        return {'response': {'news': news_obj.__json__()},
                'message': _(u'News創建成功')}

    def search(self):
        """
        搜尋標籤
        """

        # 檢查輸入參數
        request_data = form.NewsSearchForm().to_python(self.request_params)

        # 分頁機制
        current_page = request_data.get('page', 1)
        limit = request_data.get('limit', 10)
        request_data['offset'] = (current_page - 1) * limit
        request_data['limit'] = limit
        request_data['order_by'] = [('created', -1)]

        # 搜尋帳號
        news_list, total_count = self.news_service.get_list(show_count=True, **request_data)

        return {'response': {'news_list': [a.__json__(show_category=True) for a in news_list],
                             'total_count': total_count,
                             'total_page': (int(total_count / limit) + 1 if total_count % limit else 0)
                             },
                'message': u'News搜尋成功'}

    def get(self):
        """
        讀取標籤
        """

        # 用 category_id 取得分類
        news_obj = self.news_service.get_by_id(news_id=self.request.matchdict['news_id'],
                                               check=True)

        return {'response': {'news': news_obj.__json__()},
                'message': 'News讀取成功'}

    def edit(self):
        """
        編輯標籤
        """

        # 檢查輸入參數
        request_data = form.NewsEditForm().to_python(self.request_params)

        # 用 category_id 取得分類
        news_obj = self.news_service.get_by_id(news_id=self.request.matchdict['news_id'],
                                               check=True)
        
        # 更新帳號
        with transaction.manager:
            
            news_obj = self.news_service.update(old_obj=news_obj,
                                                user_id=self.account_id,
                                                **request_data)

        return {'response': {'news': news_obj.__json__()},
                'message': u'News修改成功'}

    def delete(self):
        """
        刪除標籤
        """

        # 用 tag_id 取得分類
        news_obj = self.news_service.get_by_id(news_id=self.request.matchdict['news_id'],
                                               check=True)
        # 更新 status 達到軟刪除
        with transaction.manager:
            self.news_service.delete(old_obj=news_obj)

        return {'message': u'News刪除成功'}


    def multi_set(self):
        """
        一次處理多筆
        :return:
        """
        _ = self.localizer

        # 檢查輸入參數
        request_json_list = json.loads(self.request_params.get('json_data', '{}').replace('\r\n', '\\r\\n'))

        response = []
        with transaction.manager:
            for news_data in request_json_list:
                news_obj = self.news_service.update_by_id(**news_data)
                response.append(news_obj.__json__())


        return {'response': {'news_list': response},
                'message': _(u'banner 創建更新成功')}
