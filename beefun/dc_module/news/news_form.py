# coding=utf8
from __future__ import unicode_literals

import formencode, uuid
from formencode import validators


class NewsCreateForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    title = validators.String(not_empty=True)
    focus = validators.String(not_empty=True)
    status = validators.String(not_empty=True)
    date = validators.DateConverter(month_style='yyyy/mm/dd')
    intro = validators.String(not_empty=False)
    content = validators.String(not_empty=True)
    
    
    image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    
    
    category_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    
    

class NewsSearchForm(formencode.Schema):
    """AccountSearch 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True

    title = validators.String(not_empty=True)
    status = validators.String(not_empty=True)
    category_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    language = validators.String()

    page = validators.Int(not_empty=True, min=1)  # 第幾頁
    limit = validators.Int(not_empty=True, min=1)  # 一頁幾筆


class NewsEditForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = True
    ignore_key_missing = True

    focus = validators.String(not_empty=True)
    title = validators.String(not_empty=True)
    status = validators.String(not_empty=True)
    date = validators.DateConverter(month_style='yyyy/mm/dd')
    intro = validators.String(not_empty=False)
    content = validators.String(not_empty=True)

    
    
    image_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    
    
    category_id = validators.Wrapper(convert_to_python=lambda a: uuid.UUID(a) if a else '')
    
    