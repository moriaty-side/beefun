import transaction
from ...base.base_test import BaseTest, dummy_request
from .news_domain import News
from .news_views import NewsJsonView as View
from ..category.category_service import CategoryService
from ..image.image_service import ImageService

class NewsAPITest(BaseTest):

    def setUp(self):
        super(NewsAPITest, self).setUp()
        self.init_database()
        self.category_service = CategoryService(session=self.session)
        self.image_service = ImageService(repository_path='', session=self.session)

    def create(self, title='user', content='123'):
        category = self.get_or_create_category()
        image = self.get_or_create_image()
        obj = News(title=title, content=content, category=category, image=image)
        self.session.add(obj)
        obj = self.session.query(News).filter(News.title == title).first()
        return obj

    def get_or_create_category(self):
        category = self.category_service.get_by(name="測試")
        if not category:
            with transaction.manager:
                self.category_service.create(name="測試")
            category = self.category_service.get_by(name="測試")
        return category

    def get_or_create_image(self):
        image = self.image_service.get_by(name="測試")
        if not image:
            with transaction.manager:
                self.image_service.create(name="測試", filename='test.jpg')
            image = self.image_service.get_by(name="測試")
        return image

    def test_create(self):
        """
        測試創建
        """
        title = 'admin'
        category = self.get_or_create_category()
        image = self.get_or_create_image()
        request = dummy_request(self.session)
        request.params = {'title': title,
                          'category_id': str(category.category_id),
                          'image_id': str(image.image_id)}

        view = View(request)
        response = view.create()
        self.assertEqual(response.get('message'), 'News創建成功')

    def test_search(self):
        """
        測試搜尋
        """
        request = dummy_request(self.session)
        view = View(request)
        response = view.search()
        self.assertEqual(response.get('message'), 'News搜尋成功')
        self.assertEqual(response.get('response', {}).get('total_count'), 0)
        self.assertEqual(response.get('response', {}).get('total_page'), 0)
        self.assertEqual(len(response.get('response', {}).get('news_list', [])), 0)

    def test_get(self):
        """
        測試讀取
        """
        news = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'news_id': str(news.news_id)}
        view = View(request)
        response = view.get()
        self.assertEqual(response.get('message'), 'News讀取成功')

    def test_update(self):
        """
        測試更新
        """
        news = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'news_id': str(news.news_id)}
        title = '123'
        request.params = {'title': title}
        view = View(request)
        response = view.edit()
        self.assertEqual(response.get('message'), 'News修改成功')
        self.assertEqual(response.get('response', {}).get('news', {}).get('title'), title)

    def test_delete(self):
        """
        測試刪除
        """
        news = self.create()
        request = dummy_request(self.session)
        request.matchdict = {'news_id': str(news.news_id)}
        view = View(request)
        response = view.delete()
        self.assertEqual(response.get('message'), 'News刪除成功')

