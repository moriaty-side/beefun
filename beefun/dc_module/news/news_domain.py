# coding=utf8
import uuid
from sqlalchemy import (Column, Integer, String, DateTime, Text, ForeignKey)
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from ...base.meta_module import (Base, TimestampTable, GUID, uuid4, now_func)

DeclarativeBase = declarative_base()

class News(Base, TimestampTable):
    """
    文章資料表單
    """
    __tablename__ = 't_news'
    news_id = Column('f_news_id', GUID, primary_key=True, default=uuid4, doc=u"文章ID")
    
    
    category_id = Column('f_category_id', ForeignKey('t_category.f_category_id'), nullable=False, doc=u"分類")
    
    title = Column('f_title', String(64), nullable=False, doc=u'標題')
    _focus = Column('f_focus', Integer, nullable=False, default=2,
                     info={"hide": 1, "show": 2},
                     doc=u"是否在首頁呈現:1.不顯示,2.顯示")
    
    date = Column('f_date', DateTime, nullable=False, default=now_func, doc=u'日期')

    intro = Column('f_intro', String(128), nullable=False, default='', doc=u'引言')

    content = Column('f_content', Text, nullable=False, default='', doc=u'文案內容')

    
    image_id = Column('f_image_id', ForeignKey('t_images.f_image_id'), nullable=False, doc=u"圖片ID(圖床) 尺寸210*100")
    
    _status = Column('f_status', Integer, nullable=False, default=10,
                     info={"show": 10, "hiden": 20, "delete": 40},
                     doc=u"狀態:10.上架,20.下架,40:封存")
    
    image = relationship("Images")
    
    
    category = relationship("Category")
    

    def __repr__(self):
        return '<NewsObject (news_id={0})>'.format(self.news_id)

    @hybrid_property
    def focus(self):
        if hasattr(self.__class__, '_focus'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._focus.info
            for k in info_dic.keys():
                if info_dic[k] == self._focus:
                    return k

    @focus.setter
    def focus(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._focus.info.get(value)
            if not v:
                raise Exception('focus column input value {} is error'.format(value))
            self._focus = v
        else:
            raise Exception('focus column input value_type {} is error')

    @focus.expression
    def focus(cls):
        return cls._focus

    @hybrid_property
    def status(self):
        if hasattr(self.__class__, '_status'):
            # 將數值改為對應內容自串
            info_dic = self.__class__._status.info
            for k in info_dic.keys():
                if info_dic[k] == self._status:
                    return k

    @status.setter
    def status(self, value):
        # 將對應內容自串改為數值
        if not str(value).isdigit():
            v = self.__class__._status.info.get(value)
            if not v:
                raise Exception('status column input value {} is error'.format(value))
            self._status = v
        else:
            raise Exception('status column input value_type {} is error')

    @status.expression
    def status(cls):
        return cls._status

    @classmethod
    def __getattributes__(cls):
        return [i for i in cls.__dict__.keys() if i[:1] != '_']

    def __json__(self, detail=False, show_image=False, show_category=False):
        d = {'news_id': str(self.news_id),
             
             'title': self.title,
             
             'image_id': str(self.image_id),
             
             'intro': self.intro,
             
             'category_id': str(self.category_id),
             
             'focus': self.focus,
             
             'date': str(self.date).split(' ')[0].replace('-','/'),
             'status': str(self.status),
             }

        if detail:
            d['content'] = self.content
        
        if show_image:
            d['image'] = self.image.__json__() if self.image else {}
        
        
        if show_category:
            d['category'] = self.category.__json__() if self.category else {}
        
        return d

