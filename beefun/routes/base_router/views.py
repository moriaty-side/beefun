# coding=utf8
from __future__ import unicode_literals
import logging
from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response
from ...lib.api import format_processor
from pyramid.security import remember,forget


def notfound_view(exc, request):
    """
    當頁面404 處理方式
    api => 回傳錯誤格式的json
    page => 回倒首頁
    Args:
        exc:  錯誤訊息
        request:
    Returns:
        api: json
        page : home.page
    """
    system_name = request.registry.settings.get('system_name', '')
    msg = exc.args[0] if exc.args else ""
    route_type = 'backend_page' if 'admin' in request.path_url else 'frontend_page'
    route_name = ""

    # 合法的網址
    if request.route_name:
        if "backend.page" in request.route_name:
            route_type = "backend_page"
        if "frontend.page" in request.route_name:
            route_type = "frontend_page"
        if "api" in request.route_name:
            route_type = "api"

        route_name = system_name.lower() + "." + request.route_name

    logger = logging.getLogger(system_name)
    logger_json = {"route_name ": route_name,
                   "method": request.method,
                   "path_url": request.path_url,
                   "ip": request.real_ip,
                   "run_time": "",
                   "err": msg}
    # check user type
    if request.member_id:
        logger_json['user'] = str(request.member_id) + request.role

    # 如果是檔案缺失
    # if len([s for s in ['js', 'icon', 'css', 'jpg', 'png'] if s in msg]) == 0:
    #     return Response()

    logger.warning(logger_json)
    if route_type == "backend_page":
        if request.member_id:
            return HTTPFound(location=request.route_url('page.error', _query={'message': msg, 'from': 'backend_page'}))
        return HTTPFound(location=request.route_url('backend.page.login'))

    if route_type == "frontend_page":
        # if 'permission' in msg:
        #     return HTTPFound(location=request.route_url('frontend.page.index'))
        # return HTTPFound(location=request.route_url('page.error', _query={'message': msg, 'from': 'frontend_page'}))
        return HTTPFound(location=request.route_url('frontend.page.index'))

    elif route_type == "api":
        return Response(content_type=str('application/json'),
                        json_body=format_processor(code=403, err=msg))
    return Response()


def forbidden_view(exc, request):
    """
    當頁面403 處理方式
    api => 回傳錯誤格式的json
    page => 回倒登入頁
    Args:
        exc:  錯誤訊息
        request:
    Returns:
        api: json
        page : home.login

    """
    system_name = request.registry.settings.get('system_name', '')
    msg = exc.args[0] if exc.args else ""
    route_type = 'backend_page' if 'admin' in request.path_url else 'frontend_page'
    route_name = ""
    if request.route_name:
        if "backend.page" in request.route_name:
            route_type = "backend_page"
        if "frontend.page" in request.route_name:
            route_type = "frontend_page"
        if "api" in request.route_name:
            route_type = "api"

        route_name = system_name.lower() + "." + request.route_name

    logger = logging.getLogger(system_name)
    logger_json = {"route_name ": route_name,
                   "method": request.method,
                   "path_url": request.path_url,
                   "ip": request.real_ip,
                   "run_time": "",
                   "err": msg}
    # check user type
    if request.member_id:
        logger_json['user'] = str(request.member_id) + request.role

    logger.warning(logger_json)

    if route_type == "backend_page":
        if request.member_id:
            if 'permission' in msg:
                # remove session
                headers = forget(request)
                request.response.headerlist.extend(headers)
                return HTTPFound(location=request.route_url('backend.page.login'))
            return HTTPFound(location=request.route_url('page.error',_query={'message': msg, 'from': 'backend_page'}))
        return HTTPFound(location=request.route_url('backend.page.login'))

    if route_type == "frontend_page":
        if 'permission' in msg:
            return HTTPFound(location=request.route_url('frontend.page.index'))
        return HTTPFound(location=request.route_url('page.error',_query={'message': msg, 'from': 'frontend_page'}))
    elif route_type == "api":
        return Response(content_type=str('application/json'),
                        json_body=format_processor(code=403, err=msg))

    return Response()


def error_message_view(request):
    """
    錯誤頁面
    :param request:
    :return:
    """
    message = request.params.get('message')
    come_from = request.params.get('from')
    return dict(message=message, come_from=come_from)
