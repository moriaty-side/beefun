from __future__ import unicode_literals

from ...routes.base_router import views


def includeme(config):
    config.add_route('page.error', '/error')

    config.add_view(
        views.error_message_view,
        route_name='page.error',
        renderer='templates/error.mako',
    )
    config.add_view(
        views.notfound_view,
        context='pyramid.httpexceptions.HTTPNotFound',
    )
    config.add_view(
        views.forbidden_view,
        context='pyramid.httpexceptions.HTTPForbidden',
    )
