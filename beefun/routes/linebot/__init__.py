# coding=utf8
from __future__ import unicode_literals


def includeme(config):
    config.include('.page', route_prefix='/page')
    config.include('.api', route_prefix='/api')