<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.87.0">
    <title>Dashboard Template · Bootstrap v5.1</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/dashboard/">


    <!-- Bootstrap core CSS -->
    <link href="${ request.static_path('beefun:static/dashboard/assets/dist/css/bootstrap.min.css')}" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="${ request.static_path('beefun:static/dashboard/dashboard.css')}" rel="stylesheet">
</head>
<body>

<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">BeeFun test</a>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
##     <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
    <div class="navbar-nav">
        <div class="nav-item text-nowrap">
##             <a class="nav-link px-3" href="#">Sign out</a>
        </div>
    </div>
</header>

<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="position-sticky pt-3">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">
                            <span data-feather="home"></span>
                            Dashboard
                        </a>
                    </li>
            </div>
        </nav>

        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">


##             <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
            <div class="pt-3">
            <h2>域名列表</h2>
            <div class="pt-3">
            <input Placeholder="站點號" type="text" id="step">
            <input Placeholder="godaddy key" type="text" id="g_key">
            <input Placeholder="godaddy s" type="text" id="g_s">
                <button type="button" id="check">check domain</button>
##                 <button type="button" id="set_use">use domain</button>
            </div>

            <p class="pt-3">
            <p>域名總數：<div id="a_num"></div></p>
            <p>可用總數：<div id="c_num"></div></p>
            </div>
            <div class="table-responsive pt-3">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th style="padding-left: 10px">域名列表</th>
                        <th style="padding-left: 10px">廠商</th>
                        <th style="padding-left: 10px">選用</th>
                    </tr>
                    </thead>
                    <tbody class="dns_list">


##                     <tr>
##                         <td>1,015</td>
##                         <td>random</td>
##                         <td>tabular</td>
##                         <td>information</td>
##                         <td>text</td>
##                     </tr>
                    </tbody>
                </table>
            </div>
            </div>
            <div id="set_u_list" class="pt-3 d-none">
                <h2>網域配置（sre）</h2>
                <div class="pt-3">
                    <div class="col-12">

                        <label class="col-form-label col-sm-4">
                            ${ _(u'www_domain')}：
                        </label>
                        <div class="col-sm-4">
                            <input type="text" id="www" class="form-control m-input input-group"
                                   placeholder=""
                                   value="">
                        </div>

                        <label class="col-form-label col-sm-4">
                            ${ _(u'app_domain')}：
                        </label>
                        <div class="col-sm-4">
                            <input type="text" id="app" class="form-control m-input input-group"
                                   placeholder=""
                                   value="">
                        </div>

                        <label class="col-form-label col-sm-4">
                            ${ _(u'admin_domain')}：
                        </label>
                        <div class="col-sm-4">
                            <input type="text" id="adm" class="form-control m-input input-group"
                                   placeholder=""
                                   value="">
                        </div>

                        <label class="col-form-label col-sm-4">
                            ${ _(u'aff_domain')}：
                        </label>
                        <div class="col-sm-4">
                            <input type="text" id="affs" class="form-control m-input input-group"
                                   placeholder=""
                                   value="">
                        </div>

                        <label class="col-form-label col-sm-4">
                            ${ _(u'lotto_gck_domain')}：
                        </label>
                        <div class="col-sm-4">
                            <input type="text" id="gck" class="form-control m-input input-group"
                                   placeholder=""
                                   value="">
                        </div>

                        <label class="col-form-label col-sm-4">
                            ${ _(u'lotto_gci_domain')}：
                        </label>
                        <div class="col-sm-4">
                            <input type="text" id="gci" class="form-control m-input input-group"
                                   placeholder=""
                                   value="">
                        </div>

                        <label class="col-form-label col-sm-4">
                            ${ _(u'lotto_gws_domain')}：
                        </label>
                        <div class="col-sm-4">
                            <input type="text" id="gws" class="form-control m-input input-group"
                                   placeholder=""
                                   value="">
                        </div>

                        <label class="col-form-label col-sm-4">
                            ${ _(u'lotto_adm_domain')}：
                        </label>
                        <div class="col-sm-4">
                            <input type="text" id="lotto_adm" class="form-control m-input input-group"
                                   placeholder=""
                                   value="">
                        </div>

                        <label class="col-form-label col-sm-4">
                            ${ _(u'uat_www_domain')}：
                        </label>
                        <div class="col-sm-4">
                            <input type="text" id="uat_www"
                                   class="form-control m-input input-group"
                                   placeholder=""
                                   value="">
                        </div>

                        <label class="col-form-label col-sm-4">
                            ${ _(u'uat_adm_domain')}：
                        </label>
                        <div class="col-sm-4">
                            <input type="text" id="uat_adm"
                                   class="form-control m-input input-group"
                                   placeholder=""
                                   value="">
                        </div>

                        <label class="col-form-label col-sm-4">
                            ${ _(u'uat_aff_domain')}：
                        </label>
                        <div class="col-sm-4">
                            <input type="text" id="uat_aff"
                                   class="form-control m-input input-group"
                                   placeholder=""
                                   value="">
                        </div>

                        <label class="col-form-label col-sm-4">
                            ${ _(u'uat_app_domain')}：
                        </label>
                        <div class="col-sm-4">
                            <input type="text" id="uat_app"
                                   class="form-control m-input input-group"
                                   placeholder=""
                                   value="">
                        </div>

                    </div>





                </div>
            </div>




        </main>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="${ request.static_path('beefun:static/dashboard/assets/dist/js/bootstrap.bundle.min.js')}"></script>

<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js"
        integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"
        integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha"
        crossorigin="anonymous"></script>
<script src="${ request.static_path('beefun:static/dashboard/dashboard.js')}"></script>

<script>

    $(document).on('click', '#set_use', function () {

        var save_btn = $(this);
        var url = "${ request.route_url('api.set.domain.use') }";
        var form_data = new FormData();

        var domain_list = []

        $.each($('.use:checked'), function (i, item) {
            domain_list.push($(item).val());
        });


        form_data.append('step', $('#step').val());
        form_data.append('g_key', $('#g_key').val());
        form_data.append('g_s', $('#g_s').val());
        form_data.append('domain_list', domain_list);


        ajax(url, "POST", form_data, save_btn, function (response) {
            if (response) {
                alert('設置完成')

            } else {
                alert(response['message']);
            }
        });
    });


    $(document).on('click', '#check', function () {

        var save_btn = $(this);
        var url = "${ request.route_url('api.check.domain.use.telegram') }";
        var form_data = new FormData();
        form_data.append('g_key', $('#g_key').val());
        form_data.append('g_s', $('#g_s').val());
        ajax(url, "POST", form_data, save_btn, function (response) {
            if (response) {
                var html_s = '';
                var $list = response['response']['domain_list'];
                $('.dns_list').empty();
                $('#c_num').text(response['response']['mun'])
                $('#a_num').text(response['response']['all_num'])
                $.each($list, function (i, item) {
                    var tb_row = '<tr style="padding-top: 5px">';
                    tb_row += '<td style="padding-left: 10px">' + item['domain'] + '</td>';
                    tb_row += '<td style="padding-left: 10px">' + 'godaddy' + '</td>';
                    tb_row += '<td style="padding-left: 10px"><input class="use" type="checkbox" value="' + item['domain'] + '">' + '</td>';
                    tb_row += '</tr>';

                    html_s += tb_row;
                });
                $('.dns_list').append(html_s);
                alert("${_('儲存成功')}");
            } else {
                alert(response['message']);
            }
        });
    });


    function ajax(url, method, form_data, btn, callback) {
        $.ajax({
            url: url,
            type: method,
            data: form_data,
            timeout: 2*60*60*1000,
            contentType: false,
            processData: false,


            headers: {},
            beforeSend: function () {
                $(btn).attr('disabled', true);
            },
            error: function (xhr) {
                $(btn).attr('disabled', true);
                alert('Ajax request 發生錯誤');
            },
            success: function (response) {
                callback(response);
            },
            complete: function () {

                $(btn).attr('disabled', false);
            }
        });
    }
</script>

</body>
</html>
