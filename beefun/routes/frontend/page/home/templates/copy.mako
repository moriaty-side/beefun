<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
</head>
<body>
  <h1>domain_check</h1>

<div>
<input Placeholder="站點號" type="text" id="step">
<input Placeholder="godaddy key" type="text" id="g_key">
<input Placeholder="godaddy s" type="text" id="g_s">
    <button type="button" id="check">check domain</button>
    <button type="button" id="set_use">use domain</button>
</div>
<div>
    <div>
        <p>可用數量</p>
        <p id="c_num"></p>
    </div>

    <table>
        <thead>
            <tr>
                <th style="padding-left: 10px">域名列表</th>
                <th style="padding-left: 10px">廠商</th>
                <th style="padding-left: 10px">選用</th>
##                 <th style="padding-left: 10px">www</th>
##                 <th style="padding-left: 10px">app</th>
##                 <th style="padding-left: 10px">admin</th>
##                 <th style="padding-left: 10px">lotto</th>
##                 <th style="padding-left: 10px">uat</th>
            </tr>
        </thead>
        <tbody class="dns_list">


        </tbody>
    </table>






</div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script>

      $(document).on('click', '#set_use', function () {

          var save_btn = $(this);
          var url = "${ request.route_url('api.set.domain.use') }";
          var form_data = new FormData();

          var domain_list = []

          $.each($('.use:checked'), function (i, item) {
            domain_list.push($(item).val());
          });


          form_data.append('step', $('#step').val());
          form_data.append('g_key', $('#g_key').val());
          form_data.append('g_s', $('#g_s').val());
          form_data.append('domain_list', domain_list);


          ajax(url, "POST", form_data, save_btn, function (response) {
              if (response) {
                    alert('設置完成')

              } else {
                  alert(response['message']);
              }
          });
      });


      $(document).on('click', '#check', function () {

           var save_btn = $(this);
           var url = "${ request.route_url('api.check.domain.use.telegram') }";
           var form_data = new FormData();
           form_data.append('g_key', $('#g_key').val());
          form_data.append('g_s', $('#g_s').val());
             ajax(url, "POST", form_data, save_btn, function (response) {
                 if (response) {
                     var html_s = '';
                     var $list = response['response']['domain_list'];
                     $('.dns_list').empty();
                     $('#c_num').text(response['response']['mun'])
                     $.each($list, function (i, item) {
                        var tb_row= '<tr style="padding-top: 5px">';
                        tb_row += '<td style="padding-left: 10px">' +item['domain']+ '</td>';
                        tb_row += '<td style="padding-left: 10px">' + 'godaddy' + '</td>';
                         tb_row +='<td style="padding-left: 10px"><input class="use" type="checkbox" value="' + item['domain'] + '">' + '</td>';
                        tb_row +='</tr>';

                         html_s += tb_row;
                     });
                    $('.dns_list').append(html_s);
                                           alert("${_('儲存成功')}");
                 } else {
                                           alert(response['message']);
                 }
             });
      });



      function ajax(url, method, form_data, btn, callback) {
          $.ajax({
              url: url,
              type: method,
              data: form_data,
              contentType: false,
              processData: false,

              headers: {},
              beforeSend: function () {
                  $(btn).attr('disabled', true);
              },
              error: function (xhr) {
                  $(btn).attr('disabled', true);
                  alert('Ajax request 發生錯誤');
              },
              success: function (response) {
                  callback(response);
              },
              complete: function () {

                  $(btn).attr('disabled', false);
              }
          });
      }
  </script>

</body>
</html>