<!DOCTYPE html>
<html lang="zh-TW">
<head>
    <title></title>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta content="#{description}" name="description">
    <meta content="" name="author">
    <link href="${ request.static_path('beefun:static/dist/assets/favicon.ico')}" rel="icon">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet">
    <style>
        .logo {
            margin-top: 10px;
            margin-bottom: 30px;
        }

    </style>
    <link href="${ request.static_path('beefun:static/dist/css/index.css')}" rel="stylesheet">
</head>
<body>
<div class="container-fluid privacy py-4">
    <div class="row privacy-content">

        <div class="col-12 text-center">
            <div class="title text-primary">案件追蹤</div>
        </div>
        <div class="col-12" id="list_here">

        </div>

    </div>
</div>
<script src="${ request.static_path('beefun:static/assets/global/plugins/jquery.min.js')}"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="${ request.static_path('beefun:static/dist/js/index.js')}"></script>
<script src="https://static.line-scdn.net/liff/edge/2.1/sdk.js"></script>
<script src="liff-starter.js"></script>
        <script src="${ request.static_path('beefun:static/assets/global/plugins/jquery-bootpag/jquery.bootpag.min.js') }"
        type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/make_table/make_table_dat.js')}"
            type="text/javascript"></script>
<script>

      const windowHeight = window.innerHeight;

      var user_id;

      function initializeApp() {
          liff.getProfile().then(function (profile) {
              const userId = profile.userId;
              user_id = userId;
              $('#fname').val(userId);
              const name = profile.displayName;
              const pictureUrl = profile.pictureUrl;
              const statusMessage = profile.statusMessage;

                                   tab_one_list_table_init = new MakeTable({
                         'dom_search_form': $('form#search_form'),
                         'dom_search_btn': $('.search_btn'),
                         'dom_table_tbody': $('#pair'),
                        'dom_paginate_obj': $('#page-selection'),

                         'init_search_parameter': '&limit=5',
                         'ajax_search_url': "${ request.route_url('api.case.list_f',line_id='')}"+userId,
                         'table_model': function (obj, page) {



                             //開始產生頁面資料
                             var $list = obj['case_list'];
                             var user_id = obj['user_id'];
                             $('#hid_user_id').val(user_id);
                             var html_str = '';
                             $('#pair').empty();
                             var pair_set = "";
                             $.each($list, function (i, item) {
                                 item.index = (i + 1 + ((page - 1) * 5));
                                 item['name_str'] = (item['name'] != null) ? item['name'] : "";

                               pair_set += '<div class="card border-0 shadow">';
                                          pair_set += '<div class="card-body">';

                                          pair_set += '<div class="info-content">';



                                          pair_set += '<div class="col">';
                                          pair_set += '<div class="sub-title">案件名稱</div>';
                                          pair_set += '<div class="info">' + item['case_name'] + '</div>';
                                          pair_set += '<div class="sub-title">案件狀態</div>';
                                          pair_set += '<div class="info">' + item['status'] + '</div>';

                                          pair_set += '</div>';
                                          pair_set += '</div>';
                                          pair_set += '</div>';
                                          pair_set += '</div>';


                     });

                    $('#list_here').append(pair_set);







                 }
             });


        tab_one_list_table_init.start();

          }).catch(function (error) {
              console.log('error', error);
          });
      }

          ##  測試用 code
                   ##  1655586468-zdXkQ3Za

          ##  正式用 code
                   ##  1655082439-GnLQ2Bvd

      initializeLiff('1657557034-ywxMxGab');

      function initializeLiff(myLiffId) {
          liff.init({
              liffId: myLiffId
          }).then(() => {
              // start to use LIFF's api
              initializeApp();
          }).catch((err) => {
              document.getElementById("liffAppContent").classList.add('hidden');
              document.getElementById("liffInitErrorMessage").classList.remove('hidden');
          });
      }







    </script>
</body>
</html>