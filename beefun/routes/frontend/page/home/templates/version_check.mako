<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.87.0">
    <title>Dashboard Template · Bootstrap v5.1</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/dashboard/">


    <!-- Bootstrap core CSS -->
    <link href="${ request.static_path('beefun:static/dashboard/assets/dist/css/bootstrap.min.css')}" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="${ request.static_path('beefun:static/dashboard/dashboard.css')}" rel="stylesheet">
</head>
<body>

<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">BeeFun test</a>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
##     <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
    <div class="navbar-nav">
        <div class="nav-item text-nowrap">
##             <a class="nav-link px-3" href="#">Sign out</a>
        </div>
    </div>
</header>

<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="position-sticky pt-3">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">
                            <span data-feather="home"></span>
                            Dashboard
                        </a>
                    </li>
            </div>
        </nav>

        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">


##             <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
            <div class="pt-3">
            <h2>版本檢查（sre）</h2>
            <div>範例檔案：<a href="${ request.static_path('beefun:static/sample.csv')}">範例</a></div>
            <p id="all_time"></p>
            <div class="pt-3">
                <div class="">
                    <select class="form-select-bg-size" id="type_check">
                      <option value="1">單筆</option>
                      <option value="2">檔案</option>
                    </select>
                </div>
                <div class="one_set mt-5">
                    <input Placeholder="站點號" type="text" id="step">
                    <input Placeholder="prod域名" type="text" id="pro_domain">
                    <input Placeholder="uat域名" type="text" id="test_domain">
                    <button type="button" id="check">版本查詢</button>
                </div>

                <div class="file_set mt-5 d-none">
                    <input type="file" id="file_check" name="..." class="file_up">
    ##                 <button type="button" id="file_check">檔案查詢</button>
                </div>

            </div>
            <div class="table-responsive pt-3">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th scope="col">站點號</th>
                        <th scope="col">prod後</th>
                        <th scope="col">prod前</th>
                        <th scope="col">uat後</th>
                        <th scope="col">uat前</th>
                    </tr>
                    </thead>
                    <tbody class="version_list">


##                     <tr>
##                         <td>1,015</td>
##                         <td>random</td>
##                         <td>tabular</td>
##                         <td>information</td>
##                         <td>text</td>
##                     </tr>
                    </tbody>
                </table>
            </div>
            </div>





        </main>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="${ request.static_path('beefun:static/dashboard/assets/dist/js/bootstrap.bundle.min.js')}"></script>

<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js"
        integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"
        integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha"
        crossorigin="anonymous"></script>
<script src="${ request.static_path('beefun:static/dashboard/dashboard.js')}"></script>

<script>

    $(document).on('change', '#type_check', function () {
        if($('#type_check').val()=='1'){
            $('.one_set').removeClass('d-none')
            $('.file_set').addClass('d-none')
        }else{
            $('.one_set').addClass('d-none')
            $('.file_set').removeClass('d-none')
        }
    });

      $(document).on('click', '#check', function () {

           var save_btn = $(this);
           var url = "${ request.route_url('api.version.check') }";
           var form_data = new FormData();
           form_data.append('pro_domain', $('#pro_domain').val());
           form_data.append('test_domain', $('#test_domain').val());
             ajax(url, "POST", form_data, save_btn, function (response) {
                 if (response) {
                     var html_s = '';
                     ##  var $list = response['response'][''];
                     $('.version_list').empty();

                     var tb_row = '<tr>';
                     tb_row += '<td>' +$('#step').val()+ '</td>';
                     tb_row += '<td>' + response['response']['prod_version_f'] + '</td>';
                     tb_row += '<td>' + response['response']['prod_version_b'] + '</td>';
                     tb_row += '<td>' + response['response']['uat_version_f'] + '</td>';
                     tb_row += '<td>' + response['response']['uat_version_b'] + '</td>';
                    tb_row +='</tr>';
                    html_s += tb_row;
                     ##  $.each($list, function (i, item) {
                     ##     var tb_row= '<tr>';
                     ##     tb_row += '<td>' +item['domain']+ '</td>';
                     ##     tb_row += '<td>' + 'godaddy' + '</td>';
                     ##     tb_row +='<td><input class="use" type="checkbox" value="' + item['domain'] + '">' + '</td>';
                     ##     tb_row += '<td><input class="www" type="checkbox" value="' + item['domain'] + '">' + '</td>';
                     ##     tb_row += '<td><input class="app" type="checkbox" value="' + item['domain'] + '">' + '</td>';
                     ##     tb_row += '<td><input class="admin" type="checkbox" value="' + item['domain'] + '">' + '</td>';
                     ##     tb_row += '<td><input class="lotto" type="checkbox" value="' + item['domain'] + '">' + '</td>';
                     ##     tb_row += '<td><input class="uat" type="checkbox" value="' + item['domain'] + '">' + '</td>';
                     ##      tb_row +='</tr>';
                     ##
                     ##      html_s += tb_row;
                     ##  });
                    $('.version_list').append(html_s);

                                           alert("${_('儲存成功')}");
                 } else {
                                           alert(response['message']);
                 }
             });
      });

    $(document).on('change', '#file_check', function () {

        var json_url = '${ request.route_url("api.version.file.check") }';
        var type = "POST";

        var form_data = new FormData();

        gg = document.getElementById('file_check').files[0];

        var ch = gg.name.split('.');
        var xx = ch[1];
        form_data.append('files', gg);

        $.ajax({
            url: json_url,
            type: type,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,

            success: function (data) {
                console.log(data);
                if (data['message']=='成功') {

                    var html_s = '';

                    var $list = data['response']['out_list'];

                    $('#all_time').text(data['response']['all_time']);

                    $('.version_list').empty();
                     $.each($list, function (i, item) {
                         var tb_row = '<tr>';
                         tb_row += '<td>' + item['code'] + '</td>';
                         if(item['prod_version_f']=='404'){
                             tb_row += '<td style="color: red;">' + item['prod_version_f'] + '</td>';
                         }else{
                             tb_row += '<td>' + item['prod_version_f'] + '</td>';
                         }

                         if(item['prod_version_b']=='404'){
                             tb_row += '<td style="color: red;">' + item['prod_version_b'] + '</td>';
                         }else{
                             tb_row += '<td>' + item['prod_version_b'] + '</td>';
                         }

                         if(item['uat_version_f']=='404'){
                             tb_row += '<td style="color: red;">' + item['uat_version_f'] + '</td>';
                         }else{
                             tb_row += '<td>' + item['uat_version_f'] + '</td>';
                         }

                        if(item['uat_version_b']=='404'){
                             tb_row += '<td style="color: red;">' + item['uat_version_b'] + '</td>';
                         }else{
                             tb_row += '<td>' + item['uat_version_b'] + '</td>';
                         }

                         tb_row += '</tr>';
                         html_s += tb_row;
                     });

                    $('.version_list').append(html_s);


                } else {
                    alert('數量超過100')
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            },
            complete: function () {

            }
        });
    });





      function ajax(url, method, form_data, btn, callback) {
          $.ajax({
              url: url,
              type: method,
              data: form_data,
              contentType: false,
              processData: false,

              headers: {},
              beforeSend: function () {
                  $(btn).attr('disabled', true);
              },
              error: function (xhr) {
                  $(btn).attr('disabled', true);
                  alert('Ajax request 發生錯誤');
              },
              success: function (response) {
                  callback(response);
              },
              complete: function () {

                  $(btn).attr('disabled', false);
              }
          });
      }
  </script>

</body>
</html>
