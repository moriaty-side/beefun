<%inherit file="beefun:templates/backend/master_f.mako"/>

<%block name="css">
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/allen_assets/cropper/cropper.min.css') }">
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/allen_assets/cropper/cropper_plugin.css') }">
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/allen_assets/diyUpload/css/webuploader.css') }">
    <link rel="stylesheet" type="text/css" href="${ request.static_path('beefun:static/allen_assets/diyUpload/css/diyUpload.css') }">
      <link href="${ request.static_path('beefun:static/assets/admin/css/multi-select.css')}" rel="stylesheet" type="text/css" />
    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css" />
</%block>

<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">

                <div>

                    <button type="button" class="btn btn-success m-btn m-btn--outline-2x btn-sm" id="create_btn">
                    <span>
                                        <i class="fa fa-check"></i>
                                        <span>
                                            ${ _(u'登入')}
                                        </span>
                    </span>
                    </button>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="client_list" data-parent="case">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right create_form" id="create_form">
                    <!--Begin::Section-->
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12">


                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'帳號')}
                                    </label>
                                    <div class="col-8">
                                        <input class="form-control" placeholder="" id="client_name" name="client_name">
                                    </div>
                                </div>

                                <div class="form-group m-form__group">
                                    <label class="col-form-label col-4 col-md-2">
                                    <span class="m--font-danger">
                                                        *
                                                    </span>
                                        ${ _(u'密碼')}
                                    </label>
                                    <div class="col-8">
                                        <input class="form-control" placeholder="" id="password" name="password">
                                    </div>
                                </div>
                    <!--End::Section-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>

</%block>



<%block name="script">

              <script src="${ request.static_path('beefun:static/assets/admin/js/jquery.multi-select.js')}" type="text/javascript"></script>
    <script src="${ request.static_url('beefun:static/html_fronted_work/backstage/assets/my_js/jquery.serialize-object.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/allen_assets/cropper/cropper.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/allen_assets/cropper/cropper_plugin.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>

    <script src="${ request.static_path('beefun:static/assets/other/custom_validation.js')}"
            type="text/javascript"></script>




    <script type="text/javascript" src="${ request.static_path('beefun:static/assets/other/jscolor.js') }"></script>

    <script>
        var csrfToken = "${request.session.get_csrf_token()}";
        $('.se1').select2();

        $(document).ready(function (e) {
                  $('#connection_list').multiSelect();
        });


            ## 新增消息表單驗證
          $('.create_form').validate({
            ignore: "",
            errorElement: 'div', //default input error message container
            errorClass: 'form-control-feedback', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {

                client_name: {
                    required: true,
                    byteRangeLength:60
                }
            },
            messages: {

                client_name: {
                    required: "標題為必填值",
                    byteRangeLength:"公司名稱最多輸入字數為60(中文算2個字)"
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-danger'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-danger');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            }
        });

        ## 創建登錄公司事件
        $(document).on('click', '#create_btn', function () {
            var event_btn = $(this);
            var $valid = $('.create_form').valid();

            if (!$valid ) {
                return false
            }
            var form_data = new FormData();
            var url = "${ request.route_url('api.identity.login')}";


            form_data.append('client_name', $('#client_name').val());
            form_data.append('password', $('#password').val());

            ajax(url, 'POST', form_data, event_btn, function(data){
                        if (event_btn) event_btn.attr("disabled", false);

                        if (data['status']) {
                               window.location = '${ request.route_url("frontend.page.my_info", identity_id='') }' + data['response']['identity'];
                        } else {
                            alert('${_(u"創建失敗")}')
                        }
            });

        });

        function ajax(url, method, form_data, btn, callback){
            $.ajax({
                url: url,
                type: method,
                data: form_data,
                ##  dataType: 'JSON',
                contentType: false,
                processData: false,
                headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function () {
                    if (btn) btn.attr("disabled", true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                    if (btn) btn.attr("disabled", false)
                },
                success: function (response) {
                    callback(response);
                },
                complete: function () {
                    if (btn) btn.attr("disabled", false);
                }
            });
        }


    </script>

</%block>


