<%inherit file="beefun:templates/backend/master_f.mako"/>

<%block name="css">
    <!-- 圖片預覽 -->
    <link href="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.css')}" rel="stylesheet" type="text/css"/>
    <!-- 圖片預覽 -->

</%block>

<%block name="content">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->

        <!-- END: Subheader -->
        <!--begin::Portlet-->
        <div class="m-content" data-menu="client_list" data-parent="case">
            <!--begin::Portlet-->
            <div class="languages hide"></div>
            <div class="m-portlet">

                <!--Begin::Section-->
                <!--begin::Form-->

                <form class="m-form m-form--fit m-form--label-align-right" id="search_form">
                    <!--Begin::Section-->
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12 mb-15px">

                                <table class="table m-table m-table--head-bg-success">
                                    <thead>
                                    <tr>

                                        <th> ${ _(u'方法')} </th>
                                        <th> ${ _(u'內容')} </th>

                                    </tr>
                                    </thead>
                                    <tbody id="client_tbody">
                                        %for connection in identity.get('identity_connection'):
                                        <tr class='obj_index'>
                                        <td>
                                        <div class='title'>${connection.get('connection').get('type')}</div>
                                        </td>
                                        <td>
                                        %if connection.get('connection').get('type') == 'phone':
                                                    <div class='title'><a href="tel:${connection.get('connection').get('connection_info')}">${connection.get('connection').get('connection_info')}</a></div>
                                         %else:
                                                     <div class='title'>${connection.get('connection').get('connection_info')}</div>
                                         %endif

                                        </td>
                                        </tr>

                                        %endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
        <!--end::Portlet-->
    </div>
</%block>



<%block name="script">
    <!-- 圖片預覽 -->
    <script src="${ request.static_path('beefun:static/assets/other/bootstrap-fileinput/bootstrap-fileinput.js')}" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/other/readimage.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/jquery-bootpag/jquery.bootpag.min.js') }" type="text/javascript"></script>
    <script src="${ request.static_path('beefun:static/assets/make_table/make_table.js')}" type="text/javascript"></script>
    <!-- 圖片預覽 -->
    <script>







    </script>
</%block>


