from __future__ import unicode_literals

from . import views
from .....base.context import AdminContext

from .....lib.page import return_page_format


def includeme(config):
    config.add_route('frontend.page.index', '', factory=AdminContext)
    config.add_view(
        views.HomeView, attr='index',
        route_name='frontend.page.index',
        renderer='templates/index.mako',
        request_method='GET',
        # decorator=return_page_format,
    )

    config.add_route('frontend.page.my_info', '/my_info/{identity_id}', factory=AdminContext)
    config.add_view(
        views.HomeView, attr='my_info',
        route_name='frontend.page.my_info',
        renderer='templates/my_info.mako',
        request_method='GET',
        # decorator=return_page_format,
    )

    config.add_route('frontend.page.domain_check', '/domain/check', factory=AdminContext)
    config.add_view(
        views.HomeView, attr='domain_check',
        route_name='frontend.page.domain_check',
        renderer='templates/domain_check.mako',
        request_method='GET',
        # decorator=return_page_format,
    )

    config.add_route('frontend.page.domain_check_sre', '/domain/check/sre', factory=AdminContext)
    config.add_view(
        views.HomeView, attr='domain_check_sre',
        route_name='frontend.page.domain_check_sre',
        renderer='templates/domain_check_sre.mako',
        request_method='GET',
        # decorator=return_page_format,
    )

    config.add_route('frontend.page.member_set', '/member_set', factory=AdminContext)
    config.add_view(
        views.HomeView, attr='member_set',
        route_name='frontend.page.member_set',
        renderer='templates/member_set.mako',
        request_method='GET',
        # decorator=return_page_format,
    )

    config.add_route('frontend.page.version_check', '/version/check', factory=AdminContext)
    config.add_view(
        views.HomeView, attr='version_check',
        route_name='frontend.page.version_check',
        renderer='templates/version_check.mako',
        request_method='GET',
        # decorator=return_page_format,
    )

    config.add_route('frontend.page.case_list', '/case_list', factory=AdminContext)
    config.add_view(
        views.HomeView, attr='case_list',
        route_name='frontend.page.case_list',
        renderer='templates/case_list.mako',
        request_method='GET',
        # decorator=return_page_format,
    )

    config.add_route('frontend.page.create_case', '/create_case', factory=AdminContext)
    config.add_view(
        views.HomeView, attr='create_case',
        route_name='frontend.page.create_case',
        renderer='templates/create_case.mako',
        request_method='GET',
        # decorator=return_page_format,
    )

    config.add_route('frontend.page.create_client', '/create_client', factory=AdminContext)
    config.add_view(
        views.HomeView, attr='create_client',
        route_name='frontend.page.create_client',
        renderer='templates/create_client.mako',
        request_method='GET',
        # decorator=return_page_format,
    )
