# coding=utf8
from __future__ import unicode_literals
from .....base.base_view import BaseView
from beefun.dc_module.john_work.john_work_service import ClientService, CaseService, RichMenuService,IdentityService,IdentityConnectionService,ConnectionService
import requests
import uuid
class HomeView(BaseView):
    def __init__(self, request):
        super(HomeView, self).__init__(request)
        self.identity_service = IdentityService(self.session, logger=self.logger)

    def index(self):
        """
            首頁
            :return:
        """
        _ = self.localizer

        return {}

    def my_info(self):
        """
            首頁
            :return:
        """
        _ = self.localizer

        identity_obj = self.identity_service.get_by_id(identity_id=self.request.matchdict['identity_id'])

        return {'identity': identity_obj.__json__()}

    def member_set(self):
        """
            首頁
            :return:
        """
        _ = self.localizer

        return {}

    def domain_check(self):
        """
            首頁
            :return:
        """
        _ = self.localizer

        return {}

    def domain_check_sre(self):
        """
            首頁
            :return:
        """
        _ = self.localizer

        return {}

    def version_check(self):
        """
            首頁
            :return:
        """
        _ = self.localizer

        return {}


    def case_list(self):
        """
            首頁
            :return:
        """
        _ = self.localizer

        return {}

    def create_case(self):
        """
            首頁
            :return:
        """
        _ = self.localizer

        return {}

    def create_client(self):
        """
            首頁
            :return:
        """
        _ = self.localizer

        return {}
