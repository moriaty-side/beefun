from __future__ import unicode_literals
from . import line_bot_view as views

def includeme(config):

    config.add_route('api.line_bot.control', '/line_bot/control/{app_id}')
    config.add_view(
        views.LineBotJsonView, attr='line_bot_control',
        route_name='api.line_bot.control',
        request_method='POST',
        renderer='json'

    )

    config.add_route('api.callback.notify', '/callback/notify')
    config.add_view(
        views.LineBotJsonView, attr='callback_notify',
        route_name='api.callback.notify',
        request_method='GET',
        renderer='json'

    )

    config.add_route('api.check.notify', '/check/notify')
    config.add_view(
        views.LineBotJsonView, attr='check_notify',
        route_name='api.check.notify',
        request_method='POST',
        renderer='json'

    )

    config.add_route('api.check.notify_one', '/check/notify/one')
    config.add_view(
        views.LineBotJsonView, attr='notify_one',
        route_name='api.check.notify_one',
        request_method='POST',
        renderer='json'

    )

    config.add_route('api.check.dns.who', '/check/dns/who')
    config.add_view(
        views.LineBotJsonView, attr='check_dns_who',
        route_name='api.check.dns.who',
        request_method='POST',
        renderer='json'

    )

    config.add_route('api.check.dns.telegram.who', '/check/dns/telegram/who')
    config.add_view(
        views.LineBotJsonView, attr='check_dns_who_for_telegram',
        route_name='api.check.dns.telegram.who',
        request_method='POST',
        renderer='json'

    )

    config.add_route('api.check.domain.use.telegram', '/check/domain/use/telegram')
    config.add_view(
        views.LineBotJsonView, attr='check_domain_use_for_telegram',
        route_name='api.check.domain.use.telegram',
        request_method='POST',
        renderer='json'

    )

    config.add_route('api.set.domain.use', '/set/domain/use')
    config.add_view(
        views.LineBotJsonView, attr='set_use_domain',
        route_name='api.set.domain.use',
        request_method='POST',
        renderer='json'

    )

    config.add_route('api.set.domain.sre.use', '/set/sre/domain/use')
    config.add_view(
        views.LineBotJsonView, attr='set_sre_use_domain',
        route_name='api.set.domain.sre.use',
        request_method='POST',
        renderer='json'

    )

    config.add_route('api.version.check', '/version/check')
    config.add_view(
        views.LineBotJsonView, attr='version_check',
        route_name='api.version.check',
        request_method='POST',
        renderer='json'
    )

    config.add_route('api.version.file.check', '/file/version/check')
    config.add_view(
        views.LineBotJsonView, attr='file_version_check',
        route_name='api.version.file.check',
        request_method='POST',
        renderer='json'
    )