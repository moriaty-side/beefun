# coding=utf8
from __future__ import unicode_literals

import formencode, uuid
from formencode import validators, ForEach

class NForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True

    code = validators.String(not_empty=True)
    state = validators.String(not_empty=True)

class CdnForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True

    url_str = validators.String(not_empty=True)

class DomainForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = False
    filter_extra_fields = True
    ignore_key_missing = True

    g_key = validators.String(not_empty=True)
    g_s = validators.String(not_empty=True)

class SetDomainForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = False
    ignore_key_missing = False

    g_key = validators.String(not_empty=True)
    g_s = validators.String(not_empty=True)
    step = validators.String(not_empty=True)

class VersionCheckForm(formencode.Schema):
    """FunctionCreate 檢查"""
    allow_extra_fields = True
    filter_extra_fields = False
    ignore_key_missing = False

    pro_domain = validators.String(not_empty=True)
    test_domain = validators.String(not_empty=True)






