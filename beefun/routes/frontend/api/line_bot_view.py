# coding=utf8
from __future__ import unicode_literals

import json
import transaction
import uuid
from . import line_bot_form as form
from ....base.base_view import BaseView
from ....lib.my_exception import MyException

from ....dc_module.member.member_service import MemberService
from ....dc_module.john_work.john_work_service import ClientService, CaseService, RichMenuService

import datetime
import pytz
import requests

import os, urllib
import calendar
import time
from linebot import LineBotApi, WebhookHandler
from linebot.models import *

# import telegram

# from telegram.ext import Dispatcher, MessageHandler, Filters

#測試用設定
line_bot_api_code = "flfZdCgM8gGIFNE1m3gpvPLOpgvGnEg0ySH8h7JwSz2OxQOIyaqqGIy/T4a2yR5JBlVjnlqnyN7HAaJJe2OdPA2uh2oLP67ZrsydmFBGHJkR/0WLSX88mHPZC4egiUp6X6jKcBAfOcaK8QrIv7SORQdB04t89/1O/w1cDnyilFU="

# line_bot_api = LineBotApi(
#     'flfZdCgM8gGIFNE1m3gpvPLOpgvGnEg0ySH8h7JwSz2OxQOIyaqqGIy/T4a2yR5JBlVjnlqnyN7HAaJJe2OdPA2uh2oLP67ZrsydmFBGHJkR/0WLSX88mHPZC4egiUp6X6jKcBAfOcaK8QrIv7SORQdB04t89/1O/w1cDnyilFU=')
# handler = WebhookHandler('eede4ac7dec6062c24bf92d6c4774e91')

#
client_id = "dsbqqRfFcCXzAsBAuB2O1o"
client_secret = "jaRsHFqJ63YvhsC0Rfy4HLSUuie1joJYwWDagW77DiC"
redirect_uri = "https://9960fbcaed98.ngrok.io/api/callback/notify"

class LineBotJsonView(BaseView):
    def __init__(self, request):
        super(LineBotJsonView, self).__init__(request)
        self.member_service = MemberService(self.session)
        repository_path = self.request.registry.settings['repository_path']
        self.client_service = ClientService(self.session, logger=self.logger)
        self.case_service = CaseService(self.session, logger=self.logger)

    def line_bot_control(self):
        """
        創建帳單
        """
        _ = self.localizer
        app_id = self.request.matchdict['app_id']

        if app_id=='U4834c602302e8d2dba524aca5a519c26':
            line_bot_api='8CWMhBqflor/Fd0m3scdWsdksw7xxyKJwJQs+E5JQ2fB6ZtPmqJZGRPl7PaMSeLNzWge76obE8m6ij8ZnL9ghHatdtvAZwtrT6AWt6FYU4f+38F5K9Bo1jFOZPmbn2ayEoyjRnve0czlbJRhJi0iQgdB04t89/1O/w1cDnyilFU='
            handler='4268f090f709de91b03f492e55552d65'
        else:
            line_bot_api = 'flfZdCgM8gGIFNE1m3gpvPLOpgvGnEg0ySH8h7JwSz2OxQOIyaqqGIy/T4a2yR5JBlVjnlqnyN7HAaJJe2OdPA2uh2oLP67ZrsydmFBGHJkR/0WLSX88mHPZC4egiUp6X6jKcBAfOcaK8QrIv7SORQdB04t89/1O/w1cDnyilFU='
            handler = 'eede4ac7dec6062c24bf92d6c4774e91'

        line_bot_api = LineBotApi(line_bot_api)
        handler = WebhookHandler(handler)

        try:
            if self.request.json_body['events'][0]['type'] == 'follow':
                pass
                # self.join_add(self.request.json_body['events'][0]['source']['userId'])
                # data_set = {
                #     'lien_id':self.request.json_body['events'][0]['source']['userId']
                # }
                #
                # client_id = os.environ['NOTIFY_CLIENT_ID']
                # client_secret = os.environ['NOTIFY_CLIENT_SECRET']
                #
                # ## 你的回傳連結
                # redirect_uri = f"https://{os.environ['YOUR_HEROKU_APP_NAME']}.herokuapp.com/callback/notify"
                #
                # data = {
                #     'response_type': 'code',
                #     'client_id': client_id,
                #     'redirect_uri': redirect_uri,
                #     'scope': 'notify',
                #     'state': self.request.json_body['events'][0]['source']['userId']
                # }
                # query_str = urllib.parse.urlencode(data)
                #
                # # return f'https://notify-bot.line.me/oauth/authorize?{query_str}'
                #
                # with transaction.manager:
                #
                #     self.member_service.create(**data_set)
                #     buttons_template_message = TemplateSendMessage(
                #         alt_text='建立連結',
                #         template=ButtonsTemplate(
                #             thumbnail_image_url=self.request.application_url + '/line_bot_backend/assets/image/2.jpg',
                #             text='建立連結',
                #             actions=[
                #                 URIAction(
                #                     label='連結',
                #                     uri='https://notify-bot.line.me/oauth/authorize?'+query_str
                #                 )
                #
                #             ]
                #         )
                #     )
                #
                #     line_bot_api.push_message(self.request.json_body['events'][0]['source']['userId'],
                #                               buttons_template_message)


            elif self.request.json_body['events'][0]['type'] == 'message':

                if self.request.json_body['events'][0]['source']['userId'] not in ['U4834c602302e8d2dba524aca5a519c26']:
                    client_obj = self.client_service.get_by(line_id=self.request.json_body['events'][0]['source']['userId'])
                    if client_obj:
                        request_data = {
                            'case_name':self.request.json_body['events'][0]['message']['text'],
                            'line_id': self.request.json_body['events'][0]['source']['userId']
                        }

                        with transaction.manager:
                            case_obj = self.case_service.create(**request_data)
                            reply_token = self.request.json_body['events'][0]['replyToken']
                            print(reply_token)
                            # reply_text = '案名：' + self.request.json_body['events'][0]['message']['text'] +',已通報'
                            # line_bot_api.reply_message(reply_token, TextSendMessage(text=reply_text))

                            token = "VodbvDDlQe6flSxiMRIKEDoNnQLXubpg1PH8fSy59mv"

                            headers = {
                                "Authorization": "Bearer " + token,
                                "Content-Type": "application/x-www-form-urlencoded"
                            }

                            msg = "\n站台編號：" + str(app_id)+'\n業主：'+str(client_obj.client_name)+'\n訊息內容：\n'+str(case_obj.case_name)

                            payload = {'message': msg}
                            r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)
                else:
                    token = "VodbvDDlQe6flSxiMRIKEDoNnQLXubpg1PH8fSy59mv"

                    headers = {
                        "Authorization": "Bearer " + token,
                        "Content-Type": "application/x-www-form-urlencoded"
                    }

                    msg = "\n你不是客戶\n"+str(self.request.json_body['events'][0]['source']['userId'])

                    payload = {'message': msg}
                    r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)


                # if self.request.json_body['events'][0]['message']['text'] == '安安':
                #     buttons_template_message = TemplateSendMessage(
                #         alt_text='測試',
                #         template=ButtonsTemplate(
                #             thumbnail_image_url=self.request.application_url + '/line_bot_backend/assets/image/2.jpg',
                #             text='測試',
                #             actions=[
                #                 PostbackAction(
                #                     label='一期',
                #                     text='一期',
                #                     data='one'
                #                 ),
                #                 PostbackAction(
                #                     label='二期',
                #                     text='二期！',
                #                     data='two'
                #                 ),
                #                 PostbackAction(
                #                     label='三期',
                #                     text='三期',
                #                     data='three'
                #                 )
                #             ]
                #         )
                #     )
                #
                #     line_bot_api.push_message(self.request.json_body['events'][0]['source']['userId'],
                #                               buttons_template_message)
                #
                # elif '一期' in self.request.json_body['events'][0]['message']['text']:
                #     buttons_template_message = TemplateSendMessage(
                #         alt_text='測試一',
                #         template=ButtonsTemplate(
                #             thumbnail_image_url=self.request.application_url + '/line_bot_backend/assets/image/2.jpg',
                #             text='測試一',
                #             actions=[
                #                 PostbackAction(
                #                     label='搜尋',
                #                     text='搜尋',
                #                     data='one_s'
                #                 ),
                #                 PostbackAction(
                #                     label='說明',
                #                     text='說明',
                #                     data='one_g'
                #                 )
                #             ]
                #         )
                #     )
                #
                #     line_bot_api.push_message(self.request.json_body['events'][0]['source']['userId'],
                #                               buttons_template_message)
                #
                # elif '二期' in self.request.json_body['events'][0]['message']['text']:
                #     buttons_template_message = TemplateSendMessage(
                #         alt_text='測試二',
                #         template=ButtonsTemplate(
                #             thumbnail_image_url=self.request.application_url + '/line_bot_backend/assets/image/2.jpg',
                #             text='測試二',
                #             actions=[
                #                 PostbackAction(
                #                     label='搜尋',
                #                     text='搜尋',
                #                     data='two_s'
                #                 ),
                #                 PostbackAction(
                #                     label='說明',
                #                     text='說明',
                #                     data='two_g'
                #                 )
                #             ]
                #         )
                #     )
                #
                #     line_bot_api.push_message(self.request.json_body['events'][0]['source']['userId'],
                #                               buttons_template_message)
                #
                # elif '三期' in self.request.json_body['events'][0]['message']['text']:
                #     buttons_template_message = TemplateSendMessage(
                #         alt_text='測試三',
                #         template=ButtonsTemplate(
                #             thumbnail_image_url=self.request.application_url + '/line_bot_backend/assets/image/2.jpg',
                #             text='測試三',
                #             actions=[
                #                 PostbackAction(
                #                     label='搜尋',
                #                     text='搜尋',
                #                     data='three_s'
                #                 ),
                #                 PostbackAction(
                #                     label='說明',
                #                     text='說明',
                #                     data='three_s'
                #                 )
                #             ]
                #         )
                #     )
                #
                #     line_bot_api.push_message(self.request.json_body['events'][0]['source']['userId'],
                #                               buttons_template_message)

            elif self.request.json_body['events'][0]['type'] == 'postback':
                pass
                # if self.request.json_body['events'][0]['postback']['data'] == 'one':
                #
                #     buttons_template_message = TemplateSendMessage(
                #         alt_text='測試一',
                #         template=ButtonsTemplate(
                #             thumbnail_image_url=self.request.application_url + '/line_bot_backend/assets/image/2.jpg',
                #             text='測試一',
                #             actions=[
                #                 PostbackAction(
                #                     label='搜尋',
                #                     text='搜尋',
                #                     data='one_s'
                #                 ),
                #                 PostbackAction(
                #                     label='說明',
                #                     text='說明',
                #                     data='one_g'
                #                 )
                #             ]
                #         )
                #     )
                #
                #     line_bot_api.push_message(self.request.json_body['events'][0]['source']['userId'],
                #                               buttons_template_message)
                #
                # elif self.request.json_body['events'][0]['postback']['data'] == 'two':
                #
                #     buttons_template_message = TemplateSendMessage(
                #         alt_text='測試二',
                #         template=ButtonsTemplate(
                #             thumbnail_image_url=self.request.application_url + '/line_bot_backend/assets/image/2.jpg',
                #             text='測試二',
                #             actions=[
                #                 PostbackAction(
                #                     label='搜尋',
                #                     text='搜尋',
                #                     data='two_s'
                #                 ),
                #                 PostbackAction(
                #                     label='說明',
                #                     text='說明',
                #                     data='two_g'
                #                 )
                #             ]
                #         )
                #     )
                #
                #     line_bot_api.push_message(self.request.json_body['events'][0]['source']['userId'],
                #                               buttons_template_message)
                #
                # elif self.request.json_body['events'][0]['postback']['data'] == 'three':
                #
                #     buttons_template_message = TemplateSendMessage(
                #         alt_text='測試三',
                #         template=ButtonsTemplate(
                #             thumbnail_image_url=self.request.application_url + '/line_bot_backend/assets/image/2.jpg',
                #             text='測試三',
                #             actions=[
                #                 PostbackAction(
                #                     label='搜尋',
                #                     text='搜尋',
                #                     data='three_s'
                #                 ),
                #                 PostbackAction(
                #                     label='說明',
                #                     text='說明',
                #                     data='three_s'
                #                 )
                #             ]
                #         )
                #     )
                #
                #     line_bot_api.push_message(self.request.json_body['events'][0]['source']['userId'],
                #                               buttons_template_message)
                #
                # if self.request.json_body['events'][0]['postback']['data'] == 'one_s':
                #     text_message = TextSendMessage(text='這裡是第一期搜尋')
                #     line_bot_api.push_message(self.request.json_body['events'][0]['source']['userId'], text_message)
                # elif self.request.json_body['events'][0]['postback']['data'] == 'one_g':
                #     text_message = TextSendMessage(text='這裡是第一期說明')
                #     line_bot_api.push_message(self.request.json_body['events'][0]['source']['userId'], text_message)
                # elif self.request.json_body['events'][0]['postback']['data'] == 'two_s':
                #     text_message = TextSendMessage(text='這裡是第二期搜尋')
                #     line_bot_api.push_message(self.request.json_body['events'][0]['source']['userId'], text_message)
                # elif self.request.json_body['events'][0]['postback']['data'] == 'two_g':
                #     text_message = TextSendMessage(text='這裡是第二期說明')
                #     line_bot_api.push_message(self.request.json_body['events'][0]['source']['userId'], text_message)
                # elif self.request.json_body['events'][0]['postback']['data'] == 'three_s':
                #     text_message = TextSendMessage(text='這裡是第三期搜尋')
                #     line_bot_api.push_message(self.request.json_body['events'][0]['source']['userId'], text_message)
                # elif self.request.json_body['events'][0]['postback']['data'] == 'three_g':
                #     text_message = TextSendMessage(text='這裡是第三期說明')
                #     line_bot_api.push_message(self.request.json_body['events'][0]['source']['userId'], text_message)
        except:
            pass

        return {}

    def callback_notify(self):
        """
        創建帳單
        """
        _ = self.localizer

        user_id = 'U20319446610c6245d9f48cfed77d8a16'

        assert self.request.headers['referer'] == 'https://notify-bot.line.me/'

        request_data = form.NForm().to_python(self.request.params)

        code = request_data.get('code')
        state = request_data.get('state')

        # 接下來要繼續實作的函式
        access_token = self.get_token(code, client_id, client_secret, redirect_uri)

        with transaction.manager:

            member_obj = self.member_service.get_by(line_id=state)
            self.member_service.update(old_obj=member_obj,notify_token=access_token)

        return {}

    def check_notify(self):
        """
        創建帳單
        """
        _ = self.localizer

        token = "GMau6kuienpLUnaSgq1bTDlFp85BqTl2NRv2du9fcZs"

        headers = {
            "Authorization": "Bearer " + token,
            "Content-Type": "application/x-www-form-urlencoded"
        }

        msg = 'dasdasdasdasdasdasda'

        payload = {'message': msg}
        r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)

        return {}

    def notify_one(self):
        """
        創建帳單
        """
        _ = self.localizer

        import os, urllib

        text_message = '連線成功'
        access_token = 'nb9LcKIGPrP8EObVKBbaQTQKdtv8NBMigfpDxE6tEhT'
        url = 'https://notify-api.line.me/api/notify'
        headers = {"Authorization": "Bearer " + access_token}

        data = {'message': text_message}

        data = urllib.parse.urlencode(data).encode()
        req = urllib.request.Request(url, data=data, headers=headers)
        page = urllib.request.urlopen(req).read()
        return {}

    @classmethod
    def get_token(cls, code='', client_id=client_id, client_secret=client_secret, redirect_uri=redirect_uri):
        url = 'https://notify-bot.line.me/oauth/token'
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        data = {
            'grant_type': 'authorization_code',
            'code': code,
            'redirect_uri': redirect_uri,
            'client_id': client_id,
            'client_secret': client_secret
        }
        data = urllib.parse.urlencode(data).encode()
        req = urllib.request.Request(url, data=data, headers=headers)
        page = urllib.request.urlopen(req).read()

        res = json.loads(page.decode('utf-8'))
        return res['access_token']

    def get_notify_token(self, code='', client_id=client_id, client_secret=client_secret, redirect_uri=redirect_uri):
        line_id = 'U20319446610c6245d9f48cfed77d8a16'

        assert self.request.headers['referer'] == 'https://notify-bot.line.me/'

        request_data = form.NForm().to_python(self.request.params)

        code = request_data.get('code')
        state = request_data.get('state')

        # 接下來要繼續實作的函式
        notify_token = self.get_token(code, client_id, client_secret, redirect_uri)

        return notify_token

    def join_add(self, line_id):
        data_set = {
            'lien_id': line_id
        }

        client_id = os.environ['NOTIFY_CLIENT_ID']
        client_secret = os.environ['NOTIFY_CLIENT_SECRET']

        data = {
            'response_type': 'code',
            'client_id': client_id,
            'redirect_uri': redirect_uri,
            'scope': 'notify',
            'state': line_id
        }
        query_str = urllib.parse.urlencode(data)

        # return f'https://notify-bot.line.me/oauth/authorize?{query_str}'

        with transaction.manager:
            self.member_service.create(**data_set)
            buttons_template_message = TemplateSendMessage(
                alt_text='建立連結',
                template=ButtonsTemplate(
                    thumbnail_image_url=self.request.application_url + '/line_bot_backend/assets/image/2.jpg',
                    text='建立連結',
                    actions=[
                        URIAction(
                            label='連結',
                            uri='https://notify-bot.line.me/oauth/authorize?' + query_str
                        )

                    ]
                )
            )

            line_bot_api.push_message(line_id,
                                      buttons_template_message)

        return ''

    def check_dns_who(self):
        """
        創建帳單
        """
        import DNS
        request_data = form.CdnForm().to_python(self.request_params)
        DNS.DiscoverNameServers()
        dns_list = []
        url_list = request_data.get('url_str').split(',')
        for url in url_list:
            if url!='':
                dns = DNS.DnsRequest(url)
                dns_info = dns.req()
                print('----------')
                print(dns_info.answers)
                print('----------')
                if len(dns_info.answers)!=0:
                    if len(dns_info.answers)==1:
                        check_dnx = dns_info.answers[0]['data']
                    else:
                        check_dnx = dns_info.answers[0]['data']

                    if check_dnx.find('1688cdn')!=-1:
                        set_dic = {
                            'dns':'sky',
                            'url': url
                        }
                        dns_list.append(set_dic)
                    elif check_dnx.find('impervadns')!=-1:
                        set_dic = {
                            'dns': 'imperva',
                            'url': url
                        }
                        dns_list.append(set_dic)
                    elif check_dnx.find('wslxdns')!=-1:
                        set_dic = {
                            'dns': 'pdns',
                            'url': url
                        }
                        dns_list.append(set_dic)
                    else:
                        set_dic = {
                            'dns': 'unknow',
                            'url': url
                        }
                        dns_list.append(set_dic)
                else:
                    set_dic = {
                        'dns': '錯誤網址',
                        'url': url
                    }
                    dns_list.append(set_dic)

        return {'response': {'dns_list': dns_list},
                'message':u'成功'}



    def check_dns_who_for_telegram(self):
        """
        創建帳單
        """

        ACCESS_TOKEN = '1975394087:AAEx4poumybSjfzF-RsgMno3ALwDYcwP5O8'

        import telepot

        check=self.request.json['message']['text'].split(':')

        bot = telepot.Bot(ACCESS_TOKEN)

        chat_id = self.request.json['message']['chat']['id']
        try:
            if check[0]=='dns':
                import DNS
                # request_data = form.CdnForm().to_python(self.request_params)
                DNS.DiscoverNameServers()
                dns_list = []
                url_list = check[1].split(',')

                out_str = '傳入參數：' + self.request.json['message']['text'] + ' \n'

                for url in url_list:
                    if url!='':
                        dns = DNS.DnsRequest(url)
                        dns_info = dns.req()
                        if len(dns_info.answers)!=0:
                            if len(dns_info.answers)==1:
                                check_dnx = dns_info.answers[0]['data']
                            else:
                                check_dnx = dns_info.answers[0]['data']

                            if check_dnx.find('1688cdn')!=-1:
                                set_dic = {
                                    'dns':'sky',
                                    'url': url
                                }
                                dns_list.append(set_dic)

                                out_str += '網址：' + url + ', 狀態：正確'+', 廠商：sky \n'

                            elif check_dnx.find('impervadns')!=-1:
                                set_dic = {
                                    'dns': 'imperva',
                                    'url': url
                                }
                                dns_list.append(set_dic)

                                out_str += '網址：' + url + ', 狀態：正確' + ', 廠商：imperva \n'

                            elif check_dnx.find('wslxdns')!=-1:
                                set_dic = {
                                    'dns': 'pdns',
                                    'url': url
                                }
                                dns_list.append(set_dic)

                                out_str += '網址：' + url + ', 狀態：正確' + ', 廠商：pdns \n'

                            else:
                                set_dic = {
                                    'dns': 'unknow',
                                    'url': url
                                }
                                dns_list.append(set_dic)

                                out_str += '網址：' + url + ', 狀態：正確' + ', 廠商：unknow \n'

                        else:
                            set_dic = {
                                'dns': '錯誤網址',
                                'url': url
                            }
                            dns_list.append(set_dic)

                            out_str+='網址：'+url+', 狀態：錯誤網址'

                bot.sendMessage(chat_id, out_str, parse_mode=None, disable_web_page_preview=None, disable_notification=None,
                                reply_to_message_id=None, reply_markup=None)

            elif check[0]=='domain':
                from datetime import datetime
                from godaddypy import Client, Account
                import os

                g_key = check[1].split(',')[0]
                g_s = check[1].split(',')[1]

                my_acct = Account(api_key=g_key, api_secret=g_s)
                client = Client(my_acct)

                domain_list = client.get_domains()

                outlist = []
                d_s = {}
                for a in domain_list:
                    time.sleep(5)
                    my_a = client.get_domain_info(a)

                    a_status = my_a.get('status')

                    a_nameServers = my_a.get('nameServers')

                    try:
                        if a_status != 'ACTIVE':
                            outlist.append(a)
                        else:

                            check_num = 0

                            for sa in a_nameServers:

                                if sa.find('domaincontrol') == -1:
                                    check_num += 1

                            if check_num != 0:
                                outlist.append(a)
                            else:
                                d_s[a] = client.get_records(a)
                    except:
                        outlist.append(a)

                for a in d_s:
                    for c in d_s[a]:
                        if c['data'].find('Parked') == -1 and c['data'].find('domaincontrol') == -1 and c['data'].find(
                                '_domainconnect') == -1 and c['data'].find('@') == -1:
                            outlist.append(a)
                        if c['name'] != '@' and c['name'] != '_domainconnect' and c['name'] != 'www':
                            outlist.append(a)

                        # if c['type'] == 'TXT':
                        #     outlist.append(a)
                out_str = '傳入參數：' + self.request.json['message']['text'] + ' \n'
                out_str +=  'key:'+g_key+' \n'
                count_n = 0
                for a in domain_list:
                    if a not in list(set(outlist)):
                        count_n+=1
                        out_str +=  a + ' \n'

                # out_str += '數量：'+ str(count_n) + ' \n'
                # bot.sendMessage(chat_id, out_str, parse_mode=None, disable_web_page_preview=None, disable_notification=None,
                #                 reply_to_message_id=None, reply_markup=None)

            elif check[0] == 'domain_use':
                from datetime import datetime
                from godaddypy import Client, Account
                import os
                g_key = check[1].split(',')[0]
                g_s = check[1].split(',')[1]
                my_acct = Account(api_key=g_key, api_secret=g_s)
                client = Client(my_acct)
                set_domain_list = check[2].split(',')
                set_dic = {
                    'data': 'IN_USE',
                    'name': 'IN_USE',
                    'ttl': 3600,
                    'type': 'TXT'
                }
                for d in set_domain_list:
                    client.add_record(d, set_dic)

                # out_str = '設定完成'
                # bot.sendMessage(chat_id, out_str, parse_mode=None, disable_web_page_preview=None, disable_notification=None,
                #                 reply_to_message_id=None, reply_markup=None)
                #
                # out_str = '設定完成'
                # bot.sendMessage(chat_id, out_str, parse_mode=None, disable_web_page_preview=None, disable_notification=None,
                #                 reply_to_message_id=None, reply_markup=None)


        except:
            out_str='傳入參數：'+self.request.json['message']['text']+' \n'

            out_str += '參數有誤，查詢失敗'
            bot.sendMessage(chat_id, out_str, parse_mode=None, disable_web_page_preview=None, disable_notification=None,
                            reply_to_message_id=None, reply_markup=None)





        return {'response': {'dns_list': 'ok'},
                'message':u'成功'}

    def check_domain_use_for_telegram(self):
        """
        創建帳單
        """

        request_data = form.DomainForm().to_python(self.request_params)

        from datetime import datetime
        from godaddypy import Client, Account
        import os

        my_acct = Account(api_key=request_data.get('g_key'), api_secret =request_data.get('g_s'))
        client = Client(my_acct)

        domain_list = client.get_domains()

        all_num = len(domain_list)

        outlist = []
        d_s = {}

        count = 0

        for a in domain_list:

            if count!=0 and count%20 ==0:
                time.sleep(40)
            try:
                print(a)
                my_a = client.get_domain_info(a)

                a_status = my_a.get('status')

                a_nameServers = my_a.get('nameServers')


                if a_status != 'ACTIVE':
                    outlist.append(a)
                else:

                    check_num = 0

                    for sa in a_nameServers:

                        if sa.find('domaincontrol')==-1:
                            check_num+=1

                    if check_num!=0:
                        outlist.append(a)
                    else:
                        # time.sleep(10)

                        d_s[a] = client.get_records(a)
            except:

                outlist.append(a)

            count += 1



        for a in d_s:
            for c in d_s[a]:
                if c['data'].find('Parked') == -1 and c['data'].find('domaincontrol') == -1 and c['data'].find(
                        '_domainconnect') == -1 and c['data'].find('@') == -1:
                    outlist.append(a)
                if c['name'] != '@' and c['name'] != '_domainconnect' and c['name'] != 'www':
                    outlist.append(a)

                # if c['type'] == 'TXT':
                #     outlist.append(a)



        ok_d = []
        for a in domain_list:
            if a not in list(set(outlist)):
                ok_d.append(a)



        domain_no_use_list= []

        for a in ok_d:

            set_dic = {
                'domain': a
            }
            domain_no_use_list.append(set_dic)
        out_num = len(domain_no_use_list)

        return {'response': {'domain_list': domain_no_use_list,'mun':out_num,'all_num':all_num},
                'message':u'成功'}

    @staticmethod
    def get_random_8():

        str_8 = str(uuid.uuid4()).split('-')[0]

        return str_8

    def set_use_domain(self):
        """
        創建帳單
        """

        request_data = form.SetDomainForm().to_python(self.request_params)

        from datetime import datetime
        from godaddypy import Client, Account
        import os

        my_acct = Account(api_key=request_data.get('g_key'), api_secret =request_data.get('g_s'))
        client = Client(my_acct)

        ## 編輯網域tag
        set_domain_list = request_data.get('domain_list').split(',')
        set_dic ={
                  'data': request_data.get('step'),
                  'name': request_data.get('step'),
                  'ttl':3600,
                  'type':'TXT'
               }
        for d in set_domain_list:
            client.add_record(d, set_dic)






        return {'response': {},
                'message':u'成功'}

    def set_sre_use_domain(self):
        """
        創建帳單
        """

        request_data = form.SetDomainForm().to_python(self.request_params)

        from datetime import datetime
        from godaddypy import Client, Account
        import os

        my_acct = Account(api_key=request_data.get('g_key'), api_secret =request_data.get('g_s'))
        client = Client(my_acct)

        ## 編輯網域tag
        set_domain_list = request_data.get('domain_list').split(',')
        set_dic ={
                  'data': request_data.get('step'),
                  'name': request_data.get('step'),
                  'ttl':3600,
                  'type':'TXT'
               }
        for d in set_domain_list:
            client.add_record(d, set_dic)

        www_dic = {
            'data': 'f_www',
            'name': 'f_www',
            'ttl': 3600,
            'type': 'TXT'
        }

        client.add_record(request_data.get('www_domain'), www_dic)

        app_dic = {
            'data': 'f_app',
            'name': 'f_app',
            'ttl': 3600,
            'type': 'TXT'
        }

        client.add_record(request_data.get('app_domain'), app_dic)

        admin_dic = {
            'data': 'admin',
            'name': 'admin',
            'ttl': 3600,
            'type': 'TXT'
        }

        client.add_record(request_data.get('admin_domain'), admin_dic)

        lotto_dic = {
            'data': 'lotto',
            'name': 'lotto',
            'ttl': 3600,
            'type': 'TXT'
        }

        client.add_record(request_data.get('lotto_domain'), lotto_dic)

        uat_dic = {
            'data': 'uat',
            'name': 'uat',
            'ttl': 3600,
            'type': 'TXT'
        }

        client.add_record(request_data.get('uat_domain'), uat_dic)


        ## 整理網域字串
        f_www = 'www.'+ request_data.get('www_domain')

        f_app = 'app.'+ request_data.get('app_domain')

        adm_manage_str = self.get_random_8()
        aff_manage_str = self.get_random_8()

        b_adm = 'adm'+adm_manage_str+'.'+ request_data.get('admin_domain')

        b_aff = 'aff'+aff_manage_str+'.'+ request_data.get('admin_domain')

        gck = 'gck.'+request_data.get('lotto_domain')

        gci = 'gci.'+request_data.get('lotto_domain')

        gws = 'gws.'+request_data.get('lotto_domain')

        adm = 'adm.'+request_data.get('lotto_domain')

        uat_www = 'www.' + request_data.get('uat_domain')

        uat_app = 'app.' + request_data.get('uat_domain')

        u_adm_manage_str = self.get_random_8()
        u_aff_manage_str = self.get_random_8()

        uat_adm = 'adm'+u_adm_manage_str+'.' + request_data.get('uat_domain')

        uat_aff = 'aff'+u_aff_manage_str+'.' + request_data.get('uat_domain')

        return {'response': {'www': f_www, 'app':f_app, 'admin':b_adm, 'aff':b_aff
                            ,'gck': gck, 'gci':gci, 'gws':gws, 'lotto_adm':adm
                            ,'uat_www': uat_www, 'uat_app':uat_app, 'uat_adm':uat_adm, 'uat_aff':uat_aff},
                'message':u'成功'}

    def version_check(self):
        """
        創建帳單
        """

        request_data = form.VersionCheckForm().to_python(self.request_params)

        import requests
        import json

        prod_version_f = ''
        prod_version_b = ''
        uat_version_f = ''
        uat_version_b = ''

        prod_url_b = 'http://'+request_data.get('pro_domain')+'/version.json'

        prod_url_f = 'http://'+request_data.get('pro_domain')+'/api/version'

        uat_url_b = 'http://'+request_data.get('test_domain')+'/version.json'

        uat_url_f = 'http://'+request_data.get('test_domain')+'/api/version'


        prod_b = requests.get(prod_url_b,verify=False)

        if prod_b.status_code==200:
            prod_url_b_j = json.loads(prod_b.text)
            prod_version_b = prod_url_b_j.get('GIT_GLOBAL_FRONT_STAGE_VERSION')
        else:
            prod_version_b = '404'

        prod_f = requests.get(prod_url_f,verify=False)

        if prod_f.status_code==200:
            prod_url_f_j = json.loads(prod_f.text)
            prod_version_f = prod_url_f_j.get('version')
        else:
            prod_version_f = '404'

        uat_b = requests.get(uat_url_b,verify=False)

        if uat_b.status_code==200:
            uat_url_b_j = json.loads(uat_b.text)
            uat_version_b = uat_url_b_j.get('GIT_GLOBAL_FRONT_STAGE_VERSION')
        else:
            uat_version_b = '404'

        uat_f = requests.get(uat_url_f,verify=False)

        if uat_f.status_code==200:
            uat_url_f_j = json.loads(uat_f.text)
            uat_version_f = uat_url_f_j.get('version')
        else:
            uat_version_f = '404'

        return {'response': {'prod_version_f':prod_version_f,'prod_version_b':prod_version_b,
                             'uat_version_f':uat_version_f,'uat_version_b':uat_version_b},
                'message':u'成功'}

    def file_version_check(self):
        """
        創建帳單
        """

        open_time = datetime.datetime.utcnow()

        repository_path = self.request.registry.settings['repository_path']

        version_files = self.request_params.getall('files')

        import pandas as pd
        import shutil
        import json
        import os
        import math
        from PIL import Image as PIL_Image
        from PIL import ImageDraw, ImageFont, ImageEnhance
        file_path = repository_path+'/dddd.csv'
        with open(file_path, 'wb+') as f:
            f.write(version_files[0].file.read())

        df = pd.read_csv(file_path, keep_default_na=False)

        df.status.fillna('00000')
        df.code.fillna('00000')
        df.prod_d.fillna('00000')
        df.uat_d.fillna('00000')
        if len(df)<50:
            df_l = df.to_dict()

            # df_keys = df_l['status'].keys()

            out_list = []

            code_in_list = []

            for k in df_l['status'].keys():
                print(df_l['code'][k])
                if df_l['code'][k]!='':
                    if df_l['code'][k] not in code_in_list:
                        code_in_list.append(df_l['code'][k])

                        ##正式機檢查機制
                        if df_l['prod_d'][k]!='':
                            prod_version_f = ''
                            prod_version_b = ''

                            prod_url_b = 'http://' + df_l['prod_d'][k] + '/version.json'

                            prod_url_f = 'http://' + df_l['prod_d'][k] + '/api/version'

                            try:
                                prod_b = requests.get(prod_url_b,verify=False)
                                if prod_b.status_code==200:
                                    if 'html' not in prod_b.text:
                                        prod_url_b_j = json.loads(prod_b.text)
                                        prod_version_b = prod_url_b_j.get('GIT_GLOBAL_FRONT_STAGE_VERSION')
                                    else:
                                        prod_version_b = '404'
                                else:
                                    prod_version_b = '404'
                            except:
                                prod_version_b = '404'


                            try:
                                prod_f = requests.get(prod_url_f, verify=False)
                                if prod_f.status_code == 200:
                                    if 'html' not in prod_f.text:
                                        prod_url_f_j = json.loads(prod_f.text)
                                        prod_version_f = prod_url_f_j.get('version')
                                    else:
                                        prod_version_f = '404'
                                else:
                                    prod_version_f = '404'
                            except:
                                prod_version_f = '404'


                        else:
                            prod_version_f=''
                            prod_version_b = ''

                        if df_l['uat_d'][k]!='':
                            ##測試機檢查機制
                            uat_version_f = ''
                            uat_version_b = ''

                            uat_url_b = 'http://' + df_l['uat_d'][k] + '/version.json'

                            uat_url_f = 'http://' + df_l['uat_d'][k] + '/api/version'
                            try:
                                uat_b = requests.get(uat_url_b, verify=False)
                                if uat_b.status_code == 200:
                                    if 'html' not in uat_b.text:
                                        uat_url_b_j = json.loads(uat_b.text)
                                        uat_version_b = uat_url_b_j.get('GIT_GLOBAL_FRONT_STAGE_VERSION')
                                    else:
                                        uat_version_b = '404'
                                else:
                                    uat_version_b = '404'
                            except:
                                uat_version_b = '404'

                            try:
                                uat_f = requests.get(uat_url_f, verify=False)
                                if uat_f.status_code == 200:
                                    if 'html' not in uat_f.text:
                                        uat_url_f_j = json.loads(uat_f.text)

                                        uat_version_f = uat_url_f_j.get('version')
                                    else:
                                        uat_version_f = '404'
                                else:
                                    uat_version_f = '404'
                            except:
                                uat_version_f = '404'

                        else:
                            uat_version_f=''
                            uat_version_b = ''

                        ## 物件打包
                        the_dic = {
                            'code': df_l['code'][k],
                            'prod_version_f':prod_version_f,
                            'prod_version_b': prod_version_b,
                            'uat_version_f': uat_version_f,
                            'uat_version_b': uat_version_b,
                        }

                        print(df_l['prod_d'][k])
                        print(df_l['uat_d'][k])
                        out_list.append(the_dic)

            os.remove(file_path)

            close_time = datetime.datetime.utcnow()

            all_time = str(close_time - open_time)

            return {'response': {'out_list':out_list, 'all_time': all_time},
                    'message':u'成功'}

        else:
            os.remove(file_path)

            return {'response': {},
                    'message': u'數量超過50'}

