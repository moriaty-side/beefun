# coding=utf8
from __future__ import unicode_literals


def includeme(config):
    config.include('.base_router')
    config.include('.frontend')
    config.include('.linebot')
