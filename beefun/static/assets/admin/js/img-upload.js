class ImgUpload {
  constructor(el) {
      const ele = $(el);
      this.el = ele;
      this.init();
  }
  init(){
      this.event();
  }
  event(){
      const _this = this;
      this.el.on('change',function(e){
          _this.readURL(this)
      })
  }
  readURL(el){
      const _this = this;
      if(el.files && el.files[0]){
          var reader = new FileReader();
          reader.onload = function (e) {
              _this.el.parent().siblings().empty();
              let img = $("<img>").attr('src', e.target.result).addClass("img-fluid");
              _this.el.parent().siblings().append(img);
          }
          reader.readAsDataURL(el.files[0]);
        }
  }
}

window.ImgUpload = ImgUpload;