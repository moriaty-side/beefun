(function($) {

    $.cropper_plugin = function cropper_plugin(options, callback, element) {

      this.bind_element = $(element);
      this._init(options, callback);
    };

    $.cropper_plugin.defaults = {
      url: '',
      csrfToken: '',
      cropper_window_id:'',
      filename:'',
      image_path:'',
      image_ratio:'',
      image_cut:'true',
      cut_btn_only: 'false',
      image_size:'',
      bind_element_window:'body'
      // cropper_window:'',
    };

    var generateUUID = function () {
      var d = new Date().getTime();
      var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          var r = (d + Math.random()*16)%16 | 0;
          d = Math.floor(d/16);
          return (c=='x' ? r : (r&0x3|0x8)).toString(16);
      });
      return uuid;
    };


    var updata_box_tmplate_model = function(uuid,image_path,image_ratio,image_cut,image_size){
      var template_str =
      // '<div class="form-group">'+
      //   '<label class="control-label col-xs-4"><span class="required">*</span>圖片：</label>'+
      //   '<div class="col-xs-7 val-bg">'+
          '<div class="cropper-oragin '+((image_size)?image_size:"")+'" '+((image_ratio)?'data-ratio="'+image_ratio:'')+'" '+((image_cut)?'data-cut="'+image_cut:'false')+'">'+
              '<div class="thumbnail">'+
                  '<img src="'+image_path+'" style="width: 210px;" class="my_image_data" id="banner_image"/>'+
              '</div>'+
              '<div class="cropper-btn-tool">'+
                  '<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput">移除</a>'+
                  '<div class="btn purple btn-input btn-upload-input" data-uuid="'+uuid+'">'+
                      '<i class="fa fa-photo"></i> '+
                      '<span class="tager_word">上傳圖片</span>'+
                      '<input type="file" class="upload" value="Choose a file" accept="image/*" />'+
                  '</div>'+
              '</div>'+
          '</div>';
      //   '</div>'+
      // '</div>';
      return template_str;
    };

    var cropper_window_tmplate_model = function(uuid){

      var template_str =
      '<div id="'+uuid+'" data-backdrop="static" data-keybord="false" class="modal cropper_window" role="dialog" aria-labelledby="myModalLabel10" style="display: none;">'+
        '<div class="modal-dialog modal-lg">'+
          '<div class="modal-content">'+
            '<div class="bounce-content">'+
              '<div class="modal-header">'+
                  '<button type="button" class="close" data-dismiss="modal"></button>'+
                  '<h4 class="modal-title">圖片裁切</h4>'+
              '</div>'+
              '<div class="modal-body">'+
                  '<form action="#" class="form-horizontal" role="form">'+
                      '<div class="row version">'+
                          '<div class="col-xs-offset-2 col-xs-8 mb-10">'+
                            '<div class="cropper-content-area">'+
                                '<div class="cropper-content">'+
                                    '<img width="100%" class="cropper-image" src="https://1.bp.blogspot.com/-bp0OIfjaWSI/VXEeanw7XEI/AAAAAAAAfmA/Ben_gtNvqAU/s640/05.jpg" alt="" data-src="https://1.bp.blogspot.com/-bp0OIfjaWSI/VXEeanw7XEI/AAAAAAAAfmA/Ben_gtNvqAU/s640/05.jpg" class="img-responsive">'+
                                '</div>'+
                            '</div>'+
                          '</div>'+
                      '</div>'+
                      '<div class="no-line modal-footer">'+
                          '<button type="button" class="btn default" data-dismiss="modal"><i class="fa fa-ban"></i> 取消</button>'+
                          '<button type="button" class="btn blue-steel cropper-btn-save" data-type="footer"><i class="fa fa-check"></i> 直接儲存</button>'+
                          '<button type="button" class="btn red cropper-btn-cut-save" data-type="footer"><i class="fa fa-crop"></i> 裁切並儲存</button>'+
                      '</div>'+
                    '</form>'+
                '</div>'+
              '</div>'+
          '</div>'+
        '</div>'+
      '</div>';

      return template_str;
    };

    $.cropper_plugin.cropper_window;

    $.cropper_plugin.prototype = {

      // 初始值
      _init: function cropper_plugin_init(options, callback) {


        var instance = this, opts = this.options = $.extend({}, $.cropper_plugin.defaults, options);
        // $.cropper_plugin.defaults = opts;
        // 開始做事

        // $.cropper_plugin.defaults.cropper_window = $('#'+$.cropper_plugin.defaults.cropper_window_id);
        // 最後回呼，有cb 則會回呼
        // if($.isFunction(callback)) callback(this);

      },
      _image_upload: function (image_base64,image_filename, cb) {
            var formData = new FormData();
            formData.append('image_base64', JSON.stringify({'data':image_base64.split(',')[1],'filename':image_filename }) );
            var request_setting = {
                url: this.options.url,
                type:'POST',
                data: formData,
                processData: false,
                contentType: false,
                headers: {'X-CSRF-Token': csrfToken},
                success: function (data) {
                  var msg = data['message'];

                  if(data['status'] == true && data['code'] == 200){
                      var image_response = data["response"]["image_list"][0];
                      // console.log(msg);
                      // console.log(image_response);
                      cb(image_response);
                  }else{
                    alert(msg);
                  }
                },
                beforeSend: function(){
                      $('.load_plugin').removeClass('hide');
                },
                error: function (e) {
                    // console.log(e.responseText);
                },
                complete: function () {
                    // console.log("post done");
                    $('.load_plugin').addClass('hide');
                    $('.cropper-btn-cut-save').attr('disabled', false);
                }
            };
            if(this.options.csrfToken)
              request_setting.headers['X-CSRF-Token']=this.options.csrfToken;

            $.ajax(request_setting);
        }
    };

     $.fn.cropper_plugin = function(input_options, callback) {

      input_options = (typeof input_options ==="undefined")?{}:input_options;

      return $(this).each(function() {

        var bind_element = $(this);
        var uuid = generateUUID();

        if(!bind_element.find('.cropper-oragin').length)
          {
            // console.log(uuid);
            input_options.cropper_window_id= uuid;

            var bind_element_options={
                image_path:bind_element.data('img'),
                image_ratio:bind_element.data('ratio'),
                image_cut:bind_element.data('cut'),
                image_size:bind_element.data('size'),

            }
            // console.log(input_options);
            // console.log(bind_element_options)

            options = $.extend({}, bind_element_options, input_options);
            // console.log(options)
            var new_obj = new $.cropper_plugin(options,callback, bind_element);
            // console.log(new_obj)
            new_obj.bind_element.append(updata_box_tmplate_model(uuid,new_obj.options.image_path,
                                                                      new_obj.options.image_ratio,
                                                                      new_obj.options.image_cut,
                                                                      new_obj.options.image_size));
            // 事件觸發對應視窗
            new_obj.bind_element.on('change','.btn-upload-input',function(){

                var cropper_window_id = $(this).data('uuid');

                var $oragin = $(this).closest('.cropper-oragin');
                $oragin.addClass('active-cropper');
                var ratio = eval($oragin.attr('data-ratio'));
                var cut = ( $oragin.attr('data-cut') == 'true' );
                var $input = $(this).find('input');
                // jq上傳圖片
                // 這邊用base64裁圖會變慢 所以要先上傳圖片回傳網址裁圖
                var file = $(this).find('input')[0].files[0];
                new_obj.options.filename = file.name;

                // var formData = new FormData();
                // formData.append('image_file',file)

                var reader = new FileReader();
                reader.onloadend = function() {
                    var url = reader.result;
                    // callback
                    var $cropper_window = $('#'+cropper_window_id);
                    var $cropper = $('#'+cropper_window_id).find('.cropper-image');
                    $cropper.attr('src', url);
                    $cropper.attr('data-src', url);
                    $cropper.cropper('destroy');
                    $cropper.cropper({
                      aspectRatio: ratio,
                      viewMode:3,
                      // 關閉圖片縮放
                      zoomable:false,
                      // 關閉一開始就有剪裁筐
                      // autoCrop:false,
                      minContainerWidth:2,
                      minContainerHeight:1,
                    });
                    $input.val('');
                    if(!cut){
                      $cropper_window.find('.cropper-btn-save').hide();
                    }else{
                      $cropper_window.find('.cropper-btn-save').show();
                    }
                    $cropper_window.modal('show');
                    // end callback
                }
                reader.readAsDataURL(file);
            });
            //刪除圖片
            new_obj.bind_element.on('click', '.fileinput-exists', function (e) {
                e.preventDefault();
                $(this).parent().find("span").text("上傳圖片");
                $(this).parent().parent().find("img").attr("src",new_obj.options.image_path);
                $(this).parent().parent().find("img").removeAttr("image_id");
                // $(this).css('display','none');
            });

            $(new_obj.options.bind_element_window).append(cropper_window_tmplate_model(uuid));
            var $cropper_window = $('#'+new_obj.options.cropper_window_id);
            // 裁切圖片(會產出新網址)
            // console.log($cropper_window )
            $cropper_window.on('click','.cropper-btn-cut-save',function(){

                // console.log("123123");
                var event_btn = $(this)
                // 取得啟動的圖片
                var $cropper = $cropper_window.find('.cropper-image');
                // 圖片連結
                var src_base64_image = $cropper.attr('src');
                // 將圖片連結更換為裁切後的base64碼網址
                var new_base64_image = $cropper.cropper('getCroppedCanvas').toDataURL(src_base64_image);

                // console.log( new_obj.options.filename);

                event_btn.attr("disabled", true);
                // 如果有網址進行上傳
                if(new_obj.options.url && callback)
                {
                  new_obj._image_upload(new_base64_image,new_obj.options.filename,function(new_images_data){
                    callback(new_images_data,$cropper_window);
                    event_btn.attr("disabled", false);
                  });
                }else if(new_obj.options.url && !callback){
                  new_obj._image_upload(new_base64_image,new_obj.options.filename,function(new_images_data){
                    // callback
                    var new_image_link = "https://cdn.okborn.com"+new_images_data['filepath']+new_images_data['filename'];
                    $(new_obj.bind_element.find('.cropper-oragin.active-cropper a')).css('display','inline');
                    $(new_obj.bind_element.find('.cropper-oragin.active-cropper span')).text('重置');
                    $(new_obj.bind_element.find('.cropper-oragin.active-cropper img')).attr('src', new_image_link).closest('.active-cropper').removeClass('active-cropper');
                    $cropper_window.modal('hide');
                    // end callback
                      event_btn.attr("disabled", false);
                  });
                }else if(!new_obj.options.url && callback){
                  callback(new_base64_image,$cropper_window);
                    event_btn.attr("disabled", false);
                }else{
                   $(new_obj.bind_element.find('.cropper-oragin.active-cropper img')).attr('src', new_base64_image).closest('.active-cropper').removeClass('active-cropper');
                   $cropper_window.modal('hide');
                    event_btn.attr("disabled", false);
                }
            });
            // 不裁切圖片(會產出新網址)
            $cropper_window.on('click','.cropper-btn-save',function(){

                // console.log("dfsdfs")
                // 取得啟動的圖片
                var $cropper = $cropper_window.find('.cropper-image');
                 // 圖片連結
                var new_base64_image = src_base64_image = $cropper.attr('src');

                // 如果有網址進行上傳
                if(new_obj.options.url && callback)
                {
                  new_obj._image_upload(new_base64_image,new_obj.options.filename,function(new_images_data){
                    callback(new_images_data,$cropper_window);
                  });
                }else if(new_obj.options.url && !callback){
                  new_obj._image_upload(new_base64_image,new_obj.options.filename,function(new_images_data){
                    // callback
                    var new_image_link = "https://cdn.okborn.com"+new_images_data['filepath']+new_images_data['filename'];
                    $(new_obj.bind_element.find('.cropper-oragin.active-cropper a')).css('display','inline');
                    $(new_obj.bind_element.find('.cropper-oragin.active-cropper span')).text('重置');
                    $(new_obj.bind_element.find('.cropper-oragin.active-cropper img')).attr('src', new_image_link).closest('.active-cropper').removeClass('active-cropper');
                    $cropper_window.modal('hide');
                    // end callback
                  });
                }else if(!new_obj.options.url && callback){
                  callback(new_base64_image,$cropper_window);
                }else{

                   $(new_obj.bind_element.find('.cropper-oragin.active-cropper img')).attr('src', new_base64_image).closest('.active-cropper').removeClass('active-cropper');
                   $cropper_window.modal('hide');

                   // $('#bounce-window #bounce-content').show();
                }
            })
          }
      });
    };

})(jQuery);