$(function (){

    /**
     * 格式化
     * @param   num 要轉換的數字
     * @param   pos 指定小數第幾位做四捨五入
     */
    function format_float(num, pos)
    {
        var size = Math.pow(10, pos);
        return Math.round(num * size) / size;
    }
 
    /**
     * 預覽圖
     * @param   input 輸入 input[type=file] 的 this
     */
    function preview(input) {
        // 若有選取檔案
        if (input.files && input.files[0]) {
            // 建立一個物件，使用 Web APIs 的檔案讀取器(FileReader 物件) 來讀取使用者選取電腦中的檔案
            var reader = new FileReader();

            // 事先定義好，當讀取成功後會觸發的事情
            reader.onload = function (e) {

                var image = new Image();
                image.src = e.target.result;
                image.onload = function () {
                    var fix_width = $('input[name="image"]').attr('fix_width');
                    var fix_height = $('input[name="image"]').attr('fix_height');

                    if(fix_height && fix_width){
                        if(this.width != fix_width || this.height != fix_height){
                            alert('圖片尺寸錯誤，請上傳 768x461的的圖片');
                            $('input[name="image"]').val(null);
                            $('.preview').attr('src', 'http://fakeimg.pl/768x461');
                            $('.size').text('');
                            return;
                        }
                    }
                    // 這裡看到的 e.target.result 物件，是使用者的檔案被 FileReader 轉換成 base64 的字串格式，
                    // 在這裡我們選取圖檔，所以轉換出來的，會是如 『data:image/jpeg;base64,.....』這樣的字串樣式。
                    // 我們用它當作圖片路徑就對了。
                    $('.preview').attr('src', e.target.result);

                    // 檔案大小，把 Bytes 轉換為 KB
                    var KB = format_float(e.total / 1024, 2);
                    $('.size').text("檔案大小：" + KB + " KB");
                };
            }
            // 因為上面定義好讀取成功的事情，所以這裡可以放心讀取檔案
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("body").on("change", ".upl", function (){
        preview(this);
    })

})