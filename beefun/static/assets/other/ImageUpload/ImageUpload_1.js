(function($){
  $.fn.amzImgUpload = function(set_w,set_h,addTo){
    //新增圖片    
      var cnt,setSize;
      var new_W=320,new_H;
      if(set_w==0 && set_h==0){
        setSize=false;
        new_H=320;
      }else{
        setSize=true;
        new_H = new_W*(set_h/set_w);      
      }
      
      $(this).change(function(){

            cnt=$(addTo+' .row .appendImg').length;
            if(this.disabled) return alert('File upload not supported!');

            var F = this.files;
            
            if(F && F[0]){
              for(var i=0; i<F.length; i++) {           
                  readImage(F[i]);             
              }
            }        
            $('.spinners').removeClass('hide');

            setTimeout(function(){
                $('.spinners').addClass('hide')
              },(F.length-1)*250);
            $(this).val('');
      })
    //read圖片
      var readImage = function(file){
        var reader = new FileReader();
        var image = new Image();
       
        reader.readAsDataURL(file);

        reader.onload = function(_file){

          image.src = _file.target.result;
          
          image.onload = function(){
            var w = this.width,
                h = this.height,
                n = file.name,
                s = ~~(file.size/1024)+'KB',
                big=0,
                portion=1,
                html='';
               
                switch(setSize){
                  case true:
                            w1 = new_W;
                            h1 = new_H;

                            if(w==set_w && h==set_h){
                              appendImg();
                            }
                        break;
                  case false:
                          //找出上傳最大邊
                            big = (w>h)?w:h;
                            portion = (w>h)?h/w:w/h; 

                          //大邊與對應邊比例
                            if(big == w){
                              d_portion = new_W/w
                            }else {
                              d_portion = new_H/h
                            }

                            w1 = d_portion*w;
                            h1 = d_portion*h;

                            appendImg();

                        break;
                }
                function appendImg(){
                  html+='<div class="appendImg col-xs-6 col-sm-4">';
                  html+='<div class="thumbnail">';
                  html+='<canvas class="myCanvas'+cnt+'" width="'+new_W+'px" height="'+new_H+'px"></canvas>';
                  html+='<div class="caption">';
                  html+='<p class="dotdotdot"> 檔名: '+n+'</p>';
                  html+='<p> 大小: '+s+'</p>';
                  html+='<p> 類型: '+file.type+'</p>';
                  html+='<p> 尺寸: '+w+'x'+h+'</p>';
                  html+='<p> 排序: <input type="text" class="form-control input-mini input-inline"></p>';
                  //html+='<label><input type="checkbox" value="checked" name="checkOne">主圖</label> '
                  //html+='<p class="out">';
                  // html+='<a href="#" class="btn btn-primary btn-xs"><i class="fa fa-upload"></i><span class="hidden-xs hidden-sm"> 上傳</span></a>';
                  html+='<a href="javascript:;" class="btn btn-danger btn-xs"><i class="fa fa-times"></i><span"> 取消</span></a>';
                  //html+='</p>';
                  html+='</div>';
                  html+='</div>';
                  html+='</div>';

                  $(addTo+' .row').append(html)
                  var $canvas = $(addTo+' .myCanvas'+cnt)[0];
              
                  var newC = $canvas.getContext('2d');
                    if(big == h){
                      newC.drawImage(image,(new_W-w1)/2,0,w1,h1);
                    }else if(big == w){
                      newC.drawImage(image,0,(new_H-h1)/2,w1,h1);
                    }else{
                      newC.drawImage(image,0,0,w1,h1);  
                    }
                    
                  cnt++;
                  Metronic.init();
                }                                 
                  
          }
          image.onerror= function() {
                alert('Invalid file type: '+ file.type);
          };  
        }
      }
    //主圖只能選擇一個事件
      $(addTo).on('change','.thumbnail input[name="checkOne"]',function(){
        $(this).parents('.thumbnail').toggleClass('border-red')
        if($(this).prop('checked')){
          $(this).parents('.appendImg').siblings('.appendImg').find('.thumbnail').removeClass('border-red');
          $(this).prop('checked',true).parent('span').addClass('checked');
          $(this).parents('.appendImg').siblings('.appendImg').find('input[name="checkOne"]').prop('checked',false).parent('span').removeClass('checked');
        }
      })
    //全部刪除
      $('.btn.warning.cancel').click(function(){
        $('.appendImg').animate({width: "0px",opacity:'0'}, 250 ,function(){
          $(this).remove();
        });
      });
    //個別刪除
      $(addTo+'').on('click','.thumbnail .btn-danger',function(){
         aa=$(this).parents('.appendImg');
        $(this).parents('.appendImg').animate({height:0,opacity:'0'}, 250 ,function(){     
          setTimeout(function(){ aa.remove(); }, 100);
        });
      })  
  }
  
})(jQuery)