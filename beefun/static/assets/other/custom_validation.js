// 驗證全半形 字數
jQuery.validator.addMethod("byteRangeLength", function (value, element, param) {
    var length = value.length;
    for (var i = 0; i < value.length; i++) {
        if (value[i].match(/[^\x00-\xff]/g)) {
            length++;
        }
    }
    return this.optional(element) || (length <= param);
}, $.validator.format("最多輸入字數為{0}(全形算2個字)"));

// EMAIL VALID
jQuery.validator.addMethod("isEmail", function (value, element) {
    var zip = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
    return this.optional(element) || (zip.test(value.trim()));
}, "信箱格式必須為正確格式(email has be regular type).");

// 身分證驗證
jQuery.validator.addMethod("twID", function (value, element) {
    if (value.trim()=="") return true;
    const tab = "ABCDEFGHJKLMNPQRSTUVXYWZIO";
    const A1 = new Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3);
    const A2 = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5);
    const Mx = new Array(9, 8, 7, 6, 5, 4, 3, 2, 1, 1);

    if (value.length != 10) return false;
    var i = tab.indexOf(value.charAt(0).toUpperCase());
    if (i == -1) return false;
    var sum = A1[i] + A2[i] * 9;
    for (var i = 1; i < 10; i++) {
        const v = parseInt(value.charAt(i));
        if (isNaN(v)) return false;
        sum = sum + v * Mx[i];
    }
    if (sum % 10 != 0) return false;
    return true;
}, $.validator.format("請輸入正確的身分證格式"));

//  台灣手機
jQuery.validator.addMethod("mobileTaiwan", function (value, element) {
    var str = value;
    var result = false;
    if (str.length > 0) {
        //是否只有數字;
        var patt_mobile = /^[\d]{1,}$/;
        result = patt_mobile.test(str);

        if (result) {
            //檢查前兩個字是否為 09
            //檢查前四個字是否為 8869
            var firstTwo = str.substr(0, 2);
            var firstFour = str.substr(0, 4);
            var afterFour = str.substr(4, str.length - 1);
            if (firstFour == '8869') {
                $(element).val('09' + afterFour);
                if (afterFour.length == 8) {
                    result = true;
                } else {
                    result = false;
                }
            } else if (firstTwo == '09') {
                if (str.length == 10) {
                    result = true;
                } else {
                    result = false;
                }
            } else {
                result = false;
            }
        }
    } else {
        result = true;
    }
    return result;
}, "手機號碼不符合格式，僅允許09開頭的10碼數字");
// 只能輸入英文
jQuery.validator.addMethod("english", function (value, element) {
    var chrnum = /^([a-zA-Z ]+)$/;
    return this.optional(element) || (chrnum.test(value));
}, "只能輸入英文字母");
// 只能英數字
jQuery.validator.addMethod("enString", function(value, element) {
    return this.optional(element) || /^[A-Za-z0-9]+$/.test(value);
}, "只能輸入英文字母、數字!");
// 統編驗證
jQuery.validator.addMethod("isTaxIDCode", function (value, element) {
    var zip = /^[0-9]{8}$/;
    return this.optional(element) || (zip.test(value));
}, "請填寫正確的統編格式");