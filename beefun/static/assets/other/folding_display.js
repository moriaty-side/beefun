(function(){
    var checkBtn = document.querySelectorAll('td a');
    var remoteBtn = document.querySelectorAll('tr');

    for(var i =0; i< checkBtn.length; i++) {
        if(checkBtn[i].hasAttribute('id')=== true) {
                    if(checkBtn[i].getAttribute('id').slice(0,5) === "type-") {
                    checkBtn[i].addEventListener('click',function(e) {
                        e.preventDefault();
                            var keyHead = "for-";
                            var keyBack = this.getAttribute('id');
                            var printKey = keyHead.concat(keyBack);

                            for(var j=0; j < remoteBtn.length; j++) {
                                if(remoteBtn[j].classList.contains(printKey)) {
                                    if(remoteBtn[j].classList.contains('hide')) {
                                        remoteBtn[j].classList.remove('hide');
                                    } else {
                                        remoteBtn[j].classList.add('hide');
                                    }
                                }
                            }
                    }, false);
                }
        }
    }
})();