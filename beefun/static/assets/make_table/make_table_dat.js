var MakeTable = (function () {
    var MakeTable = function (OptionsSetting) {
        ////// DOM 參數 //////
        this.dom_paginate_obj = OptionsSetting.dom_paginate_obj || $('#page-selection');
        this.dom_search_btn = OptionsSetting.dom_search_btn || $('.btn-search');
        this.dom_onchange_search = OptionsSetting.dom_onchange_search || $('.search_select');
        this.dom_search_form = OptionsSetting.dom_search_form || $('form');
        this.dom_table_tbody = OptionsSetting.dom_table_tbody || $('.company_list');
        this.dom_once_param  = OptionsSetting.dom_once_param || "";
        ////// 全域參數 //////
        // 搜尋功能對應網址
        this.ajax_search_url = OptionsSetting.ajax_search_url;
        this.ajax_csrf_token = OptionsSetting.ajax_csrf_token;
        // 搜尋條件
        // this.search_parameter = OptionsSetting.search_parameter || '';
        this.search_limit = OptionsSetting.search_limit || 5;
        this.init_search_parameter = OptionsSetting.init_search_parameter || '';
        this.search_parameter = this.init_search_parameter;
        // 頁碼機制啟動布林
        this.paginate_start = false;
        this.now_page = 1;
        var self = this;

        // 分頁功能
        var Paginate = function (total, page) {
            if (total == 0) {
                total = 1;
                self.dom_paginate_obj.hide()
            }
            else {
                var limit = 5;
                total = total % limit != 0 ? parseInt(total / limit) + 1 : parseInt(total / limit);
                self.dom_paginate_obj.show()
            }
            if (!self.paginate_start) {
                self.paginate_start = true;
                self.dom_paginate_obj.bootpag({
                    total: total,
                    page: 1,
                    maxVisible: 5,
                    next: ">",
                    prev: "<",
                    leaps: false

                }).on("page", function (event, num) {
                    self.now_page = num;
                    var form_data = self.dom_search_form.serializeArray();
                    var form_data_string = jQuery.map(form_data, function (n, i) {
                        if (n.name == 'start_time' && n.value) {
                            n.value = moment.utc(n.value).format("YYYY-MM-DDT00:00:00");
                            return (n.name + '=' + n.value);
                        }
                        else if (n.name == 'end_time' && n.value) {
                            n.value = moment.utc(n.value).format("YYYY-MM-DDT23:59:59");
                            return (n.name + '=' + n.value);
                        }
                        else if (n.name == 'created' && n.value) {
                             n.value = moment.utc(n.value).format("YYYY-MM-DDT23:59:59");
                            return (n.name + '=' + n.value);
                        }
                        else if (n.value.trim())
                            return (n.name + '=' + n.value);
                    }).join("&");
                    var search_parameter = self.search_parameter + '&' + form_data_string;
                    // $('html, body').animate({scrollTop: 0 });
                    self.load_ajax(num, search_parameter, self.base_table_model);
                });
            } else {
                var paginate_data = {
                    total: total,
                    leaps: false,
                };
                if (page)
                    paginate_data['page'] = page;
                self.dom_paginate_obj.bootpag(paginate_data);
            }
        };
        // Table 繪製 功能
        this.base_table_model = function (obj, page) {
            self.dom_table_tbody.find('tr').remove();
            self.table_model(obj, page);
            Paginate(obj['total_count'], page);// 分頁功能製作
        }
        this.table_model = OptionsSetting.table_model || function (obj, page) {
            //開始產生頁面資料
            var $list = obj['company_list'];
            var html_str = '';
            $.each($list, function (i, item) {
                item.index = (i + 1 + ((page - 1) * 5));
                var tb_row = "<tr>" +
                    "<td>" + item['display_name'] + "</td>" +
                    "<td>" + item['name'] + "</td>" +
                    "<td>" + item['type'] + "</td>" +
                    "<td>" +
                    "<button type='button' class='btn yellow btn-outline'>" +
                    "<i class='fa fa-search'>" + "${ _(u'修改')}" + "</i>" +
                    "</button>" +
                    "<button type='button' class='btn yellow btn-outline'>" +
                    "<i class='fa fa-search'>" + "${ _(u'群組編輯')}" + "</i>" +
                    "</button>" +
                    "</td>" +
                    "</tr>";
                html_str += tb_row;
            });
            self.dom_table_tbody.append(html_str);//Table頁面資料繪製
        };
        // Table Ajax 功能
        this.load_ajax = function (page, search_parameter, cb, event_btn) {
            if(self.dom_once_param){
                search_parameter += self.dom_once_param;
                self.dom_once_param = "";
            }
            var ajax_information = {
                url: this.ajax_search_url + "?page=" + page + search_parameter,
                type: "GET",
                dataType: 'JSON',
                success: function (data) {
                    var msg = data['message'];
                    var response = data['response'];
                    if (data['status']) {
                        cb(response, page)
                    } else {
                        console.log(msg);
                    }
                },
                beforeSend: function () {
                    if (event_btn)
                        event_btn.attr("disabled", true);
                },
                complete: function () {
                    if (event_btn)
                        event_btn.attr("disabled", false);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('您尚在審核中');
                    // alert(thrownError);
                    // if (event_btn)
                    //     event_btn.attr("disabled", false);
                }
            };
            if (this.ajax_csrf_token)
                ajax_information['headers'] = {'X-CSRF-Token': this.ajax_csrf_token};
            $.ajax(ajax_information);
        };
        this.start = function (page) {
            page = page ? page : self.now_page;
            self.load_ajax(page, self.search_parameter, self.base_table_model);
        };
    }
    return function (opt) {
        var new_obj = new MakeTable(opt);
        new_obj.dom_search_btn.click(function () {
            var form_data = new_obj.dom_search_form.serializeArray();
            var form_data_string = jQuery.map(form_data, function (n, i) {
                if (n.name == 'start_time' && n.value) {
                    n.value = moment.utc(n.value).format("YYYY-MM-DDT00:00:00");
                    return (n.name + '=' + n.value);
                }
                else if (n.name == 'end_time' && n.value) {
                    n.value = moment.utc(n.value).format("YYYY-MM-DDT23:59:59");
                    return (n.name + '=' + n.value);
                }
                else if (n.name == 'created' && n.value) {
                             n.value = moment.utc(n.value).format("YYYY-MM-DDT23:59:59");
                            return (n.name + '=' + n.value);
                        }
                else if (n.value.trim())
                    return (n.name + '=' + n.value);
            }).join("&");
            var search_parameter = new_obj.search_parameter + '&' + form_data_string;
            new_obj.load_ajax(1, search_parameter, new_obj.base_table_model);//get table data
        });
        new_obj.dom_onchange_search.change(function () {
            var form_data = new_obj.dom_search_form.serializeArray();
            var form_data_string = jQuery.map(form_data, function (n, i) {
                if (n.value.trim())
                    return (n.name + '=' + n.value);
            }).join("&");
            var search_parameter = new_obj.search_parameter + '&' + form_data_string;
            new_obj.load_ajax(1, search_parameter, new_obj.base_table_model);//get table data
        });
        return new_obj
    };
})(jQuery);