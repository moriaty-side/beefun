(function($) {
    var settings = {};
    var _func = {
    	_changeImgBtn: function(){
    		var method = ''
    		if ($(this).hasClass('pre')) {
                method = 'pre'
            }else{
            	method = 'next'
            }
            _func._changeImg(method)

    	},
        _changeImg: function(method) {
            var index = $cropperList.find('.item.active').index();
            var length = $cropperList.find('.item').length;
            if (method==='pre') {
                if (index === 0) {
                    index = length - 1
                } else {
                    index--
                }
            }else{
            	if (index + 1 === length) {
                    index = 0
                } else {
                    index++
                }
            }
            $cropperWindow.find('.cropper-content-area .page span').eq(0).text(index + 1)
            $cropperList.find('.item').eq(index).addClass('active').siblings('.active').removeClass('active');
        },
        _cutImg: function() {
            // console.log('裁切')
            $(this).prop('disabled', true)
            var $cropper = $cropperList.find('.item.active .cropper-image');
            var link = $cropper.attr('src');
            var canvas = $cropper.cropper('getCroppedCanvas').toDataURL(link);
            $cropper.cropper('replace', canvas);
            $('.cropper-list .item.active').addClass('cutted')
            _func._changeImg('next')
            canSave()
        },
        _removeImg: function() {
            if (confirm('確認移除圖片？')) {
                var old_length = $cropperList.find('.item').length;
                if (old_length === 1) {
                    $cropperWindow.modal('hide')
                } else {
                    var $old = $cropperList.find('.item.active');
                    var old_index = $old.index();
                    if (old_index === old_length - 1) {
                        old_index = 0
                    } else {
                        old_index++
                    }
                    $cropperList.find('.item').eq(old_index).addClass('active').siblings('.active').removeClass('active');
                    $old.remove()
                    $cropperWindow.find('.cropper-content-area .page').find('span').eq(0).text($('.cropper-list .item.active').index() + 1)
                    $cropperWindow.find('.cropper-content-area .page').find('span').eq(1).text(old_length - 1)
                    canSave()

                }
            }
        },
        _returnImg: function() {
            var $cropper = $cropperList.find('.item.active .cropper-image');
            var link = $cropper.attr('data-src');
            $cropper.cropper('replace', link);
            $cropperList.find('.item.active').removeClass('cutted')
        },
        _saveImg: function() {
            var length = $cropperList.find('.item').length;
            var ncutlength = $cropperList.find('.item.cutted').length;
            if (length !== ncutlength) {
                alert('還有' + (length - ncutlength) + '張圖尚未裁切');
            } else {
                var list = [];
                var old = $('.active-cropper').find('.cropper-album .item').length;
                $cropperList.find('.item.cutted').each(function(i, el) {
                    var item = {
                        url: $(el).find('.cropper-image').attr('src'),
                        index: old + i + 1,
                        main: false,
                        id: getuuid()
                    }
                    list.push(item);
                })
                // console.log('_saveImg')
                _func._uploadImg($('.active-cropper'), list)

            }
        },
        _pushImg: function($element, list) {
            _func._newImg($element, list, 'push')
        },
        _replaceImg: function($element, list) {
            _func._newImg($element, list, 'replace')
        },
        _uploadImg: function($element, list) {
            _func._newImg($element, list, 'upload')
        },
        _newImg: function($element, list, method) {
            // 排序
            // console.log(list)
            var uuid = $element.attr('data-id');
            // console.log($element.attr('data-id'))
            // console.log(settings[uuid])
            // console.log(this)
            var html = ''
            if(list){

                list.sort(function(a, b) {
	                return a.index - b.index;
	            });
	            html =
                ` 
					${list.map(list=>`
						<div 
							class="item ${(list.main)?"main":""}" 
							data-id="${list.id}" 
							style="width: ${settings[uuid].itemWidth};"
						>
							<div class="btn-remove"></div>
							<label>
								<input type="checkbox" class="input-remove"/>
								<span></span>
							</label>									
							<div class="border">
			                	<img src="${list.url}" style="width: 100%; max-width: 100%;"/>
			                	<div class="item-tool">
			                		<span class="item-btn btn-main"><i class="fa fa-file-image-o"></i> 主圖</span>
			                		<label>排序：</label>
			                		<input type="number" min="0" value="${list.index}" />
			                	</div>
			            	</div>
		            	</div>
					`).join('')}
	            `.trim();
            }

            var end = function($element) {
            	_func._removeBtn($element)
                $('.active-cropper').removeClass('active-cropper')
                $cropperWindow.modal('hide')
            }
            switch (method) {
                case 'push':
                    $element.find('.cropper-album').append(html)
                    end($element)
                    break;
                case 'replace':
                    $element.find('.cropper-album').html(html)
                    end($element)
                    break;
                case 'upload':
                    // console.log('開始上傳')
                    // _func._image_upload($element, list, end)
                    if (settings[uuid].uploadUrl) {
                        _func._image_upload($element, list, end)
                    } else {
                        $element.find('.cropper-album').append(html)
                        end($element)
                    }
                    break;
                default:
                    break;
            }

        },
        _image_upload: function($element, files, end, cb) {
            // var csrfToken = "${request.session.get_csrf_token()}";\
            var uuid = $element.attr('data-id');
            console.log(files)
            var data = new FormData();

            for (i = 0; i < files.length; i++) {
                // console.log(files[i])
                data.append("dom", files[i].index);
                data.append("images", files[i].url);
                data.append("image_ids", files[i].id);
            }
            // console.log(data);
            var request_setting = {
                url: settings[uuid].uploadUrl,
                type: 'POST',
                // dataType: 'json',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                // headers: {'X-CSRF-Token': csrfToken},
                beforeSend: function() {},
                success: function(data) {
                    console.log(data)
                    var msg = data['message'];
                    var image_response = data['response'];
                    if (data['status'] == 'success' && data['code'] == 200) {
                        // console.log(msg);
                        console.log(image_response);
                        _func._newImg($element, image_response.images, 'push')
                        if (cb) {
                            cb(image_response);
                        }
                    } else {
                        alert(msg);
                    }
                },
                error: function(e) {
                    // console.log(e.responseText);
                },
                complete: function() {
                    // console.log("post done");
                }
            };
            if (settings[uuid].csrfToken) {
                request_setting.headers['X-CSRF-Token'] = settings[uuid].csrfToken;
            }

            $.ajax(request_setting);
        },
        _removeBtn: function($element){
        	var length = $element.find('.cropper-album .item').length;
        	if(length===0){
        		$element.addClass('noimg');
        	}else{
        		$element.removeClass('noimg');
        	}
        }
    };

    function sortOnKeys(dict) {

        var sorted = [];
        for(var key in dict) {
            sorted[sorted.length] = key;
        }
        sorted.sort();

        var tempDict = {};
        for(var i = 0; i < sorted.length; i++) {
            tempDict[sorted[i]] = dict[sorted[i]];
        }
        return tempDict;
    }


    function getuuid(){
	    function s4() {
	        return Math.floor((1 + Math.random()) * 0x10000)
	            .toString(16)
	            .substring(1);
	    }
	    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	        s4() + '-' + s4() + s4() + s4();
	}
    function canSave() {
        var length = $cropperList.find('.item').length;
        var ncutlength = $cropperList.find('.item.cutted').length;
        if (length !== ncutlength) {
            $cropperWindow.find('.cropper-btn-save').prop('disabled', true)
        } else {
            $cropperWindow.find('.cropper-btn-save').prop('disabled', false)
        }
    }

    function crop(ratio) {
        var $cropper = $cropperList.find('.item .cropper-image');
        $cropper.cropper({
            aspectRatio: (eval(ratio)) ? eval(ratio) : 0,
            viewMode: 3,
            // 關閉圖片縮放
            zoomable: false,
            // 關閉一開始就有剪裁筐
            // autoCrop:false,
            minContainerWidth: 2,
            minContainerHeight: 1,
            ready: function() {
                $cropperWindow.find('.cropper-btn-cut').prop('disabled', false)
            }
        });
    }
    if ($('#cropper-window').length === 0) {
        var html = `
			        	<div id="cropper-window" data-backdrop="static" class="modal" role="dialog" aria-labelledby="myModalLabel10"> 
					        <div class="modal-dialog modal-lg"> 
					            <div class="modal-content">
					                <div id="bounce-content">
										<div class="modal-header">
										    <button type="button" class="close" data-dismiss="modal"></button>
										    <h4 class="modal-title">圖片裁切</h4>
										</div>
										<div class="modal-body">
										    <form action="#" class="form-horizontal" role="form">
										        <div class="row version">
										            <div class="col-xs-offset-2 col-xs-8 mb-10">
										              <div class="cropper-content-area">
										                  <!-- <div class="cropper-content">
										                      <img width="100%" class="cropper-image" src="http://1.bp.blogspot.com/-bp0OIfjaWSI/VXEeanw7XEI/AAAAAAAAfmA/Ben_gtNvqAU/s640/05.jpg" alt="" data-src="http://1.bp.blogspot.com/-bp0OIfjaWSI/VXEeanw7XEI/AAAAAAAAfmA/Ben_gtNvqAU/s640/05.jpg" class="img-responsive">
										                  </div> -->
										                  <div class="page text-center">
										                  	<a href="javascript:;" class="btn btn-icon-only blue-steel pull-left pre">
						                                        <i class="fa fa-angle-left"></i>
						                                    </a>
										                  	<span></span> / <span></span>
										                  	<a href="javascript:;" class="btn btn-icon-only blue-steel pull-right next">
						                                        <i class="fa fa-angle-right"></i>
						                                    </a>
										                  </div>
										                  <div class="cropper-list">
										                  </div>
										              </div>
										            </div>
										        </div>
										        <div class="no-line modal-footer">
										            <button type="button" class="btn default" data-dismiss="modal"><i class="fa fa-ban"></i> 取消</button>
										            <button type="button" class="btn red cropper-btn-remove"><i class="fa fa-times"></i> 移除</button>
										            <button type="button" class="btn blue-steel cropper-return"><i class="fa fa-reply"></i> 還原</button>
										            <button type="button" class="btn purple cropper-btn-cut"><i class="fa fa-crop"></i> 裁切</button>
										            <button type="button" class="btn green-seagreen cropper-btn-save" disabled="true"><i class="fa fa-check"></i> 儲存</button>
										        </div>
										    </form>
										</div>
					                </div>
					            </div>
					        </div>
					    </div>
				    `
        var cropperWindow = $(html).appendTo("body");
        var $cropperWindow = $(cropperWindow);
        var $cropperList = $(cropperWindow).find('.cropper-list');
        $(cropperWindow).find('.cropper-btn-cut').bind('click', _func._cutImg);
        $(cropperWindow).find('.cropper-content-area .page a').bind('click', _func._changeImg);
        $(cropperWindow).find('.cropper-btn-remove').bind('click', _func._removeImg);
        $(cropperWindow).find('.cropper-return').bind('click', _func._returnImg);
        $(cropperWindow).find('.cropper-btn-save').bind('click', _func._saveImg);

        $cropperWindow.on('hide.bs.modal', function() {
            $('.active-cropper').removeClass('active-cropper')
        })
    } else {
        $.error('#cropper-window 已存在 ');
        return false
    }

    var methods = {
        init: function(options, _eachFunc) {
        	// console.log(options.uuid)
            // console.log($(this))
            if (!$(this).hasClass('active')) {
                var html = `
								<div class="cropper-album">
					            </div>
					            <div class="cropper-btn-tool">
					                <div class="btn purple btn-input btn-upload-input">
					                    <i class="fa fa-photo"></i> 
					                    上傳圖片
					                    <input type="file" multiple="multiple" class="upload" value="Choose a file" accept="image/*" />
					                </div>
					                <div class="btn red btn-open-remove">
					                    <i class="fa fa-times"></i> 
					                    刪除圖片
					                </div>
					                <div class="btn blue btn-save-remove">
					                    確認
					                </div>
					                <div class="btn default btn-close-remove">
					                    取消
					                </div>
					            </div> 
				        	`
                $(this).attr('data-id', options.uuid).addClass('active').html(html)
                // console.log(options.imgList)
                _func._replaceImg($(this), options.imgList)

                $(this).find('input.upload').on('change', options._eachFunc._updateImg);
                $(this).find('.btn-open-remove').on('click', options._eachFunc._openRemove);
                $(this).find('.btn-save-remove').on('click', options._eachFunc._saveRemove);
                $(this).find('.btn-close-remove').on('click', options._eachFunc._closeRemove);
                $(this).find('.cropper-album').on('click','.item-tool .btn-main',options._eachFunc._changeMain);
            }
        },
        replace: function(options = []) {
            console.log('replace')
            if (Array.isArray(options)) {
                _func._replaceImg($(this), options)
            } else {
                $.error('replace option must be an array on jQuery.cropperList');
            }

        },
        push: function(options = []) {
            console.log('push')
            if (Array.isArray(options)) {
                _func._pushImg($(this), options)
            } else {
                $.error('replace option must be an array on jQuery.cropperList');
            }

        },
        distory: function() {
            //   	$(cropperWindow).find('.cropper-btn-cut').off('click');
            // $(cropperWindow).find('.cropper-content-area .page a').off('click');
            // $(cropperWindow).find('.cropper-btn-remove').off('click');
            // $(cropperWindow).find('.cropper-return').off('click',_func._returnImg);

            $(this).find('input.upload').off('change');
            $(this).find('.btn-open-remove').off('click');
            $(this).find('.btn-save-remove').off('click');
            $(this).find('.btn-close-remove').off('click');
            $(this).find('.cropper-album').off('click','.item-tool .btn-main');
            $(this).removeClass('active').html('');
        }
    };

    $.fn.cropperList = function(methodOrOptions) {
        var myArguments = arguments;

        switch (methodOrOptions) {
            case 'getList':
                var list = [];
                $(this).find('.cropper-album > div').each(function(i, item) {
                	var obj = {
                		url: $(item).find('img').attr('src'),
                		order: $(item).find('.item-tool input').val(),
                		image_id: $(item).attr('data-id'),
                		main: ($(item).hasClass('main'))
                	}
                    list.push(obj);
                })
                return list
                break;
            default:
                // statements_def
                break;
        }

        return this.each(function() {
            var default_settings = {
                ratio: 0,
                size: 1048576,
                max: 5,
                itemWidth: '33.3%',
                imgList: [],
                uploadUrl: null,
                deleteUrl: null,
                csrfToken: false,
                uuid: getuuid()
            }
            var user_settimgs = {
                ratio: eval($(this).attr('data-ratio')),
                size: $(this).attr('data-size'),
                max: $(this).attr('data-max'),
            }
            var user_func_settimgs = Array.prototype.slice.call(myArguments, 1);
            var is_user_setting = (typeof methodOrOptions === 'object' || !methodOrOptions);

            $.extend(default_settings, user_settimgs);

            var _eachFunc = {
                _updateImg: function() {
                    // console.log('_updateImg')
                    var $oragin = $(this).closest('.cropper-oragin');
                    $oragin.addClass('active-cropper');
                    var $input = $(this)
                    // console.log($input)
                    // 這邊用base64裁圖會變慢 所以要先上傳圖片回傳網址裁圖
                    var file_list = $input[0].files;
                    var url_list = [];
                    var oversize = [];
                    if (file_list.length > default_settings.max) {
                        alert(`圖片超過${default_settings.max}張`)
                        return false
                    }

                    function readAndPreview(file, i, cb) {

                        if (/\.(jpe?g|png|gif)$/i.test(file.name)) {
                            var reader = new FileReader();
                            reader.addEventListener("load", function() {
                                var image = new Image();
                                image.title = file.name;
                                image.src = this.result;
                                // console.log(file)
                                var obj = {
                                    name: file.name,
                                    src: this.result,
                                    size: file.size
                                }
                                // console.log(obj.size)
                                if (obj.size <= default_settings.size) {
                                    url_list.push(obj)

                                } else {
                                    // console.log(obj.name)
                                    oversize.push(obj.name)
                                    // for(var key in file_list){
                                    // 	if(file_list[key]===obj.name){
                                    // 		delete file_list[key]
                                    // 	}
                                    // }
                                }

                                cb(i);
                            }, false);

                            reader.readAsDataURL(file);
                        }
                    }
                    if (file_list) {
                        var test2 = []
                        var cb = function(n) {
                            test2.push(n)
                            if (test2.length == file_list.length) {
                                if (oversize.length !== file_list.length) {
                                    // console.log(url_list)
                                    // $.templates("renderimg", "#img-item");
                                    // var html = $.render.renderimg(url_list);
                                    var html = '';
                                    // console.log(url_list[0].name)
                                    for(var j=0;j<url_list.length;j++){
                                    	html+=`
	                                    	<div class="item">
									  			<img 
									  				src="${url_list[j].src}"
										  			data-src="${url_list[j].src}" 
										  			class="cropper-image" alt=""
									  			/>
									  		</div>
									  		`
                                    }
                                    $('#cropper-window .cropper-list').html(html);
                                    $('#cropper-window .cropper-list .item').eq(0).addClass('active')
                                    $('#cropper-window .cropper-content-area .page span').eq(0).text(1)
                                    $('#cropper-window .cropper-content-area .page span').eq(1).text(url_list.length)
                                    crop(default_settings.ratio)


                                    $('#cropper-window').modal('show')
                                    if (oversize.length > 0) {
                                        var alert_span = oversize.join('\n');
                                        alert(`${alert_span}\n超過檔案過大(1Ｍ)`)
                                    }
                                } else {
                                    alert('所有檔案過大(1Ｍ)')
                                }

                            }
                        }
                        for (var i = 0, len = file_list.length; i < len; i++) {
                            readAndPreview(file_list[i], i, cb)
                        }
                    }

                },
                _openRemove: function() {
                    // console.log($(this))
                    $(this).closest('.cropper-oragin').addClass('remove-active');
                },
                _saveRemove: function() {
                    var $album = $(this).closest('.cropper-oragin').find('.cropper-album');
                    var $items = $(this).closest('.cropper-oragin').find('.cropper-album .item');
                    // console.log(items)
                    var removeList = [];
                    $items.each(function(i, item) {
                        if ($(item).find('.input-remove').is(":checked")) {
                            var id = $(item).attr('data-id');
                            removeList.push(id)
                        }
                    })
                    // console.log(removeList)
                    var remove = function($album, removeList) {
                        for (var i = 0; i < removeList.length; i++) {
                            $album.find('.item[data-id="' + removeList[i] + '"]').remove();
                        }
                        $album.closest('.cropper-oragin').removeClass('remove-active');

                        var $element = $album.closest('.cropper-oragin')
                        var length = $element.find('.cropper-album .item').length;
			        	if(length===0){
			        		$element.addClass('noimg');
			        	}else{
			        		$element.removeClass('noimg');
			        	}
                    }

                    var remove_data = {'image_ids':removeList};
                    if (confirm('確認刪除？')) {

                        if (default_settings.deleteUrl) {
                            var request_setting = {
                                url: default_settings.deleteUrl,
                                type: 'POST',
                                dataType: 'json',
                                traditional: true,
                                data: remove_data,
                                beforeSend: function() {},
                                success: function(data) {
                                    var msg = data['message'];
                                    var image_response = data['response'];
                                    if (data['status'] == 'success' && data['code'] == 200) {
                                        // console.log(msg);
                                        console.log(image_response);
                                        remove($album, removeList);
                                    } else {
                                        alert(msg);
                                    }
                                },
                                error: function(e) {
                                    // console.log(e.responseText);
                                },
                                complete: function() {
                                    // console.log("post done");
                                }
                            };
                            if (default_settings.csrfToken) {
                                request_setting.headers['X-CSRF-Token'] = settings.csrfToken;
                            }

                            $.ajax(request_setting);

                        } else {
                            remove($album, removeList);
                        }

                    }

                },
                _closeRemove: function() {
                    var oragin = $(this).closest('.cropper-oragin');
                    oragin.removeClass('remove-active');
                    oragin.find('.cropper-album label input').prop('checked', false)
                },
                _changeMain: function(){
                	// console.log($(this))
                	$(this).closest('.item').addClass('main').siblings('.main').removeClass('main');
                }
            }
            default_settings._eachFunc = _eachFunc
            if (methods[methodOrOptions]) {
                return methods[methodOrOptions].apply(this, user_func_settimgs);
            } else if (is_user_setting) {
                // Default to "init"
                $.extend(default_settings, (myArguments[0]) ? myArguments[0] : {});
                settings[default_settings.uuid] = default_settings;

		        return methods.init.apply($(this), [default_settings]);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.cropperList');
            }
        });

    };


})(jQuery);