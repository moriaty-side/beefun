# coding=utf-8
from __future__ import unicode_literals

from pyramid.decorator import reify
from pyramid.events import NewRequest, subscriber
from pyramid.request import Request
from pyramid.settings import asbool

from .lib.i18n import normalize_locale_name


@subscriber(NewRequest)
def select_lanuage(event):
    """Select language from accept-languages header of request."""
    request = event.request
    if request.cookies.get('_LOCALE_'):
        return
    settings = request.registry.settings
    offers = [lang.replace('_', '-').lower()
              for lang, _ in settings['available_langs']]
    accept = request.accept_language
    match = accept.best_match(offers, settings['default_locale_name'])
    match = match.replace('-', '_')
    match = normalize_locale_name(match)
    request._LOCALE_ = match
    request.response.set_cookie('_LOCALE_', match)
    


@subscriber(NewRequest)
def clean_db_session(event):
    """Clean up DB session when the request processing is finished."""

    def clean_up(request):
        request.db_session.remove()

    settings = event.request.registry.settings
    db_session_cleanup = asbool(settings.get('db_session_cleanup', True))
    if db_session_cleanup:
        event.request.add_finished_callback(clean_up)


class WebRequest(Request):
    @reify
    def db_session(self):
        """Session object for database operations."""
        settings = self.registry.settings
        return settings['session']

    @reify
    def remember_id(self):
        """Current logged in user or member object."""
        from pyramid.security import authenticated_userid
        remember_id = authenticated_userid(self)
        return remember_id

    @reify
    def account_id(self):
        """Current logged in user_id."""
        if self.remember_id:
            _id, user_type = self.remember_id.split('@')
            if user_type == 'account':
                return _id
        return None

    @reify
    def account_data(self):
        """ Current Login in account_data"""
        if self.account_id:
            return self.session.get('account_data', None)
        return None

    @reify
    def member_id(self):
        """Current logged in member_id."""
        if self.remember_id:
            _id, user_type = self.remember_id.split('@')
            if user_type == 'member':
                return int(_id)
        return None

    @reify
    def member(self):
        """Current logged in member."""
        if self.member_id:
            return self.session.get('member_data', None)
        return None


    @reify
    def route_name(self):
        """Return route name if available, otherwise None is returned."""
        return self.matched_route.name if self.matched_route else None

    @reify
    def real_ip(self):
        """The real IP address from x-forwarded-for, or x-real-ip,
            mainly depends on configuration."""
        addresses = self.headers.get('X-Forwarded-For')
        if addresses:
            parts = addresses.split(',')
            return parts[0].strip()
        else:
            return self.remote_addr
