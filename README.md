beefun README
==================

Getting Started
---------------
- cd <directory containing this file>
- $VENV/bin/pip install -e .
- $VENV/bin/initialize_beefun_db development.ini
- $VENV/bin/pserve development.ini

Test Started
---------------
- $VENV/bin/pip install -e ".[testing]"

- $VENV/bin/py.test -q

- $VENV/bin/py.test --junitxml=pytest_result.xml --cov=beefun --cov-report=xml

Docker Container 容器
=====================

創建環境變數
----------
- virtualenv env

切換到環境變數
-----------
- source env/bin/activate

安裝cryptography相依套件
----------------------
- pip install cryptography

安裝setup.py
------------
- python setup.py develop

建立基本Docker
--------------
- pip freeze |grep -v oklib |grep -v git > deploy/docker/base/requirements.txt
- docker build --rm -t beefun:base deploy/docker/base

- pip freeze |grep -v oklib |grep -v git > deploy/docker/alpine/requirements.txt
- docker build --rm -t beefun:base deploy/docker/alpine


建立Docker
----------
- docker build --rm -t beefun .


執行Docker
----------
docker run -d -p 6543:6543 --name beefun \
-v `pwd`/logs/:/usr/src/app/logs beefun

初始化DB
--------
- docker exec beefun initialize_beefun_db production.ini

進入Docker
----------
- docker exec -ti beefun /bin/bash

查看Container log
-----------------
- docker logs -f contain_id

Docker 基本指令
--------------
- docker images #查看映像檔
- docker ps -a  #查看容器
- docker rmi images_id   #刪除映像檔
- docker rm container_id #刪除容器


Docker Registory 倉庫
=====================
上傳到私有倉庫
------------
- docker login docker.okborn.com
- docker tag beefun docker.okborn.com/beefun
- docker push docker.okborn.com/beefun

上傳到 Google 專案 docker 版本號使用 git describe
---------------------------------------------
- docker tag beefun:latest asia.gcr.io/okborn/beefun:$(git describe)
- gcloud docker -- push asia.gcr.io/okborn/beefun:$(git describe)


kubernetes 叢集
===============

查看 Kubernetes
---------------
- kubectl get rc
- kubectl get pod
- kubectl get service

創建 RC, Service
- kubectl create -f deploy/kubernetes/kubernetes-rc.yaml
- kubectl create -f deploy/kubernetes/kubernetes-service.yaml

更新 設定檔
---------
- kubectl update beefun-rc-v2 -f deploy/kubernetes/beefun-rc.yaml

循序更新 Image
-------------
- kubectl rolling-update beefun-rc-v2 --image=asia.gcr.io/okborn/beefun:$(git describe)

循序更新 RC Replication Controller
---------------------------------
- kubectl rolling-update beefun-rc-v2 -f deploy/kubernetes/kubernetes-rc.yaml

查看log
------
- kubectl logs -f beefun-rc-v1

進入Container 查看
------------------
- kubectl get pods
- kubectl exec beefun-rc-v2 -c beefun -ti -- /bin/bash

刪除 RC, Service
------------------
- kubectl delete -f deploy/kubernetes/kubernetes-rc.yaml
- kubectl delete -f deploy/kubernetes/kubernetes-service.yaml

恢復 更新前的一個版本
-----------------
- kubectl rolling-update beefun-rc-v1 -f beefun-rc-v1.yaml --update-period=10s --rollback


更新RC設定
---------
- kubectl apply -f deploy/kubernetes/beefun-rc.yaml
- kubectl update -f deploy/kubernetes/beefun-rc.yaml
- kubectl rolling-update rc beefun-rc-v1 --image=asia.gcr.io/okbank-180307/beefun:$(git describe) --image-pull-policy=Always

更新失敗回到上一個版本
- kubectl rolling-update rc beefun-rc-v1 --image=asia.gcr.io/okbank-180307/beefun:$(git describe) --image-pull-policy=Always --rollback


