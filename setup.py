import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'pyramid',
    'pyramid_mako',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'pyramid_beaker',
    'SQLAlchemy',
    'transaction',
    'zope.sqlalchemy',
    'waitress',
    'PyYAML',
    'requests',
    'psycopg2==2.7.7',
    'psycopg2-binary==2.7.7',
    # 'psycopg2==2.8.6',
    # 'psycopg2-binary==2.8.6',
    'formencode',
    'jwcrypto',
    'cryptography',
    'sqlalchemy_utils',
    'Babel',
    'apscheduler',
    'pillow',
    'redis',
    'python-magic',
    ]

tests_require = [
    'WebTest >= 1.3.1',  # py3 compat
    'pytest',  # includes virtualenv
    'pytest-cov',
    'mock'
    ]

setup(name='beefun',
      version='1.0',
      description='Simple app that features a gray button!',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
          "Programming Language :: Python",
          "Framework :: Pyramid",
          "Topic :: Internet :: WWW/HTTP",
          "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
      ],
      dependency_links=[],
      author='Double Cash',
      author_email='double@double-cash.com',
      url='https://bitbucket.org/doublecashweb/beefun.git',
      license='DC',
      keywords='web pyramid pylons',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      extras_require={
          'testing': tests_require,
      },
      install_requires=requires,
      
      entry_points="""\
      [paste.app_factory]
      main = beefun:main
      [console_scripts]
      initialize_beefun_db = beefun.scripts.initializedb:main
      """,
      )
