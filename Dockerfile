FROM beefun:base

RUN python setup.py develop && \
    find /usr/local \( -type d -a -name test -o -name tests \) -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) -exec rm -rf '{}' + && \
    runDeps="$( scanelf --needed --nobanner --recursive /usr/local | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' | sort -u | xargs -r apk info --installed | sort -u )" && \
    apk add --virtual .rundeps $runDeps && \
    apk del deps build-base openssh gcc linux-headers git autoconf automake && \
    rm -f /root/.ssh/id_rsa && rm -f /usr/src/app/requirements.txt && rm -rf /var/cache/apk/*

EXPOSE 6543

CMD pserve production.ini