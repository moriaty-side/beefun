#!/bin/bash

# 套用環境變數
source ~/.bash_profile

# 檢查pyenv
which pyenv
if [ $? == "0" ]; then
    echo "already have pyenv"
else
    echo "install pyenv"
    echo "install virtualenv"
fi

# 設定專案
PYENV_HOME="`pwd`/env/"
echo ${PYENV_HOME}
if [ -d ${PYENV_HOME} ]; then
    source ${PYENV_HOME}/bin/activate
else
    pyenv global 3.5.0
    virtualenv ${PYENV_HOME}
    source ${PYENV_HOME}/bin/activate
    # 準備專案相依插件
    pip install cryptography psycopg2 SQLAlchemy
    python setup.py develop
    pip install -e ".[testing]"
fi

# 初始化 DB
initialize_beefun_db production.ini#beefun
